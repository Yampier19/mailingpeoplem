<?php
header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');
if ($user_name != '' && $id_user != '') {
    if (isset($_POST['txt_var'])) {  ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".ocultar_formulario").css('display', 'none');
                $(".ocultar_generador").css('display', 'none');
                $(".otra_solicitud").css('display', 'block');
            });
        </script>
    <?php
    }
    $id_users = base64_decode($id_user);
    $consul_user = mysqli_query($conex, 'SELECT * FROM `userlogin` AS A LEFT JOIN user AS B ON A.id_user = B.id_user  WHERE A.`id_user` = ' . base64_decode($id_user) . '');
    while ($consul = (mysqli_fetch_array($consul_user))) {
        $nombre = $consul['names'];
        $apellido = $consul['surnames'];
        $id_userlog = $consul['id_loginrol'];
    }
    if ($id_userlog == base64_decode($id_loginrol)) {
        //echo 'Bueno';
    }
    $boton = 2;
    include('../../DROPDOWN/menu_admin.php');
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Usuarios</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard v1</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <?php
        $select_proxima_gestion = mysqli_query($conex, "SELECT id_shipping FROM shipping ORDER BY id_shipping DESC LIMIT 1; ");
        while ($gestion = (mysqli_fetch_array($select_proxima_gestion))) {
            $id_shipping = $gestion['id_shipping'];
        }
        $id_proxima_shipping = $id_shipping + 1;
        $select_proxima_qr = mysqli_query($conex, "SELECT id_qr FROM qr_generated ORDER BY id_qr DESC LIMIT 1; ");
        while ($qr_nuevo = (mysqli_fetch_array($select_proxima_qr))) {
            $id_qr = $qr_nuevo['id_qr'];
        }
        $id_proximo_generated = $id_qr + 1;
        $usuario = $id_users;
        $DesdeLetra = "a";
        $HastaLetra = "z";
        $letraAleatoria = chr(rand(ord($DesdeLetra), ord($HastaLetra)));
        $id_correponde = $id_proxima_shipping . $letraAleatoria . $usuario . "0";
        $token = "0";
        /// Fecha Actual ///
        date_default_timezone_set("America/Bogota");
        $d      = date('d');
        $mes_nu = date('m');
        $anio    = date('Y');
        $fecha_actual = $d . $mes_nu . $anio;
        ?>

        <?php
        // $infoUsuarios = mysqli_query($conex, "SELECT * FROM `personal` ");
        // while ($datos = mysqli_fetch_array($infoUsuarios)) {

        //     $nombres = $datos['names'];
        //     $apellidos = $datos['surnames'];
        //     $contrasena = $datos['password'];
        //     $tipoDocumento = $datos['tipo_documento'];
        //     $numDocumento = $datos['documento'];
        //     $usuario = $datos['name_user'];
        //     $fechaNacimiento = $datos['fecha_nacimiento'];
        //     $correo = $datos['correo'];
        //     $telefono = $datos['telefono'];
        //     $ciudad = $datos['ciudad'];
        //     $cargo = $datos['cargo'];
        //     $telefonoCor = $datos['telefono_cor'];
        //     $piso = $datos['piso'];
        //     $area = $datos['area'];
        //     $estado = $datos['estado'];
        // }
        ?>

        <!-- Main content -->
        <section class="content">
            <form action="../../../FUNCTIONS/CRUD/actualizar_usuarios.php" method="post">
                <div class="container-fluid">
                    <!-- SELECT2 EXAMPLE -->
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">Actualizar Usuarios</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!---------------- FORMULARIO ----------------->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nombres:</label>
                                        <input type="text" name="nombres" class="form-control" value="<?php echo $nombre; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Apellidos:</label>
                                        <input type="text" name="apellidos" class="form-control" value="<?php echo $apellido; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Usuario:</label>
                                        <input type="email" name="usuario" class="form-control" value="<?php echo $usuario; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Contraseña:</label>
                                        <input type="password" name="contrasena" class="form-control" value="<?php echo $contrasena; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tipo de Documento:</label>
                                        <input type="text" name="tipo_documento" class="form-control" value="<?php echo $tipoDocumento; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>No. de Documento:</label>
                                        <input type="number" name="noDocumento" class="form-control" value="<?php echo $numDocumento; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Fecha de Nacimiento:</label>
                                        <input type="date" name="fechaNacimiento" class="form-control" value="<?php echo $fechaNacimiento; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Correo:</label>
                                        <input type="email" name="correo" class="form-control" value="<?php echo $correo; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Telefono:</label>
                                        <input type="number" name="telefono" class="form-control" value="<?php echo $telefono; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Ciudad:</label>
                                        <input type="text" name="ciudad" class="form-control" value="<?php echo $ciudad; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Cargo:</label>
                                        <input type="text" name="cargo" class="form-control" value="<?php echo $cargo; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Telefono Corporativo:</label>
                                        <input type="number" name="telCorporativo" class="form-control" value="<?php echo $telefonoCor; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Piso:</label>
                                        <input type="text" name="piso" class="form-control" value="<?php echo $piso; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Area:</label>
                                        <input type="text" name="area" class="form-control" value="<?php echo $area; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Estado:</label>
                                        <select class="form-control" name="estado" id="">
                                            <value><?php echo $estado; ?></value>
                                            <value>Activo</value>
                                            <value>Desactivado</value>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-block btn-success">Actualizar Usuario</button>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                            <!----------------- FIN FORMULARIO ----------------->
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div> <?php require('../../FOOTER/index.php'); ?> </div>

    <!--  -->



    <!-- REQUIRED SCRIPTS -->



    <!-- jQuery -->



    <!-- Bootstrap -->

    <script src="../../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- AdminLTE -->

    <script src="../../../DESIGN/JS/principal_js/adminlte.js"></script>



    <!-- OPTIONAL SCRIPTS -->

    <script src="../../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>

    <script src="../../../DESIGN/JS/principal_js/demo.js"></script>

    <script src="../../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>

    <script src="../../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE -->
    <script src="../../../DESIGN/JS/principal_js/adminlte.js"></script>
    <!-- OPTIONAL SCRIPTS -->
    <script src="../../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>
    <script src="../../../DESIGN/JS/principal_js/demo.js"></script>
    <script src="../../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>
    </body>



    </html>


    <?php  } else {
  header('location: ./../../../../index.php');
}  ?>