<?php 
header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');


$select = mysqli_query($conex,"SELECT COUNT(*) as conteo FROM `shipping`");
while ($dato = mysqli_fetch_array($select)){
   
$dato1 = $dato['conteo'];

}

$select1 = mysqli_query($conex,"SELECT COUNT(*) as conteo FROM `office`");
while ($dato10 = mysqli_fetch_array($select1)){
   
$dato2 = $dato10['conteo'];

}
$select2 = mysqli_query($conex,"SELECT COUNT(*) as conteo FROM `point`");
while ($dato11 = mysqli_fetch_array($select2)){
   
$dato3 = $dato11['conteo'];

}
$select3 = mysqli_query($conex,"SELECT COUNT(*) as conteo FROM `external_courier`");
while ($dato12 = mysqli_fetch_array($select3)){
   
$dato4 = $dato12['conteo'];

}
$select4 = mysqli_query($conex,"SELECT COUNT(*) as conteo FROM `office_courier`");
while ($dato13 = mysqli_fetch_array($select4)){
   
$dato5 = $dato13['conteo'];

}
$select5 = mysqli_query($conex,"SELECT COUNT(*) as conteo FROM `returned`");
while ($dato14 = mysqli_fetch_array($select5)){
   
$dato6 = $dato14['conteo'];

}
$select6 = mysqli_query($conex,"SELECT COUNT(*) as conteo FROM `restitution`");
while ($dato15 = mysqli_fetch_array($select6)){
   
$dato7 = $dato15['conteo'];

}
$select7 = mysqli_query($conex,"SELECT COUNT(*) as conteo FROM `delivery`");
while ($dato16 = mysqli_fetch_array($select7)){
   
$dato8 = $dato16['conteo'];

}

?>



<html>
  <head>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Total Solicitudes',    <?php echo $dato1; ?>],
          ['Pull Correspondencia',      <?php echo $dato2; ?>],
          ['Punto Correspondencia',  <?php echo $dato3; ?>],
          ['Mensajero Externo', <?php echo $dato4; ?>],
          ['Mensajero Interno', <?php echo $dato5; ?>],
          ['Devueltos', <?php echo $dato6; ?>],
          ['Fuera de Tiempo', <?php echo $dato7; ?>],
          ['Entregados',    <?php echo $dato8; ?>]
        ]);

        var options = {
          title: 'Correspondencia',
          is3D: true,
          'width': 500,
            'height': 300,
            chartArea: {left: "20%", top: "15%", width: "100%", height: "100%"}, 
            legend: {
            textStyle: {
              bold: true,
              color: '#000',
              fontSize: 14
            }
          }
            
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }




</script>



  <script type="text/javascript">
    google.charts.load("current", {packages:['corechart'], language: 'es_ES'});
    google.charts.setOnLoadCallback(drawChart);

      <?php

$sqlidioma = mysqli_query($conex, "SET lc_time_names = 'es_MX'");
if($sqlidioma == true){

$sql ="SELECT MONTHNAME(fecha_registro) AS MES, YEAR(fecha_registro) AS AÑO, COUNT(*) AS REGISTROS FROM delivery GROUP BY MES";
     $result = mysqli_query($conex,$sql);
     $chart_data="";
     while ($row = mysqli_fetch_array($result)) { 
        $mes[]  = $row['MES'];
        $year[] = $row['AÑO'];
        $registros[] = $row['REGISTROS'];
    }
}else{
    echo "Error en la consulta";
}

?>


    
    function drawChart() {
      var data = google.visualization.arrayToDataTable([
        ["Element", "Registros", { role: "style" }, "Año" ],
        ["<?= $mes[0] ?>", <?= $registros[0] ?>, "blue", "<?= $year[0] ?>"],
        ["<?= $mes[1] ?>", <?= $registros[1] ?>, "red", "<?= $year[1] ?>"],
        ["<?= $mes[2] ?>", <?= $registros[2] ?>, "gold", "<?= $year[2] ?>"],
        ["<?= $mes[3] ?>", <?= $registros[3] ?>, "green", "<?= $year[3] ?>"]
      ]);

      var view = new google.visualization.DataView(data);
      view.setColumns([0, 1,
                    { calc: "stringify",
                         sourceColumn: 3,
                         type: "string",
                         role: "annotation" },
                       2]);

      var options = {
        title: "ENTREGAS MAS EFECTIVAS ENTRE EL 2020/2021",
        width: 1000,
        height: 600,
        bar: {groupWidth: "50%"},
        legend: { position: "none" },
      };
      
      var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
      chart.draw(view, options);

      
     /* function resizeHandler () {
        chart.draw(data, options);
    }
    if (window.addEventListener) {
        window.addEventListener('resize', resizeHandler, false);
    }
    else if (window.attachEvent) {
        window.attachEvent('onresize', resizeHandler);
    } */
        

      
  }
  </script>

 




    

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../../../DESIGN/DATATABLE/bootstrap/css/bootstrap.min.css">

  <link rel="stylesheet" href="../../../DESIGN/DATATABLE/main.css">  
      
    <!--datables CSS básico-->
    <link rel="stylesheet" type="text/css" href="../../../DESIGN/DATATABLE/datatables/datatables.min.css"/>
    <!--datables estilo bootstrap 4 CSS-->  
    <link rel="stylesheet"  type="text/css" href="../../../DESIGN/DATATABLE/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">

    
  </head>
  <body>

  
<?php

$boton = 6;

include ('../../DROPDOWN/menu_admin.php');
?>
<div class="content-wrapper">

<div class="row col-md-12 col-lg-12 mx-auto">
      <div class="col-md-11 col-lg-11 card shadow mx-auto my-3">
        <div id="piechart_3d" class="mx-auto"> </div>
      </div>
    </div>




   
    <div class="row col-md-11 mx-auto ">
    <div class="col-md-12 col-lg-12 card shadow mx-auto my-3 p-0">
        <div class="card p-2 ">
            <div class="card-header mb-2">
                <label for="">Mensajero con mas entregas</label>
            </div>
            <table id="example" class="table table-striped table-bordered" cellspacing="0" >
                        <thead>
                            <tr>
                                <th>Mensajero</th>
                                <th>Total Asignado</th>   
                                <th>Entregado</th>                     
                                <th>Porcentaje</th>
                                <th>No.</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php 

                          $sql = "SELECT u.id_user, names AS nombre_mensajero
                          FROM userlogin ul 
                          INNER JOIN user u ON u.id_user = ul.id_user 
                          WHERE ul.id_loginrol = 4";

                          $query = mysqli_query($conex, $sql);
                          while($row = mysqli_fetch_array($query)){
                           $id_user = $row['id_user'];
                          ?>
                            <tr>
                                <td><?= $row['nombre_mensajero'] ?></td>
                                <td><?php $total_asignado = mysqli_query($conex, "SELECT COUNT(id_mensajero) as cont FROM `apppeopl_mailingpeoplem`.`external_courier` WHERE id_mensajero = '$id_user'"); 
                                while( $dato = mysqli_fetch_array($total_asignado)){ $conteo = $dato['cont']; echo $conteo; }
                                ?></td> 
                                <td><?php $total_entregado = mysqli_query($conex, "SELECT COUNT(id_mensajero) as entregado FROM delivery WHERE id_mensajero = '$id_user'"); 
                                while( $dato2 = mysqli_fetch_array($total_entregado)){ $conteo2 = $dato2['entregado']; echo $conteo2; }
                                ?></td>  
                                <td>
                              <div class="progress progress-xs progress-striped active">
                                <div class="progress-bar bg-success" style="width: 90%"></div>
                              </div>
                            </td>
                            <td><span class="badge bg-success">90%</span></td>
                            </tr>


                            <?php } ?>
                                               
                        </tbody>        
                       </table>    
               
        </div>
      </div>
    </div>

  
    <div class="row col-md-11 mx-auto ">
    <div class="col-md-12 col-lg-12 card shadow mx-auto my-3 p-0">
        <div class="card p-2 col-md-12">
            <div class="card-header mb-2">
                <label for="">Estadistica por entrega</label>
            </div>                       
            <div class="column card-body col-md-12">

               <div id="columnchart_values" ></div>
            </div>
        
                      
        </div>
      </div>
    </div>

  



</div>
  </body>


    <!-- jQuery, Popper.js, Bootstrap JS -->
    <script src="../../../DESIGN/DATATABLE/jquery/jquery-3.3.1.min.js"></script>
    <script src="../../../DESIGN/DATATABLE/popper/popper.min.js"></script>
    <script src="../../../DESIGN/DATATABLE/bootstrap/js/bootstrap.min.js"></script>
      
    <!-- datatables JS -->
    <script type="text/javascript" src="../../../DESIGN/DATATABLE/datatables/datatables.min.js"></script>    
     
    <!-- para usar botones en datatables JS -->  
    <script src="../../../DESIGN/DATATABLE/datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>  
    <script src="../../../DESIGN/DATATABLE/datatables/JSZip-2.5.0/jszip.min.js"></script>    
    <script src="../../../DESIGN/DATATABLE/datatables/pdfmake-0.1.36/pdfmake.min.js"></script>    
    <script src="../../../DESIGN/DATATABLE/datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
    <script src="../../../DESIGN/DATATABLE/datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>
     
    <!-- código JS propìo-->    
    <script type="text/javascript" src="../../../DESIGN/DATATABLE/main.js"></script>   

 
</html>