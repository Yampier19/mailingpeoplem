<?php

$string_intro = getenv("QUERY STRING");
parse_str($string_intro);
if (isset($_GET['xnfgtiyy'])) {
    $id_shipping = base64_decode($_GET['xnfgtiyy']);
} else {
    //echo "";
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tracking</title>
    <link rel="stylesheet" href="../../../DESIGN/CSS/principal_css/adminlte.min.css">
    <link rel="stylesheet" href="../../../DESIGN/CSS/principal_fontawesome-free/css/all.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        body {
            margin: 0;
            font-family: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
            font-size: .75rem;
            font-weight: 400;
            line-height: 1.5;
            color: black !important;
            text-align: left;
            background-color: #f1f1f1 !important;
        }

        .imagen_1 {
            background-image: url("../../DESIGN/IMG/prueba.png");
            -moz-background-size: 100% 100%;
            /*Firefox 3.6*/
            -o-background-size: 100% 100%;
            /*opera*/
            -webkit-background-size: 100% 100%;
            /*Safari*/
            background-size: 100% 100%;
            /*estandar css3*/
            -moz-border-image: url('img/fondo.jpg') 0;
            /*Firefox 3.5*/
            height: 90px;
        }

        #subencabezado {
            background-color: #f1f1f1;
            border: 1px solid #f1f1f1;
        }

        .colorWarning {
            color: #ffc107;
            text-decoration: none;
            background-color: transparent;
        }

        .colorFucsia {
            color: #d81b60;
            text-decoration: none;
            background-color: transparent;
        }

        .azulClaro {
            color: #17a2b8;
            text-decoration: none;
            background-color: transparent;
        }

        .AzulOscuro {
            color: #007bff;
            text-decoration: none;
            background-color: transparent;

        }

        .colorNaranja {
            color: #ff851b;
            text-decoration: none;
            background-color: transparent;

        }

        .colorSuccess {
            color: #28a745;
            text-decoration: none;
            background-color: transparent;
        }

        .colorSegundary {
            color: #6c757d;
            text-decoration: none;
            background-color: transparent;
        }
    </style>

</head>

<body onLoad="load()" >
    <?php

    header("Content-Type: text/html;charset=utf-8");
    require('../../../CONNECTION/SECURITY/conex.php');
    require('../../../CONNECTION/SECURITY/session_cookie.php');
    function solicitud($conex, $id_shipping)
    {
        $select_solicitud = mysqli_query($conex, "SELECT C.`id_qr_generado` AS id_qr_generado, A.`nombres` AS nombres, A.`apellidos` AS apellidos, A.`telefono` AS telefono, A.`email` AS email, A.`direccion` AS direccion, A.`prioridad` AS prioridad, B.`ciudad` AS ciudad_origen, A.`ciudad_destino` AS ciudad_destino, A.`barrio_destino` AS barrio_destino, A.`centro_costos` AS centro_costos, A.`tipo_solicitud` AS producto, A.`ubicacion` AS ubicacion_correspondencia, A.`fecha_inicio` AS fecha_entrega, A.`fecha_fin` AS fecha_max, A.`fecha_registro` AS fecha_registro_solicitud, B.`names` AS names, B.`surnames` AS surnames FROM shipping AS A LEFT JOIN personal AS B ON A.id_personal = B.id_personal LEFT JOIN qr_generated AS C ON A.id_generate = id_qr WHERE A.`id_shipping` = '$id_shipping';");
        while ($solicitud = (mysqli_fetch_array($select_solicitud))) {
            $id_qr_generado = $solicitud['id_qr_generado'];
            $nombres_destinatario = $solicitud['nombres'];
            $apellidos_destinatario = $solicitud['apellidos'];
            $telefono_destinatario = $solicitud['telefono'];
            $email = $solicitud['email'];
            $direccion = $solicitud['direccion'];
            $prioridad = $solicitud['prioridad'];
            $ciudad_origen = $solicitud['ciudad_origen'];
            $ciudad_destino = $solicitud['ciudad_destino'];
            $barrio_destino = $solicitud['barrio_destino'];
            $centro_costos = $solicitud['centro_costos'];
            $producto = $solicitud['producto'];
            $ubicacion_correspondencia = $solicitud['ubicacion_correspondencia'];
            $fecha_entrega = $solicitud['fecha_entrega'];
            $fecha_max = $solicitud['fecha_max'];
            $fecha_registro_solicitud = $solicitud['fecha_registro_solicitud'];
            $nombre_remitente = $solicitud['names'];
            $apellidos_remitente = $solicitud['surnames'];
        }
    ?>
        <!-- timeline time label -->
        <div class="time-label">
            <span class="bg-warning"><?php echo "QR: " . $id_qr_generado; ?></span>
        </div>
        <!-- /.timeline-label -->
        <!-- timeline item -->
        <div>
            <i class="fas fa-dolly-flatbed bg-warning"></i>
            <div class="timeline-item">
                <span class="time"><i class="fas fa-clock"></i><?php echo $fecha_registro_solicitud; ?></span>
                <h3 class="timeline-header"><strong class="colorWarning">Solicitud</strong> - Correspondencia solicitada</h3>
                <div class="timeline-body">
                    <div class="row" id="subencabezado">
                        <div class="col-md-12">
                            Informaci&oacute;n del remitente:
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            Nombre remitente:
                        </div>
                        <div class="col-md-4">
                            Casa Editorial EL TIEMPO
                        </div>
                        <div class="col-md-3">
                            <?php echo $nombre_remitente . " " . $apellidos_remitente; ?>
                        </div>
                    </div>
                    <div class="row" id="subencabezado">
                        <div class="col-md-12">
                            Informaci&oacute;n del destinatario:
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            Nombre completo:
                        </div>
                        <div class="col-md-3">
                            <?php echo $nombres_destinatario . " " . $apellidos_destinatario ?>
                        </div>
                        <div class="col-md-3">
                            Tel&eacute;fono:
                        </div>
                        <div class="col-md-3">
                            <?php echo $telefono_destinatario; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            Ciudad:
                        </div>
                        <div class="col-md-3">
                            <?php echo $ciudad_destino ?>
                        </div>
                        <div class="col-md-3">
                            Email:
                        </div>
                        <div class="col-md-3">
                            <?php echo $email ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            Direcci&oacute;n:
                        </div>
                        <div class="col-md-3">
                            <?php echo $direccion ?>
                        </div>
                        <div class="col-md-3">
                            Barrio:
                        </div>
                        <div class="col-md-3">
                            <?php echo $barrio_destino ?>
                        </div>
                    </div>
                    <div class="row" id="subencabezado">
                        <div class="col-md-12">
                            Informacion de la corespondencia:
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            Tipo de correpondencia:
                        </div>
                        <div class="col-md-3">
                            <?php echo $producto ?>
                        </div>
                        <div class="col-md-3">
                            Centro costos:
                        </div>
                        <div class="col-md-3">
                            <?php echo $centro_costos ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            Ubicacion Inicial:
                        </div>
                        <div class="col-md-3">
                            <?php echo $ubicacion_correspondencia ?>
                        </div>
                        <div class="col-md-3">
                            Fecha Entrega:
                        </div>
                        <div class="col-md-3">
                            <?php echo $fecha_entrega ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">

                            Prioridad:

                        </div>

                        <div class="col-md-3">

                            <?php echo $prioridad . " Dias"; ?>

                        </div>

                        <div class="col-md-3">

                            Fecha M&aacute;xima de entrega

                        </div>

                        <div class="col-md-3">

                            <?php echo $fecha_max ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }

    function ubicacionBandeja($conex, $id_shipping)
    {
        $select_point = mysqli_query($conex, "SELECT B.novedad AS novedad_point, B.fecha_registro AS fecha_registro_point FROM shipping AS A LEFT JOIN `point` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");
        while ($point = (mysqli_fetch_array($select_point))) {
            $novedad_point = $point['novedad_point'];
            $fecha_registro_point = $point['fecha_registro_point'];
        }

    ?>
        <!-- .timeline item -->
        <div>
            <i class="fab fa-product-hunt" style="background-color:#d81b60; color:#FFFFFF;"></i>
            <div class="timeline-item">
                <span class="time"><i class="fas fa-clock"></i><?php echo $fecha_registro_point; ?></span>
                <h3 class="timeline-header no-border"><strong class="colorFucsia">Ubicacion de la corespondencia</strong> - Bandeja de Correspondencia </h3>
                <div class="timeline-body">
                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control" placeholder="Novedad" name="novedad" id="novedad" cols="30" rows="1" disabled><?php echo $novedad_point  ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  /.timeline item -->
    <?php
    }
    function ubicacionPool($conex, $id_shipping)
    {
        $select_office = mysqli_query($conex, "SELECT B.novedad AS novedad_office, B.fecha_registro AS fecha_registro_office FROM shipping AS A LEFT JOIN office AS B ON A.id_shipping =B.id_ship WHERE B.id_ship = '$id_shipping';");
        while ($office = (mysqli_fetch_array($select_office))) {
            $novedad_office = $office['novedad_office'];
            $fecha_registro_office = $office['fecha_registro_office'];
        }
    ?>
        <!-- timeline item -->
        <div>
            <i class="fab fa-product-hunt" style="background-color:#d81b60; color:#FFFFFF;"></i>
            <div class="timeline-item">
                <span class="time"><i class="fas fa-clock"></i><?php echo $fecha_registro_office; ?></span>

                <h3 class="timeline-header no-border"><strong class="colorFucsia">Ubicacion de la corespondencia</strong> - Pool de correspondencia </h3>

                <div class="timeline-body">

                    <div class="row">

                        <div class="col-md-12">

                            <textarea class="form-control" placeholder="Novedad" name="novedad" id="novedad" cols="30" rows="1" disabled><?php echo $novedad_office ?></textarea>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }

    function asignacionBandeja($conex, $id_shipping)

    {

        $select_point_courier = mysqli_query($conex, "SELECT B.novedad AS novedad_point_courier, B.fecha_registro AS fecha_regis_point_courier FROM shipping AS A LEFT JOIN point_courier AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");



        while ($point_courier = (mysqli_fetch_array($select_point_courier))) {

            $novedad_point_courier = $point_courier['novedad_point_courier'];

            $fecha_regis_point_courier = $point_courier['fecha_regis_point_courier'];
        }

    ?>

        <!-- timeline item -->

        <div>

            <i class="fas fa-dolly bg-info"></i>

            <div class="timeline-item">

                <span class="time"><i class="fas fa-clock"></i><?php echo  $fecha_regis_point_courier; ?></span>

                <h3 class="timeline-header"><strong class="azulClaro">Asignacion de la bandeja de correspondencia</strong> - La correspondencia est&aacute; asignada a un mensajero</h3>

                <div class="timeline-body">

                    <textarea class="form-control" placeholder="Novedad" name="novedad2" id="novedad2" cols="30" rows="1" disabled><?php echo $novedad_point_courier; ?></textarea>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }

    function asignacionPool($conex, $id_shipping)

    {

        $select_office_courier = mysqli_query($conex, "SELECT B.novedad AS novedad_office_courier, B.fecha_registro AS fecha_regis_office_courier FROM shipping AS A LEFT JOIN office_courier AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");



        while ($office_courier = (mysqli_fetch_array($select_office_courier))) {

            $novedad_office_courier = $office_courier['novedad_office_courier'];

            $fecha_regis_office_courier = $office_courier['fecha_regis_office_courier'];
        }

    ?>

        <!-- timeline item -->

        <div>

            <i class="fas fa-dolly bg-info"></i>

            <div class="timeline-item">

                <span class="time"><i class="fas fa-clock"></i><?php echo  $fecha_regis_office_courier; ?></span>

                <h3 class="timeline-header"><strong class="azulClaro">Asignacion de pool de correspondencia</strong> - La correspondencia est&aacute; asignada a un mensajero</h3>

                <div class="timeline-body">

                    <textarea class="form-control" placeholder="Novedad" name="novedad2" id="novedad2" cols="30" rows="1" disabled><?php echo $novedad_office_courier; ?></textarea>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }

    function asignacionExternal($conex, $id_shipping)

    {

        $select_ezternal_courier = mysqli_query($conex, "SELECT B.novedad AS novedad_ext_courier, B.fecha_registro AS fecha_regis_ext_courier FROM shipping AS A LEFT JOIN external_courier AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");



        while ($external_courier = (mysqli_fetch_array($select_ezternal_courier))) {

            $novedad_ext_courier = $external_courier['novedad_ext_courier'];

            $fecha_regis_ext_courier = $external_courier['fecha_regis_ext_courier'];
        }

    ?>

        <!-- timeline item -->

        <div>

            <i class="fas fa-dolly bg-info"></i>

            <div class="timeline-item">

                <span class="time"><i class="fas fa-clock"></i><?php echo  $fecha_regis_ext_courier; ?></span>

                <h3 class="timeline-header"><strong class="azulClaro">Asignacion de mensajero externo</strong> - La correspondencia est&aacute; asignado a un mensajero</h3>

                <div class="timeline-body">

                    <textarea class="form-control" placeholder="Novedad" name="novedad2" id="novedad2" cols="30" rows="1" disabled><?php echo $novedad_ext_courier; ?></textarea>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }

    function rutaBandeja($conex, $id_shipping)

    {

        $select_received_punto = mysqli_query($conex, "SELECT B.novedad AS novedad_ruta_bandeja, B.fecha_registro AS fecha_regis_bandeja  FROM shipping AS A LEFT JOIN received_punto AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");



        while ($received_punto = (mysqli_fetch_array($select_received_punto))) {

            $novedad_ruta_bandeja = $received_punto['novedad_ruta_bandeja'];

            $fecha_regis_bandeja = $received_punto['fecha_regis_bandeja'];
        }

    ?>

        <!-- timeline item -->

        <div>

            <i class="fas fa-shipping-fast bg-primary"></i>

            <div class="timeline-item">

                <span class="time"><i class="fas fa-clock"></i><?php echo $fecha_regis_bandeja; ?></span>

                <h3 class="timeline-header"><strong class="AzulOscuro">En ruta</strong> - El mensajero recibi&oacute; el producto en la badeja de correspondencia</h3>

                <div class="timeline-body">

                    <textarea class="form-control" placeholder="Novedad" name="novedad3" id="novedad3" cols="30" rows="1" disabled><?php echo $novedad_ruta_bandeja  ?></textarea>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }

    function rutaPool($conex, $id_shipping)

    {

        $select_reiceved_despacho = mysqli_query($conex, "SELECT B.novedad AS novedad_ruta_pool, B.fecha_registro AS fecha_regis_pool  FROM shipping AS A LEFT JOIN received_despacho AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");



        while ($reiceved_despacho = (mysqli_fetch_array($select_reiceved_despacho))) {

            $novedad_ruta_pool = $reiceved_despacho['novedad_ruta_pool'];

            $fecha_regis_pool = $reiceved_despacho['fecha_regis_pool'];
        }

    ?>

        <!-- timeline item -->

        <div>

            <i class="fas fa-shipping-fast bg-primary"></i>

            <div class="timeline-item">

                <span class="time"><i class="fas fa-clock"></i><?php echo $fecha_regis_pool; ?></span>

                <h3 class="timeline-header"><strong class="AzulOscuro">En ruta</strong> - El mensajero recibi&oacute; el producto en pool corespondencia </h3>

                <div class="timeline-body">

                    <textarea class="form-control" placeholder="Novedad" name="novedad3" id="novedad3" cols="30" rows="1" disabled><?php echo $novedad_ruta_pool  ?></textarea>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }

    function rutaExternal($conex, $id_shipping)

    {

        $select_received_external = mysqli_query($conex, "SELECT B.novedad AS novedad_ruta_external, B.fecha_registro AS fecha_regis_extenal  FROM shipping AS A LEFT JOIN received_externo AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");



        while ($received_external = (mysqli_fetch_array($select_received_external))) {

            $novedad_ruta_external = $received_external['novedad_ruta_external'];

            $fecha_regis_extenal = $received_external['fecha_regis_extenal'];
        }

    ?>

        <!-- timeline item -->

        <div>

            <i class="fas fa-shipping-fast bg-primary"></i>

            <div class="timeline-item">

                <span class="time"><i class="fas fa-clock"></i><?php echo $fecha_regis_extenal; ?></span>

                <h3 class="timeline-header"><strong class="AzulOscuro">En ruta</strong> - El mensajero externo recibi&oacute; el producto en pool corespondencia</h3>

                <div class="timeline-body">

                    <textarea class="form-control" placeholder="Novedad" name="novedad3" id="novedad3" cols="30" rows="1" disabled><?php echo $novedad_ruta_external  ?></textarea>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }

    function devoluciones($conex, $id_shipping)

    {

        $select_returned = mysqli_query($conex, "SELECT B.novedad AS novedad_devolucion, B.fecha_registro AS fecha_regis_devolucion FROM shipping AS A LEFT JOIN returned AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");



        while ($returned = (mysqli_fetch_array($select_returned))) {

            $novedad_devolucion = $returned['novedad_devolucion'];

            $fecha_regis_devolucion = $returned['fecha_regis_devolucion'];
        }

    ?>

        <!-- timeline item -->

        <div>

            <i class="fas fa-sync-alt" style="background-color:#ff851b; color:#000000;"></i>

            <div class="timeline-item">

                <span class="time"><i class="fas fa-clock"></i><?php echo $fecha_regis_devolucion; ?></span>

                <h3 class="timeline-header"><strong class="colorNaranja">Devoluciones</strong> - La correspondencia est&aacute; en estado de devoluci&oacute;n</h3>

                <div class="timeline-body">

                    <textarea class="form-control" placeholder="Novedad (Motivo de la devolucion)" name="novedad3" id="novedad3" cols="30" rows="1" disabled><?php echo $novedad_devolucion; ?></textarea>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }

    function entregas($conex, $id_shipping)

    {

        $select_delivery = mysqli_query($conex, "SELECT B.novedad AS novedad_entrega, B.fecha_Registro AS fecha_regis_entrega FROM shipping AS A LEFT JOIN delivery AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");



        while ($delivery = (mysqli_fetch_array($select_delivery))) {

            $novedad_entrega = $delivery['novedad_entrega'];

            $fecha_regis_entrega = $delivery['fecha_regis_entrega'];
        }

    ?>

        <!-- timeline item -->

        <div>

            <i class="fas fa-vote-yea bg-success"></i>

            <div class="timeline-item">

                <span class="time"><i class="fas fa-clock"></i><?php echo $fecha_regis_entrega ?></span>

                <h3 class="timeline-header"><strong class="colorSuccess">Correspondencia Entregada </strong>El mensajero entreg&oacute; la correspondencia</h3>

                <div class="timeline-body">

                    <textarea class="form-control" placeholder="Novedad (Motivo de la devolucion)" name="novedad4" id="novedad4" cols="30" rows="1" disabled><?php echo $novedad_entrega ?></textarea>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }

    function restitucion($conex, $id_shipping)

    {

        $select_restitution = mysqli_query($conex, "SELECT B.novedad AS novedad_restitucion, B.fecha_Registro AS fecha_regis_restitucion FROM shipping AS A LEFT JOIN restitution AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");



        while ($restitution = (mysqli_fetch_array($select_restitution))) {

            $novedad_restitucion = $restitution['novedad_restitucion'];

            $fecha_regis_restitucion = $restitution['fecha_regis_restitucion'];
        }

    ?>

        <!-- timeline item -->

        <div>

            <i class="fas fa-reply bg-secondary "></i>

            <div class="timeline-item">

                <span class="time"><i class="fas fa-clock"></i><?php echo $fecha_regis_restitucion ?></span>

                <h3 class="timeline-header"><strong class="colorSegundary">Restituci&oacute;n de la correpsondencia</strong> - El producto se devolvi&oacute; al remitente</h3>

                <div class="timeline-body">

                    <textarea class="form-control" placeholder="Novedad (Motivo de la devolucion)" name="novedad4" id="novedad4" cols="30" rows="1" disabled><?php echo $novedad_restitucion ?></textarea>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }

    function buscarFechaBD($conex, $fechaBuscar, $id_shipping)

    {

        $select_point = mysqli_query($conex, "SELECT * FROM shipping AS A LEFT JOIN `point` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping' AND B.fecha_registro = '$fechaBuscar';");

        $select_office = mysqli_query($conex, "SELECT * FROM shipping AS A LEFT JOIN `office` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping' AND B.fecha_registro = '$fechaBuscar';");

        $select_courier_point = mysqli_query($conex, "SELECT * FROM shipping AS A LEFT JOIN `point_courier` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping' AND B.fecha_registro = '$fechaBuscar';");

        $select_courier_office = mysqli_query($conex, "SELECT * FROM shipping AS A LEFT JOIN `office_courier` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping' AND B.fecha_registro = '$fechaBuscar';");

        $select_courier_external = mysqli_query($conex, "SELECT * FROM shipping AS A LEFT JOIN `external_courier` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping' AND B.fecha_registro = '$fechaBuscar';");

        $select_received_point = mysqli_query($conex, "SELECT * FROM shipping AS A LEFT JOIN `received_punto` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping' AND B.fecha_registro = '$fechaBuscar';");

        $select_received_office = mysqli_query($conex, "SELECT * FROM shipping AS A LEFT JOIN `received_despacho` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping' AND B.fecha_registro = '$fechaBuscar';");

        $select_received_external = mysqli_query($conex, "SELECT * FROM shipping AS A LEFT JOIN `received_externo` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping' AND B.fecha_registro = '$fechaBuscar';");

        $select_returned = mysqli_query($conex, "SELECT * FROM shipping AS A LEFT JOIN `returned` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping' AND B.fecha_registro = '$fechaBuscar';");

        $select_delivery = mysqli_query($conex, "SELECT * FROM shipping AS A LEFT JOIN `delivery` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping' AND B.fecha_registro = '$fechaBuscar';");

        $select_restitution = mysqli_query($conex, "SELECT * FROM shipping AS A LEFT JOIN `restitution` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping' AND B.fecha_registro = '$fechaBuscar';");





        $count_point = mysqli_num_rows($select_point); //echo "j".$count_point;

        $count_office = mysqli_num_rows($select_office); //echo "j".$count_office;

        $count_corrier_point = mysqli_num_rows($select_courier_point); // echo "j".$count_corrier_point;

        $count_courier_office = mysqli_num_rows($select_courier_office); // echo "j".$count_courier_office; 

        $count_courier_external = mysqli_num_rows($select_courier_external); //echo "j".$count_courier_external;

        $count_received_point = mysqli_num_rows($select_received_point); //echo "j".$count_received_point;

        $count_received_office = mysqli_num_rows($select_received_office); // echo "j".$count_received_office; 

        $count_received_external = mysqli_num_rows($select_received_external); // echo "j".$count_received_external;

        $count_returned = mysqli_num_rows($select_returned); //echo "j".$count_returned; 

        $count_delivery = mysqli_num_rows($select_delivery); // echo "j".$count_delivery;

        $count_restitution = mysqli_num_rows($select_restitution); // echo "j".$count_restitution;



        if ($count_point > '0') {

            $caracteristicaFecha = "bandeja";
        }

        if ($count_office > '0') {

            $caracteristicaFecha = "pool";
        }

        if ($count_corrier_point > '0') {

            $caracteristicaFecha = "bandeja_courrier";
        }

        if ($count_courier_office > '0') {

            $caracteristicaFecha = "pool_courrier";
        }

        if ($count_courier_external > '0') {

            $caracteristicaFecha = "external_courrier";
        }

        if ($count_received_point > '0') {

            $caracteristicaFecha = "bandeja_received";
        }

        if ($count_received_office > '0') {

            $caracteristicaFecha = "pool_received";
        }

        if ($count_received_external > '0') {

            $caracteristicaFecha = "external_received";
        }

        if ($count_returned > '0') {

            $caracteristicaFecha = "returned";
        }

        if ($count_delivery > '0') {

            $caracteristicaFecha = "delivery";
        }

        if ($count_restitution > '0') {

            $caracteristicaFecha = "restitution";
        }



        $validacionNConsultas = $count_point + $count_office + $count_corrier_point + $count_courier_office + $count_courier_external + $count_received_point + $count_received_office + $count_received_external + $count_returned + $count_delivery + $count_restitution;

        //echo "-aqui".$validacionNConsultas."-";

        if ($validacionNConsultas == '1') {



            $infoConsulta =  $caracteristicaFecha;

            return $infoConsulta;
        } else

            return false;
    }

    function imprimirSeccion($conex, $tipo, $id_shipping)

    {

        switch ($tipo) {

            case 'bandeja':

                ubicacionBandeja($conex, $id_shipping);

                break;

            case 'pool':

                ubicacionPool($conex, $id_shipping);

                break;

            case 'bandeja_courrier':

                asignacionBandeja($conex, $id_shipping);

                break;

            case 'pool_courrier':

                asignacionPool($conex, $id_shipping);

                break;

            case 'external_courrier':

                asignacionExternal($conex, $id_shipping);

                break;

            case 'bandeja_received':

                rutaBandeja($conex, $id_shipping);

                break;

            case 'pool_received':

                rutaPool($conex, $id_shipping);

                break;

            case 'external_received':

                rutaExternal($conex, $id_shipping);

                break;

            case 'returned':

                devoluciones($conex, $id_shipping);

                break;

            case 'delivery':

                entregas($conex, $id_shipping);

                break;

            case 'restitution':

                restitucion($conex, $id_shipping);

                break;

            case false:

                echo  "error";

                break;

            default:

                # code...

                break;
        }
    }

    function cosultarFechas($conex, $id_shipping)

    {



        $select_point = mysqli_query($conex, "SELECT B.fecha_registro AS fecha_regis_point  FROM shipping AS A LEFT JOIN `point` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");

        while ($point = (mysqli_fetch_array($select_point))) {

            $fechaUno = $point['fecha_regis_point'];
        }



        $select_office = mysqli_query($conex, "SELECT B.fecha_registro AS fecha_regis_office  FROM shipping AS A LEFT JOIN `office` AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");

        while ($office = (mysqli_fetch_array($select_office))) {

            $fechaDos = $office['fecha_regis_office'];
        }



        $select_courier_point = mysqli_query($conex, "SELECT B.fecha_registro AS fecha_regis_cour_point FROM shipping AS A LEFT JOIN point_courier AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");

        while ($courier_point = (mysqli_fetch_array($select_courier_point))) {

            $fechaTres = $courier_point['fecha_regis_cour_point'];
        }



        $select_courrier_office = mysqli_query($conex, "SELECT 	B.fecha_registro AS fecha_regis_cour_offfice FROM shipping AS A LEFT JOIN office_courier AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");

        while ($courier_office = (mysqli_fetch_array($select_courrier_office))) {

            $fechaCuatro = $courier_office['fecha_regis_cour_offfice'];
        }



        $select_courier_external = mysqli_query($conex, "SELECT B.fecha_registro AS fecha_regis_cour_external  FROM shipping AS A LEFT JOIN external_courier AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");

        while ($ocurier_external = (mysqli_fetch_array($select_courier_external))) {

            $fechaCinco = $ocurier_external['fecha_regis_cour_external'];
        }



        $select_received_point = mysqli_query($conex, "SELECT B.fecha_registro AS fecha_regis_recei_point  FROM shipping AS A LEFT JOIN received_punto AS B ON A.id_shipping = B.id_ship  WHERE B.id_ship = '$id_shipping';");

        while ($received_point = (mysqli_fetch_array($select_received_point))) {

            $fechaSeis = $received_point['fecha_regis_recei_point'];
        }



        $select_received_office = mysqli_query($conex, "SELECT B.fecha_registro AS fecha_regis_recei_office  FROM shipping AS A LEFT JOIN received_despacho AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");

        while ($received_office = (mysqli_fetch_array($select_received_office))) {

            $fechaSiete = $received_office['fecha_regis_recei_office'];
        }



        $select_received_external = mysqli_query($conex, "SELECT B.fecha_registro AS fecha_regis_recei_external  FROM shipping AS A LEFT JOIN received_externo AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");

        while ($received_external = (mysqli_fetch_array($select_received_external))) {

            $fechaOcho = $received_external['fecha_regis_recei_external'];
        }



        $select_returned = mysqli_query($conex, "SELECT B.fecha_registro AS fecha_regis_returned FROM shipping AS A LEFT JOIN returned AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");

        while ($returned = (mysqli_fetch_array($select_returned))) {

            $fechaNueve = $returned['fecha_regis_returned'];
        }



        $select_delivery = mysqli_query($conex, "SELECT B.fecha_registro AS fecha_regis_delivery FROM shipping AS A LEFT JOIN delivery AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");

        while ($delivery = (mysqli_fetch_array($select_delivery))) {

            $fechaDiez = $delivery['fecha_regis_delivery'];
        }



        $select_restitution = mysqli_query($conex, "SELECT B.fecha_registro AS fecha_regis_restitution FROM shipping AS A LEFT JOIN restitution AS B ON A.id_shipping = B.id_ship WHERE B.id_ship = '$id_shipping';");

        while ($restitution = (mysqli_fetch_array($select_restitution))) {

            $fechaOnce = $restitution['fecha_regis_restitution'];
        }



        $countPoint = mysqli_num_rows($select_point); //echo "<center>a".$countPoint;

        $countOffice = mysqli_num_rows($select_office); //echo "b".$countOffice;

        $countCourierPoint = mysqli_num_rows($select_courier_point); //echo "c".$countCourierPoint;

        $countCourierOfice = mysqli_num_rows($select_courrier_office); //echo "d".$countCourierOfice;

        $countCourirerExternal = mysqli_num_rows($select_courier_external); // echo "e".$countCourirerExternal;

        $countRecivedPoint = mysqli_num_rows($select_received_point); // echo "f".$countRecivedPoint;

        $countRecivedOffice = mysqli_num_rows($select_received_office); // echo "g".$countRecivedOffice;

        $countRecivedExternal = mysqli_num_rows($select_received_external); // echo "h".$countRecivedExternal;

        $countReturned = mysqli_num_rows($select_returned); //echo "i".$countReturned;

        $countDelivery = mysqli_num_rows($select_delivery); //echo "j".$countDelivery;

        $countRestitution = mysqli_num_rows($select_restitution); //echo "j".$countRestitution."</center>";



        // Fechas enviadas armar array

        // fechaUno -> point

        // fechaDos -> office

        // fechaTres -> courier_point

        // fechaCuatro -> courier_office

        // fechaCinco -> courier_external

        // fechaSeis -> received_point

        // fechaSiete -> received_office

        // fechaOcho -> received_external

        // fechaNueve -> returned

        // fechaDiez -> delivery

        // fechaOnce -> restitution



        if ($countPoint < '1' && $countOffice < '1' && $countCourierPoint < '1' && $countCourierOfice < '1' && $countCourirerExternal < '1' && $countRecivedPoint < '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {



            $validacionArreglo = '1';

            $arreglo = [

                $validacionArreglo,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice < '1' && $countCourierPoint < '1' && $countCourierOfice < '1' && $countCourirerExternal < '1' && $countRecivedPoint < '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {



            $validacionArreglo = '2';

            $caracteristica = "bandeja";



            $arreglo = [

                $validacionArreglo,

                $caracteristica,

                $fechaUno,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice < '1' && $countCourirerExternal < '1' && $countRecivedPoint < '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {



            $validacionArreglo = '2';

            $caracteristica = "pool";



            $arreglo = [

                $validacionArreglo,

                $caracteristica,

                $fechaUno,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice < '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal < '1' && $countRecivedPoint < '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '3';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint < '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '3';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCuatro,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint < '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '3';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCinco,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice < '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '4';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaSeis,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint < '1' && $countRecivedOffice == '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '4';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCuatro,

                $fechaSiete,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint < '1' && $countRecivedOffice < '1' && $countRecivedExternal == '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '4';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCinco,

                $fechaOcho,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice < '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned == '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '5';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaSeis,

                $fechaNueve,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint < '1' && $countRecivedOffice == '1' && $countRecivedExternal < '1' && $countReturned == '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '5';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCuatro,

                $fechaSiete,

                $fechaNueve,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint < '1' && $countRecivedOffice < '1' && $countRecivedExternal == '1' && $countReturned == '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '5';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCinco,

                $fechaOcho,

                $fechaNueve,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice < '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery == '1' && $countRestitution < '1') {

            $validacionArreglo = '5';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaSeis,

                $fechaDiez,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint < '1' && $countRecivedOffice == '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery == '1' && $countRestitution < '1') {

            $validacionArreglo = '5';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCuatro,

                $fechaSiete,

                $fechaDiez,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint < '1' && $countRecivedOffice < '1' && $countRecivedExternal == '1' && $countReturned < '1' && $countDelivery == '1' && $countRestitution < '1') {

            $validacionArreglo = '5';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCinco,

                $fechaOcho,

                $fechaDiez,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '5';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaSeis,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice < '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned == '1' && $countDelivery < '1' && $countRestitution == '1') {

            $validacionArreglo = '6';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaSeis,

                $fechaNueve,

                $fechaOnce,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint < '1' && $countRecivedOffice == '1' && $countRecivedExternal < '1' && $countReturned == '1' && $countDelivery < '1' && $countRestitution == '1') {

            $validacionArreglo = '6';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCuatro,

                $fechaSiete,

                $fechaNueve,

                $fechaOnce,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint < '1' && $countRecivedOffice < '1' && $countRecivedExternal == '1' && $countReturned == '1' && $countDelivery < '1' && $countRestitution == '1') {

            $validacionArreglo = '6';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCinco,

                $fechaOcho,

                $fechaNueve,

                $fechaOnce,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice < '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned == '1' && $countDelivery == '1' && $countRestitution < '1') {

            $validacionArreglo = '6';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaSeis,

                $fechaNueve,

                $fechaDiez,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint < '1' && $countRecivedOffice == '1' && $countRecivedExternal < '1' && $countReturned == '1' && $countDelivery == '1' && $countRestitution < '1') {

            $validacionArreglo = '6';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCuatro,

                $fechaSiete,

                $fechaNueve,

                $fechaDiez,

            ];

            return $arreglo;
        }

        if ($countPoint < '1' && $countOffice == '1' && $countCourierPoint < '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint < '1' && $countRecivedOffice < '1' && $countRecivedExternal == '1' && $countReturned == '1' && $countDelivery == '1' && $countRestitution < '1') {

            $validacionArreglo = '6';

            $arreglo = [

                $validacionArreglo,

                $fechaDos,

                $fechaCinco,

                $fechaOcho,

                $fechaNueve,

                $fechaDiez,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '6';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCuatro,

                $fechaSeis,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '6';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCinco,

                $fechaSeis,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice == '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '7';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCuatro,

                $fechaSeis,

                $fechaSiete,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal == '1' && $countReturned < '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '7';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCinco,

                $fechaSeis,

                $fechaOcho,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice == '1' && $countRecivedExternal < '1' && $countReturned == '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '8';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCuatro,

                $fechaSeis,

                $fechaSiete,

                $fechaNueve,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal == '1' && $countReturned == '1' && $countDelivery < '1' && $countRestitution < '1') {

            $validacionArreglo = '8';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCinco,

                $fechaSeis,

                $fechaOcho,

                $fechaNueve,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice == '1' && $countRecivedExternal < '1' && $countReturned < '1' && $countDelivery == '1' && $countRestitution < '1') {

            $validacionArreglo = '8';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCuatro,

                $fechaSeis,

                $fechaSiete,

                $fechaDiez,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal == '1' && $countReturned < '1' && $countDelivery == '1' && $countRestitution < '1') {

            $validacionArreglo = '8';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCinco,

                $fechaSeis,

                $fechaOcho,

                $fechaDiez,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice == '1' && $countRecivedExternal < '1' && $countReturned == '1' && $countDelivery == '1' && $countRestitution < '1') {

            $validacionArreglo = '9';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCuatro,

                $fechaSeis,

                $fechaSiete,

                $fechaNueve,

                $fechaDiez,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal == '1' && $countReturned == '1' && $countDelivery == '1' && $countRestitution < '1') {

            $validacionArreglo = '9';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCinco,

                $fechaSeis,

                $fechaOcho,

                $fechaNueve,

                $fechaDiez,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice == '1' && $countCourirerExternal < '1' && $countRecivedPoint == '1' && $countRecivedOffice == '1' && $countRecivedExternal < '1' && $countReturned == '1' && $countDelivery < '1' && $countRestitution == '1') {

            $validacionArreglo = '9';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCuatro,

                $fechaSeis,

                $fechaSiete,

                $fechaNueve,

                $fechaOnce,

                $fechaDos,

            ];

            return $arreglo;
        }

        if ($countPoint == '1' && $countOffice == '1' && $countCourierPoint == '1' && $countCourierOfice < '1' && $countCourirerExternal == '1' && $countRecivedPoint == '1' && $countRecivedOffice < '1' && $countRecivedExternal == '1' && $countReturned == '1' && $countDelivery < '1' && $countRestitution == '1') {

            $validacionArreglo = '9';

            $arreglo = [

                $validacionArreglo,

                $fechaUno,

                $fechaTres,

                $fechaCinco,

                $fechaSeis,

                $fechaOcho,

                $fechaNueve,

                $fechaOnce,

                $fechaDos,

            ];

            return $arreglo;
        }



        return false;
    }

    function imprimir($conex, $arreglo, $id_shipping)

    {

        if ($arreglo[0] == false) {

            errorLogica();

            $icon = 'fas fa-times';

            $color = '#dc3545';

            finImprimir($icon, $color);
        } else if ($arreglo[0] == "1") {

            solicitud($conex, $id_shipping);

            $icon = 'fas fa-spinner';

            $color = '#ffc107';

            finImprimir($icon, $color);
        } else if ($arreglo[0] == "2") {



            if ($arreglo[1] == "pool") {

                solicitud($conex, $id_shipping);

                ubicacionPool($conex, $id_shipping);

                $icon = 'fas fa-spinner';

                $color = '#d81b60';

                finImprimir($icon, $color);
            } else if ($arreglo[1] == "bandeja") {

                solicitud($conex, $id_shipping);

                ubicacionBandeja($conex, $id_shipping);

                $icon = 'fas fa-spinner';

                $color = '#d81b60';

                finImprimir($icon, $color);
            }
        } else if ($arreglo[0] >= "3") {

            $nArreglos = $arreglo[0] - 1;

            solicitud($conex, $id_shipping);

            /*echo "Antes: ";

            print_r($arreglo);*/

            sort($arreglo, SORT_STRING);

            /*echo "<br>Despues: ";

            print_r($arreglo);*/

            for ($i = 0; $i < $nArreglos; $i++) {

                // echo "contadpr".$arreglo[$i];

                $fechaBuscar = $arreglo[$i];

                $tipo = buscarFechaBD($conex, $fechaBuscar, $id_shipping);

                // echo "tipo".$tipo;

                imprimirSeccion($conex, $tipo, $id_shipping);
            }

            estadoActual($tipo);
        }
    }

    function estadoActual($tipo)

    {

        switch ($tipo) {

            case 'bandeja':

                $icon = 'fas fa-spinner';

                $color = '#d81b60';

                finImprimir($icon, $color);

                break;

            case 'pool':

                $icon = 'fas fa-spinner';

                $color = '#d81b60';

                finImprimir($icon, $color);

                break;

            case 'bandeja_courrier':

                $icon = 'fas fa-spinner';

                $color = '#17a2b8';

                finImprimir($icon, $color);

                break;

            case 'pool_courrier':

                $icon = 'fas fa-spinner';

                $color = '#17a2b8';

                finImprimir($icon, $color);

                break;

            case 'external_courrier':

                $icon = 'fas fa-spinner';

                $color = '#17a2b8';

                finImprimir($icon, $color);

                break;

            case 'bandeja_received':

                $icon = 'fas fa-spinner';

                $color = '#007bff';

                finImprimir($icon, $color);

                break;

            case 'pool_received':

                $icon = 'fas fa-spinner';

                $color = '#007bff';

                finImprimir($icon, $color);

                break;

            case 'external_received':

                $icon = 'fas fa-spinner';

                $color = '#007bff';

                finImprimir($icon, $color);

                break;

            case 'returned':

                $icon = 'fas fa-spinner';

                $color = '#ff851b';

                finImprimir($icon, $color);

                break;

            case 'delivery':

                $icon = 'fas fa-check-circle';

                $color = '#28a745';

                finImprimir($icon, $color);

                break;

            case 'restitution':

                $icon = 'fas fa-times';

                $color = '#6c757d';

                finImprimir($icon, $color);

                break;

            case false:

                echo  "error";

                break;

            default:

                # code...

                break;
        }
    }

    function finImprimir($icon, $color)

    {

    ?>

        <div>

            <i class="<?php echo $icon ?>" style="background-color:<?php echo $color ?>; color:#FFFFFF;"></i>

        </div>

    <?php

    }

    function errorLogica()

    {

    ?>

        <!-- timeline item -->

        <div>

            <i class="fas fa-exclamation-triangle bg-danger"></i>

            <div class="timeline-item">

                <span class="time"><i class="fas fa-exclamation-circle"></i></span>

                <h3 class="timeline-header"><strong>¡Error en traking!...</strong></h3>

                <div class="timeline-body">

                    <textarea class="form-control" placeholder="Error" name="novedad4" id="novedad4" cols="30" rows="1" disabled>Hay algo fuera de lo comun en el proceso de correspondencia, porfavor comunicarselo al administrador</textarea>

                </div>

            </div>

        </div>

        <!-- END timeline item -->

    <?php

    }



    ?>

    <!-- Traking  -->

    <div class="row">

        <div class="col-md-12">

            <div class="card card-warning">

                <div class="card-header"><span>
                        <h3 style="font-size: 1.1rem;"><i class="fas fa-mail-bulk"></i>&nbsp;&nbsp;Mis Solicitudes - Tracking</h3>
                    </span></div>

                <div class="card-body">

                    <!-- The time line -->

                    <div class="timeline">

                        <?php

                        $arreglo = cosultarFechas($conex, $id_shipping);

                        //echo $arreglo;

                        imprimir($conex, $arreglo, $id_shipping);

                        ?>

                    </div>
                    <?php $evidencia = mysqli_query($conex, "SELECT * FROM `delivery`as d LEFT JOIN ` photo_files_info` AS p ON d.id_ship = p.id_shipfil WHERE d.`id_ship` = '$id_shipping'");

                    $conteo = mysqli_num_rows($evidencia);
                    if ($conteo >= 1) {
                        while ($evi_fil = mysqli_fetch_array($evidencia)) {
                            $imagen = $evi_fil['nombre_file'];
                            $LATITUD = $evi_fil['file_latitud'];
                            $LONGITUD = $evi_fil['file_longitud'];
                        }

                    ?>
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVAVPaGOuf3neASoLo_F96Udd_IEuha7s&callback=initMap" async defer></script>
                        <script type="text/javascript">
                            function load() {
                                var cordenadax = document.getElementById("cordenadax").value;
                                var cordenaday = document.getElementById("cordenaday").value;

                                var pos_A = new google.maps.LatLng(cordenadax, cordenaday);


                                var options = {
                                    zoom: 16,
                                    center: new google.maps.LatLng(cordenadax, cordenaday),
                                    mapTypeId: google.maps.MapTypeId.DRIVING,
                                    panControl: true,
                                    zoomControl: true,
                                    mapTypeControl: true,
                                    scaleControl: true,
                                    streetViewControl: true,
                                    overviewMapControl: true
                                };

                                var map = new google.maps.Map(document.getElementById('mapa'), options);

                                var marcadorA = new google.maps.Marker({
                                    position: pos_A,
                                    map: map,
                                    title: 'Arrastrame',
                                    animation: google.maps.Animation.DROP,
                                    draggable: true
                                });


                            }
                        </script>
                        <div class="row">
                            <div class="col-md-4">

                                <div class="card card-success">
                                    <div class="card-header">
                                        <h3 class="card-title">Evidencia <i class="fas fa-camera-retro"></i></h3>
                                    </div>
                                    <div class="card-body">

                                        <img class="img-fluid" src="https://app-peoplemarketing.com/MailingPeopleM/NAVIGATION/MAIN_FOLDER/FILES/EVIDENCE/<?php print $imagen; ?>" alt="evidencia"  >
                                    </div>
                                    <!-- /.card-body -->
                                </div>


                            </div>
                            <div class="col-md-8">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title">Ubicaci&oacute;n <i class="fas fa-map-marked-alt"></i></h3>
                                    </div>
                                    <div class="card-body">
                                    <div style="width:100%">
 
 <fieldset style="border:1px solid #CCC; border-radius:10px; width:100%; display:inline-block;">
<legend style="color:#0062a7; font-weight:bold;">Geolocalizaci&oacute;n</legend>

      <div style="display:none;" >
      <input type="text" id="cordenadax" required style="display:" value="<?php echo $LATITUD; ?>">
    <input type="text" id="cordenaday" required style="display:" value="<?php echo $LONGITUD; ?>">
    </div>
    <center>
 <div id="mapa" style="width: 100%; height: 350px;"> </div>
    </center>
      </fieldset></div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>

                    <?php } ?>

                </div>

            </div>

        </div>

        <!-- /.col -->

    </div>

</body>

</html>