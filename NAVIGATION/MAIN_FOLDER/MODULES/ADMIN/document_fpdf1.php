<?php
 require('rotation_fpdf.php');
 require('../../../CONNECTION/SECURITY/conex.php');

 class PDF extends PDF_Rotate
 {
 function RotatedText($x,$y,$txt,$angle)
 {
     //Text rotated around its origin
     $this->Rotate($angle,$x,$y);
     $this->Text($x,$y,$txt);
     $this->Rotate(0);
 }
 
 function RotatedImage($file,$x,$y,$w,$h,$angle)
 {
     //Image rotated around its upper-left corner
     $this->Rotate($angle,$x,$y);
     $this->Image($file,$x,$y,$w,$h);
     $this->Rotate(0);
 }
 }

$pdf = new PDF('P','mm','letter');
$pdf ->SetMargins(0, 0 ,0 );
$pdf->SetAutoPageBreak(false);
$pdf ->AddPage('');
//tipo de letra//
$pdf ->SetFont('Arial', '',8);


$select_qr_generado=mysqli_query($conex,"SELECT C.nombres AS nombres, C.apellidos AS apellidos, C.tipo_solicitud AS tipo_solicitud, C.direccion AS direccion, C.telefono AS telefono, D.names AS names, D.documento AS documento, D.telefono AS telefono, C.barrio_destino AS barrio_destino, A.id_qr AS id_qr,A.id_qr_generado AS id_qr_generado ,B.celda_1_x AS celda_1_x, B.celda_1_y AS celda_1_y, B.celda_2_x AS celda_2_x, B.celda_2_y AS celda_2_y, B.celda_3_x AS celda_3_x, B.celda_3_y AS celda_3_y, B.celda_4_x AS celda_4_x, B.celda_4_y AS celda_4_y, B.Set_X AS Set_X, B.Set_Y AS Set_Y, B.Set2_X AS Set2_X, B.Set2_Y AS Set2_Y, B.Set3_X AS Set3_X, B.Set3_Y AS Set3_Y, B.Set4_X AS Set4_X, B.Set4_Y AS Set4_Y, B.Set5_X AS Set5_X, B.Set5_Y AS Set5_Y, B.Set6_X AS Set6_X, B.Set6_Y AS Set6_Y, B.Set7_X AS Set7_X, B.Set7_Y AS Set7_Y,B.Set8_X AS Set8_X, B.Set8_Y AS Set8_Y, B.Set9_X AS Set9_X, B.Set9_Y AS Set9_Y,B.Set10_X AS Set10_X, B.Set10_Y AS Set10_Y, B.Set11_X AS Set11_X, B.Set11_Y AS Set11_Y,B.Set12_X AS Set12_X, B.Set12_Y AS Set12_Y,B.Set13_X AS Set13_X, B.Set13_Y AS Set13_Y, B.Set14_X AS Set14_X, B.Set14_Y AS Set14_Y, B.Set15_X AS Set15_X, B.Set15_Y AS Set15_Y, B.Set16_X AS Set16_X, B.Set16_Y AS Set16_Y, B.Set17_X AS Set17_X, B.Set17_Y AS Set17_Y, B.Set18_X AS Set18_X, B.Set18_Y AS Set18_Y,B.Set19_X AS Set19_X, B.Set19_Y AS Set19_Y, B.Set20_X AS Set20_X, B.Set20_Y AS Set20_Y,B.Set21_X AS Set21_X, B.Set21_Y AS Set21_Y,B.Set22_X AS Set22_X, B.Set22_Y AS Set22_Y,B.Set23_X AS Set23_X, B.Set23_Y AS Set23_Y, B.Set24_X AS Set24_X, B.Set24_Y AS Set24_Y, B.Set25_X AS Set25_X, B.Set25_Y AS Set25_Y,B.Set26_X AS Set26_X, B.Set26_Y AS Set26_Y, B.rotated_1_x AS rotated_1_x, B.rotated_1_y AS rotated_1_y, B.rotated_2_x AS rotated_2_x, B.rotated_2_y AS rotated_2_y, B.rotated_3_x AS rotated_3_x, B.rotated_3_y AS rotated_3_y, B.rotated_4_x AS rotated_4_x, B.rotated_4_y AS rotated_4_y, B.rotated_5_x AS rotated_5_x, B.rotated_5_y AS rotated_5_y, B.rotated_6_x AS rotated_6_x, B.rotated_6_y AS rotated_6_y, B.rotated_7_x AS rotated_7_x, B.rotated_7_y AS rotated_7_y, B.imagen_1_x AS imagen_1_x, B.imagen_1_y AS imagen_1_y, B.imagen_2_x AS imagen_2_x ,B.imagen_2_y AS imagen_2_y, B.imagen_3_x AS imagen_3_x, B.imagen_3_y AS imagen_3_y,B.imagen_4_x AS imagen_4_x ,B.imagen_4_y AS imagen_4_y, A.link_qr AS link_qr  FROM qr_generated AS A
LEFT JOIN coordenadas_x_y AS B ON A.posicion = B.id_coordenadas
LEFT JOIN shipping AS C ON C.id_generate = A.id_qr  
LEFT JOIN personal AS D ON C.id_personal = D.id_personal");

while ($qr =(mysqli_fetch_array($select_qr_generado))){

    if ($qr['id_qr']%9==0){
        $pdf->AddPage('PORTRAIT','LETTER');
}

//verde -> celda 3

$pdf->Rect($qr['celda_1_x'], $qr['celda_1_y'], 40, 45, false );

$pdf->SetXY($qr['Set_X'], $qr['Set_Y'] );

$pdf->Cell(6, 6,'Remitente '.$qr['names']);

$pdf->SetXY($qr['Set2_X'], $qr['Set2_Y']);

$pdf->Cell(6, 6,'Tel.'.$qr['telefono']);

$pdf->SetXY($qr['Set3_X'], $qr['Set3_Y']);

$pdf->Cell(6, 6,'CC.'.$qr['documento']);

$pdf->SetXY($qr['Set16_X'], $qr['Set16_Y']);

$pdf->Cell(6, 6,'_________________________');

$pdf->SetXY($qr['Set17_X'], $qr['Set17_Y']);

$pdf->Cell(6, 6,'C.C');

$pdf->SetXY($qr['Set18_X'], $qr['Set18_Y']);

$pdf->Cell(6, 6,'_________________________');
$pdf->SetXY($qr['Set19_X'], $qr['Set19_Y']);

$pdf->Cell(6, 6,'Firma');

//azul

// url local { 
//$pdf->Cell(15, 6, $pdf->Image('http://172.16.20.153/MailingPeopleM/NAVIGATION/DESIGN/IMG/logo.png',$qr['imagen_3_x'],$qr['imagen_3_y'],23),false);
//$pdf->Cell(15, 6, $pdf->Image('http://172.16.20.153/MailingPeopleM/NAVIGATION/DESIGN/IMG/logo.png',$qr['imagen_4_x'],$qr['imagen_4_y'],23),false);
//$pdf->Cell(15, 6, $pdf->Image('http://172.16.20.153/MailingPeopleM/NAVIGATION/DESIGN/IMG/tiempo.png',$qr['imagen_1_x'],$qr['imagen_1_y'],23),false);
// }
// url sever {
$pdf->Cell(15, 6, $pdf->Image('https://app-peoplemarketing.com/MailingPeopleM/NAVIGATION/DESIGN/IMG/logo.png',$qr['imagen_3_x'],$qr['imagen_3_y'],23),false);
$pdf->Cell(15, 6, $pdf->Image('https://app-peoplemarketing.com/MailingPeopleM/NAVIGATION/DESIGN/IMG/logo.png',$qr['imagen_4_x'],$qr['imagen_4_y'],23),false);
$pdf->Cell(15, 6, $pdf->Image('https://app-peoplemarketing.com/MailingPeopleM/NAVIGATION/DESIGN/IMG/tiempo.png',$qr['imagen_1_x'],$qr['imagen_1_y'],23),false);
// }

$pdf->Rect($qr['celda_2_x'], $qr['celda_2_y'], 25, 70, false );
$pdf->SetXY($qr['Set20_X'], $qr['Set20_Y']);
$pdf->cell(6,6,$pdf->RotatedText(45,45,'Remitente',90), false);
$pdf->SetXY($qr['Set21_X'], $qr['Set21_Y']);
$pdf->cell(6,6,$pdf->RotatedText($qr['rotated_2_x'],$qr['rotated_2_y'],$qr['names'],90), false);
$pdf->SetXY($qr['Set22_X'], $qr['Set22_Y']);
$pdf->cell(6,6,$pdf->RotatedText($qr['rotated_3_x'],$qr['rotated_3_y'],'Destinatario',90), false);
$pdf->SetXY($qr['Set23_X'], $qr['Set23_Y']);
$pdf->cell(6,6,$pdf->RotatedText($qr['rotated_4_x'],$qr['rotated_4_y'],$qr['nombres'].' '.$qr['apellidos'],90), false);
$pdf->SetXY($qr['Set24_X'], $qr['Set24_Y']);
$pdf->cell(6,6,$pdf->RotatedText($qr['rotated_5_x'],$qr['rotated_5_y'],$qr['direccion'],90), false);
$pdf->SetXY($qr['Set25_X'], $qr['Set25_Y']);
$pdf->cell(6,6,$pdf->RotatedText($qr['rotated_6_x'],$qr['rotated_6_y'],'Telefono',90), false);
$pdf->SetXY($qr['Set26_X'], $qr['Set26_Y']);
$pdf->cell(6,6,$pdf->RotatedText($qr['rotated_7_x'],$qr['rotated_7_y'],$qr['telefono'],90), false);

//cafe

$pdf->Rect($qr['celda_3_x'], $qr['celda_3_y'] , 85, 25, false );

$pdf->SetXY($qr['Set4_X'], $qr['Set4_Y']);

$pdf->Cell(6, 6,'FECHA:');

$pdf->SetXY($qr['Set5_X'], $qr['Set5_Y']);

$pdf->Cell(6, 6,'DESTINATARIO');

$pdf->SetXY($qr['Set6_X'], $qr['Set6_Y']);

$pdf->Cell(6, 6,$qr['nombres'].' '.$qr['apellidos']);

$pdf->SetXY($qr['Set7_X'], $qr['Set7_Y']);

$pdf->Cell(6, 6,'Producto:');

$pdf->SetXY($qr['Set8_X'], $qr['Set8_Y']);

$pdf->Cell(6, 6,$qr['tipo_solicitud']);

$pdf->SetXY($qr['Set9_X'], $qr['Set9_Y']);

$pdf->Cell(6, 6,'Barrio:');

$pdf->SetXY($qr['Set10_X'], $qr['Set10_Y']);

$pdf->Cell(6, 6,$qr['barrio_destino']);

$pdf->SetXY($qr['Set11_X'], $qr['Set11_Y']);
$pdf->Cell(6, 6,'Zona');

$pdf->SetXY($qr['Set12_X'], $qr['Set12_Y']);

$pdf->Cell(6, 6,$qr['id_qr_generado']);

$pdf->SetXY($qr['Set13_X'], $qr['Set13_Y']);

$pdf->Cell(6, 6,'Dirección.');

$pdf->SetXY($qr['Set14_X'], $qr['Set14_Y']);

$pdf->Cell(6, 6,$qr['direccion']);

 

//azul otro//

$pdf->Rect($qr['celda_4_x'], $qr['celda_4_y'], 45, 45, false );

$pdf->Cell(15, 6, $pdf->Image($qr['link_qr'],$qr['imagen_2_x'],$qr['imagen_2_y'],30),false);

$pdf->SetXY($qr['Set15_X'], $qr['Set15_Y']);

$pdf->Cell(6, 6,$qr['id_qr_generado']);


if ($qr['id_qr']%2==0){
   $pdf ->Ln();
}else{

}

        }

        

$pdf->OutPut();

 ?>