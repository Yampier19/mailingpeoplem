<?php

 require('rotation_fpdf.php');
 require('../../../CONNECTION/SECURITY/conex.php');
  date_default_timezone_set("America/Bogota");

$fecha = date('Y-m-d');

 class PDF extends PDF_Rotate

 {

 function RotatedText($x,$y,$txt,$angle)
 {
     //Text rotated around its origin

     $this->Rotate($angle,$x,$y);
     $this->Text($x,$y,$txt);
     $this->Rotate(0);

 }
 function RotatedImage($file,$x,$y,$w,$h,$angle)
 {
     //Image rotated around its upper-left corner
     $this->Rotate($angle,$x,$y);
     $this->Image($file,$x,$y,$w,$h);
     $this->Rotate(0);
 }
 }

$pdf = new PDF('P','mm','letter');
$pdf ->SetMargins(0, 0 ,0 );
$pdf->SetAutoPageBreak(false);
$pdf ->AddPage('');

//tipo de letra//


$pdf ->SetFont('Arial', '',8);
$fecha_qr = $_GET["fecha_qr"]; 
$buscar = $_GET["buscar"];

if($buscar == 'PMeEx' && $fecha_qr != ''){

 $filtro = "DATE(E.fecha_registro) = '".$fecha_qr."' AND E.estado = '1'"; 
  
}elseif($buscar == 'PMeEx' && $fecha_qr == ''){

    $filtro = "E.estado = '1'";

}elseif($buscar == 'QR'){

    $filtro = "AND A.`id_qr_generado` = '4h80'";
}elseif ($buscar == 'reg' && $fecha_qr != '') {

    $filtro = "AND date(C.fecha_registro) = '".$fecha_qr."'";
}elseif ($buscar == 'AP' && $fecha_qr != '') {
    
    $filtro = "AND C.`fecha_inicio` = '".$fecha_qr."' OR C.`fecha_fin` = '".$fecha_qr."'";
}

if($buscar == 'PMeEx'){
// mensajero externo por día o por que esta asignado y no lo ha recibido
$select_qr_generado=mysqli_query($conex,"SELECT * FROM `qr_generated` AS Q  LEFT JOIN  `shipping` AS S ON Q.`id_qr` = S.`id_generate` LEFT JOIN `external_courier` AS E ON S.`id_shipping` = E.`id_ship` WHERE $filtro");

}elseif ($buscar == 'QR' || $buscar == 'reg' || $buscar == 'AP') {

 // registros o por codigo o por fecha asignada de entrega
$select_qr_generado=mysqli_query($conex,"SELECT * FROM qr_generated AS A LEFT JOIN shipping AS C ON C.id_generate = A.id_qr WHERE C.nombres <> '' $filtro");

}


$num = '0';

while ($qr =(mysqli_fetch_array($select_qr_generado))){
    if ($num==8){
        $pdf->AddPage('PORTRAIT','LETTER');
	         $num = '0';
                     }

$num++;
$sql_posicion = mysqli_query($conex, "SELECT * FROM `coordenadas_x_y` WHERE `id_posicion` = '$num'");

while ($cor =(mysqli_fetch_array($sql_posicion))){


//verde -> celda 3

$pdf->Rect($cor['celda_1_x'], $cor['celda_1_y'], 40, 45, false ); // cuadricula de los datos del remitente 
$pdf ->SetFont('Arial', '', 8);
$pdf->SetXY($cor['Set29_X'], $cor['Set29_Y']); // posicion 1
$pdf->Cell(6, 6,'Observacion:');
$pdf->SetXY($cor['Set29_X'] + 18, $cor['Set29_Y']); /// posicion 1 y sumamos a X
$pdf->Cell(6, 6,substr(utf8_decode(utf8_decode($qr['observacion'])), 0, 15));
$pdf->SetXY($cor['Set29_X'], $num1 = $cor['Set29_Y'] + 3);// toma la posicion inicial en Y + 3
$pdf->Cell(6, 6,substr(utf8_decode($qr['observacion']), 15, 29));
$pdf->SetXY($cor['Set29_X'], $num2 = $num1 + 3);
$pdf->Cell(6, 6,substr(utf8_decode($qr['observacion']), 44, 29));
$pdf->SetXY($cor['Set29_X'], $num3 = $num2 + 3);
$pdf->Cell(6, 6,substr(utf8_decode($qr['observacion']), 73, 29));
$pdf->SetXY($cor['Set29_X'], $num4 = $num3 + 3);
$pdf->Cell(6, 6,substr(utf8_decode($qr['observacion']), 102, 29));
$pdf->SetXY($cor['Set29_X'], $num5 = $num4 + 3);
$pdf->Cell(6, 6,substr(utf8_decode($qr['observacion']), 131, 29));
$pdf->SetXY($cor['Set29_X'], $num6 = $num5 + 3);
$pdf->Cell(6, 6,substr(utf8_decode($qr['observacion']), 160, 29));
$pdf->SetXY($cor['Set_X'], $cor['Set_Y'] );
$pdf->Cell(6, 6,'Remitente:');
$pdf->SetXY($cor['Set2_X'], $cor['Set2_Y']);
$pdf->Cell(6, 6,'Tel.'.$qr['telefono']);
$pdf->SetXY($cor['Set3_X'], $cor['Set3_Y']);// Linea de separacion de observacion y remitente 
$pdf->Cell(6, 6,'_________________________');// linea de separacion y obser... y remi...
$pdf->SetXY($cor['Set16_X'], $cor['Set16_Y']);
$pdf->Cell(6, 6,'_________________________');
$pdf->SetXY($cor['Set17_X'], $cor['Set17_Y']);
$pdf->Cell(6, 6,'C.C');
$pdf->SetXY($cor['Set18_X'], $cor['Set18_Y']);
$pdf->Cell(6, 6,'_________________________');
$pdf->SetXY($cor['Set19_X'], $cor['Set19_Y']);
$pdf->Cell(6, 6,'Firma');

//azul

// url local { 

//$pdf->Cell(15, 6, $pdf->Image('http://172.16.20.182/MailingPeopleM/NAVIGATION/DESIGN/IMG/logo.png',$cor['imagen_3_x'],$cor['imagen_3_y'],23),false);

//$pdf->Cell(15, 6, $pdf->Image('http://172.16.20.182/MailingPeopleM/NAVIGATION/DESIGN/IMG/logo.png',$cor['imagen_4_x'],$cor['imagen_4_y'],23),false);

//$pdf->Cell(15, 6, $pdf->Image('http://172.16.20.182/MailingPeopleM/NAVIGATION/DESIGN/IMG/tiempo.png',$cor['imagen_1_x'],$cor['imagen_1_y'],23),false);

// }

// url sever {

$pdf->Cell(15, 6, $pdf->Image('https://app-peoplemarketing.com/MailingPeopleM/NAVIGATION/DESIGN/IMG/logo.png',$cor['imagen_3_x'],$cor['imagen_3_y'],23),false);// imagen de Mailing del desprendible
$pdf->Cell(15, 6, $pdf->Image('https://app-peoplemarketing.com/MailingPeopleM/NAVIGATION/DESIGN/IMG/logo.png',$cor['imagen_4_x'],$cor['imagen_4_y'],23),false);// imagen de Mailing del QR
$pdf->Cell(15, 6, $pdf->Image('https://app-peoplemarketing.com/MailingPeopleM/NAVIGATION/DESIGN/IMG/tiempo.png',$cor['imagen_1_x'],$cor['imagen_1_y'],23),false);// lOGO DEL TIEMPO

// }

$pdf->Rect($cor['celda_2_x'], $cor['celda_2_y'], 25, 70, false ); // cuadriccla del desprendible
$pdf->SetXY($cor['Set20_X'], $cor['Set20_Y']);
$pdf->cell(6,6,$pdf->RotatedText($cor['rotated_1_x'],$cor['rotated_1_y'],'Remitente',90), false);
$pdf->SetXY($cor['Set21_X'], $cor['Set21_Y']);
$pdf->cell(6,6,$pdf->RotatedText($cor['rotated_2_x'],$cor['rotated_2_y'],$qr['names'],90), false);
$pdf->SetXY($cor['Set22_X'], $cor['Set22_Y']);
$pdf->cell(6,6,$pdf->RotatedText($cor['rotated_3_x'],$cor['rotated_3_y'],'Destinatario',90), false);
$pdf->SetXY($cor['Set23_X'], $cor['Set23_Y']);
$pdf->cell(6,6,$pdf->RotatedText($cor['rotated_4_x'],$cor['rotated_4_y'],substr(utf8_decode($qr['nombres']), 0, 10).' '.substr(utf8_decode($qr['apellidos']), 0, 15),90), false);
$pdf->SetXY($cor['Set24_X'], $cor['Set24_Y']);
$pdf->cell(6,6,$pdf->RotatedText($cor['rotated_5_x'],$cor['rotated_5_y'],substr(utf8_decode($qr['direccion']),0,35),90), false);
$pdf->SetXY($cor['Set25_X'], $cor['Set25_Y']);
$pdf->cell(6,6,$pdf->RotatedText($cor['rotated_6_x'],$cor['rotated_6_y'],'Telefono',90), false);
$pdf->SetXY($cor['Set26_X'], $cor['Set26_Y']);
$pdf->cell(6,6,$pdf->RotatedText($cor['rotated_7_x'],$cor['rotated_7_y'],$qr['telefono'],90), false);

//cafe

//$pdf->Rect($qr['celda_3_x'], $qr['celda_3_y'] , 85, 25, false );

$pdf->SetXY($cor['Set4_X'], $cor['Set4_Y']);
$pdf->Cell(6, 6,'FECHA: '.$fecha);
$pdf->SetXY($cor['Set5_X'], $cor['Set5_Y']);
$pdf->Cell(6, 6,'DESTINATARIO:');
$pdf->SetXY($cor['Set6_X'], $cor['Set6_Y']);
$pdf->Cell(6, 6,substr($qr['nombres'], 0, 12).' '.substr($qr['apellidos'], 0, 12));
$pdf->SetXY($cor['Set7_X'], $cor['Set7_Y']);
$pdf->Cell(6, 6,'Producto:');
$pdf->SetXY($cor['Set8_X'], $cor['Set8_Y']);
$pdf->Cell(6, 6,$qr['tipo_solicitud']);
$pdf->SetXY($cor['Set9_X'], $cor['Set9_Y']);
$pdf->Cell(6, 6,'Barrio:');
$pdf->SetXY($cor['Set10_X'], $cor['Set10_Y']);
$pdf->Cell(6, 6,substr($qr['barrio_destino'], 0, 16));
$pdf->SetXY($cor['Set11_X'], $cor['Set11_Y']);
$pdf->Cell(6, 6,'Zona:');
$pdf->SetXY($cor['Set12_X'], $cor['Set12_Y']);
$pdf->Cell(6, 6,$qr['zona']);
$pdf->SetXY($cor['Set13_X'], $cor['Set13_Y']);
$pdf->Cell(6, 6,utf8_decode('Dir.:'));
$pdf->SetXY($cor['Set14_X'], $cor['Set14_Y']);
$pdf->Cell(6, 6,substr(utf8_decode($qr['direccion']),0,57));

//azul otro//

$pdf->Rect($cor['celda_4_x'], $cor['celda_4_y'], 40, 45, false ); ///CUADRICULA DEL QR
$pdf->Cell(15, 6, $pdf->Image($qr['link_qr'],$cor['imagen_2_x'],$cor['imagen_2_y'],30),false);
$pdf->SetXY($cor['Set15_X'], $cor['Set15_Y']);
$pdf->Cell(6, 6,$qr['id_qr_generado']);

if ($num%2==0){

   $pdf ->Ln();

}else{
}
}
        }
$pdf->OutPut();



 ?>