<?php

$string_intro = getenv("QUERY STRING"); 
parse_str($string_intro);

?>
<?php 

	if (isset($_GET['xnfgti'])) {

        $id_shipping = base64_decode($_GET['xnfgti']);

    }

?>
<style>

body {
    margin: 0;
    font-family: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: black  !important;
    text-align: left;
    background-color: #f1f1f1 !important;
}

.card-header{

}

.card{
    margin:10%;
}
</style>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ventana Emergente</title>
    <link rel="stylesheet" href="../../../DESIGN/CSS/principal_css/adminlte.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript">

    function ajax_pendiente() { // ajax_formulario_solicitud 

        var hid_shipping = $('#hid_shipping').val();
        var sel_ubicacion = $('#sel_ubicacion').val();

        $.ajax({
            url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_update_pendiente.php',

            data:
            {
                hid_shipping: hid_shipping,
                sel_ubicacion: sel_ubicacion
            },
            type: 'post',
            beforesend: function () 
            {

            },
            success: function (data)
            {

                $('#return_ajax_pendiente').html(data);
                
            }
        });
    }

    $(document).ready(function () {

        $('#btn_actualizar').click(function ()
        {

            ajax_pendiente();

        });
    });

    </script>    
</head>
<body>
<div class="card">
  <h5 class="card-header" style="background-color:#ffc107;"><center>Pendiente</center></h5>
  <div class="card-body">
    <h5 class="card-title">Correpondencia</h5>
    <p class="card-text">Actualizar la ubicacion de la correspondecia pendiente.</p><br>
    <input type="hidden" id="hid_shipping" value="<?php echo $id_shipping ?>">
    <select class="form-control" name="sel_ubicacion" id="sel_ubicacion">
        <option value="">Selecionar...</option>
        <option value="Bandeja">Bandeja de correspondencia</option>
        <option value="Pool">Pool de correspondencia</option>
    </select><br>
    <div class="col-sm-6">
    <center><input type="button" class="btn btn-warning btn-block" value="Actualizar" id="btn_actualizar"></center>
    </div>
    <div class="col-sm-6">
    </div>
    <br>
    <div class="col-sm-12">
    <div id="return_ajax_pendiente"></div>
    </div>
  </div>
</div>
</body>
</html>