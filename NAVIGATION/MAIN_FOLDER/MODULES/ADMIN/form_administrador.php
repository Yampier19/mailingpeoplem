<?php

header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');

if ($user_name != '' && $id_user != '') {
    $consul_user = mysqli_query($conex, 'SELECT * FROM `userlogin` AS A LEFT JOIN user AS B ON A.id_user = B.id_user  WHERE A.`id_user` = ' . base64_decode($id_user) . '');
    while ($consul = (mysqli_fetch_array($consul_user))) {
        $nombre = $consul['names'];
        $apellido = $consul['surnames'];
        $id_userlog = $consul['id_loginrol'];
    }
    if ($id_userlog == base64_decode($id_loginrol)) {
        //echo 'Bueno';
    }
    $boton = 1;
    include('../../DROPDOWN/menu_admin.php');
?>
    <script>
        $(document).ready(function() {
            function SolicitEnFu() {
                var estado = "1"; //solo usa para activar el ajax
                $.ajax({
                    url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_correspondencia_admin.php',
                    data: {
                        estado: estado
                    },
                    type: 'post',
                    beforesend: function() {},
                    success: function(data) {
                        $('#SolicitEn').html(data);
                    }
                });
            }

            function VerTablaCorresp() {
                var estado = "2"; //solo usa para activar el ajax
                $.ajax({
                    url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_correspondencia_admin.php',
                    data: {
                        estado: estado
                    },
                    type: 'post',
                    beforesend: function() {},
                    success: function(data) {
                        $('#AsignarCorres').html(data);
                    }
                });
            }

            function MensajeroInternoFu() {
                var estado = "3"; //solo usa para activar el ajax
                $.ajax({
                    url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_correspondencia_admin.php',
                    data: {
                        estado: estado
                    },
                    type: 'post',
                    beforesend: function() {},
                    success: function(data) {
                        $('#MensajeroInterno').html(data);
                    }
                });
            }


            function PoolFu() {
                var estado = "4"; //solo usa para activar el ajax
                var TipoMensajero = $('#TipoMensajero').val();
                $.ajax({
                    url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_correspondencia_admin.php',
                    data: {
                        estado: estado,
                        TipoMensajero: TipoMensajero
                    },

                    type: 'post',
                    beforesend: function() {},
                    success: function(data) {
                        $('#BanerListaMensaje').html(data);
                    }
                });
            }


            function FiltrarPoolFu() {
                var estado = "4.1"; //solo usa para activar el ajax
                var ListaMensajerosI = $('#ListaMensajerosI').val();
                var TipoMensajero = $('#TipoMensajero').val();
                var QueDeseaHacer = $('#QueDeseaHacer').val();
                $.ajax({
                    url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_correspondencia_admin.php',
                    data: {
                        estado: estado,
                        ListaMensajerosI: ListaMensajerosI,
                        TipoMensajero: TipoMensajero,
                        QueDeseaHacer: QueDeseaHacer
                    },
                    type: 'post',
                    beforesend: function() {},
                    success: function(data) {

                        $('#TablaPool').html(data);
                    }
                });
            }

            function MensajeroExternoFu() {
                var estado = "5"; //solo usa para activar el ajax
                $.ajax({
                    url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_correspondencia_admin.php',
                    data: {
                        estado: estado
                    },
                    type: 'post',
                    beforesend: function() {},
                    success: function(data) {
                        $('#MensajeroExterno').html(data);
                    }
                });
            }
			
			 function EntregadoFu() {
                var estado = "6"; //solo usa para activar el ajax
                $.ajax({
                    url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_correspondencia_admin.php',
                    data: {
                        estado: estado
                    },
                    type: 'post',
                    beforesend: function() {},
                    success: function(data) {
                        $('#BEntregado').html(data);
                    }
                });
            }
			
			function DevueltoFu() {
                var estado = "7"; //solo usa para activar el ajax
                $.ajax({
                    url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_correspondencia_admin.php',
                    data: {
                        estado: estado
                    },
                    type: 'post',
                    beforesend: function() {},
                    success: function(data) {
                        $('#BDevuelto').html(data);
                    }
                });
            }
			
			
			function FueradeTiempoFu() {
                var estado = "8"; //solo usa para activar el ajax
                $.ajax({
                    url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_correspondencia_admin.php',
                    data: {
                        estado: estado
                    },
                    type: 'post',
                    beforesend: function() {},
                    success: function(data) {
                        $('#BFueradeTiempo').html(data);
                    }
                });
            }
			
			
            $("#Bandeja_Correspondencia-tab").click(function() {
                VerTablaCorresp();
            });
            $("#Solicitud-tab").click(function() {
                SolicitEnFu();
            });
            $("#Mensajero-Interno").click(function() {
                MensajeroInternoFu();
            });
            $("#Mensajero-Externo").click(function() {
                MensajeroExternoFu();
            });
			$("#Entregado").click(function() {
                EntregadoFu();
            });
			$("#Devueltos").click(function() {
                DevueltoFu();
            });
			$("#FueradeTiempo").click(function() {
                FueradeTiempoFu();
            });
            $("#FiltarPool").click(function() {
                FiltrarPoolFu();
            });
            $("#TipoMensajero").change(function() {
                PoolFu();
                $("#BanerList").css('display', 'block');
            });
            SolicitEnFu();
        });
    </script>
    <script>
        $(document).ready(function poolselect() {
            $('#QueDeseaHacer').change(function() {
                var QueDeseaHacer = $('#QueDeseaHacer').val();
                if (QueDeseaHacer == "recibir") {
                    $("#BanerTipoMensajero").css('display', 'block');
                } else if (QueDeseaHacer == "asignar") {
                    $("#BanerTipoMensajero").css('display', 'block');
                } else {
                    $("#BanerTipoMensajero").css('display', 'none');
                    $("#BanerList").css('display', 'none');
                }
            });
        });
    </script>

    <!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Correspondencia</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <!--<li class="breadcrumb-item"><a href="#">Inicio</a></li>-->
                            <li class="breadcrumb-item active">Inicio / Correspondencia</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3><span id="consul_form_solicitud">0</span></h3>
                               <!----------------------------Solicitudes--------------------------->
                                <p>Solicitudes</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-mail-bulk"></i>
                            </div>
                            <a href="#Solicitud-tab" class="small-box-footer" >M&aacute;s Info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg" style="background-color:#d81b60; color:#FFFFFF;">
                            <div class="inner">
                                <h3><span id="consul_form_pun_corres">0</span></h3>
                               <!----------------------------Punto de correspondecia--------------------------->
                                <p>Bandeja Corrrespondencia</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-inbox"></i>
                            </div>
                            <a href="#" class="small-box-footer" style="color:#FFFFFF;">M&aacute;s Info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg" style="background-color:#39cccc; color:#FFFFFF;">
                            <div class="inner">
                                <h3><span id="consul_form_mens_int">0</span>
                                    <!--<sup style="font-size: 20px">%</sup>-->
                                </h3>
                               <!-----------------Mensajero Interno--------------------->
                                <p>Mensajero Interno</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-dolly"></i>
                            </div>
                            <a href="#" class="small-box-footer" style="color:#FFFFFF;">M&aacute;s Info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg" style="background-color:#3d9970; color:#ffffff;">
                            <div class="inner">
                                <h3><span id="consul_form_despacho">0</span></h3>
                                <!----------------------------Despacho--------------------------->
                                <p>Pool Correspondencia</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-boxes"></i>
                            </div>
                            <a href="#" class="small-box-footer" style="color:#ffffff;">M&aacute;s Info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->
                <!-- Main row -->
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3><span id="consul_form_mens_exter">0</span></h3>
                               <!----------------------------Mensajero Externo--------------------------->
                                <p>Mensajero externo</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-shipping-fast"></i>
                            </div>
                            <a href="#" class="small-box-footer">M&aacute;s Info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                            <div class="inner">
                                <h3><span id="consul_form_entregado">0</span>
                                   <!---<sup style="font-size: 20px">%</sup>----->
                               </h3>
                               <!---------------Entregado------------------------>
                                <p>Entregado</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-vote-yea"></i>
                            </div>
                            <a href="#" class="small-box-footer">M&aacute;s Info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg" style="background-color:#ff851b; color:#000000;">
                            <div class="inner">
                                <h3><span id="consult_form_devuelto">0</span></h3>
                                <!---------------------Devuelto--------------------------->
                                <p>Devueltos</p>
                            </div>
                            <div class="icon">
                                <i class="fas fa-sync-alt"></i>
                            </div>
                            <a href="#" class="small-box-footer" style="color:#000000;">M&aacute;s Info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3><span id="consult_form_fuera_tiempo">0</span></h3>
                               <!----------------------------Fuera de Tiempo--------------------------->
                                <p>Fuera de tiempo</p>
                            </div>
                            <div class="icon">
                                <i class="fab fa-algolia"></i>
                            </div>
                            <a href="#" class="small-box-footer">M&aacute;s Info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->

        <section class="contact">
            <div class="container-fluid">
                <div class="card card-primary card-outline">
                    <div class="card-body" id="verpg">
                        <h4>Correspondence</h4>
                        <ul class="nav nav-tabs" id="custom-content-above-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="Solicitud-tab" data-toggle="pill" href="#Solicitud" role="tab" aria-controls="Solicitud" aria-selected="true" style="color:#af8403;" onclick="SolicitEnFu()">Solicitudes</a>
                            </li>
                            <li class="nav-item">
               <a class="nav-link" id="Bandeja_Correspondencia-tab" data-toggle="pill" href="#Bandeja_Correspondencia" role="tab" aria-controls="Bandeja_Correspondencia" aria-selected="false" style="color:#c21856;" onclick="VerTablaCorresp()">Bandeja de Correspondencia</a>
                           </li>
                           <li class="nav-item">
                                <a class="nav-link" id="Mensajero-Interno" data-toggle="pill" href="#MensajeroInt3" role="tab" aria-controls="MensajeroInt3" aria-selected="false" style="color:#0fa0a0;" onclick="MensajeroInternoFu()">Mensajero Interno</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="Pool_correspondencia" data-toggle="pill" href="#pool4" role="tab" aria-controls="pool4" aria-selected="false" style="color:#378965;">Pool de Correspondencia</a>
                            </li>
                            <!---------------------------------///////////////////////////////-------------------------->
                            <li class="nav-item">
                                <a class="nav-link" id="Mensajero-Externo" data-toggle="pill" href="#home5" role="tab" aria-controls="home5" aria-selected="false" style="color:#107484;" onclick="MensajeroExternoFu()">Mensajero Externo</a>
                            </li>
                            <li class="nav-item">
                          <a class="nav-link" id="Entregado" data-toggle="pill" href="#profile6" role="tab" aria-controls="profile6" aria-selected="false" style="color:#23963d;">Entregado</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="Devueltos" data-toggle="pill" href="#messages7" role="tab" aria-controls="messages7" aria-selected="false" style="color:#bd6112;">Devueltos</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="FueradeTiempo" data-toggle="pill" href="#settings8" role="tab" aria-controls="settings8" aria-selected="false" style="color:#c6303e;">Fuera de Tiempo</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="custom-content-above-tabContent">
                            <div class="tab-pane fade show active" id="Solicitud" role="tabpanel" aria-labelledby="Solicitud-tab">
                                <div class="col-12">
                                    <div class="card">
                                       <div class="card-body">
                                            <div id="SolicitEn"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-------------/////////////////////////////////////////////////////////-------------------------->
                            <div class="tab-pane fade" id="Bandeja_Correspondencia" role="tabpanel" aria-labelledby="Bandeja_Correspondencia-tab">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <!-- <div class="card-header">
              <h3 class="card-title">DataTable with minimal features & hover style</h3>
            </div>
             /.card-header -->
                                            <div class="card-body">
                                                <div id="AsignarCorres"> </div>
                                           </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                        <!-- /.card -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </div>
                            <!--------------------------///////////////////////////////////------------------>
                            <div class="tab-pane fade" id="MensajeroInt3" role="tabpanel" aria-labelledby="Mensajero-Interno">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div id="MensajeroInterno"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pool4" role="tabpanel" aria-labelledby="pool4">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Qu&eacute; desea hacer</label>
                                                    <select class="form-control" id="QueDeseaHacer" name="QueDeseaHacer">
                                                        <option value="" selected="" disabled="">Seleccione...</option>
                                                        <option value="recibir">Recibir</option>
                                                        <option value="asignar">Asignar</option>
                                                        <option value="total">Total</option>
                                                   </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3" id="BanerTipoMensajero" style="display:none;">
                                               <div class="form-group">
                                                    <label>Tipo de Mensajero</label>
                                                    <select class="form-control" id="TipoMensajero" name="TipoMensajero">
                                                        <option value="" selected="">Seleccione...</option>
                                                        <option value="interno">Interno</option>
                                                        <option value="externo">Externo</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="display:none;" id="BanerList">
                                                <div id="BanerListaMensaje">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>
                                                    <button type="button" class="btn btn-block btn-success" id="FiltarPool" name="FiltarPool" onclick="FiltrarPoolFu()"><i class="fas fa-filter"></i> Filtrar</button>
                                                </div>
                                            </div>
                                        </div>
                                       <div id="TablaPool"></div>
                                   </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show " id="home5" role="tabpanel" aria-labelledby="home5">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div id='MensajeroExterno'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="profile6" role="tabpanel" aria-labelledby="profile6">
                             <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div id='BEntregado'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="messages7" role="tabpanel" aria-labelledby="messages7">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div id='BDevuelto'></div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>

                            <div class="tab-pane fade" id="settings8" role="tabpanel" aria-labelledby="settings8">
                                 <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div id='BFueradeTiempo'></div>
                                        </div>
                                    </div>
                                </div>                        

                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
           </div>
        </section>
    </div>
    <!-- /.content-wrapper -->
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    <!-- Main Footer -->
    <?php require('../../FOOTER/index.php'); ?>
    </div>
    <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <!-- Bootstrap -->
    <script src="../../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- AdminLTE -->

    <script src="../../../DESIGN/JS/principal_js/adminlte.js"></script>

    <script>
        $(function() {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="../../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>
    <script src="../../../DESIGN/JS/principal_js/demo.js"></script>
    <script src="../../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>
    </body>
    </html>
<?php
} else {
    echo 'No tiene permisos para ingresar a la informaci&oacute;n';
}
?>