<?php

class grid_shipping_and_delivery_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Texto_tag;
   var $Arquivo;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function grid_shipping_and_delivery_rtf()
   {
      $this->nm_data   = new nm_data("es");
      $this->Texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo    = "sc_rtf";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_grid_shipping_and_delivery";
      $this->Arquivo   .= ".rtf";
      $this->Tit_doc    = "grid_shipping_and_delivery.rtf";
   }

   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_shipping_and_delivery']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_shipping_and_delivery']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_shipping_and_delivery']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->s_nombres = $Busca_temp['s_nombres']; 
          $tmp_pos = strpos($this->s_nombres, "##@@");
          if ($tmp_pos !== false)
          {
              $this->s_nombres = substr($this->s_nombres, 0, $tmp_pos);
          }
          $this->s_apellidos = $Busca_temp['s_apellidos']; 
          $tmp_pos = strpos($this->s_apellidos, "##@@");
          if ($tmp_pos !== false)
          {
              $this->s_apellidos = substr($this->s_apellidos, 0, $tmp_pos);
          }
          $this->s_tipo_documento = $Busca_temp['s_tipo_documento']; 
          $tmp_pos = strpos($this->s_tipo_documento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->s_tipo_documento = substr($this->s_tipo_documento, 0, $tmp_pos);
          }
          $this->s_documento = $Busca_temp['s_documento']; 
          $tmp_pos = strpos($this->s_documento, "##@@");
          if ($tmp_pos !== false)
          {
              $this->s_documento = substr($this->s_documento, 0, $tmp_pos);
          }
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['rtf_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['rtf_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['rtf_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT s.nombres as s_nombres, s.apellidos as s_apellidos, s.tipo_documento as s_tipo_documento, s.documento as s_documento, s.telefono as s_telefono, s.email as s_email, s.direccion as s_direccion, s.prioridad as s_prioridad, s.ciudad_origen as s_ciudad_origen, s.ciudad_destino as s_ciudad_destino, s.barrio_destino as s_barrio_destino, s.zona as s_zona, s.valor_declarado as s_valor_declarado, s.centro_costos as s_centro_costos, s.tipo_solicitud as s_tipo_solicitud, s.ubicacion as s_ubicacion, str_replace (convert(char(10),s.fecha_inicio,102), '.', '-') + ' ' + convert(char(8),s.fecha_inicio,20) as s_fecha_inicio, str_replace (convert(char(10),s.fecha_fin,102), '.', '-') + ' ' + convert(char(8),s.fecha_fin,20) as s_fecha_fin, str_replace (convert(char(10),s.fecha_registro,102), '.', '-') + ' ' + convert(char(8),s.fecha_registro,20) as s_fecha_registro, s.estado_proceso as s_estado_proceso, s.estado as s_estado, str_replace (convert(char(10),d.fecha_registro,102), '.', '-') + ' ' + convert(char(8),d.fecha_registro,20) as d_fecha_registro, d.novedad as d_novedad, d.estado as d_estado from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT s.nombres as s_nombres, s.apellidos as s_apellidos, s.tipo_documento as s_tipo_documento, s.documento as s_documento, s.telefono as s_telefono, s.email as s_email, s.direccion as s_direccion, s.prioridad as s_prioridad, s.ciudad_origen as s_ciudad_origen, s.ciudad_destino as s_ciudad_destino, s.barrio_destino as s_barrio_destino, s.zona as s_zona, s.valor_declarado as s_valor_declarado, s.centro_costos as s_centro_costos, s.tipo_solicitud as s_tipo_solicitud, s.ubicacion as s_ubicacion, s.fecha_inicio as s_fecha_inicio, s.fecha_fin as s_fecha_fin, s.fecha_registro as s_fecha_registro, s.estado_proceso as s_estado_proceso, s.estado as s_estado, d.fecha_registro as d_fecha_registro, d.novedad as d_novedad, d.estado as d_estado from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT s.nombres as s_nombres, s.apellidos as s_apellidos, s.tipo_documento as s_tipo_documento, s.documento as s_documento, s.telefono as s_telefono, s.email as s_email, s.direccion as s_direccion, s.prioridad as s_prioridad, s.ciudad_origen as s_ciudad_origen, s.ciudad_destino as s_ciudad_destino, s.barrio_destino as s_barrio_destino, s.zona as s_zona, s.valor_declarado as s_valor_declarado, s.centro_costos as s_centro_costos, s.tipo_solicitud as s_tipo_solicitud, s.ubicacion as s_ubicacion, convert(char(23),s.fecha_inicio,121) as s_fecha_inicio, convert(char(23),s.fecha_fin,121) as s_fecha_fin, convert(char(23),s.fecha_registro,121) as s_fecha_registro, s.estado_proceso as s_estado_proceso, s.estado as s_estado, convert(char(23),d.fecha_registro,121) as d_fecha_registro, d.novedad as d_novedad, d.estado as d_estado from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT s.nombres as s_nombres, s.apellidos as s_apellidos, s.tipo_documento as s_tipo_documento, s.documento as s_documento, s.telefono as s_telefono, s.email as s_email, s.direccion as s_direccion, s.prioridad as s_prioridad, s.ciudad_origen as s_ciudad_origen, s.ciudad_destino as s_ciudad_destino, s.barrio_destino as s_barrio_destino, s.zona as s_zona, s.valor_declarado as s_valor_declarado, s.centro_costos as s_centro_costos, s.tipo_solicitud as s_tipo_solicitud, s.ubicacion as s_ubicacion, s.fecha_inicio as s_fecha_inicio, s.fecha_fin as s_fecha_fin, s.fecha_registro as s_fecha_registro, s.estado_proceso as s_estado_proceso, s.estado as s_estado, d.fecha_registro as d_fecha_registro, d.novedad as d_novedad, d.estado as d_estado from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT s.nombres as s_nombres, s.apellidos as s_apellidos, s.tipo_documento as s_tipo_documento, s.documento as s_documento, s.telefono as s_telefono, s.email as s_email, s.direccion as s_direccion, s.prioridad as s_prioridad, s.ciudad_origen as s_ciudad_origen, s.ciudad_destino as s_ciudad_destino, s.barrio_destino as s_barrio_destino, s.zona as s_zona, s.valor_declarado as s_valor_declarado, s.centro_costos as s_centro_costos, s.tipo_solicitud as s_tipo_solicitud, s.ubicacion as s_ubicacion, EXTEND(s.fecha_inicio, YEAR TO DAY) as s_fecha_inicio, EXTEND(s.fecha_fin, YEAR TO DAY) as s_fecha_fin, EXTEND(s.fecha_registro, YEAR TO FRACTION) as s_fecha_registro, s.estado_proceso as s_estado_proceso, s.estado as s_estado, EXTEND(d.fecha_registro, YEAR TO FRACTION) as d_fecha_registro, d.novedad as d_novedad, d.estado as d_estado from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT s.nombres as s_nombres, s.apellidos as s_apellidos, s.tipo_documento as s_tipo_documento, s.documento as s_documento, s.telefono as s_telefono, s.email as s_email, s.direccion as s_direccion, s.prioridad as s_prioridad, s.ciudad_origen as s_ciudad_origen, s.ciudad_destino as s_ciudad_destino, s.barrio_destino as s_barrio_destino, s.zona as s_zona, s.valor_declarado as s_valor_declarado, s.centro_costos as s_centro_costos, s.tipo_solicitud as s_tipo_solicitud, s.ubicacion as s_ubicacion, s.fecha_inicio as s_fecha_inicio, s.fecha_fin as s_fecha_fin, s.fecha_registro as s_fecha_registro, s.estado_proceso as s_estado_proceso, s.estado as s_estado, d.fecha_registro as d_fecha_registro, d.novedad as d_novedad, d.estado as d_estado from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $this->Texto_tag .= "<table>\r\n";
      $this->Texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['s_nombres'])) ? $this->New_label['s_nombres'] : "Nombres"; 
          if ($Cada_col == "s_nombres" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_apellidos'])) ? $this->New_label['s_apellidos'] : "Apellidos"; 
          if ($Cada_col == "s_apellidos" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_tipo_documento'])) ? $this->New_label['s_tipo_documento'] : "Tipo Documento"; 
          if ($Cada_col == "s_tipo_documento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_documento'])) ? $this->New_label['s_documento'] : "Documento"; 
          if ($Cada_col == "s_documento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_telefono'])) ? $this->New_label['s_telefono'] : "Telefono"; 
          if ($Cada_col == "s_telefono" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_email'])) ? $this->New_label['s_email'] : "Email"; 
          if ($Cada_col == "s_email" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_direccion'])) ? $this->New_label['s_direccion'] : "Direccion"; 
          if ($Cada_col == "s_direccion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_prioridad'])) ? $this->New_label['s_prioridad'] : "Prioridad"; 
          if ($Cada_col == "s_prioridad" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_ciudad_origen'])) ? $this->New_label['s_ciudad_origen'] : "Ciudad Origen"; 
          if ($Cada_col == "s_ciudad_origen" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_ciudad_destino'])) ? $this->New_label['s_ciudad_destino'] : "Ciudad Destino"; 
          if ($Cada_col == "s_ciudad_destino" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_barrio_destino'])) ? $this->New_label['s_barrio_destino'] : "Barrio Destino"; 
          if ($Cada_col == "s_barrio_destino" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_zona'])) ? $this->New_label['s_zona'] : "Zona"; 
          if ($Cada_col == "s_zona" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_valor_declarado'])) ? $this->New_label['s_valor_declarado'] : "Valor Declarado"; 
          if ($Cada_col == "s_valor_declarado" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_centro_costos'])) ? $this->New_label['s_centro_costos'] : "Centro Costos"; 
          if ($Cada_col == "s_centro_costos" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_tipo_solicitud'])) ? $this->New_label['s_tipo_solicitud'] : "Tipo Solicitud"; 
          if ($Cada_col == "s_tipo_solicitud" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_ubicacion'])) ? $this->New_label['s_ubicacion'] : "Ubicacion"; 
          if ($Cada_col == "s_ubicacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_fecha_inicio'])) ? $this->New_label['s_fecha_inicio'] : "Fecha Inicio"; 
          if ($Cada_col == "s_fecha_inicio" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_fecha_fin'])) ? $this->New_label['s_fecha_fin'] : "Fecha Fin"; 
          if ($Cada_col == "s_fecha_fin" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_fecha_registro'])) ? $this->New_label['s_fecha_registro'] : "Fecha Registro"; 
          if ($Cada_col == "s_fecha_registro" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_estado_proceso'])) ? $this->New_label['s_estado_proceso'] : "Estado Proceso"; 
          if ($Cada_col == "s_estado_proceso" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['s_estado'])) ? $this->New_label['s_estado'] : "Estado"; 
          if ($Cada_col == "s_estado" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['d_fecha_registro'])) ? $this->New_label['d_fecha_registro'] : "Fecha Registro"; 
          if ($Cada_col == "d_fecha_registro" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['d_novedad'])) ? $this->New_label['d_novedad'] : "Novedad"; 
          if ($Cada_col == "d_novedad" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['d_estado'])) ? $this->New_label['d_estado'] : "Estado"; 
          if ($Cada_col == "d_estado" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->Texto_tag .= "</tr>\r\n";
      while (!$rs->EOF)
      {
         $this->Texto_tag .= "<tr>\r\n";
         $this->s_nombres = $rs->fields[0] ;  
         $this->s_apellidos = $rs->fields[1] ;  
         $this->s_tipo_documento = $rs->fields[2] ;  
         $this->s_documento = $rs->fields[3] ;  
         $this->s_telefono = $rs->fields[4] ;  
         $this->s_email = $rs->fields[5] ;  
         $this->s_direccion = $rs->fields[6] ;  
         $this->s_prioridad = $rs->fields[7] ;  
         $this->s_ciudad_origen = $rs->fields[8] ;  
         $this->s_ciudad_destino = $rs->fields[9] ;  
         $this->s_barrio_destino = $rs->fields[10] ;  
         $this->s_zona = $rs->fields[11] ;  
         $this->s_valor_declarado = $rs->fields[12] ;  
         $this->s_centro_costos = $rs->fields[13] ;  
         $this->s_tipo_solicitud = $rs->fields[14] ;  
         $this->s_ubicacion = $rs->fields[15] ;  
         $this->s_fecha_inicio = $rs->fields[16] ;  
         $this->s_fecha_fin = $rs->fields[17] ;  
         $this->s_fecha_registro = $rs->fields[18] ;  
         $this->s_estado_proceso = $rs->fields[19] ;  
         $this->s_estado = $rs->fields[20] ;  
         $this->s_estado = (string)$this->s_estado;
         $this->d_fecha_registro = $rs->fields[21] ;  
         $this->d_novedad = $rs->fields[22] ;  
         $this->d_estado = $rs->fields[23] ;  
         $this->d_estado = (string)$this->d_estado;
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->Texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->Texto_tag .= "</table>\r\n";

      $rs->Close();
   }
   //----- s_nombres
   function NM_export_s_nombres()
   {
         $this->s_nombres = html_entity_decode($this->s_nombres, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_nombres = strip_tags($this->s_nombres);
         if (!NM_is_utf8($this->s_nombres))
         {
             $this->s_nombres = sc_convert_encoding($this->s_nombres, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_nombres = str_replace('<', '&lt;', $this->s_nombres);
         $this->s_nombres = str_replace('>', '&gt;', $this->s_nombres);
         $this->Texto_tag .= "<td>" . $this->s_nombres . "</td>\r\n";
   }
   //----- s_apellidos
   function NM_export_s_apellidos()
   {
         $this->s_apellidos = html_entity_decode($this->s_apellidos, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_apellidos = strip_tags($this->s_apellidos);
         if (!NM_is_utf8($this->s_apellidos))
         {
             $this->s_apellidos = sc_convert_encoding($this->s_apellidos, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_apellidos = str_replace('<', '&lt;', $this->s_apellidos);
         $this->s_apellidos = str_replace('>', '&gt;', $this->s_apellidos);
         $this->Texto_tag .= "<td>" . $this->s_apellidos . "</td>\r\n";
   }
   //----- s_tipo_documento
   function NM_export_s_tipo_documento()
   {
         $this->s_tipo_documento = html_entity_decode($this->s_tipo_documento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_tipo_documento = strip_tags($this->s_tipo_documento);
         if (!NM_is_utf8($this->s_tipo_documento))
         {
             $this->s_tipo_documento = sc_convert_encoding($this->s_tipo_documento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_tipo_documento = str_replace('<', '&lt;', $this->s_tipo_documento);
         $this->s_tipo_documento = str_replace('>', '&gt;', $this->s_tipo_documento);
         $this->Texto_tag .= "<td>" . $this->s_tipo_documento . "</td>\r\n";
   }
   //----- s_documento
   function NM_export_s_documento()
   {
         $this->s_documento = html_entity_decode($this->s_documento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_documento = strip_tags($this->s_documento);
         if (!NM_is_utf8($this->s_documento))
         {
             $this->s_documento = sc_convert_encoding($this->s_documento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_documento = str_replace('<', '&lt;', $this->s_documento);
         $this->s_documento = str_replace('>', '&gt;', $this->s_documento);
         $this->Texto_tag .= "<td>" . $this->s_documento . "</td>\r\n";
   }
   //----- s_telefono
   function NM_export_s_telefono()
   {
         $this->s_telefono = html_entity_decode($this->s_telefono, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_telefono = strip_tags($this->s_telefono);
         if (!NM_is_utf8($this->s_telefono))
         {
             $this->s_telefono = sc_convert_encoding($this->s_telefono, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_telefono = str_replace('<', '&lt;', $this->s_telefono);
         $this->s_telefono = str_replace('>', '&gt;', $this->s_telefono);
         $this->Texto_tag .= "<td>" . $this->s_telefono . "</td>\r\n";
   }
   //----- s_email
   function NM_export_s_email()
   {
         $this->s_email = html_entity_decode($this->s_email, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_email = strip_tags($this->s_email);
         if (!NM_is_utf8($this->s_email))
         {
             $this->s_email = sc_convert_encoding($this->s_email, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_email = str_replace('<', '&lt;', $this->s_email);
         $this->s_email = str_replace('>', '&gt;', $this->s_email);
         $this->Texto_tag .= "<td>" . $this->s_email . "</td>\r\n";
   }
   //----- s_direccion
   function NM_export_s_direccion()
   {
         $this->s_direccion = html_entity_decode($this->s_direccion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_direccion = strip_tags($this->s_direccion);
         if (!NM_is_utf8($this->s_direccion))
         {
             $this->s_direccion = sc_convert_encoding($this->s_direccion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_direccion = str_replace('<', '&lt;', $this->s_direccion);
         $this->s_direccion = str_replace('>', '&gt;', $this->s_direccion);
         $this->Texto_tag .= "<td>" . $this->s_direccion . "</td>\r\n";
   }
   //----- s_prioridad
   function NM_export_s_prioridad()
   {
         $this->s_prioridad = html_entity_decode($this->s_prioridad, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_prioridad = strip_tags($this->s_prioridad);
         if (!NM_is_utf8($this->s_prioridad))
         {
             $this->s_prioridad = sc_convert_encoding($this->s_prioridad, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_prioridad = str_replace('<', '&lt;', $this->s_prioridad);
         $this->s_prioridad = str_replace('>', '&gt;', $this->s_prioridad);
         $this->Texto_tag .= "<td>" . $this->s_prioridad . "</td>\r\n";
   }
   //----- s_ciudad_origen
   function NM_export_s_ciudad_origen()
   {
         $this->s_ciudad_origen = html_entity_decode($this->s_ciudad_origen, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_ciudad_origen = strip_tags($this->s_ciudad_origen);
         if (!NM_is_utf8($this->s_ciudad_origen))
         {
             $this->s_ciudad_origen = sc_convert_encoding($this->s_ciudad_origen, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_ciudad_origen = str_replace('<', '&lt;', $this->s_ciudad_origen);
         $this->s_ciudad_origen = str_replace('>', '&gt;', $this->s_ciudad_origen);
         $this->Texto_tag .= "<td>" . $this->s_ciudad_origen . "</td>\r\n";
   }
   //----- s_ciudad_destino
   function NM_export_s_ciudad_destino()
   {
         $this->s_ciudad_destino = html_entity_decode($this->s_ciudad_destino, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_ciudad_destino = strip_tags($this->s_ciudad_destino);
         if (!NM_is_utf8($this->s_ciudad_destino))
         {
             $this->s_ciudad_destino = sc_convert_encoding($this->s_ciudad_destino, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_ciudad_destino = str_replace('<', '&lt;', $this->s_ciudad_destino);
         $this->s_ciudad_destino = str_replace('>', '&gt;', $this->s_ciudad_destino);
         $this->Texto_tag .= "<td>" . $this->s_ciudad_destino . "</td>\r\n";
   }
   //----- s_barrio_destino
   function NM_export_s_barrio_destino()
   {
         $this->s_barrio_destino = html_entity_decode($this->s_barrio_destino, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_barrio_destino = strip_tags($this->s_barrio_destino);
         if (!NM_is_utf8($this->s_barrio_destino))
         {
             $this->s_barrio_destino = sc_convert_encoding($this->s_barrio_destino, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_barrio_destino = str_replace('<', '&lt;', $this->s_barrio_destino);
         $this->s_barrio_destino = str_replace('>', '&gt;', $this->s_barrio_destino);
         $this->Texto_tag .= "<td>" . $this->s_barrio_destino . "</td>\r\n";
   }
   //----- s_zona
   function NM_export_s_zona()
   {
         $this->s_zona = html_entity_decode($this->s_zona, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_zona = strip_tags($this->s_zona);
         if (!NM_is_utf8($this->s_zona))
         {
             $this->s_zona = sc_convert_encoding($this->s_zona, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_zona = str_replace('<', '&lt;', $this->s_zona);
         $this->s_zona = str_replace('>', '&gt;', $this->s_zona);
         $this->Texto_tag .= "<td>" . $this->s_zona . "</td>\r\n";
   }
   //----- s_valor_declarado
   function NM_export_s_valor_declarado()
   {
         $this->s_valor_declarado = html_entity_decode($this->s_valor_declarado, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_valor_declarado = strip_tags($this->s_valor_declarado);
         if (!NM_is_utf8($this->s_valor_declarado))
         {
             $this->s_valor_declarado = sc_convert_encoding($this->s_valor_declarado, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_valor_declarado = str_replace('<', '&lt;', $this->s_valor_declarado);
         $this->s_valor_declarado = str_replace('>', '&gt;', $this->s_valor_declarado);
         $this->Texto_tag .= "<td>" . $this->s_valor_declarado . "</td>\r\n";
   }
   //----- s_centro_costos
   function NM_export_s_centro_costos()
   {
         $this->s_centro_costos = html_entity_decode($this->s_centro_costos, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_centro_costos = strip_tags($this->s_centro_costos);
         if (!NM_is_utf8($this->s_centro_costos))
         {
             $this->s_centro_costos = sc_convert_encoding($this->s_centro_costos, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_centro_costos = str_replace('<', '&lt;', $this->s_centro_costos);
         $this->s_centro_costos = str_replace('>', '&gt;', $this->s_centro_costos);
         $this->Texto_tag .= "<td>" . $this->s_centro_costos . "</td>\r\n";
   }
   //----- s_tipo_solicitud
   function NM_export_s_tipo_solicitud()
   {
         $this->s_tipo_solicitud = html_entity_decode($this->s_tipo_solicitud, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_tipo_solicitud = strip_tags($this->s_tipo_solicitud);
         if (!NM_is_utf8($this->s_tipo_solicitud))
         {
             $this->s_tipo_solicitud = sc_convert_encoding($this->s_tipo_solicitud, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_tipo_solicitud = str_replace('<', '&lt;', $this->s_tipo_solicitud);
         $this->s_tipo_solicitud = str_replace('>', '&gt;', $this->s_tipo_solicitud);
         $this->Texto_tag .= "<td>" . $this->s_tipo_solicitud . "</td>\r\n";
   }
   //----- s_ubicacion
   function NM_export_s_ubicacion()
   {
         $this->s_ubicacion = html_entity_decode($this->s_ubicacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_ubicacion = strip_tags($this->s_ubicacion);
         if (!NM_is_utf8($this->s_ubicacion))
         {
             $this->s_ubicacion = sc_convert_encoding($this->s_ubicacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_ubicacion = str_replace('<', '&lt;', $this->s_ubicacion);
         $this->s_ubicacion = str_replace('>', '&gt;', $this->s_ubicacion);
         $this->Texto_tag .= "<td>" . $this->s_ubicacion . "</td>\r\n";
   }
   //----- s_fecha_inicio
   function NM_export_s_fecha_inicio()
   {
         $conteudo_x = $this->s_fecha_inicio;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->s_fecha_inicio, "YYYY-MM-DD");
             $this->s_fecha_inicio = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", "ddmmaaaa"));
         } 
         if (!NM_is_utf8($this->s_fecha_inicio))
         {
             $this->s_fecha_inicio = sc_convert_encoding($this->s_fecha_inicio, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_fecha_inicio = str_replace('<', '&lt;', $this->s_fecha_inicio);
         $this->s_fecha_inicio = str_replace('>', '&gt;', $this->s_fecha_inicio);
         $this->Texto_tag .= "<td>" . $this->s_fecha_inicio . "</td>\r\n";
   }
   //----- s_fecha_fin
   function NM_export_s_fecha_fin()
   {
         $conteudo_x = $this->s_fecha_fin;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->s_fecha_fin, "YYYY-MM-DD");
             $this->s_fecha_fin = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", "ddmmaaaa"));
         } 
         if (!NM_is_utf8($this->s_fecha_fin))
         {
             $this->s_fecha_fin = sc_convert_encoding($this->s_fecha_fin, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_fecha_fin = str_replace('<', '&lt;', $this->s_fecha_fin);
         $this->s_fecha_fin = str_replace('>', '&gt;', $this->s_fecha_fin);
         $this->Texto_tag .= "<td>" . $this->s_fecha_fin . "</td>\r\n";
   }
   //----- s_fecha_registro
   function NM_export_s_fecha_registro()
   {
         if (substr($this->s_fecha_registro, 10, 1) == "-") 
         { 
             $this->s_fecha_registro = substr($this->s_fecha_registro, 0, 10) . " " . substr($this->s_fecha_registro, 11);
         } 
         if (substr($this->s_fecha_registro, 13, 1) == ".") 
         { 
            $this->s_fecha_registro = substr($this->s_fecha_registro, 0, 13) . ":" . substr($this->s_fecha_registro, 14, 2) . ":" . substr($this->s_fecha_registro, 17);
         } 
         $conteudo_x = $this->s_fecha_registro;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD HH:II:SS");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->s_fecha_registro, "YYYY-MM-DD HH:II:SS");
             $this->s_fecha_registro = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DH", "ddmmaaaa;hhiiss"));
         } 
         if (!NM_is_utf8($this->s_fecha_registro))
         {
             $this->s_fecha_registro = sc_convert_encoding($this->s_fecha_registro, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_fecha_registro = str_replace('<', '&lt;', $this->s_fecha_registro);
         $this->s_fecha_registro = str_replace('>', '&gt;', $this->s_fecha_registro);
         $this->Texto_tag .= "<td>" . $this->s_fecha_registro . "</td>\r\n";
   }
   //----- s_estado_proceso
   function NM_export_s_estado_proceso()
   {
         $this->s_estado_proceso = html_entity_decode($this->s_estado_proceso, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->s_estado_proceso = strip_tags($this->s_estado_proceso);
         if (!NM_is_utf8($this->s_estado_proceso))
         {
             $this->s_estado_proceso = sc_convert_encoding($this->s_estado_proceso, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_estado_proceso = str_replace('<', '&lt;', $this->s_estado_proceso);
         $this->s_estado_proceso = str_replace('>', '&gt;', $this->s_estado_proceso);
         $this->Texto_tag .= "<td>" . $this->s_estado_proceso . "</td>\r\n";
   }
   //----- s_estado
   function NM_export_s_estado()
   {
         nmgp_Form_Num_Val($this->s_estado, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->s_estado))
         {
             $this->s_estado = sc_convert_encoding($this->s_estado, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->s_estado = str_replace('<', '&lt;', $this->s_estado);
         $this->s_estado = str_replace('>', '&gt;', $this->s_estado);
         $this->Texto_tag .= "<td>" . $this->s_estado . "</td>\r\n";
   }
   //----- d_fecha_registro
   function NM_export_d_fecha_registro()
   {
         if (substr($this->d_fecha_registro, 10, 1) == "-") 
         { 
             $this->d_fecha_registro = substr($this->d_fecha_registro, 0, 10) . " " . substr($this->d_fecha_registro, 11);
         } 
         if (substr($this->d_fecha_registro, 13, 1) == ".") 
         { 
            $this->d_fecha_registro = substr($this->d_fecha_registro, 0, 13) . ":" . substr($this->d_fecha_registro, 14, 2) . ":" . substr($this->d_fecha_registro, 17);
         } 
         $conteudo_x = $this->d_fecha_registro;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD HH:II:SS");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->d_fecha_registro, "YYYY-MM-DD HH:II:SS");
             $this->d_fecha_registro = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DH", "ddmmaaaa;hhiiss"));
         } 
         if (!NM_is_utf8($this->d_fecha_registro))
         {
             $this->d_fecha_registro = sc_convert_encoding($this->d_fecha_registro, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->d_fecha_registro = str_replace('<', '&lt;', $this->d_fecha_registro);
         $this->d_fecha_registro = str_replace('>', '&gt;', $this->d_fecha_registro);
         $this->Texto_tag .= "<td>" . $this->d_fecha_registro . "</td>\r\n";
   }
   //----- d_novedad
   function NM_export_d_novedad()
   {
         $this->d_novedad = html_entity_decode($this->d_novedad, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->d_novedad = strip_tags($this->d_novedad);
         if (!NM_is_utf8($this->d_novedad))
         {
             $this->d_novedad = sc_convert_encoding($this->d_novedad, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->d_novedad = str_replace('<', '&lt;', $this->d_novedad);
         $this->d_novedad = str_replace('>', '&gt;', $this->d_novedad);
         $this->Texto_tag .= "<td>" . $this->d_novedad . "</td>\r\n";
   }
   //----- d_estado
   function NM_export_d_estado()
   {
         nmgp_Form_Num_Val($this->d_estado, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->d_estado))
         {
             $this->d_estado = sc_convert_encoding($this->d_estado, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->d_estado = str_replace('<', '&lt;', $this->d_estado);
         $this->d_estado = str_replace('>', '&gt;', $this->d_estado);
         $this->Texto_tag .= "<td>" . $this->d_estado . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $rtf_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->Texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_and_delivery'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo $this->Ini->Nm_lang['lang_othr_grid_titl'] ?> -  :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="grid_shipping_and_delivery_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="grid_shipping_and_delivery"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
