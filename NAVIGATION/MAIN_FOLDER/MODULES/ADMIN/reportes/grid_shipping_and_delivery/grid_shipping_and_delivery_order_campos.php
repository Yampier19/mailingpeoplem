<?php
   include_once('grid_shipping_and_delivery_session.php');
   session_start();
   if (!function_exists("NM_is_utf8"))
   {
       include_once("../_lib/lib/php/nm_utf8.php");
   }
    $Ord_Cmp = new grid_shipping_and_delivery_Ord_cmp(); 
    $Ord_Cmp->Ord_cmp_init();
   
class grid_shipping_and_delivery_Ord_cmp
{
function Ord_cmp_init()
{
  global $sc_init, $path_img, $path_btn, $tab_ger_campos, $tab_def_campos, $tab_converte, $tab_labels, $embbed, $tbar_pos, $_POST, $_GET;
   if (isset($_POST['script_case_init']))
   {
       $sc_init    = $_POST['script_case_init'];
       $path_img   = $_POST['path_img'];
       $path_btn   = $_POST['path_btn'];
       $use_alias  = (isset($_POST['use_alias']))  ? $_POST['use_alias']  : "S";
       $fsel_ok    = (isset($_POST['fsel_ok']))    ? $_POST['fsel_ok']    : "";
       $campos_sel = (isset($_POST['campos_sel'])) ? $_POST['campos_sel'] : "";
       $sel_regra  = (isset($_POST['sel_regra']))  ? $_POST['sel_regra']  : "";
       $embbed     = isset($_POST['embbed_groupby']) && 'Y' == $_POST['embbed_groupby'];
       $tbar_pos   = isset($_POST['toolbar_pos']) ? $_POST['toolbar_pos'] : '';
   }
   elseif (isset($_GET['script_case_init']))
   {
       $sc_init    = $_GET['script_case_init'];
       $path_img   = $_GET['path_img'];
       $path_btn   = $_GET['path_btn'];
       $use_alias  = (isset($_GET['use_alias']))  ? $_GET['use_alias']  : "S";
       $fsel_ok    = (isset($_GET['fsel_ok']))    ? $_GET['fsel_ok']    : "";
       $campos_sel = (isset($_GET['campos_sel'])) ? $_GET['campos_sel'] : "";
       $sel_regra  = (isset($_GET['sel_regra']))  ? $_GET['sel_regra']  : "";
       $embbed     = isset($_GET['embbed_groupby']) && 'Y' == $_GET['embbed_groupby'];
       $tbar_pos   = isset($_GET['toolbar_pos']) ? $_GET['toolbar_pos'] : '';
   }
   $STR_lang    = (isset($_SESSION['scriptcase']['str_lang']) && !empty($_SESSION['scriptcase']['str_lang'])) ? $_SESSION['scriptcase']['str_lang'] : "es";
   $NM_arq_lang = "../_lib/lang/" . $STR_lang . ".lang.php";
   $this->Nm_lang = array();
   if (is_file($NM_arq_lang))
   {
       include_once($NM_arq_lang);
   }
   
   $tab_ger_campos = array();
   $tab_def_campos = array();
   $tab_labels     = array();
   $tab_ger_campos['s_nombres'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_nombres'] = "s_nombres";
       $tab_converte["s_nombres"]   = "s_nombres";
   }
   else
   {
       $tab_def_campos['s_nombres'] = "s.nombres";
       $tab_converte["s.nombres"]   = "s_nombres";
   }
   $tab_labels["s_nombres"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_nombres"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_nombres"] : "Nombres";
   $tab_ger_campos['s_apellidos'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_apellidos'] = "s_apellidos";
       $tab_converte["s_apellidos"]   = "s_apellidos";
   }
   else
   {
       $tab_def_campos['s_apellidos'] = "s.apellidos";
       $tab_converte["s.apellidos"]   = "s_apellidos";
   }
   $tab_labels["s_apellidos"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_apellidos"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_apellidos"] : "Apellidos";
   $tab_ger_campos['s_tipo_documento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_tipo_documento'] = "s_tipo_documento";
       $tab_converte["s_tipo_documento"]   = "s_tipo_documento";
   }
   else
   {
       $tab_def_campos['s_tipo_documento'] = "s.tipo_documento";
       $tab_converte["s.tipo_documento"]   = "s_tipo_documento";
   }
   $tab_labels["s_tipo_documento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_tipo_documento"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_tipo_documento"] : "Tipo Documento";
   $tab_ger_campos['s_documento'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_documento'] = "s_documento";
       $tab_converte["s_documento"]   = "s_documento";
   }
   else
   {
       $tab_def_campos['s_documento'] = "s.documento";
       $tab_converte["s.documento"]   = "s_documento";
   }
   $tab_labels["s_documento"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_documento"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_documento"] : "Documento";
   $tab_ger_campos['s_telefono'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_telefono'] = "s_telefono";
       $tab_converte["s_telefono"]   = "s_telefono";
   }
   else
   {
       $tab_def_campos['s_telefono'] = "s.telefono";
       $tab_converte["s.telefono"]   = "s_telefono";
   }
   $tab_labels["s_telefono"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_telefono"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_telefono"] : "Telefono";
   $tab_ger_campos['s_email'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_email'] = "s_email";
       $tab_converte["s_email"]   = "s_email";
   }
   else
   {
       $tab_def_campos['s_email'] = "s.email";
       $tab_converte["s.email"]   = "s_email";
   }
   $tab_labels["s_email"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_email"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_email"] : "Email";
   $tab_ger_campos['s_direccion'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_direccion'] = "s_direccion";
       $tab_converte["s_direccion"]   = "s_direccion";
   }
   else
   {
       $tab_def_campos['s_direccion'] = "s.direccion";
       $tab_converte["s.direccion"]   = "s_direccion";
   }
   $tab_labels["s_direccion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_direccion"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_direccion"] : "Direccion";
   $tab_ger_campos['s_prioridad'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_prioridad'] = "s_prioridad";
       $tab_converte["s_prioridad"]   = "s_prioridad";
   }
   else
   {
       $tab_def_campos['s_prioridad'] = "s.prioridad";
       $tab_converte["s.prioridad"]   = "s_prioridad";
   }
   $tab_labels["s_prioridad"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_prioridad"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_prioridad"] : "Prioridad";
   $tab_ger_campos['s_ciudad_origen'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_ciudad_origen'] = "s_ciudad_origen";
       $tab_converte["s_ciudad_origen"]   = "s_ciudad_origen";
   }
   else
   {
       $tab_def_campos['s_ciudad_origen'] = "s.ciudad_origen";
       $tab_converte["s.ciudad_origen"]   = "s_ciudad_origen";
   }
   $tab_labels["s_ciudad_origen"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_ciudad_origen"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_ciudad_origen"] : "Ciudad Origen";
   $tab_ger_campos['s_ciudad_destino'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_ciudad_destino'] = "s_ciudad_destino";
       $tab_converte["s_ciudad_destino"]   = "s_ciudad_destino";
   }
   else
   {
       $tab_def_campos['s_ciudad_destino'] = "s.ciudad_destino";
       $tab_converte["s.ciudad_destino"]   = "s_ciudad_destino";
   }
   $tab_labels["s_ciudad_destino"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_ciudad_destino"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_ciudad_destino"] : "Ciudad Destino";
   $tab_ger_campos['s_barrio_destino'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_barrio_destino'] = "s_barrio_destino";
       $tab_converte["s_barrio_destino"]   = "s_barrio_destino";
   }
   else
   {
       $tab_def_campos['s_barrio_destino'] = "s.barrio_destino";
       $tab_converte["s.barrio_destino"]   = "s_barrio_destino";
   }
   $tab_labels["s_barrio_destino"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_barrio_destino"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_barrio_destino"] : "Barrio Destino";
   $tab_ger_campos['s_zona'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_zona'] = "s_zona";
       $tab_converte["s_zona"]   = "s_zona";
   }
   else
   {
       $tab_def_campos['s_zona'] = "s.zona";
       $tab_converte["s.zona"]   = "s_zona";
   }
   $tab_labels["s_zona"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_zona"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_zona"] : "Zona";
   $tab_ger_campos['s_valor_declarado'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_valor_declarado'] = "s_valor_declarado";
       $tab_converte["s_valor_declarado"]   = "s_valor_declarado";
   }
   else
   {
       $tab_def_campos['s_valor_declarado'] = "s.valor_declarado";
       $tab_converte["s.valor_declarado"]   = "s_valor_declarado";
   }
   $tab_labels["s_valor_declarado"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_valor_declarado"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_valor_declarado"] : "Valor Declarado";
   $tab_ger_campos['s_centro_costos'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_centro_costos'] = "s_centro_costos";
       $tab_converte["s_centro_costos"]   = "s_centro_costos";
   }
   else
   {
       $tab_def_campos['s_centro_costos'] = "s.centro_costos";
       $tab_converte["s.centro_costos"]   = "s_centro_costos";
   }
   $tab_labels["s_centro_costos"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_centro_costos"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_centro_costos"] : "Centro Costos";
   $tab_ger_campos['s_tipo_solicitud'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_tipo_solicitud'] = "s_tipo_solicitud";
       $tab_converte["s_tipo_solicitud"]   = "s_tipo_solicitud";
   }
   else
   {
       $tab_def_campos['s_tipo_solicitud'] = "s.tipo_solicitud";
       $tab_converte["s.tipo_solicitud"]   = "s_tipo_solicitud";
   }
   $tab_labels["s_tipo_solicitud"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_tipo_solicitud"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_tipo_solicitud"] : "Tipo Solicitud";
   $tab_ger_campos['s_ubicacion'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_ubicacion'] = "s_ubicacion";
       $tab_converte["s_ubicacion"]   = "s_ubicacion";
   }
   else
   {
       $tab_def_campos['s_ubicacion'] = "s.ubicacion";
       $tab_converte["s.ubicacion"]   = "s_ubicacion";
   }
   $tab_labels["s_ubicacion"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_ubicacion"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_ubicacion"] : "Ubicacion";
   $tab_ger_campos['s_fecha_inicio'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_fecha_inicio'] = "s_fecha_inicio";
       $tab_converte["s_fecha_inicio"]   = "s_fecha_inicio";
   }
   else
   {
       $tab_def_campos['s_fecha_inicio'] = "s.fecha_inicio";
       $tab_converte["s.fecha_inicio"]   = "s_fecha_inicio";
   }
   $tab_labels["s_fecha_inicio"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_fecha_inicio"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_fecha_inicio"] : "Fecha Inicio";
   $tab_ger_campos['s_fecha_fin'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_fecha_fin'] = "s_fecha_fin";
       $tab_converte["s_fecha_fin"]   = "s_fecha_fin";
   }
   else
   {
       $tab_def_campos['s_fecha_fin'] = "s.fecha_fin";
       $tab_converte["s.fecha_fin"]   = "s_fecha_fin";
   }
   $tab_labels["s_fecha_fin"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_fecha_fin"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_fecha_fin"] : "Fecha Fin";
   $tab_ger_campos['s_fecha_registro'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_fecha_registro'] = "s_fecha_registro";
       $tab_converte["s_fecha_registro"]   = "s_fecha_registro";
   }
   else
   {
       $tab_def_campos['s_fecha_registro'] = "s.fecha_registro";
       $tab_converte["s.fecha_registro"]   = "s_fecha_registro";
   }
   $tab_labels["s_fecha_registro"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_fecha_registro"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_fecha_registro"] : "Fecha Registro";
   $tab_ger_campos['s_estado_proceso'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_estado_proceso'] = "s_estado_proceso";
       $tab_converte["s_estado_proceso"]   = "s_estado_proceso";
   }
   else
   {
       $tab_def_campos['s_estado_proceso'] = "s.estado_proceso";
       $tab_converte["s.estado_proceso"]   = "s_estado_proceso";
   }
   $tab_labels["s_estado_proceso"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_estado_proceso"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_estado_proceso"] : "Estado Proceso";
   $tab_ger_campos['s_estado'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['s_estado'] = "s_estado";
       $tab_converte["s_estado"]   = "s_estado";
   }
   else
   {
       $tab_def_campos['s_estado'] = "s.estado";
       $tab_converte["s.estado"]   = "s_estado";
   }
   $tab_labels["s_estado"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_estado"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["s_estado"] : "Estado";
   $tab_ger_campos['d_fecha_registro'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['d_fecha_registro'] = "d_fecha_registro";
       $tab_converte["d_fecha_registro"]   = "d_fecha_registro";
   }
   else
   {
       $tab_def_campos['d_fecha_registro'] = "d.fecha_registro";
       $tab_converte["d.fecha_registro"]   = "d_fecha_registro";
   }
   $tab_labels["d_fecha_registro"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["d_fecha_registro"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["d_fecha_registro"] : "Fecha Registro";
   $tab_ger_campos['d_novedad'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['d_novedad'] = "d_novedad";
       $tab_converte["d_novedad"]   = "d_novedad";
   }
   else
   {
       $tab_def_campos['d_novedad'] = "d.novedad";
       $tab_converte["d.novedad"]   = "d_novedad";
   }
   $tab_labels["d_novedad"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["d_novedad"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["d_novedad"] : "Novedad";
   $tab_ger_campos['d_estado'] = "on";
   if ($use_alias == "S")
   {
       $tab_def_campos['d_estado'] = "d_estado";
       $tab_converte["d_estado"]   = "d_estado";
   }
   else
   {
       $tab_def_campos['d_estado'] = "d.estado";
       $tab_converte["d.estado"]   = "d_estado";
   }
   $tab_labels["d_estado"]   = (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["d_estado"])) ? $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['labels']["d_estado"] : "Estado";
   if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_shipping_and_delivery']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_shipping_and_delivery']['field_display']))
   {
       foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_shipping_and_delivery']['field_display'] as $NM_cada_field => $NM_cada_opc)
       {
           if ($NM_cada_opc == "off")
           {
              $tab_ger_campos[$NM_cada_field] = "none";
           }
       }
   }
   if (isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['php_cmp_sel']))
   {
       foreach ($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
       {
           if ($NM_cada_opc == "off")
           {
              $tab_ger_campos[$NM_cada_field] = "none";
           }
       }
   }
   if (!isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['ordem_select']))
   {
       $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['ordem_select'] = array();
   }
   
   if ($fsel_ok == "cmp")
   {
       $this->Sel_processa_out_sel($campos_sel);
   }
   else
   {
       if ($embbed)
       {
           ob_start();
           $this->Sel_processa_form();
           $Temp = ob_get_clean();
           echo NM_charset_to_utf8($Temp);
       }
       else
       {
           $this->Sel_processa_form();
       }
   }
   exit;
   
}
function Sel_processa_out_sel($campos_sel)
{
   global $tab_ger_campos, $sc_init, $tab_def_campos, $tab_converte, $embbed;
   $arr_temp = array();
   $campos_sel = explode("@?@", $campos_sel);
   $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['ordem_select'] = array();
   $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['ordem_grid']   = "";
   $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['ordem_cmp']    = "";
   foreach ($campos_sel as $campo_sort)
   {
       $ordem = (substr($campo_sort, 0, 1) == "+") ? "asc" : "desc";
       $campo = substr($campo_sort, 1);
       if (isset($tab_converte[$campo]))
       {
           $_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['ordem_select'][$campo] = $ordem;
       }
   }
?>
    <script language="javascript"> 
<?php
   if (!$embbed)
   {
?>
      self.parent.tb_remove(); 
      parent.nm_gp_submit_ajax('inicio', ''); 
<?php
   }
   else
   {
?>
      nm_gp_submit_ajax('inicio', ''); 
<?php
   }
?>
   </script>
<?php
}
   
function Sel_processa_form()
{
  global $sc_init, $path_img, $path_btn, $tab_ger_campos, $tab_def_campos, $tab_converte, $tab_labels, $embbed, $tbar_pos;
   $size = 10;
   $_SESSION['scriptcase']['charset']  = (isset($this->Nm_lang['Nm_charset']) && !empty($this->Nm_lang['Nm_charset'])) ? $this->Nm_lang['Nm_charset'] : "ISO-8859-1";
   foreach ($this->Nm_lang as $ind => $dados)
   {
      if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($ind))
      {
          $ind = sc_convert_encoding($ind, $_SESSION['scriptcase']['charset'], "UTF-8");
          $this->Nm_lang[$ind] = $dados;
      }
      if ($_SESSION['scriptcase']['charset'] != "UTF-8" && NM_is_utf8($dados))
      {
          $this->Nm_lang[$ind] = sc_convert_encoding($dados, $_SESSION['scriptcase']['charset'], "UTF-8");
      }
   }
   $str_schema_all = (isset($_SESSION['scriptcase']['str_schema_all']) && !empty($_SESSION['scriptcase']['str_schema_all'])) ? $_SESSION['scriptcase']['str_schema_all'] : "Sc8_BlueWood/Sc8_BlueWood";
   include("../_lib/css/" . $str_schema_all . "_grid.php");
   $Str_btn_grid = trim($str_button) . "/" . trim($str_button) . $_SESSION['scriptcase']['reg_conf']['css_dir'] . ".php";
   include("../_lib/buttons/" . $Str_btn_grid);
   if (!function_exists("nmButtonOutput"))
   {
       include_once("../_lib/lib/php/nm_gp_config_btn.php");
   }
   if (!$embbed)
   {
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo $this->Nm_lang['lang_othr_grid_titl'] ?> - </TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
 <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
 <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
 <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
 <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
 <META http-equiv="Pragma" content="no-cache"/>
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
   <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_dir'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_div'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $_SESSION['scriptcase']['css_popup_div_dir'] ?>" /> 
 <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $_SESSION['scriptcase']['css_btn_popup'] ?>" /> 
</HEAD>
<BODY class="scGridPage" style="margin: 0px; overflow-x: hidden">
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery/js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery/js/jquery-ui.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/jquery_plugin/touch_punch/jquery.ui.touch-punch.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo $_SESSION['sc_session']['path_third'] ?>/tigra_color_picker/picker.js"></script>
<?php
   }
?>
<script language="javascript"> 
<?php
if ($embbed)
{
?>
  function scSubmitOrderCampos(sPos, sType) {
    $("#id_fsel_ok_sel_ord").val(sType);
    if(sType == 'cmp')
    {
       scPackSelectedOrd();
    }
   $.ajax({
    type: "POST",
    url: "grid_shipping_and_delivery_order_campos.php",
    data: {
     script_case_init: $("#id_script_case_init_sel_ord").val(),
     script_case_session: $("#id_script_case_session_sel_ord").val(),
     path_img: $("#id_path_img_sel_ord").val(),
     path_btn: $("#id_path_btn_sel_ord").val(),
     campos_sel: $("#id_campos_sel_sel_ord").val(),
     sel_regra: $("#id_sel_regra_sel_ord").val(),
     fsel_ok: $("#id_fsel_ok_sel_ord").val(),
     embbed_groupby: 'Y'
    }
   }).success(function(data) {
    $("#sc_id_order_campos_placeholder_" + sPos).find("td").html(data);
    scBtnOrderCamposHide(sPos);
   });
  }
<?php
}
?>
 // Submeter o formularior
 //-------------------------------------
 function submit_form_Fsel_ord()
 {
     scPackSelectedOrd();
      document.Fsel_ord.submit();
 }
 function scPackSelectedOrd() {
  var fieldList, fieldName, i, selectedFields = new Array;
 fieldList = $("#sc_id_fldord_selected").sortable("toArray");
 for (i = 0; i < fieldList.length; i++) {
  fieldName  = fieldList[i].substr(14);
  selectedFields.push($("#sc_id_class_" + fieldName).val() + fieldName);
 }
 $("#id_campos_sel_sel_ord").val( selectedFields.join("@?@") );
 }
 </script>
<FORM name="Fsel_ord" method="POST">
  <INPUT type="hidden" name="script_case_init"    id="id_script_case_init_sel_ord"    value="<?php echo NM_encode_input($sc_init); ?>"> 
  <INPUT type="hidden" name="script_case_session" id="id_script_case_session_sel_ord" value="<?php echo NM_encode_input(session_id()); ?>"> 
  <INPUT type="hidden" name="path_img"            id="id_path_img_sel_ord"            value="<?php echo NM_encode_input($path_img); ?>"> 
  <INPUT type="hidden" name="path_btn"            id="id_path_btn_sel_ord"            value="<?php echo NM_encode_input($path_btn); ?>"> 
  <INPUT type="hidden" name="fsel_ok"             id="id_fsel_ok_sel_ord"             value=""> 
<?php
if ($embbed)
{
    echo "<div class='scAppDivMoldura'>";
    echo "<table id=\"main_table\" style=\"width: 100%\" cellspacing=0 cellpadding=0>";
}
elseif ($_SESSION['scriptcase']['reg_conf']['html_dir'] == " DIR='RTL'")
{
    echo "<table id=\"main_table\" style=\"position: relative; top: 20px; right: 20px\">";
}
else
{
    echo "<table id=\"main_table\" style=\"position: relative; top: 20px; left: 20px\">";
}
?>
<?php
if (!$embbed)
{
?>
<tr>
<td>
<div class="scGridBorder">
<table width='100%' cellspacing=0 cellpadding=0>
<?php
}
?>
 <tr>
  <td class="<?php echo ($embbed)? 'scAppDivHeader scAppDivHeaderText':'scGridLabelVert'; ?>">
   <?php echo $this->Nm_lang['lang_btns_sort_hint']; ?>
  </td>
 </tr>
 <tr>
  <td class="<?php echo ($embbed)? 'scAppDivContent css_scAppDivContentText':'scGridTabelaTd'; ?>">
   <table class="<?php echo ($embbed)? '':'scGridTabela'; ?>" style="border-width: 0; border-collapse: collapse; width:100%;" cellspacing=0 cellpadding=0>
    <tr class="<?php echo ($embbed)? '':'scGridFieldOddVert'; ?>">
     <td style="vertical-align: top">
     <table>
   <tr><td style="vertical-align: top">
 <script language="javascript" type="text/javascript">
  $(function() {
   $(".sc_ui_litem").mouseover(function() {
    $(this).css("cursor", "all-scroll");
   });
   $("#sc_id_fldord_available").sortable({
    connectWith: ".sc_ui_fldord_selected",
    placeholder: "scAppDivSelectFieldsPlaceholder",
    remove: function(event, ui) {
     var fieldName = $(ui.item[0]).find("select").attr("id");
     $("#" + fieldName).show();
     $('#f_sel_sub').css('display', 'inline-block');
    }
   }).disableSelection();
   $("#sc_id_fldord_selected").sortable({
    connectWith: ".sc_ui_fldord_available",
    placeholder: "scAppDivSelectFieldsPlaceholder",
    remove: function(event, ui) {
     var fieldName = $(ui.item[0]).find("select").attr("id");
     $("#" + fieldName).hide();
     $('#f_sel_sub').css('display', 'inline-block');
    }
   });
   scUpdateListHeight();
  });
  function scUpdateListHeight() {
   $("#sc_id_fldord_available").css("min-height", "<?php echo sizeof($tab_ger_campos) * 35 ?>px");
   $("#sc_id_fldord_selected").css("min-height", "<?php echo sizeof($tab_ger_campos) * 35 ?>px");
  }
 </script>
 <style type="text/css">
  .sc_ui_sortable_ord {
   list-style-type: none;
   margin: 0;
   min-width: 225px;
  }
  .sc_ui_sortable_ord li {
   margin: 0 3px 3px 3px;
   padding: 1px 3px 1px 15px;
   min-height: 28px;
  }
  .sc_ui_sortable_ord li span {
   position: absolute;
   margin-left: -1.3em;
  }
 </style>
    <ul class="sc_ui_sort_groupby sc_ui_sortable_ord sc_ui_fldord_available scAppDivSelectFields" id="sc_id_fldord_available">
<?php
   foreach ($tab_ger_campos as $NM_cada_field => $NM_cada_opc)
   {
       if ($NM_cada_opc != "none")
       {
           if (!isset($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['ordem_select'][$tab_def_campos[$NM_cada_field]]))
           {
?>
     <li class="sc_ui_litem scAppDivSelectFieldsEnabled" id="sc_id_itemord_<?php echo NM_encode_input($tab_def_campos[$NM_cada_field]); ?>">
      <?php echo $tab_labels[$NM_cada_field]; ?>
      <select id="sc_id_class_<?php echo NM_encode_input($tab_def_campos[$NM_cada_field]); ?>" class="scAppDivToolbarInput" style="display: none">
       <option value="+">Asc</option>
       <option value="-">Desc</option>
      </select><br/>
     </li>
<?php
           }
       }
   }
?>
    </ul>
   </td>
   <td style="vertical-align: top">
    <ul class="sc_ui_sort_groupby sc_ui_sortable_ord sc_ui_fldord_selected scAppDivSelectFields" id="sc_id_fldord_selected">
<?php
   foreach ($_SESSION['sc_session'][$sc_init]['grid_shipping_and_delivery']['ordem_select'] as $NM_cada_field => $NM_cada_opc)
   {
       if (isset($tab_converte[$NM_cada_field]))
       {
           $sAscSelected  = " selected";
           $sDescSelected = "";
           if ($NM_cada_opc == "desc")
           {
               $sAscSelected  = "";
               $sDescSelected = " selected";
           }
?>
     <li class="sc_ui_litem scAppDivSelectFieldsEnabled" id="sc_id_itemord_<?php echo $NM_cada_field; ?>">
      <?php echo $tab_labels[$tab_converte[$NM_cada_field]]; ?>
      <select id="sc_id_class_<?php echo NM_encode_input($tab_def_campos[ $tab_converte[$NM_cada_field] ]); ?>" class="scAppDivToolbarInput" onchange="$('#f_sel_sub').css('display', 'inline-block');">
       <option value="+"<?php echo $sAscSelected; ?>>Asc</option>
       <option value="-"<?php echo $sDescSelected; ?>>Desc</option>
      </select>
     </li>
<?php
       }
   }
?>
    </ul>
    <input type="hidden" name="campos_sel" id="id_campos_sel_sel_ord" value="">
   </td>
   </tr>
   </table>
   </td>
   </tr>
   </table>
  </td>
 </tr>
   <tr><td class="<?php echo ($embbed)? 'scAppDivToolbar':'scGridToolbar'; ?>">
<?php
   if (!$embbed)
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bok", "document.Fsel_ord.fsel_ok.value='cmp';submit_form_Fsel_ord()", "document.Fsel_ord.fsel_ok.value='cmp';submit_form_Fsel_ord()", "f_sel_sub", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bapply", "scSubmitOrderCampos('" . NM_encode_input($tbar_pos) . "', 'cmp')", "scSubmitOrderCampos('" . NM_encode_input($tbar_pos) . "', 'cmp')", "f_sel_sub", "", "", "display: none;", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
  &nbsp;&nbsp;&nbsp;
<?php
   if (!$embbed)
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bsair", "self.parent.tb_remove()", "self.parent.tb_remove()", "Bsair", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
   else
   {
?>
   <?php echo nmButtonOutput($this->arr_buttons, "bcancelar", "scBtnOrderCamposHide('" . NM_encode_input($tbar_pos) . "')", "scBtnOrderCamposHide('" . NM_encode_input($tbar_pos) . "')", "Bsair", "", "", "", "absmiddle", "", "0px", $path_btn, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
?>
<?php
   }
?>
   </td>
   </tr>
<?php
if (!$embbed)
{
?>
</table>
</div>
</td>
</tr>
<?php
}
?>
</table>
<?php
if ($embbed)
{
?>
    </div>
<?php
}
?>
</FORM>
<script language="javascript"> 
var bFixed = false;
function ajusta_window_Fsel_ord()
{
<?php
   if ($embbed)
   {
?>
  return false;
<?php
   }
?>
  var mt = $(document.getElementById("main_table"));
  if (0 == mt.width() || 0 == mt.height())
  {
    setTimeout("ajusta_window_Fsel_ord()", 50);
    return;
  }
  else if(!bFixed)
  {
    var oOrig = $(document.Fsel_ord.sel_orig),
        oDest = $(document.Fsel_ord.sel_dest),
        mHeight = Math.max(oOrig.height(), oDest.height()),
        mWidth = Math.max(oOrig.width() + 5, oDest.width() + 5);
    oOrig.height(mHeight);
    oOrig.width(mWidth);
    oDest.height(mHeight);
    oDest.width(mWidth + 15);
    bFixed = true;
    if (navigator.userAgent.indexOf("Chrome/") > 0)
    {
      strMaxHeight = Math.min(($(window.parent).height()-80), mt.height());
      self.parent.tb_resize(strMaxHeight + 40, mt.width() + 40);
      setTimeout("ajusta_window_Fsel_ord()", 50);
      return;
    }
  }
  strMaxHeight = Math.min(($(window.parent).height()-80), mt.height());
  self.parent.tb_resize(strMaxHeight + 40, mt.width() + 40);
}
$( document ).ready(function() {
  ajusta_window_Fsel_ord();
});
</script>
<script>
    ajusta_window_Fsel_ord();
</script>
</BODY>
</HTML>
<?php
}
}
