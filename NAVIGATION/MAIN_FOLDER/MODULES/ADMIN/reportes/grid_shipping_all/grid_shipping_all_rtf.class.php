<?php

class grid_shipping_all_rtf
{
   var $Db;
   var $Erro;
   var $Ini;
   var $Lookup;
   var $nm_data;
   var $Texto_tag;
   var $Arquivo;
   var $Tit_doc;
   var $sc_proc_grid; 
   var $NM_cmp_hidden = array();

   //---- 
   function grid_shipping_all_rtf()
   {
      $this->nm_data   = new nm_data("es");
      $this->Texto_tag = "";
   }

   //---- 
   function monta_rtf()
   {
      $this->inicializa_vars();
      $this->gera_texto_tag();
      $this->grava_arquivo_rtf();
      $this->monta_html();
   }

   //----- 
   function inicializa_vars()
   {
      global $nm_lang;
      $dir_raiz          = strrpos($_SERVER['PHP_SELF'],"/") ;  
      $dir_raiz          = substr($_SERVER['PHP_SELF'], 0, $dir_raiz + 1) ;  
      $this->nm_location = $this->Ini->sc_protocolo . $this->Ini->server . $dir_raiz; 
      $this->Arquivo    = "sc_rtf";
      $this->Arquivo   .= "_" . date("YmdHis") . "_" . rand(0, 1000);
      $this->Arquivo   .= "_grid_shipping_all";
      $this->Arquivo   .= ".rtf";
      $this->Tit_doc    = "grid_shipping_all.rtf";
   }

   //----- 
   function gera_texto_tag()
   {
     global $nm_lang;
      global
             $nm_nada, $nm_lang;

      $_SESSION['scriptcase']['sc_sql_ult_conexao'] = ''; 
      $this->sc_proc_grid = false; 
      $nm_raiz_img  = ""; 
      if (isset($_SESSION['scriptcase']['sc_apl_conf']['grid_shipping_all']['field_display']) && !empty($_SESSION['scriptcase']['sc_apl_conf']['grid_shipping_all']['field_display']))
      {
          foreach ($_SESSION['scriptcase']['sc_apl_conf']['grid_shipping_all']['field_display'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['usr_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['usr_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['usr_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['php_cmp_sel']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['php_cmp_sel']))
      {
          foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['php_cmp_sel'] as $NM_cada_field => $NM_cada_opc)
          {
              $this->NM_cmp_hidden[$NM_cada_field] = $NM_cada_opc;
          }
      }
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['campos_busca']) && !empty($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['campos_busca']))
      { 
          $Busca_temp = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['campos_busca'];
          if ($_SESSION['scriptcase']['charset'] != "UTF-8")
          {
              $Busca_temp = NM_conv_charset($Busca_temp, $_SESSION['scriptcase']['charset'], "UTF-8");
          }
          $this->id_shipping = $Busca_temp['id_shipping']; 
          $tmp_pos = strpos($this->id_shipping, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_shipping = substr($this->id_shipping, 0, $tmp_pos);
          }
          $this->id_shipping_2 = $Busca_temp['id_shipping_input_2']; 
          $this->id_generate = $Busca_temp['id_generate']; 
          $tmp_pos = strpos($this->id_generate, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_generate = substr($this->id_generate, 0, $tmp_pos);
          }
          $this->id_generate_2 = $Busca_temp['id_generate_input_2']; 
          $this->id_personal = $Busca_temp['id_personal']; 
          $tmp_pos = strpos($this->id_personal, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_personal = substr($this->id_personal, 0, $tmp_pos);
          }
          $this->id_personal_2 = $Busca_temp['id_personal_input_2']; 
          $this->id_user = $Busca_temp['id_user']; 
          $tmp_pos = strpos($this->id_user, "##@@");
          if ($tmp_pos !== false)
          {
              $this->id_user = substr($this->id_user, 0, $tmp_pos);
          }
          $this->id_user_2 = $Busca_temp['id_user_input_2']; 
      } 
      $this->nm_field_dinamico = array();
      $this->nm_order_dinamico = array();
      $this->sc_where_orig   = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['where_orig'];
      $this->sc_where_atual  = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['where_pesq'];
      $this->sc_where_filtro = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['where_pesq_filtro'];
      if (isset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['rtf_name']))
      {
          $this->Arquivo = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['rtf_name'];
          $this->Tit_doc = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['rtf_name'];
          unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['rtf_name']);
      }
      if (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_sybase))
      { 
          $nmgp_select = "SELECT nombres, apellidos, tipo_documento, documento, telefono, email, direccion, prioridad, ciudad_origen, ciudad_destino, barrio_destino, zona, valor_declarado, centro_costos, tipo_solicitud, ubicacion, str_replace (convert(char(10),fecha_inicio,102), '.', '-') + ' ' + convert(char(8),fecha_inicio,20), str_replace (convert(char(10),fecha_fin,102), '.', '-') + ' ' + convert(char(8),fecha_fin,20), str_replace (convert(char(10),fecha_registro,102), '.', '-') + ' ' + convert(char(8),fecha_registro,20), estado_proceso, estado, id_shipping, id_generate, id_personal, id_user, id_detail from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mysql))
      { 
          $nmgp_select = "SELECT nombres, apellidos, tipo_documento, documento, telefono, email, direccion, prioridad, ciudad_origen, ciudad_destino, barrio_destino, zona, valor_declarado, centro_costos, tipo_solicitud, ubicacion, fecha_inicio, fecha_fin, fecha_registro, estado_proceso, estado, id_shipping, id_generate, id_personal, id_user, id_detail from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_mssql))
      { 
       $nmgp_select = "SELECT nombres, apellidos, tipo_documento, documento, telefono, email, direccion, prioridad, ciudad_origen, ciudad_destino, barrio_destino, zona, valor_declarado, centro_costos, tipo_solicitud, ubicacion, convert(char(23),fecha_inicio,121), convert(char(23),fecha_fin,121), convert(char(23),fecha_registro,121), estado_proceso, estado, id_shipping, id_generate, id_personal, id_user, id_detail from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_oracle))
      { 
          $nmgp_select = "SELECT nombres, apellidos, tipo_documento, documento, telefono, email, direccion, prioridad, ciudad_origen, ciudad_destino, barrio_destino, zona, valor_declarado, centro_costos, tipo_solicitud, ubicacion, fecha_inicio, fecha_fin, fecha_registro, estado_proceso, estado, id_shipping, id_generate, id_personal, id_user, id_detail from " . $this->Ini->nm_tabela; 
      } 
      elseif (in_array(strtolower($this->Ini->nm_tpbanco), $this->Ini->nm_bases_informix))
      { 
          $nmgp_select = "SELECT nombres, apellidos, tipo_documento, documento, telefono, email, direccion, prioridad, ciudad_origen, ciudad_destino, barrio_destino, zona, valor_declarado, centro_costos, tipo_solicitud, ubicacion, EXTEND(fecha_inicio, YEAR TO DAY), EXTEND(fecha_fin, YEAR TO DAY), EXTEND(fecha_registro, YEAR TO FRACTION), estado_proceso, estado, id_shipping, id_generate, id_personal, id_user, id_detail from " . $this->Ini->nm_tabela; 
      } 
      else 
      { 
          $nmgp_select = "SELECT nombres, apellidos, tipo_documento, documento, telefono, email, direccion, prioridad, ciudad_origen, ciudad_destino, barrio_destino, zona, valor_declarado, centro_costos, tipo_solicitud, ubicacion, fecha_inicio, fecha_fin, fecha_registro, estado_proceso, estado, id_shipping, id_generate, id_personal, id_user, id_detail from " . $this->Ini->nm_tabela; 
      } 
      $nmgp_select .= " " . $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['where_pesq'];
      $nmgp_order_by = $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['order_grid'];
      $nmgp_select .= $nmgp_order_by; 
      $_SESSION['scriptcase']['sc_sql_ult_comando'] = $nmgp_select;
      $rs = $this->Db->Execute($nmgp_select);
      if ($rs === false && !$rs->EOF && $GLOBALS["NM_ERRO_IBASE"] != 1)
      {
         $this->Erro->mensagem(__FILE__, __LINE__, "banco", $this->Ini->Nm_lang['lang_errm_dber'], $this->Db->ErrorMsg());
         exit;
      }

      $this->Texto_tag .= "<table>\r\n";
      $this->Texto_tag .= "<tr>\r\n";
      foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['field_order'] as $Cada_col)
      { 
          $SC_Label = (isset($this->New_label['nombres'])) ? $this->New_label['nombres'] : "Nombres"; 
          if ($Cada_col == "nombres" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['apellidos'])) ? $this->New_label['apellidos'] : "Apellidos"; 
          if ($Cada_col == "apellidos" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['tipo_documento'])) ? $this->New_label['tipo_documento'] : "Tipo Documento"; 
          if ($Cada_col == "tipo_documento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['documento'])) ? $this->New_label['documento'] : "Documento"; 
          if ($Cada_col == "documento" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['telefono'])) ? $this->New_label['telefono'] : "Telefono"; 
          if ($Cada_col == "telefono" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['email'])) ? $this->New_label['email'] : "Email"; 
          if ($Cada_col == "email" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['direccion'])) ? $this->New_label['direccion'] : "Direccion"; 
          if ($Cada_col == "direccion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['prioridad'])) ? $this->New_label['prioridad'] : "Prioridad"; 
          if ($Cada_col == "prioridad" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['ciudad_origen'])) ? $this->New_label['ciudad_origen'] : "Ciudad Origen"; 
          if ($Cada_col == "ciudad_origen" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['ciudad_destino'])) ? $this->New_label['ciudad_destino'] : "Ciudad Destino"; 
          if ($Cada_col == "ciudad_destino" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['barrio_destino'])) ? $this->New_label['barrio_destino'] : "Barrio Destino"; 
          if ($Cada_col == "barrio_destino" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['zona'])) ? $this->New_label['zona'] : "Zona"; 
          if ($Cada_col == "zona" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['valor_declarado'])) ? $this->New_label['valor_declarado'] : "Valor Declarado"; 
          if ($Cada_col == "valor_declarado" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['centro_costos'])) ? $this->New_label['centro_costos'] : "Centro Costos"; 
          if ($Cada_col == "centro_costos" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['tipo_solicitud'])) ? $this->New_label['tipo_solicitud'] : "Tipo Solicitud"; 
          if ($Cada_col == "tipo_solicitud" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['ubicacion'])) ? $this->New_label['ubicacion'] : "Ubicacion"; 
          if ($Cada_col == "ubicacion" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_inicio'])) ? $this->New_label['fecha_inicio'] : "Fecha Inicio"; 
          if ($Cada_col == "fecha_inicio" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_fin'])) ? $this->New_label['fecha_fin'] : "Fecha Fin"; 
          if ($Cada_col == "fecha_fin" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['fecha_registro'])) ? $this->New_label['fecha_registro'] : "Fecha Registro"; 
          if ($Cada_col == "fecha_registro" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['estado_proceso'])) ? $this->New_label['estado_proceso'] : "Estado Proceso"; 
          if ($Cada_col == "estado_proceso" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['estado'])) ? $this->New_label['estado'] : "Estado"; 
          if ($Cada_col == "estado" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['id_shipping'])) ? $this->New_label['id_shipping'] : "Id Shipping"; 
          if ($Cada_col == "id_shipping" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['id_generate'])) ? $this->New_label['id_generate'] : "Id Generate"; 
          if ($Cada_col == "id_generate" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['id_personal'])) ? $this->New_label['id_personal'] : "Id Personal"; 
          if ($Cada_col == "id_personal" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['id_user'])) ? $this->New_label['id_user'] : "Id User"; 
          if ($Cada_col == "id_user" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
          $SC_Label = (isset($this->New_label['id_detail'])) ? $this->New_label['id_detail'] : "Id Detail"; 
          if ($Cada_col == "id_detail" && (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off"))
          {
              if (!NM_is_utf8($SC_Label))
              {
                  $SC_Label = sc_convert_encoding($SC_Label, "UTF-8", $_SESSION['scriptcase']['charset']);
              }
              $SC_Label = str_replace('<', '&lt;', $SC_Label);
              $SC_Label = str_replace('>', '&gt;', $SC_Label);
              $this->Texto_tag .= "<td>" . $SC_Label . "</td>\r\n";
          }
      } 
      $this->Texto_tag .= "</tr>\r\n";
      while (!$rs->EOF)
      {
         $this->Texto_tag .= "<tr>\r\n";
         $this->nombres = $rs->fields[0] ;  
         $this->apellidos = $rs->fields[1] ;  
         $this->tipo_documento = $rs->fields[2] ;  
         $this->documento = $rs->fields[3] ;  
         $this->telefono = $rs->fields[4] ;  
         $this->email = $rs->fields[5] ;  
         $this->direccion = $rs->fields[6] ;  
         $this->prioridad = $rs->fields[7] ;  
         $this->ciudad_origen = $rs->fields[8] ;  
         $this->ciudad_destino = $rs->fields[9] ;  
         $this->barrio_destino = $rs->fields[10] ;  
         $this->zona = $rs->fields[11] ;  
         $this->valor_declarado = $rs->fields[12] ;  
         $this->centro_costos = $rs->fields[13] ;  
         $this->tipo_solicitud = $rs->fields[14] ;  
         $this->ubicacion = $rs->fields[15] ;  
         $this->fecha_inicio = $rs->fields[16] ;  
         $this->fecha_fin = $rs->fields[17] ;  
         $this->fecha_registro = $rs->fields[18] ;  
         $this->estado_proceso = $rs->fields[19] ;  
         $this->estado = $rs->fields[20] ;  
         $this->estado = (string)$this->estado;
         $this->id_shipping = $rs->fields[21] ;  
         $this->id_shipping = (string)$this->id_shipping;
         $this->id_generate = $rs->fields[22] ;  
         $this->id_generate = (string)$this->id_generate;
         $this->id_personal = $rs->fields[23] ;  
         $this->id_personal = (string)$this->id_personal;
         $this->id_user = $rs->fields[24] ;  
         $this->id_user = (string)$this->id_user;
         $this->id_detail = $rs->fields[25] ;  
         $this->id_detail = (string)$this->id_detail;
         $this->sc_proc_grid = true; 
         foreach ($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['field_order'] as $Cada_col)
         { 
            if (!isset($this->NM_cmp_hidden[$Cada_col]) || $this->NM_cmp_hidden[$Cada_col] != "off")
            { 
                $NM_func_exp = "NM_export_" . $Cada_col;
                $this->$NM_func_exp();
            } 
         } 
         $this->Texto_tag .= "</tr>\r\n";
         $rs->MoveNext();
      }
      $this->Texto_tag .= "</table>\r\n";

      $rs->Close();
   }
   //----- nombres
   function NM_export_nombres()
   {
         $this->nombres = html_entity_decode($this->nombres, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->nombres = strip_tags($this->nombres);
         if (!NM_is_utf8($this->nombres))
         {
             $this->nombres = sc_convert_encoding($this->nombres, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->nombres = str_replace('<', '&lt;', $this->nombres);
         $this->nombres = str_replace('>', '&gt;', $this->nombres);
         $this->Texto_tag .= "<td>" . $this->nombres . "</td>\r\n";
   }
   //----- apellidos
   function NM_export_apellidos()
   {
         $this->apellidos = html_entity_decode($this->apellidos, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->apellidos = strip_tags($this->apellidos);
         if (!NM_is_utf8($this->apellidos))
         {
             $this->apellidos = sc_convert_encoding($this->apellidos, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->apellidos = str_replace('<', '&lt;', $this->apellidos);
         $this->apellidos = str_replace('>', '&gt;', $this->apellidos);
         $this->Texto_tag .= "<td>" . $this->apellidos . "</td>\r\n";
   }
   //----- tipo_documento
   function NM_export_tipo_documento()
   {
         $this->tipo_documento = html_entity_decode($this->tipo_documento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->tipo_documento = strip_tags($this->tipo_documento);
         if (!NM_is_utf8($this->tipo_documento))
         {
             $this->tipo_documento = sc_convert_encoding($this->tipo_documento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->tipo_documento = str_replace('<', '&lt;', $this->tipo_documento);
         $this->tipo_documento = str_replace('>', '&gt;', $this->tipo_documento);
         $this->Texto_tag .= "<td>" . $this->tipo_documento . "</td>\r\n";
   }
   //----- documento
   function NM_export_documento()
   {
         $this->documento = html_entity_decode($this->documento, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->documento = strip_tags($this->documento);
         if (!NM_is_utf8($this->documento))
         {
             $this->documento = sc_convert_encoding($this->documento, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->documento = str_replace('<', '&lt;', $this->documento);
         $this->documento = str_replace('>', '&gt;', $this->documento);
         $this->Texto_tag .= "<td>" . $this->documento . "</td>\r\n";
   }
   //----- telefono
   function NM_export_telefono()
   {
         $this->telefono = html_entity_decode($this->telefono, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->telefono = strip_tags($this->telefono);
         if (!NM_is_utf8($this->telefono))
         {
             $this->telefono = sc_convert_encoding($this->telefono, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->telefono = str_replace('<', '&lt;', $this->telefono);
         $this->telefono = str_replace('>', '&gt;', $this->telefono);
         $this->Texto_tag .= "<td>" . $this->telefono . "</td>\r\n";
   }
   //----- email
   function NM_export_email()
   {
         $this->email = html_entity_decode($this->email, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->email = strip_tags($this->email);
         if (!NM_is_utf8($this->email))
         {
             $this->email = sc_convert_encoding($this->email, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->email = str_replace('<', '&lt;', $this->email);
         $this->email = str_replace('>', '&gt;', $this->email);
         $this->Texto_tag .= "<td>" . $this->email . "</td>\r\n";
   }
   //----- direccion
   function NM_export_direccion()
   {
         $this->direccion = html_entity_decode($this->direccion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->direccion = strip_tags($this->direccion);
         if (!NM_is_utf8($this->direccion))
         {
             $this->direccion = sc_convert_encoding($this->direccion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->direccion = str_replace('<', '&lt;', $this->direccion);
         $this->direccion = str_replace('>', '&gt;', $this->direccion);
         $this->Texto_tag .= "<td>" . $this->direccion . "</td>\r\n";
   }
   //----- prioridad
   function NM_export_prioridad()
   {
         $this->prioridad = html_entity_decode($this->prioridad, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->prioridad = strip_tags($this->prioridad);
         if (!NM_is_utf8($this->prioridad))
         {
             $this->prioridad = sc_convert_encoding($this->prioridad, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->prioridad = str_replace('<', '&lt;', $this->prioridad);
         $this->prioridad = str_replace('>', '&gt;', $this->prioridad);
         $this->Texto_tag .= "<td>" . $this->prioridad . "</td>\r\n";
   }
   //----- ciudad_origen
   function NM_export_ciudad_origen()
   {
         $this->ciudad_origen = html_entity_decode($this->ciudad_origen, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->ciudad_origen = strip_tags($this->ciudad_origen);
         if (!NM_is_utf8($this->ciudad_origen))
         {
             $this->ciudad_origen = sc_convert_encoding($this->ciudad_origen, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->ciudad_origen = str_replace('<', '&lt;', $this->ciudad_origen);
         $this->ciudad_origen = str_replace('>', '&gt;', $this->ciudad_origen);
         $this->Texto_tag .= "<td>" . $this->ciudad_origen . "</td>\r\n";
   }
   //----- ciudad_destino
   function NM_export_ciudad_destino()
   {
         $this->ciudad_destino = html_entity_decode($this->ciudad_destino, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->ciudad_destino = strip_tags($this->ciudad_destino);
         if (!NM_is_utf8($this->ciudad_destino))
         {
             $this->ciudad_destino = sc_convert_encoding($this->ciudad_destino, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->ciudad_destino = str_replace('<', '&lt;', $this->ciudad_destino);
         $this->ciudad_destino = str_replace('>', '&gt;', $this->ciudad_destino);
         $this->Texto_tag .= "<td>" . $this->ciudad_destino . "</td>\r\n";
   }
   //----- barrio_destino
   function NM_export_barrio_destino()
   {
         $this->barrio_destino = html_entity_decode($this->barrio_destino, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->barrio_destino = strip_tags($this->barrio_destino);
         if (!NM_is_utf8($this->barrio_destino))
         {
             $this->barrio_destino = sc_convert_encoding($this->barrio_destino, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->barrio_destino = str_replace('<', '&lt;', $this->barrio_destino);
         $this->barrio_destino = str_replace('>', '&gt;', $this->barrio_destino);
         $this->Texto_tag .= "<td>" . $this->barrio_destino . "</td>\r\n";
   }
   //----- zona
   function NM_export_zona()
   {
         $this->zona = html_entity_decode($this->zona, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->zona = strip_tags($this->zona);
         if (!NM_is_utf8($this->zona))
         {
             $this->zona = sc_convert_encoding($this->zona, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->zona = str_replace('<', '&lt;', $this->zona);
         $this->zona = str_replace('>', '&gt;', $this->zona);
         $this->Texto_tag .= "<td>" . $this->zona . "</td>\r\n";
   }
   //----- valor_declarado
   function NM_export_valor_declarado()
   {
         $this->valor_declarado = html_entity_decode($this->valor_declarado, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->valor_declarado = strip_tags($this->valor_declarado);
         if (!NM_is_utf8($this->valor_declarado))
         {
             $this->valor_declarado = sc_convert_encoding($this->valor_declarado, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->valor_declarado = str_replace('<', '&lt;', $this->valor_declarado);
         $this->valor_declarado = str_replace('>', '&gt;', $this->valor_declarado);
         $this->Texto_tag .= "<td>" . $this->valor_declarado . "</td>\r\n";
   }
   //----- centro_costos
   function NM_export_centro_costos()
   {
         $this->centro_costos = html_entity_decode($this->centro_costos, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->centro_costos = strip_tags($this->centro_costos);
         if (!NM_is_utf8($this->centro_costos))
         {
             $this->centro_costos = sc_convert_encoding($this->centro_costos, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->centro_costos = str_replace('<', '&lt;', $this->centro_costos);
         $this->centro_costos = str_replace('>', '&gt;', $this->centro_costos);
         $this->Texto_tag .= "<td>" . $this->centro_costos . "</td>\r\n";
   }
   //----- tipo_solicitud
   function NM_export_tipo_solicitud()
   {
         $this->tipo_solicitud = html_entity_decode($this->tipo_solicitud, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->tipo_solicitud = strip_tags($this->tipo_solicitud);
         if (!NM_is_utf8($this->tipo_solicitud))
         {
             $this->tipo_solicitud = sc_convert_encoding($this->tipo_solicitud, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->tipo_solicitud = str_replace('<', '&lt;', $this->tipo_solicitud);
         $this->tipo_solicitud = str_replace('>', '&gt;', $this->tipo_solicitud);
         $this->Texto_tag .= "<td>" . $this->tipo_solicitud . "</td>\r\n";
   }
   //----- ubicacion
   function NM_export_ubicacion()
   {
         $this->ubicacion = html_entity_decode($this->ubicacion, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->ubicacion = strip_tags($this->ubicacion);
         if (!NM_is_utf8($this->ubicacion))
         {
             $this->ubicacion = sc_convert_encoding($this->ubicacion, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->ubicacion = str_replace('<', '&lt;', $this->ubicacion);
         $this->ubicacion = str_replace('>', '&gt;', $this->ubicacion);
         $this->Texto_tag .= "<td>" . $this->ubicacion . "</td>\r\n";
   }
   //----- fecha_inicio
   function NM_export_fecha_inicio()
   {
         $conteudo_x = $this->fecha_inicio;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->fecha_inicio, "YYYY-MM-DD");
             $this->fecha_inicio = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", "ddmmaaaa"));
         } 
         if (!NM_is_utf8($this->fecha_inicio))
         {
             $this->fecha_inicio = sc_convert_encoding($this->fecha_inicio, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_inicio = str_replace('<', '&lt;', $this->fecha_inicio);
         $this->fecha_inicio = str_replace('>', '&gt;', $this->fecha_inicio);
         $this->Texto_tag .= "<td>" . $this->fecha_inicio . "</td>\r\n";
   }
   //----- fecha_fin
   function NM_export_fecha_fin()
   {
         $conteudo_x = $this->fecha_fin;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->fecha_fin, "YYYY-MM-DD");
             $this->fecha_fin = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DT", "ddmmaaaa"));
         } 
         if (!NM_is_utf8($this->fecha_fin))
         {
             $this->fecha_fin = sc_convert_encoding($this->fecha_fin, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_fin = str_replace('<', '&lt;', $this->fecha_fin);
         $this->fecha_fin = str_replace('>', '&gt;', $this->fecha_fin);
         $this->Texto_tag .= "<td>" . $this->fecha_fin . "</td>\r\n";
   }
   //----- fecha_registro
   function NM_export_fecha_registro()
   {
         if (substr($this->fecha_registro, 10, 1) == "-") 
         { 
             $this->fecha_registro = substr($this->fecha_registro, 0, 10) . " " . substr($this->fecha_registro, 11);
         } 
         if (substr($this->fecha_registro, 13, 1) == ".") 
         { 
            $this->fecha_registro = substr($this->fecha_registro, 0, 13) . ":" . substr($this->fecha_registro, 14, 2) . ":" . substr($this->fecha_registro, 17);
         } 
         $conteudo_x = $this->fecha_registro;
         nm_conv_limpa_dado($conteudo_x, "YYYY-MM-DD HH:II:SS");
         if (is_numeric($conteudo_x) && $conteudo_x > 0) 
         { 
             $this->nm_data->SetaData($this->fecha_registro, "YYYY-MM-DD HH:II:SS");
             $this->fecha_registro = $this->nm_data->FormataSaida($this->nm_data->FormatRegion("DH", "ddmmaaaa;hhiiss"));
         } 
         if (!NM_is_utf8($this->fecha_registro))
         {
             $this->fecha_registro = sc_convert_encoding($this->fecha_registro, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->fecha_registro = str_replace('<', '&lt;', $this->fecha_registro);
         $this->fecha_registro = str_replace('>', '&gt;', $this->fecha_registro);
         $this->Texto_tag .= "<td>" . $this->fecha_registro . "</td>\r\n";
   }
   //----- estado_proceso
   function NM_export_estado_proceso()
   {
         $this->estado_proceso = html_entity_decode($this->estado_proceso, ENT_COMPAT, $_SESSION['scriptcase']['charset']);
         $this->estado_proceso = strip_tags($this->estado_proceso);
         if (!NM_is_utf8($this->estado_proceso))
         {
             $this->estado_proceso = sc_convert_encoding($this->estado_proceso, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->estado_proceso = str_replace('<', '&lt;', $this->estado_proceso);
         $this->estado_proceso = str_replace('>', '&gt;', $this->estado_proceso);
         $this->Texto_tag .= "<td>" . $this->estado_proceso . "</td>\r\n";
   }
   //----- estado
   function NM_export_estado()
   {
         nmgp_Form_Num_Val($this->estado, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->estado))
         {
             $this->estado = sc_convert_encoding($this->estado, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->estado = str_replace('<', '&lt;', $this->estado);
         $this->estado = str_replace('>', '&gt;', $this->estado);
         $this->Texto_tag .= "<td>" . $this->estado . "</td>\r\n";
   }
   //----- id_shipping
   function NM_export_id_shipping()
   {
         nmgp_Form_Num_Val($this->id_shipping, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->id_shipping))
         {
             $this->id_shipping = sc_convert_encoding($this->id_shipping, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->id_shipping = str_replace('<', '&lt;', $this->id_shipping);
         $this->id_shipping = str_replace('>', '&gt;', $this->id_shipping);
         $this->Texto_tag .= "<td>" . $this->id_shipping . "</td>\r\n";
   }
   //----- id_generate
   function NM_export_id_generate()
   {
         nmgp_Form_Num_Val($this->id_generate, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->id_generate))
         {
             $this->id_generate = sc_convert_encoding($this->id_generate, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->id_generate = str_replace('<', '&lt;', $this->id_generate);
         $this->id_generate = str_replace('>', '&gt;', $this->id_generate);
         $this->Texto_tag .= "<td>" . $this->id_generate . "</td>\r\n";
   }
   //----- id_personal
   function NM_export_id_personal()
   {
         nmgp_Form_Num_Val($this->id_personal, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->id_personal))
         {
             $this->id_personal = sc_convert_encoding($this->id_personal, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->id_personal = str_replace('<', '&lt;', $this->id_personal);
         $this->id_personal = str_replace('>', '&gt;', $this->id_personal);
         $this->Texto_tag .= "<td>" . $this->id_personal . "</td>\r\n";
   }
   //----- id_user
   function NM_export_id_user()
   {
         nmgp_Form_Num_Val($this->id_user, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->id_user))
         {
             $this->id_user = sc_convert_encoding($this->id_user, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->id_user = str_replace('<', '&lt;', $this->id_user);
         $this->id_user = str_replace('>', '&gt;', $this->id_user);
         $this->Texto_tag .= "<td>" . $this->id_user . "</td>\r\n";
   }
   //----- id_detail
   function NM_export_id_detail()
   {
         nmgp_Form_Num_Val($this->id_detail, $_SESSION['scriptcase']['reg_conf']['grup_num'], $_SESSION['scriptcase']['reg_conf']['dec_num'], "0", "S", "2", "", "N:" . $_SESSION['scriptcase']['reg_conf']['neg_num'] , $_SESSION['scriptcase']['reg_conf']['simb_neg'], $_SESSION['scriptcase']['reg_conf']['num_group_digit']) ; 
         if (!NM_is_utf8($this->id_detail))
         {
             $this->id_detail = sc_convert_encoding($this->id_detail, "UTF-8", $_SESSION['scriptcase']['charset']);
         }
         $this->id_detail = str_replace('<', '&lt;', $this->id_detail);
         $this->id_detail = str_replace('>', '&gt;', $this->id_detail);
         $this->Texto_tag .= "<td>" . $this->id_detail . "</td>\r\n";
   }

   //----- 
   function grava_arquivo_rtf()
   {
      global $nm_lang, $doc_wrap;
      $rtf_f = fopen($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo, "w");
      require_once($this->Ini->path_third      . "/rtf_new/document_generator/cl_xml2driver.php"); 
      $text_ok  =  "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n"; 
      $text_ok .=  "<DOC config_file=\"" . $this->Ini->path_third . "/rtf_new/doc_config.inc\" >\r\n"; 
      $text_ok .=  $this->Texto_tag; 
      $text_ok .=  "</DOC>\r\n"; 
      $xml = new nDOCGEN($text_ok,"RTF"); 
      fwrite($rtf_f, $xml->get_result_file());
      fclose($rtf_f);
   }

   function nm_conv_data_db($dt_in, $form_in, $form_out)
   {
       $dt_out = $dt_in;
       if (strtoupper($form_in) == "DB_FORMAT")
       {
           if ($dt_out == "null" || $dt_out == "")
           {
               $dt_out = "";
               return $dt_out;
           }
           $form_in = "AAAA-MM-DD";
       }
       if (strtoupper($form_out) == "DB_FORMAT")
       {
           if (empty($dt_out))
           {
               $dt_out = "null";
               return $dt_out;
           }
           $form_out = "AAAA-MM-DD";
       }
       nm_conv_form_data($dt_out, $form_in, $form_out);
       return $dt_out;
   }
   //---- 
   function monta_html()
   {
      global $nm_url_saida, $nm_lang;
      include($this->Ini->path_btn . $this->Ini->Str_btn_grid);
      unset($_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['rtf_file']);
      if (is_file($this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo))
      {
          $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all']['rtf_file'] = $this->Ini->root . $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      }
      $path_doc_md5 = md5($this->Ini->path_imag_temp . "/" . $this->Arquivo);
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all'][$path_doc_md5][0] = $this->Ini->path_imag_temp . "/" . $this->Arquivo;
      $_SESSION['sc_session'][$this->Ini->sc_page]['grid_shipping_all'][$path_doc_md5][1] = $this->Tit_doc;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML<?php echo $_SESSION['scriptcase']['reg_conf']['html_dir'] ?>>
<HEAD>
 <TITLE><?php echo $this->Ini->Nm_lang['lang_othr_grid_titl'] ?> - shipping :: RTF</TITLE>
 <META http-equiv="Content-Type" content="text/html; charset=<?php echo $_SESSION['scriptcase']['charset_html'] ?>" />
<?php
if ($_SESSION['scriptcase']['proc_mobile'])
{
?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<?php
}
?>
  <META http-equiv="Expires" content="Fri, Jan 01 1900 00:00:00 GMT"/>
  <META http-equiv="Last-Modified" content="<?php echo gmdate("D, d M Y H:i:s"); ?> GMT"/>
  <META http-equiv="Cache-Control" content="no-store, no-cache, must-revalidate"/>
  <META http-equiv="Cache-Control" content="post-check=0, pre-check=0"/>
  <META http-equiv="Pragma" content="no-cache"/>
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/css/<?php echo $this->Ini->str_schema_all ?>_export<?php echo $_SESSION['scriptcase']['reg_conf']['css_dir'] ?>.css" /> 
  <link rel="stylesheet" type="text/css" href="../_lib/buttons/<?php echo $this->Ini->Str_btn_css ?>" /> 
</HEAD>
<BODY class="scExportPage">
<?php echo $this->Ini->Ajax_result_set ?>
<table style="border-collapse: collapse; border-width: 0; height: 100%; width: 100%"><tr><td style="padding: 0; text-align: center; vertical-align: middle">
 <table class="scExportTable" align="center">
  <tr>
   <td class="scExportTitle" style="height: 25px">RTF</td>
  </tr>
  <tr>
   <td class="scExportLine" style="width: 100%">
    <table style="border-collapse: collapse; border-width: 0; width: 100%"><tr><td class="scExportLineFont" style="padding: 3px 0 0 0" id="idMessage">
    <?php echo $this->Ini->Nm_lang['lang_othr_file_msge'] ?>
    </td><td class="scExportLineFont" style="text-align:right; padding: 3px 0 0 0">
     <?php echo nmButtonOutput($this->arr_buttons, "bexportview", "document.Fview.submit()", "document.Fview.submit()", "idBtnView", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bdownload", "document.Fdown.submit()", "document.Fdown.submit()", "idBtnDown", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
     <?php echo nmButtonOutput($this->arr_buttons, "bvoltar", "document.F0.submit()", "document.F0.submit()", "idBtnBack", "", "", "", "", "", "", $this->Ini->path_botoes, "", "", "", "", "", "only_text", "text_right", "", "", "", "", "", "");
 ?>
    </td></tr></table>
   </td>
  </tr>
 </table>
</td></tr></table>
<form name="Fview" method="get" action="<?php echo $this->Ini->path_imag_temp . "/" . $this->Arquivo ?>" target="_blank" style="display: none"> 
</form>
<form name="Fdown" method="get" action="grid_shipping_all_download.php" target="_blank" style="display: none"> 
<input type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<input type="hidden" name="nm_tit_doc" value="grid_shipping_all"> 
<input type="hidden" name="nm_name_doc" value="<?php echo $path_doc_md5 ?>"> 
</form>
<FORM name="F0" method=post action="./"> 
<INPUT type="hidden" name="script_case_init" value="<?php echo NM_encode_input($this->Ini->sc_page); ?>"> 
<INPUT type="hidden" name="script_case_session" value="<?php echo NM_encode_input(session_id()); ?>"> 
<INPUT type="hidden" name="nmgp_opcao" value="volta_grid"> 
</FORM> 
</BODY>
</HTML>
<?php
   }
   function nm_gera_mask(&$nm_campo, $nm_mask)
   { 
      $trab_campo = $nm_campo;
      $trab_mask  = $nm_mask;
      $tam_campo  = strlen($nm_campo);
      $trab_saida = "";
      $mask_num = false;
      for ($x=0; $x < strlen($trab_mask); $x++)
      {
          if (substr($trab_mask, $x, 1) == "#")
          {
              $mask_num = true;
              break;
          }
      }
      if ($mask_num )
      {
          $ver_duas = explode(";", $trab_mask);
          if (isset($ver_duas[1]) && !empty($ver_duas[1]))
          {
              $cont1 = count(explode("#", $ver_duas[0])) - 1;
              $cont2 = count(explode("#", $ver_duas[1])) - 1;
              if ($cont2 >= $tam_campo)
              {
                  $trab_mask = $ver_duas[1];
              }
              else
              {
                  $trab_mask = $ver_duas[0];
              }
          }
          $tam_mask = strlen($trab_mask);
          $xdados = 0;
          for ($x=0; $x < $tam_mask; $x++)
          {
              if (substr($trab_mask, $x, 1) == "#" && $xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_campo, $xdados, 1);
                  $xdados++;
              }
              elseif ($xdados < $tam_campo)
              {
                  $trab_saida .= substr($trab_mask, $x, 1);
              }
          }
          if ($xdados < $tam_campo)
          {
              $trab_saida .= substr($trab_campo, $xdados);
          }
          $nm_campo = $trab_saida;
          return;
      }
      for ($ix = strlen($trab_mask); $ix > 0; $ix--)
      {
           $char_mask = substr($trab_mask, $ix - 1, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               $trab_saida = $char_mask . $trab_saida;
           }
           else
           {
               if ($tam_campo != 0)
               {
                   $trab_saida = substr($trab_campo, $tam_campo - 1, 1) . $trab_saida;
                   $tam_campo--;
               }
               else
               {
                   $trab_saida = "0" . $trab_saida;
               }
           }
      }
      if ($tam_campo != 0)
      {
          $trab_saida = substr($trab_campo, 0, $tam_campo) . $trab_saida;
          $trab_mask  = str_repeat("z", $tam_campo) . $trab_mask;
      }
   
      $iz = 0; 
      for ($ix = 0; $ix < strlen($trab_mask); $ix++)
      {
           $char_mask = substr($trab_mask, $ix, 1);
           if ($char_mask != "x" && $char_mask != "z")
           {
               if ($char_mask == "." || $char_mask == ",")
               {
                   $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
               }
               else
               {
                   $iz++;
               }
           }
           elseif ($char_mask == "x" || substr($trab_saida, $iz, 1) != "0")
           {
               $ix = strlen($trab_mask) + 1;
           }
           else
           {
               $trab_saida = substr($trab_saida, 0, $iz) . substr($trab_saida, $iz + 1);
           }
      }
      $nm_campo = $trab_saida;
   } 
}

?>
