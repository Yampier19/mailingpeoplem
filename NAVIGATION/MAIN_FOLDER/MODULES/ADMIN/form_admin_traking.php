<?php

header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');

if ($user_name != '' && $id_user != '') {
  $consul_user = mysqli_query($conex, 'SELECT * FROM `userlogin` AS A LEFT JOIN user AS B ON A.id_user = B.id_user  WHERE A.`id_user` = ' . base64_decode($id_user) . '');
  while ($consul = (mysqli_fetch_array($consul_user))) {
    $nombre = $consul['names'];
    $apellido = $consul['surnames'];
    $id_userlog = $consul['id_loginrol'];
  }
  $boton = 5;
  include('../../DROPDOWN/menu_admin.php')
?>
  <script language=javascript>
    function ventanaSecundaria(URL) {
      window.open(URL, "ventana1", "width=600,height=400,Top=100,Left=300px")
    }

    function ventanaSecundaria2(URL) {
      window.open(URL, "ventana1", "width=900,height=500,Top=100,Left=50%")
    }

    function ajax_formulario_solicitud() { // ajax_formulario_solicitud 
      var input = $('#input').val();
      alert(input);
    }
    $(document).ready(function() {
      /*$('#btn_actualizar_ubicacion').click(function ()
      {
        ajax_formulario_solicitud();
      });*/
    });
  </script>
  <link rel="stylesheet" href="../../../DESIGN/CSS/principal_fontawesome-free/css/all.min.css">
  <style type="text/css">
    .ajustar_tabla_style {
      padding-top: 10px !important;
      width: 97% !important;
      border: 1px solid #BDC3C7;
      box-shadow: 0px 0px 2px #BDC3C7;
      margin: auto;
    }

    .sizeIcon {
      font-size: 2rem !important;
    }

    thead {
      font-size: .9rem;
    }

    tbody {
      font-size: .75rem;
    }

    .table .thead-dark th {
      color: #fff;
      background-color: #17a2b8;
      border-color: #17a2b8;
    }

    #styleIcons {
      border-radius: 50%;
      font-size: 15px;
      height: 30px;
      left: 18px;
      line-height: 30px;
      text-align: center;
      width: 30px;
    }
  </style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> Solicitud - Tabla Traking <button class="btn btn-info" id="recargar" onclick="location.reload()"><i class="fas fa-spinner"></i></button> / <button type="button" class="btn btn-outline-info" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-exclamation"></i></button></h1>
          </div>
          <!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="form_request.php">Inicio</a></li>
              <li class="breadcrumb-item active">mis envios</li>
            </ol>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="container-fluid">
      <div class="form-group row">
        <!-- Modal -->
        <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">semiolog�a y la semi�tica</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="callout callout-info">
                  <h5>Gestion</h5>
                  <ul>
                    <li>
                      <p><button type="button" class="btn btn-outline-info"><i class="fas fa-tasks"></i></button> Gestionar solicitudes en Bandeja de correspondencia y Pool de correspondencia</p>
                    </li>
                    <br>
                    <li>
                      <p><button type="button" class="btn btn-outline-warning"><i class="fas fa-exclamation"></i></button> Gestionar solicitudes pendientes</p>
                    </li>
                  </ul>
                </div>
                <div class="callout callout-info">
                  <h5>Productos</h5>
                  <div class="row">
                    <div class="col-sm-6">
                      <ul>
                        <li>
                          <p><i class="fas fa-envelope-open-text" style="font-size:1.4rem;"></i> Sobre Manila</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-envelope-square" style="font-size:1.4rem;"></i> Sobre Plastico</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-archive"></i> Caja </p>
                        </li>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <ul>
                        <li>
                          <p><i class="fas fa-box" style="font-size:1.4rem;"></i> Paquete</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-gift" style="font-size:1.4rem;"></i> Obsequio</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-file" style="font-size:1.4rem;"></i> Documento</p>
                        </li>
                        <br>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="callout callout-info">
                  <h5>Traking</h5>
                  <div class="row">
                    <div class="col-sm-6">
                      <ul>
                        <li>
                          <p><i class="fas fa-dolly-flatbed bg-warning" id="styleIcons"></i> Solicitud generada</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fab fa-product-hunt" style="background-color:#d81b60; color:#FFFFFF;" id="styleIcons"></i> Ubicacion de la correpondencia</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-dolly bg-info" id="styleIcons"></i> Asignacion de la correpondencia</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-shipping-fast bg-primary" id="styleIcons"></i> En ruta</p>
                        </li>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <ul>
                        <li>
                          <p><i class="fas fa-sync-alt" style="background-color:#ff851b; color:#000000;" id="styleIcons"></i> Devoluciones</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-vote-yea bg-success" id="styleIcons"></i> Correpondencia entregada</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-reply bg-secondary" id="styleIcons"></i> Restitucion de la correspondencia</p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="callout callout-info">
                  <h5>Proceso</h5>
                  <div class="row">
                    <div class="col-sm-6">
                      <ul>
                        <li>
                          <p><i class="fas fa-spinner" style="background-color:#d81b60; color:#FFFFFF;" id="styleIcons"></i> Proceso ubcacion</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-spinner bg-info" id="styleIcons"></i> Proceso asignacion</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-spinner bg-primary" id="styleIcons"></i> Proceso en ruta</p>
                        </li>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <ul>
                        <li>
                          <p><i class="fas fa-spinner" style="background-color:#ff851b; color:#FFFFFF;" id="styleIcons"></i></i> Proceso devolucion</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-check-circle" style="background-color:#28a745; color:#FFFFFF;" id="styleIcons"></i> Proceso entregado</p>
                        </li>
                        <br>
                        <li>
                          <p><i class="fas fa-times" style="background-color:#6c757d; color:#FFFFFF;" id="styleIcons"></i> Proceso restitucion</p>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Aceptar</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ajustar_tabla_style">
      <!-- .ajustar_tabla_style -->
      <div class="table-responsive">
        <table class="table table-striped" id="myTable">
          <thead class="thead-dark">
            <tr>
              <th id="conteenido_encabezado4" scope="col">Fecha_Solicitud</th>
              <th id="conteenido_encabezado4" scope="col">QR</th>
              <th id="conteenido_encabezado4" scope="col">Destinatario</th>
              <th id="conteenido_encabezado4" scope="col">Ciudad_Destino</th>
              <th id="conteenido_encabezado4" scope="col">Barrio</th>
              <th id="conteenido_encabezado4" scope="col">Direccion</th>
              <th id="conteenido_encabezado4" scope="col">Telefono</th>
              <th id="conteenido_encabezado4" scope="col">Producto</th>
              <th id="conteenido_encabezado4" scope="col">Estado</th>
              <th id="conteenido_encabezado4" scope="col">Gestion</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $resultado = mysqli_query($conex, "SELECT C.`id_qr_generado` AS id_qr_generado, A.`id_shipping` AS id_shipping, A.`nombres` AS nombres, A.`apellidos` AS apellidos, A.`ciudad_destino` AS ciudad_destino, A.`barrio_destino` AS barrio_destino, A.`direccion` AS direccion, A.`telefono` AS telefono_destinatario, A.`tipo_solicitud` AS tipo_solicitud,A.`ubicacion` AS ubicacion, B.`names` AS nombre_remitente, B.`surnames` AS apellido_remitente, A.`fecha_registro` AS fecha_registro, A.`estado_proceso` AS estado_proceso, A.estado FROM `shipping` AS A LEFT JOIN personal AS B ON A.id_personal = B.id_personal LEFT JOIN qr_generated AS C ON A.id_generate = C.id_qr");
            while ($resul = mysqli_fetch_array($resultado)) {
              $id_shipping = $resul['id_shipping'];
              $id_qr_generado = $resul['id_qr_generado'];
              $nombre_destinatario = $resul['nombres'];
              $apellido_destinatarios = $resul['apellidos'];
              $ciudad_destino = $resul['ciudad_destino'];
              $barrio_destino = $resul['barrio_destino'];
              $direccion_des = $resul['direccion'];
              $telefono_destinatario = $resul['telefono_destinatario'];
              $tipo_solicitud = $resul['tipo_solicitud'];
              $ubicacion = $resul['ubicacion'];
              $nombre_remitente = $resul['nombre_remitente'];
              $apellido_remitente = $resul['apellido_remitente'];
              $fecha_registro = $resul['fecha_registro'];
              $estado = $resul['estado_proceso'];
              $estado_ai = $resul['estado'];
              $se_entre = mysqli_query($conex,"SELECT * FROM `delivery` WHERE `id_ship` = '$id_shipping'"); 
              while($dat = mysqli_fetch_array($se_entre)){ $id_ship_se = $dat['id_ship']; }
           

            ?>
              <tr>
                <td><?php echo $fecha_registro ?></td>
                <td><?php echo $id_qr_generado ?></td>
                <td><?php echo $nombre_destinatario . " " . $apellido_destinatarios; ?></td>
                <td><?php echo $ciudad_destino; ?></td>
                <td><?php echo $barrio_destino; ?></td>
                <td><?php echo $direccion_des; ?></td>
                <td><?php echo $telefono_destinatario ?></td>
                <td>
                  <?php if ($tipo_solicitud == "Sobre Manila") {  
                 ?>    
                    <i class="fas fa-envelope-open-text" style="font-size:1.4rem; <?php if($id_ship_se == $id_shipping){echo 'color: green'; }elseif($estado_ai == '1'){
                      echo 'color: red';}else{  echo 'color: #D0B300'; } ?>"></i>
                   
                  <?php } else if ($tipo_solicitud == "Sobre Plastico") {  ?>
                    <i class="fas fa-envelope-square" style="font-size:1.4rem; <?php if($id_ship_se == $id_shipping){echo 'color: green'; }elseif($estado_ai == '1'){
                      echo 'color: red';}else{  echo 'color: #D0B300'; } ?>"></i>
                  <?php } else if ($tipo_solicitud == "Caja") {  ?>
                    <i class="fas fa-archive" style="font-size:1.4rem; <?php if($id_ship_se == $id_shipping){echo 'color: green'; }elseif($estado_ai == '1'){
                      echo 'color: red';}else{  echo 'color: #D0B300'; } ?>"></i>
                  <?php } else if ($tipo_solicitud == "Paquete") {  ?>
                    <i class="fas fa-box" style="font-size:1.4rem; <?php if($id_ship_se == $id_shipping){echo 'color: green'; }elseif($estado_ai == '1'){
                      echo 'color: red';}else{  echo 'color: #D0B300'; } ?>"></i>
                  <?php } else if ($tipo_solicitud == "Obsequio") {  ?>
                    <i class="fas fa-gift" style="font-size:1.4rem; <?php if($id_ship_se == $id_shipping){echo 'color: green'; }elseif($estado_ai == '1'){
                      echo 'color: red';}else{  echo 'color: #D0B300'; } ?>"></i>
                  <?php } else if ($tipo_solicitud == "Documento") {  ?>
                    <i class="fas fa-file" style="font-size:1.4rem; <?php if($id_ship_se == $id_shipping){echo 'color: green'; }elseif($estado_ai == '1'){
                      echo 'color: red';}else{  echo 'color: #D0B300'; } ?>"></i>
                  <?php } ?>
                </td>
                <td><?php echo $estado; ?></td>
                <td>
                  <?php if ($ubicacion == "Pendiente") { ?>
                    <center><button type="button" class="btn btn-outline-warning" onclick="javascript:ventanaSecundaria('vent_emergente_request.php?xnfgti=<?php echo base64_encode($id_shipping) ?>')" />
                      <i class="fas fa-exclamation"></i>
                      </button>
                    </center>
                  <?php } else {  ?>
                    <center>
                      <button type="button" class="btn btn-outline-info" onclick="javascript:ventanaSecundaria2('vent_emerg_traking.php?xnfgtiyy=<?php echo base64_encode($id_shipping) ?>')" />
                      <i class="fas fa-tasks"></i>
                      </button>
                    </center>
                  <?php } ?>
                </td>
              </tr>
            <?php }  ?>
          </tbody>
        </table>
      </div>
    </div><!-- ./ajustar_tabla_style -->
  </div><!-- ./wrapper -->
  <!-- REQUIRED SCRIPTS -->
  <!-- Bootstrap -->
  <script src="../../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE -->
  <script src="../../../DESIGN/JS/principal_js/adminlte.js"></script>
  <!-- OPTIONAL SCRIPTS -->
  <script src="../../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>
  <script src="../../../DESIGN/JS/principal_js/demo.js"></script>
  <script src="../../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>
  </body>

  </html>

  <?php  } else {
  header('location: ./../../../../index.php');
}  ?>