<?php
header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');
if ($user_name != '' && $id_user != '') {
    if (isset($_POST['txt_var'])) {  ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".ocultar_formulario").css('display', 'none');
                $(".ocultar_generador").css('display', 'none');
                $(".otra_solicitud").css('display', 'block');
            });
        </script>
    <?php
    }
    $id_users = base64_decode($id_user);
    $consul_user = mysqli_query($conex, 'SELECT * FROM `userlogin` AS A LEFT JOIN user AS B ON A.id_user = B.id_user  WHERE A.`id_user` = ' . base64_decode($id_user) . '');
    while ($consul = (mysqli_fetch_array($consul_user))) {
        $nombre = $consul['names'];
        $apellido = $consul['surnames'];
        $id_userlog = $consul['id_loginrol'];
    }
    if ($id_userlog == base64_decode($id_loginrol)) {
        //echo 'Bueno';
    }
    $boton = 2;
    include('../../DROPDOWN/menu_admin.php');
    ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"> Formulario - Solicitud</h1>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="form_administrador.php">Inicio</a></li>
                            <li class="breadcrumb-item active">Solicitud</li>
                        </ol>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <?php
        $select_proxima_gestion = mysqli_query($conex, "SELECT id_shipping FROM shipping ORDER BY id_shipping DESC LIMIT 1; ");
        while ($gestion = (mysqli_fetch_array($select_proxima_gestion))) {
            $id_shipping = $gestion['id_shipping'];
        }
        $id_proxima_shipping = $id_shipping + 1;
        $select_proxima_qr = mysqli_query($conex, "SELECT id_qr FROM qr_generated ORDER BY id_qr DESC LIMIT 1; ");
        while ($qr_nuevo = (mysqli_fetch_array($select_proxima_qr))) {
            $id_qr = $qr_nuevo['id_qr'];
        }
        $id_proximo_generated = $id_qr + 1;
        $usuario = $id_users;
        $DesdeLetra = "a";
        $HastaLetra = "z";
        $letraAleatoria = chr(rand(ord($DesdeLetra), ord($HastaLetra)));
        $id_correponde = $id_proxima_shipping . $letraAleatoria . $usuario . "0";
        $token = "0";
        /// Fecha Actual ///
        date_default_timezone_set("America/Bogota");
        $d      = date('d');
        $mes_nu = date('m');
        $anio    = date('Y');
        $fecha_actual = $d . $mes_nu . $anio;
        ?>

        <!-- Main content -->
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5">
                <!-- <form action="form_administrador_solicitud.php" method="post">
                    <div class="input-group input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Email" name="correob" id="correob">
                        <span class="input-group-append">
                            <button type="submit" class="btn btn-info btn-flat">Buscar</button>
                        </span>
                    </div>
                </form>-->
            </div>
            <div class="col-md-2"></div>
        </div>
        <br>
        <form action="form_administrador_solicitud.php" id="form_request" name="form_request" method="post">
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <?php/*
                        if (isset($_POST['correob']) != '') {

                            $correobuscar = $_POST['correob'];
                            $select_corre = mysqli_query($conex, "SELECT * FROM `personal` WHERE `correo` = '$correobuscar' AND `estado` = '1'");
                            while ($datoB = mysqli_fetch_array($select_corre)) {

                                $nombreB = $datoB['names'];
                                $apellidoB = $datoB['surnames'];
                                $telefonoB = $datoB['telefono'];
                                $emailB = $datoB['correo'];
                                $ubicacionB = 'Casa Editorial Ofi #';
                            }
                            $num_rows = mysqli_num_rows($select_corre);
                            if ($num_rows == 0) {
                                $select_correoS = mysqli_query($conex, "SELECT * FROM `shipping` WHERE `email`='$correobuscar' ORDER BY `shipping`.`id_shipping` DESC LIMIT 1");
                                while ($datoS = mysqli_fetch_array($select_correoS)) {

                                    $nombreB = $datoS['nombres'];
                                    $apellidoB = $datoS['apellidos'];
                                    $telefonoB = $datoS['telefono'];
                                    $emailB = $datoS['email'];
                                    $ubicacionB =  $datoS['direccion'];
                                }
                            }
                        }*/

                        ?>
                        <div class="col-lg-12">
                            <div class="card card-warning">
                                <div class="card-header"><span>
                                        <h3 style="font-size: 1.1rem;"><i class="fas fa-mail-bulk"></i>&nbsp;&nbsp;Datos del Destinatario</h3>
                                    </span></div>
                                <div class="card-body">
                                    <div class="ocultar_formulario">
                                        <!-- .ocultar_formulario -->
                                        <div class="row" id="parte_1_ocultar">
                                            <div class="col-md-6">
                                                <label for="txt_nombre">Nombres <span class="asterisco_obligatorio">*</span></label>
                                                <div class="input-group">
                                                    <input name="usuario" id="usuario" value="" type="hidden">
                                                    <input type="text" class="form-control" id="txt_nombre" name="txt_nombre" placeholder="Nombres" value="<?php if (isset($nombreB) != '') {
                                                                                                                                                                echo $nombreB;
                                                                                                                                                            } else {
                                                                                                                                                                echo '';
                                                                                                                                                            } ?>" required="required">
                                                    <input type="hidden" name="id_form" id="id_form" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-6"> <label for="txt_apellido">Apellidos <span class="asterisco_obligatorio">*</span></label>
                                                <input type="text" class="form-control" id="txt_apellido" name="txt_apellido" placeholder="Apellidos" value="<?php if (isset($apellidoB) != '') {
                                                                                                                                                                    echo $apellidoB;
                                                                                                                                                                } else {
                                                                                                                                                                    echo '';
                                                                                                                                                                } ?>" required="required">
                                            </div>
                                        </div>
                                        <div class="row" id="parte_2_ocultar">
                                            <div class="col-md-3">
                                                <label for="txt_telefono">Tel&eacute;fono <span class="asterisco_obligatorio">*</span></label>
                                                <div class="input-group">
                                                    <input name="usuario" id="usuario" value="" type="hidden">
                                                    <input type="text" class="form-control" id="txt_telefono" name="txt_telefono" value="<?php if (isset($telefonoB) != '') {
                                                                                                                                                echo $telefonoB;
                                                                                                                                            } else {
                                                                                                                                                echo '';
                                                                                                                                            } ?>" required="required" placeholder="Tel&eacute;fono">
                                                    <input type="hidden" name="" id="" value="" />
                                                    <input type="hidden" name="id_form" id="id_form" value="">
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-3">
                                                <label for="txt_email">Email</label>
                                                <div class="input-group">
                                                    <input type="email" class="form-control" id="txt_email" name="txt_email" value="<?php if (isset($emailB) != '') {
                                                                                                                                        echo $emailB;
                                                                                                                                    } else {
                                                                                                                                        echo '';
                                                                                                                                    } ?>" placeholder="Email">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                                    </div>
                                                </div>
                                            </div>-->
                                            <div class="col-md-9">
                                                <label for="txt_direccion">Direcci&oacute;n <span class="asterisco_obligatorio">*</span></label>
                                                <input type="text" class="form-control" id="txt_direccion" name="txt_direccion" value="<?php if (isset($ubicacionB) != '') {
                                                                                                                                            echo $ubicacionB;
                                                                                                                                        } else {
                                                                                                                                            echo '';
                                                                                                                                        } ?>" required="required" placeholder="Direcci&oacute;n">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label for="fecha_inicio">Fecha de Entrega <span class="asterisco_obligatorio">*</span></label>
                                                <input min="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="fecha_inicio" id="fecha_inicio" value="" required="required">
                                            </div>
                                            <div class="col-md-6">
                                                <label for="select_prioridad">Prioridad en D&iacute;as <span class="asterisco_obligatorio">*</span></label>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <span class="font-weight-bold info-text mr-2 mt-1"><i class="fas fa-minus" aria-hidden="true"></i></span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="inputDiv">
                                                            <div class="etiqueta"></div>
                                                            <input name="prioridad_dias" id="select_prioridad" class="custom-range custom-range-info" type="range" value="1" min="1" max="5" value="1" autocomplete="off" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <span class="font-weight-bold info-text ml-2 mt-1"><i class="fas fa-plus" aria-hidden="true"></i></span>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <span id="dias_p" class="font-weight-bold ml-2 mt-1 valueSpan"></span>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="col-md-3">
                                                <label for="fecha_fin">Fecha Max. Entrega</label>
                                                <div class="imprimir_fecha_fin" id="imprimir_fecha_fin"><input type="date" value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" disabled="disabled" /></div>
                                                <!-- <input type="date" class="form-control" name="fecha_fin" id="fecha_fin" disabled="disabled"> -->
                                            </div>



                                        </div>

                                        <div class="row" id="parte_3_ocultar">
                                            <div class="col-md-6">
                                                <label for="ciudad_destino">Ciudad de Destino <span class="asterisco_obligatorio">*</span></label>
                                                <select class="form-control select2 select2-info" data-dropdown-css-class="select2-info"  name="ciudad_destino" required>
                                                    <option value="">Seleccione...</option>
                                                    <?php $select_ciudad = mysqli_query($conex, "SELECT `ciudad` FROM `ciudades`");
                                                    while($ciuda_d = mysqli_fetch_array($select_ciudad)){ 
                                                        $ciud = $ciuda_d['ciudad'];
                                                        print '<option>'.$ciud.'</option>';
                                                       }
                                                    ?>
                                                </select>
                                            </div>

                                            <div class="col-md-6">
                                                <label for="barrio_destino">Barrio Destino </label>
                                                <select class="form-control select2 select2-warning" data-dropdown-css-class="select2-warning" style="width: 100%;" name="barrio_destino">
                                                    <option value="" data-tokens="">Seleccione...</option>
                                                    <?php $select_barrio = mysqli_query($conex, "SELECT barrio FROM barrios;");
                                                    while ($barrio = (mysqli_fetch_array($select_barrio))) {
                                                        $barrios = $barrio['barrio'];
                                                    ?>
                                                        <option value="<?php echo $barrios; ?>" data-tokens=""><?php echo $barrios;  ?></option>
                                                    <?php   } ?>

                                                </select>

                                            </div>

                                        </div>



                                        <div class="row" id="parte_4_ocultar">

                                            <div class="col-md-4">

                                                <?php /* ?><label for="valor_declarado">Valor Declarado</label>

                                        <input type="text" class="form-control" id="valor_declarado" name="valor_declarado" value = ""  required="required" placeholder="Valor Declarado">   

                                        <?php */ ?>



                                                <label for="centro_costos">Centro de costos <span class="asterisco_obligatorio">*</span></label>

                                                <div class="input-group">

                                                    <div class="input-group-prepend">

                                                        <span class="input-group-text"><i class="fas fa-ellipsis-v"></i></span>

                                                    </div>

                                                    <input type="text" class="form-control" id="centro_costos" name="centro_costos" value="" placeholder="Centro de costos" required="required">

                                                </div>

                                            </div>

                                            <div class="col-md-4">

                                                <label for="tipo_solicitud">Producto <span class="asterisco_obligatorio">*</span></label>

                                                <select class="form-control" name="tipo_solicitud" required>

                                                    <option>Seleccione...</option>

                                                    <option value="Sobre Manila">Sobre Manila</option>

                                                    <option value="Sobre Plastico">Sobre Pl&aacute;stico</option>

                                                    <option value="Caja">Caja</option>

                                                    <option value="Paquete">Paquete</option>

                                                    <option value="Obsequio">Obsequio</option>

                                                    <option value="Documento">Documento</option>

                                                </select>

                                            </div>

                                            <div class="col-md-4">

                                                <label for="ubicacion">Ubicaci&oacute;n Correspondencia <span class="asterisco_obligatorio">*</span></label>

                                                <select class="form-control" name="ubicacion" id="ubicacion" required>

                                                    <option value="">Seleccione...</option>

                                                    <option value="Bandeja">Bandeja de correspondencia</option>

                                                    <option value="Pool">Pool de correspondencia</option>

                                                    <option value="Pendiente">Pendiente</option>

                                                </select>

                                            </div>

                                        </div>
                                          <p></p>
                                        <div class="row"> 
                                  <div class="col-md-3"></div>
                                  <div class="col-md-7"><textarea class="form-control" name="observacion" id="observacion" placeholder="Observaci&oacute;n"></textarea></div>
                                  <div class="col-md-2"></div>
                                  </div>

                                    </div><!-- .ocultar_formulario -->

                                </div>

                                  


                                <div class="row">

                                    <div class="col-md-12">

                                        <input type="hidden" name="txt_var" id="txt_var" value="none"><?php // validacion para mostrar qr 
                                                                                                        ?>

                                    </div>

                                </div>



                                <div class="qr">
                                    <!-- .qr -->
                                    <?php
                                    /*
                           * PHP QR Code encoder
                            *
                            * Exemplatory usage
                            *
                            * PHP QR Code is distributed under LGPL 3
                            * Copyright (C) 2010 Dominik Dzienia <deltalab at poczta dot fm>
                            *
                            * This library is free software; you can redistribute it and/or
                            * modify it under the terms of the GNU Lesser General Public
                            * License as published by the Free Software Foundation; either
                            * version 3 of the License, or any later version.
                            *
                            * This library is distributed in the hope that it will be useful,
                            * but WITHOUT ANY WARRANTY; without even the implied warranty of
                            * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
                            * Lesser General Public License for more details.
                            *
                            * You should have received a copy of the GNU Lesser General Public
                            * License along with this library; if not, write to the Free Software
                            * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
                            */
                                    //set it to writable location, a place for temp generated PNG files
                                    $PNG_TEMP_DIR = dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../FILES/QR_TEMP/' . DIRECTORY_SEPARATOR;
                                    //html PNG location prefix
                                    $PNG_WEB_DIR = '../../FILES/QR_TEMP/';
                                    include "../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/GENERAR_QR/qrlib.php";
                                    //ofcourse we need rights to create temp dir
                                    if (!file_exists($PNG_TEMP_DIR))
                                        mkdir($PNG_TEMP_DIR);
                                    $filename = $PNG_TEMP_DIR . 'test.png';
                                    //processing form input
                                   //remember to sanitize user input in real-life solution !!!
                                    $errorCorrectionLevel = 'L';
                                    if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L', 'M', 'Q', 'H')))
                                        $errorCorrectionLevel = $_REQUEST['level'];
                                    $matrixPointSize = 7;
                                    if (isset($_REQUEST['size']))
                                        $matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);
                                    if (isset($_REQUEST['data'])) {
                                        //it's very important!
                                        if (trim($_REQUEST['data']) == '')
                                            die('data cannot be empty! <a href="?">back</a>');
                                        // url local
                                        //$PNG_TEMP_DIR2 = 'http://192.168.0.3/MailingPeopleM/NAVIGATION/MAIN_FOLDER/FILES/QR_TEMP/';
                                        // server 
                                        $PNG_TEMP_DIR2 = 'https://app-peoplemarketing.com/MailingPeopleM/NAVIGATION/MAIN_FOLDER/FILES/QR_TEMP/';
                                        // user data
                                        $filename2 = $PNG_TEMP_DIR2 . 'qr' . md5($_REQUEST['data'] . '|' . $errorCorrectionLevel . '|' . $matrixPointSize) . '.png';
                                        $filename = $PNG_TEMP_DIR . 'qr' . md5($_REQUEST['data'] . '|' . $errorCorrectionLevel . '|' . $matrixPointSize) . '.png';
                                        QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, 2);
                                    } else {
                                        //default data
                                        QRcode::png('PHP QR Code :)', $filename, $errorCorrectionLevel, $matrixPointSize, 2);
                                    }
                                    //display generated file
                                    ?>
                                    <?php
                                    if (isset($_POST['txt_var'])) {  ?>
                                        <script type="text/javascript">
                                            $(document).ready(function() {
                                                $(".ocultar_formulario").css('display', 'none');
                                                $(".ocultar_generador").css('display', 'none');
                                                $(".otra_solicitud").css('display', 'block');
                                            });
                                        </script>
                                    <?php
                                       $qr_generado_1 = $_POST['data']; // echo $qr_generado_1;
                                        $txt_nombre = $_POST['txt_nombre']; // echo $txt_nombre;
                                        $txt_apellido = $_POST['txt_apellido'];  // echo $txt_apellido;
                                        $tipo_documento = $_POST['tipo_documento'];  // echo $tipo_documento;
                                        $txt_documento = $_POST['txt_documento']; // echo $txt_cedula;
                                        $txt_telefono = $_POST['txt_telefono']; // echo $txt_telefono;
                                        $txt_email = $_POST['txt_email']; // echo $txt_email;
                                        $txt_direccion = $_POST['txt_direccion']; // echo $txt_direccion;
                                        $prioridad_dias = $_POST['prioridad_dias']; // echo $prioridad_dias;
                                        $ciudad_origen = $_POST['ciudad_origen']; // echo $ciudad_origen;
                                        $ciudad_destino = $_POST['ciudad_destino']; // echo $ciudad_destino;
                                        $barrio_destino = $_POST['barrio_destino']; // echo $barrio_destino;
                                        // $valor_declarado = $_POST['valor_declarado']; // echo $valor_declarado;
                                        $centro_costos = $_POST['centro_costos']; // echo $centro_costos;
                                        $tipo_solicitud = $_POST['tipo_solicitud']; // echo $tipo_solicitud;
                                        $ubicacion = $_POST['ubicacion']; // echo $ubicacion;
                                        $fecha_inicio  = $_POST['fecha_inicio']; // echo $fecha_inicio;
                                        $fecha_fin =  $_POST['fecha_fin']; // echo $fecha_fin;
                                        $observacion = $_POST['observacion']; //Observacion
                                        $select_position = mysqli_query($conex, "SELECT id_qr,posicion FROM qr_generated ORDER BY id_qr DESC LIMIT 1; ");
                                        while ($data_posicion = (mysqli_fetch_array($select_position))) {
                                            $id_qr = $data_posicion['id_qr'];
                                            $posicion = $data_posicion['posicion'];
                                        }
                                        if ($id_qr % 8 == 0) {
                                            $asignacion_posicion = '1';
                                        } else {
                                            $asignacion_posicion = $posicion + 1;
                                        }
                                        $insert_solicitud = mysqli_query($conex, "INSERT INTO `qr_generated` (`id_qr_generado`, `id_unico`, `link_qr` , `posicion`)VALUES('" . $qr_generado_1 . "', '" . $token . "', '" . $filename2 . "' , '" . $asignacion_posicion . "');");
                                        $select_id_qr = mysqli_query($conex, "SELECT id_qr FROM qr_generated ORDER BY id_qr DESC LIMIT 1; ");
                                        while ($id_qr = (mysqli_fetch_array($select_id_qr))) {
                                            $id_qr1 = $id_qr['id_qr'];
                                            $id_qr_general = $id_qr1;
                                        }
                                        $select_qr_generado = mysqli_query($conex, "SELECT id_qr_generado FROM qr_generated ORDER BY id_qr DESC LIMIT 1; ");
                                        while ($qr = (mysqli_fetch_array($select_qr_generado))) {
                                            $qr_generado = $qr['id_qr_generado'];
                                        }
                                        if ($ubicacion == "Bandeja") {
                                            $insert_qrtemp = mysqli_query($conex, "INSERT INTO `shipping` (`id_generate`, `id_user`, `nombres`, `apellidos`, `tipo_documento`, `documento`, `telefono`, `email`, `direccion`, `prioridad`, `ciudad_origen`, `ciudad_destino`, `barrio_destino`, `valor_declarado`,`centro_costos`, `tipo_solicitud`, `ubicacion`, `fecha_inicio`, `fecha_fin`, `fecha_registro`, `estado_proceso`, `observacion`,`estado`) VALUES('" . $id_qr_general . "' , '" . $id_users . "', '" . $txt_nombre . "', '" . $txt_apellido . "', '" . $tipo_documento . "', '" . $txt_documento . "', '" . $txt_telefono . "', '" . $txt_email . "', '" . $txt_direccion . "', '" . $prioridad_dias . "', '" . $ciudad_origen . "', '" . $ciudad_destino . "', '" . $barrio_destino . "', 'N/A', '" . $centro_costos . "' , '" . $tipo_solicitud . "', '" . $ubicacion . "', '" . $fecha_inicio . "', '" . $fecha_fin . "', NOW(), 'Solicitud Generada', '".$observacion."', '0'); ");
                                            $select_ultimo = mysqli_query($conex, "SELECT `id_shipping` FROM `shipping` ORDER BY `shipping`.`id_shipping` DESC LIMIT 1");
                                            while ($dato = (mysqli_fetch_array($select_ultimo))) {
                                                $id_ultimoShip = $dato['id_shipping'];
                                            }
                                            $insert_point = mysqli_query($conex, "INSERT INTO `point` (id_ship, id_user, fecha_registro, novedad, estado)VALUES('$id_ultimoShip','$id_users',NOW(),'N/A','1');");
                                        } else if ($ubicacion == "Pool") {
                                            $insert_qrtemp = mysqli_query($conex, "INSERT INTO `shipping` (`id_generate`, `id_user`, `nombres`, `apellidos`, `tipo_documento`, `documento`, `telefono`, `email`, `direccion`, `prioridad`, `ciudad_origen`, `ciudad_destino`, `barrio_destino`, `valor_declarado`,`centro_costos`, `tipo_solicitud`, `ubicacion`, `fecha_inicio`, `fecha_fin`, `fecha_registro`, `estado_proceso`,  `observacion`, `estado`) VALUES('" . $id_qr_general . "' , '" . $id_users . "', '" . $txt_nombre . "', '" . $txt_apellido . "', '" . $tipo_documento . "', '" . $txt_documento . "', '" . $txt_telefono . "', '" . $txt_email . "', '" . $txt_direccion . "', '" . $prioridad_dias . "', '" . $ciudad_origen . "', '" . $ciudad_destino . "', '" . $barrio_destino . "', 'N/A', '" . $centro_costos . "' , '" . $tipo_solicitud . "', '" . $ubicacion . "', '" . $fecha_inicio . "', '" . $fecha_fin . "', NOW(), 'Solicitud Generada', '".$observacion."', '0'); ");
                                            $select_ultimo = mysqli_query($conex, "SELECT `id_shipping` FROM `shipping` ORDER BY `shipping`.`id_shipping` DESC LIMIT 1");
                                            while ($dato = (mysqli_fetch_array($select_ultimo))) {
                                                $id_ultimoShip = $dato['id_shipping'];
                                            }
                                            $insert_office = mysqli_query($conex, "INSERT INTO `office` (id_ship, id_user, fecha_registro, novedad, estado)VALUES('$id_ultimoShip','$id_users',NOW(),'N/A','1');");
                                        } else if ($ubicacion == "Pendiente") {
                                            $insert_qrtemp = mysqli_query($conex, "INSERT INTO `shipping` (`id_generate`, `id_user`, `nombres`, `apellidos`, `tipo_documento`, `documento`, `telefono`, `email`, `direccion`, `prioridad`, `ciudad_origen`, `ciudad_destino`, `barrio_destino`, `valor_declarado`,`centro_costos`, `tipo_solicitud`, `ubicacion`, `fecha_inicio`, `fecha_fin`, `fecha_registro`, `estado_proceso`,  `observacion`, `estado`) VALUES('" . $id_qr_general . "' , '" . $id_users . "', '" . $txt_nombre . "', '" . $txt_apellido . "', '" . $tipo_documento . "', '" . $txt_documento . "', '" . $txt_telefono . "', '" . $txt_email . "', '" . $txt_direccion . "', '" . $prioridad_dias . "', '" . $ciudad_origen . "', '" . $ciudad_destino . "', '" . $barrio_destino . "', 'N/A', '" . $centro_costos . "' , '" . $tipo_solicitud . "', '" . $ubicacion . "', '" . $fecha_inicio . "', '" . $fecha_fin . "', NOW(), 'Solicitud Generada', '".$observacion."', '1'); ");
                                        }
                                        echo '<center>Tu Codigo QR es: ' . $qr_generado . '</center><br>';
                                        echo '<center><img src="' . $PNG_WEB_DIR . basename($filename) . '" /><hr/><center>';
                                    } else {
                                    }
                                    ?>
                                    <?php
                                    // nombre del archivo: $filename



                                    //config form

                                    echo '

                            ';/*Codigo:&nbsp;*/
                                    echo '<input class="form-control" name="data" value="' . $id_correponde . '" style="display:none;" />';/* &nbsp; 

                            ECC:&nbsp;*/
                                    echo '<select name="level" style="display:none;">

                            <option value="L"' . (($errorCorrectionLevel == 'L') ? ' selected' : '') . '>L - smallest</option>

                            <option value="M"' . (($errorCorrectionLevel == 'M') ? ' selected' : '') . '>M</option>

                            <option value="Q"' . (($errorCorrectionLevel == 'Q') ? ' selected' : '') . '>Q</option>

                            <option value="H"' . (($errorCorrectionLevel == 'H') ? ' selected' : '') . '>H - best</option>

                            </select>&nbsp;

                            ';/*Size:&nbsp; */
                                    echo '<select name="size" style="display:none;>';



                                    for ($i = 1; $i <= 10; $i++)

                                        echo '<option value="' . $i . '"' . (($matrixPointSize == $i) ? ' selected' : '') . '>' . $i . '</option>';



                                    echo '</select>&nbsp;';

                                    echo '<div class="ocultar_generador">

                            <center>Generar Codigo QR: ' . $id_correponde . '</center><br>

                            <center><input type="submit" id="btn_enviar" class="btn btn-warning" value="Enviar Solicitud"></center></form><hr/>

                            </div>';



                                    // benchmark

                                    // QRtools::timeBenchmark();    

                                    ?>

                                    <div class="otra_solicitud" style="display: none;">
                                        <!-- otra_solicitud -->

                                        <center><input type="button" id="recargar_pagina" class="btn btn-info" value="Realizar Otra Solicitud" /></center>

                                        <br><br><br><br><br>

                                    </div><!-- /otra_solicitud -->

                                    <script type="text/javascript">
                                        $(document).ready(function() {



                                            function recargar_pagina() {



                                                window.location = "form_administrador_solicitud.php";



                                            }

                                            if (select_prioridad == '1') {



                                                $("#dias_p").css('color', 'red');



                                            }

                                            $('#recargar_pagina').click(function() {



                                                recargar_pagina();



                                            });

                                            $('#btn_enviar').click(function() {



                                                $("#mostrar_ocultar_qr").css('display', 'block');



                                            });

                                            $('#select_prioridad').change(function() {



                                                var select_prioridad = $('#select_prioridad').val();



                                                if (select_prioridad == '1') {



                                                    $("#dias_p").css('color', 'red');



                                                } else if (select_prioridad == '2') {



                                                    $("#dias_p").css('color', 'orange');



                                                } else if (select_prioridad == '3') {



                                                    $("#dias_p").css('color', '#E5D837');



                                                } else if (select_prioridad == '4') {



                                                    $("#dias_p").css('color', '#79BD4D');



                                                } else if (select_prioridad == '5') {



                                                    $("#dias_p").css('color', 'green');



                                                } else {



                                                    $("#dias_p").css('border', 'none');

                                                }

                                            });



                                            $('#select_prioridad').change(function() {

                                                dir();

                                            });

                                            $('#fecha_inicio').change(function() {

                                                dir();

                                            });

                                            function dir() {



                                                var fecha_inicial = new Date($('#fecha_inicio').val());

                                                var prioridad = $('#select_prioridad').val();

                                                dia = (fecha_inicial.getDate() + 1);

                                                mes = (fecha_inicial.getMonth() + 1);

                                                anio = fecha_inicial.getFullYear();



                                                var formato_fecha = dia + "-" + mes + "-" + anio;

                                                var fecha = sumaFecha(prioridad, formato_fecha);



                                                document.getElementById("imprimir_fecha_fin").innerHTML = "<input type='date' value='" + fecha + "' name='fecha_fin' id='fecha_fin' class='form-control' placeholder='' readonly>"; // aca usamos innerHTML para agragar el campo con la nueva informacion en tiempo real

                                            };



                                            sumaFecha = function(d, fecha)

                                            {

                                                var Fecha = new Date();

                                                var sFecha = fecha || (Fecha.getDate() + "-" + (Fecha.getMonth() + 1) + "-" + Fecha.getFullYear());

                                                var sep = sFecha.indexOf('-') != -1 ? '-' : '-';

                                                var aFecha = sFecha.split(sep);

                                                var fecha = aFecha[2] + '-' + aFecha[1] + '-' + aFecha[0];

                                                fecha = new Date(fecha);

                                                fecha.setDate(fecha.getDate() + parseInt(d));

                                                var anno = fecha.getFullYear();

                                                var mes = fecha.getMonth() + 1;

                                                var dia = fecha.getDate();

                                                mes = (mes < 10) ? ("0" + mes) : mes;

                                                dia = (dia < 10) ? ("0" + dia) : dia;

                                                var fechaFinal = anno + sep + mes + sep + dia;

                                                return (fechaFinal);

                                            }



                                            $('#select_prioridad').change(function() {



                                                numero_dias();



                                            });



                                            function numero_dias()

                                            {

                                                // EL INPUT 



                                            }

                                            $(document).ready(function() {

                                                const $valueSpan = $('.valueSpan');

                                                const $value = $('#select_prioridad');

                                                $valueSpan.html($value.val() + " D&iacute;as");

                                                $value.on('input change', () => {

                                                    $valueSpan.html($value.val() + " D&iacute;as");

                                                });

                                            });

                                            $('.select2').select2()



                                            //Initialize Select2 Elements

                                            $('.select2bs4').select2({

                                                theme: 'bootstrap4'

                                            })

                                        });
                                    </script>

                                    <script src="../../../DESIGN/CSS/select2/js/select2.full.min.js"></script>

                                </div>

                            </div><!-- /.qr  -->

                        </div>

                        <!-- /.col-md-6 -->

                    </div>

                    <!-- /.row -->

                </div>

                <!-- /.container-fluid -->

            </div>

            <!-- /.content -->

    </div>

    <!-- /.content-wrapper -->



    <!-- Control Sidebar -->

    <aside class="control-sidebar control-sidebar-dark">

        <!-- Control sidebar content goes here -->

    </aside>

    <!-- /.control-sidebar -->



    <!-- Main Footer -->

    <?php require('../../FOOTER/index.php'); ?>

    </div>

    <!-- ./wrapper -->



    <!-- REQUIRED SCRIPTS -->



    <!-- jQuery -->



    <!-- Bootstrap -->

    <script src="../../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- AdminLTE -->

    <script src="../../../DESIGN/JS/principal_js/adminlte.js"></script>



    <!-- OPTIONAL SCRIPTS -->

    <script src="../../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>

    <script src="../../../DESIGN/JS/principal_js/demo.js"></script>

    <script src="../../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>

    </body>



    </html>

<?php

} else {
    echo 'No tiene permisos para ingresar a la informaci&oacute;n';
}

?>