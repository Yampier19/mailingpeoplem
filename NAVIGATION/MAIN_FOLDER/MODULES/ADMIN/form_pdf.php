<?php
header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');
if ($user_name != '' && $id_user != '') {
    $consul_user = mysqli_query($conex, 'SELECT * FROM `userlogin` AS A LEFT JOIN user AS B ON A.id_user = B.id_user  WHERE A.`id_user` = ' . base64_decode($id_user) . '');
    while ($consul = (mysqli_fetch_array($consul_user))) {
        $nombre = $consul['names'];
        $apellido = $consul['surnames'];
        $id_userlog = $consul['id_loginrol'];
    }
    if ($id_userlog == base64_decode($id_loginrol)) {
        //echo 'Bueno';
    }
?>
    <?php
    $boton = 4;
    include('../../DROPDOWN/menu_admin.php');
    ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"> Formulario - Solicitud</h1>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="form_administrador.php">Inicio</a></li>
                            <li class="breadcrumb-item active">Solicitud</li>
                        </ol>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <section class="contact">
            <div class="container-fluid">
                <form method="post">
                    <div class="row">

                    <div class="col-sm-3 form-group">
                        <label>Buscar por:</label>
                        <select name="buscar" id="buscar" class="form-control" style="height: 32px;  padding: 5px;">
                          <option>Seleccione...</option>
                          <option value="reg">Registros</option>
                          <option value="AP">Asignados por Prioridad</option>
                          <option value="PMeEx">Pendientes Mensajero Externo</option>
                          <option value="QR">Por codigo QR</option>
                          
                        </select>
                      </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>Fecha</label>
                                <input type="date" name="fecha_qr" id="fecha_qr" class="form-control form-control-sm">
                            </div>
                        </div>
                      

                        


                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <input type="submit" class="btn btn-block bg-gradient-success btn-sm" name="filtrar" id="filtrar" value="FILTRAR">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <?php
        if (isset($_POST["filtrar"])) {
            $fecha_qr = $_POST["fecha_qr"];
            $buscar = $_POST['buscar'];
        ?>
            <iframe style="border:none" src="document_fpdf.php?fecha_qr=<?php echo $fecha_qr; ?>&&buscar=<?php echo $buscar; ?>" width="100%" height="800px"></iframe>
        <?php
        }
        ?>
    </div>
<?php  require('../../FOOTER/index.php');  ?>
    <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <!-- Bootstrap -->
    <script src="../../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- AdminLTE -->

    <script src="../../../DESIGN/JS/principal_js/adminlte.js"></script>

    <script>
        $(function() {

            $("#example1").DataTable();

            $('#example2').DataTable({

                "paging": true,

                "lengthChange": false,

                "searching": false,

                "ordering": true,

                "info": true,

                "autoWidth": false,

            });

        });
    </script>

    <!-- OPTIONAL SCRIPTS -->

    <script src="../../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>

    <script src="../../../DESIGN/JS/principal_js/demo.js"></script>

    <script src="../../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>

    </body>





    </html>

<?php





} else {
    echo 'No tiene permisos para ingresar a la informaci&oacute;n';
}

?>