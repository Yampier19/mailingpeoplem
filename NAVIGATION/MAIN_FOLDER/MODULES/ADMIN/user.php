<?php
header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');
if ($user_name != '' && $id_user != '') {
    if (isset($_POST['txt_var'])) {  ?>
<script type="text/javascript">
$(document).ready(function() {
    $(".ocultar_formulario").css('display', 'none');
    $(".ocultar_generador").css('display', 'none');
    $(".otra_solicitud").css('display', 'block');
});
</script>
<?php
    }
    $id_users = base64_decode($id_user);
    $consul_user = mysqli_query($conex, 'SELECT * FROM `userlogin` AS A LEFT JOIN user AS B ON A.id_user = B.id_user  WHERE A.`id_user` = ' . base64_decode($id_user) . '');
    while ($consul = (mysqli_fetch_array($consul_user))) {
        $nombre = $consul['names'];
        $apellido = $consul['surnames'];
        $id_userlog = $consul['id_loginrol'];
    }
    if ($id_userlog == base64_decode($id_loginrol)) {
        //echo 'Bueno';
    }
    $boton = 2;
    include('../../DROPDOWN/menu_admin.php');
    ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Usuarios</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <?php
        $select_proxima_gestion = mysqli_query($conex, "SELECT id_shipping FROM shipping ORDER BY id_shipping DESC LIMIT 1; ");
        while ($gestion = (mysqli_fetch_array($select_proxima_gestion))) {
            $id_shipping = $gestion['id_shipping'];
        }
        $id_proxima_shipping = $id_shipping + 1;
        $select_proxima_qr = mysqli_query($conex, "SELECT id_qr FROM qr_generated ORDER BY id_qr DESC LIMIT 1; ");
        while ($qr_nuevo = (mysqli_fetch_array($select_proxima_qr))) {
            $id_qr = $qr_nuevo['id_qr'];
        }
        $id_proximo_generated = $id_qr + 1;
        $usuario = $id_users;
        $DesdeLetra = "a";
        $HastaLetra = "z";
        $letraAleatoria = chr(rand(ord($DesdeLetra), ord($HastaLetra)));
        $id_correponde = $id_proxima_shipping . $letraAleatoria . $usuario . "0";
        $token = "0";
        /// Fecha Actual ///
        date_default_timezone_set("America/Bogota");
        $d      = date('d');
        $mes_nu = date('m');
        $anio    = date('Y');
        $fecha_actual = $d . $mes_nu . $anio;
        ?>
    <script>
    $('#myModal').on('shown.bs.modal', function() {
        $('#myInput').trigger('focus')
    })
    </script>

    <?php
        // $infoUsuarios = mysqli_query($conex, "SELECT * FROM `personal` ");
        // while ($datos = mysqli_fetch_array($infoUsuarios)) {

        //     $nombres = $datos['names'];
        //     $apellidos = $datos['surnames'];
        //     // $contrasena = $datos['password'];
        //     $tipoDocumento = $datos['tipo_documento'];
        //     $numDocumento = $datos['documento'];
        //     // $usuario = $datos['name_user'];
        //     $fechaNacimiento = $datos['fecha_nacimiento'];
        //     $correo = $datos['correo'];
        //     $telefono = $datos['telefono'];
        //     $ciudad = $datos['ciudad'];
        //     $cargo = $datos['cargo'];
        //     $telefonoCor = $datos['telefono_cor'];
        //     $piso = $datos['piso'];
        //     $area = $datos['area'];
        //     $estado = $datos['estado'];
        // }
        ?>
    <!-- Main content -->
    <section class="content">
        <form action="../../../FUNCTIONS/CRUD/insertar_usuarios.php" method="post">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default">
                    <div class="card-header">
                        <h3 class="card-title">Crear Usuarios</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>

                    <!---------------- FORMULARIO ----------------->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nombres <span class="text-danger">*</span></label>
                                    <input type="text" name="nombres" class="form-control"
                                        placeholder="Ejemplo: Laura Daniela">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Apellidos <span class="text-danger">*</span></label>
                                    <input type="text" name="apellidos" class="form-control"
                                        placeholder="Ejemplo: Gonzales Rodriguez">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Documento <span class="text-danger">*</span></label>
                                    <input type="number" name="documento" class="form-control"
                                        placeholder="Ejemplo: 123456789">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Correo <span class="text-danger">*</span></label>
                                    <input class="form-control" id="user" name="correo" oninput="actualizarValoruser()"
                                        placeholder="Ejemplo: prueba@gmail.com" required type="text">
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Usuario <span class="text-danger">*</span></label>
                                    <input class="form-control" id="user_2" name="usuario" type="text" readonly></input>
                                    </input>

                                </div>
                            </div>
                            <script>
                            function actualizarValoruser() {
                                let user = document.getElementById("user").value;

                                document.getElementById("user_2").value = user;
                            }

                            
                                    $("#user").keyup(function() {
                                        var ta = $("#user");
                                        letras = ta.val().replace(/ /g, "");
                                        ta.val(letras)
                                    });
                                    
                            </script>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Contraseña <span class="text-danger">*</span></label>
                                    <input type="text" name="contrasena" class="form-control"
                                        placeholder="Ejemplo: 123456789" value="12345">
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-block btn-success">Crear Nuevo Usuario</button>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                        <!----------------- FIN FORMULARIO ----------------->
                    </div>
                </div>
            </div>
        </form>
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Ver Usuarios</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-striped" id="myTable">
                                <thead class="thead-primary">
                                    <tr>
                                        <th id="conteenido_encabezado4" scope="col">ID</th>
                                        <th id="conteenido_encabezado4" scope="col">Nombre</th>
                                        <th id="conteenido_encabezado4" scope="col">Apellido</th>
                                        <th id="conteenido_encabezado4" scope="col">Usuario</th>
                                        <th id="conteenido_encabezado4" scope="col">Rol</th>
                                        <th id="conteenido_encabezado4" scope="col">Activo</th>
                                        <th id="conteenido_encabezado4" scope="col">Acciones</th>
                                    </tr>
                                </thead>

                                <tbody>




                                    <?php


                                        if (isset($_POST['id_us']) && !empty($_POST['id_us'])) {
                                            $status = (($_POST['status'] == 0) ?  1 :  0);


                                            $actualizar_estado = 'UPDATE userlogin SET activo = ' . $status . ' WHERE id_log = ' . $_POST['id_us'] . '';
                                            $query = mysqli_query($conex, $actualizar_estado);
                                            //  header('Location: ./user.php');
                                        }



                                        $id = null;
                                        $resultado = mysqli_query($conex, "SELECT * FROM personal p INNER JOIN userlogin ul on ul.id_user = p.id_personal WHERE ul.id_loginrol = 6 AND ul.activo <> 2"
                                        );

                                        while ($usuarios = mysqli_fetch_array($resultado)) {
                                            $id = $usuarios['id_personal'];
                                            $nombres = $usuarios['names'];
                                            $apellidos = $usuarios['surnames'];
                                            $correo = $usuarios['correo'];
                                            $rol = $usuarios['id_loginrol'];
                                            $activo = $usuarios['activo'];
                                        ?>



                                    <tr>
                                        <td><?php echo $id; ?></td>
                                        <td><?php echo $nombres; ?></td>
                                        <td><?php echo $apellidos; ?></td>
                                        <td><?php echo $correo; ?></td>
                                        <td><?php echo $rol; ?></td>
                                        <td>
                                            <form action="" id="form<?= $usuarios['id_log'] ?>" method="post">
                                                <input type="hidden" name="id_us" value="<?= $usuarios['id_log'] ?>">
                                                <div
                                                    class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                                    <input id="status<?= $usuarios['id_log'] ?>"
                                                        class="custom-control-input" type="checkbox"
                                                        <?= (($activo == 1) ? ' checked ' : '') ?> />
                                                    <label class="custom-control-label"
                                                        for="status<?= $usuarios['id_log'] ?>"></label>
                                                </div>
                                                <input type="hidden" name="status" value="<?= $activo ?>">
                                                <input type="hidden" id="a" name="a" value="">
                                            </form>





                                        </td>
                                        <td>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <a type="submit"
                                                        href="../../../FUNCTIONS/CRUD/actualizar_usuarios.php?idu=<?= $usuarios['id_personal'] ?>"
                                                        class="btn-primary btn-sm" data-toggle="modal"
                                                        data-target="#update<?= $usuarios['id_personal'] ?>">
                                                        <i class="fas fa-pencil-alt"></i></a>
                                                    </a>
                                                </div>
                                                <a type="submit"
                                                    href="../../../FUNCTIONS/CRUD/eliminar_usuario.php?idu=<?= $usuarios['id_log'] ?>"
                                                    class="btn btn-danger btn-sm" data-toggle="modal"
                                                    data-target="#delete<?= $usuarios['id_log'] ?>">
                                                    <i class="far fa-trash-alt"></i>
                                                </a>

                                            </div>
                                        </td>
                                    </tr>
                                    <script>
                                    $("#status<?= $usuarios['id_log'] ?>").click(function() {
                                        $('#form<?= $usuarios['id_log'] ?>').submit();
                                    });
                                    </script>
                                    <?php }      ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </section>


    <!----------------------------------- Modal Eliminar ------------------------------------->
    <?php
        $id = null;
        $resultado = mysqli_query($conex, "SELECT * FROM personal p
        INNER JOIN userlogin ul on ul.`id_user` = p.id_personal
        WHERE ul.`id_loginrol` = 6 AND ul.activo = 1");
        while ($usuarios = mysqli_fetch_array($resultado)) {
            $id = $usuarios['id_personal'];
            $nombres = $usuarios['names'];
            $apellidos = $usuarios['surnames'];
            $correo = $usuarios['correo'];
            $name_user = $usuarios['name_user'];
            $tipoDocumento = $usuarios['tipo_documento'];
            $numDocumento = $usuarios['documento'];
            $fechaNacimiento = $usuarios['fecha_nacimiento'];
            $correo = $usuarios['correo'];
            $telefono = $usuarios['telefono'];
            $ciudad = $usuarios['ciudad'];
            $cargo = $usuarios['cargo'];
            $telefonoCor = $usuarios['telefono_cor'];
            $piso = $usuarios['piso'];
            $area = $usuarios['area'];
            $oficina = $usuarios['oficina'];
        ?>
    <div class="modal fade" id="delete<?= $usuarios['id_log'] ?>" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><b>Eliminar Usuario:</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">Esta seguro que desea eliminar el registro...</div>
                        <div class="col-md-2"></div>
                    </div>
                    <div class="row mx-auto">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 mx-auto"><?php echo $usuarios['names'] . ' ' . $usuarios['surnames']; ?><i
                                class="ml-1 far fa-trash-alt"></i></div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

                    <a href="../../../FUNCTIONS/CRUD/eliminar_usuario.php?idu=<?= $usuarios['id_log'] ?>" type="submit"
                        class="btn btn-success" name="eliminarU" value="Confirmar">Eliminar</a>

                </div>
            </div>
        </div>
    </div>

    <!------------------------------------ Fin Modal Eliminar ---------------------------------->

    <!------------------------------------ Modal Actualizar ------------------------------------>
    <div class="modal fade bd-example-modal-lg" id="update<?= $usuarios['id_personal'] ?>" tabindex="-1" role="dialog"
        aria-labelledby="FormModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><b>Actualizar Usuario:
                            <?php echo $nombres . " " . $apellidos; ?></b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <form
                                    action="../../../FUNCTIONS/CRUD/actualizar_usuarios.php?idu=<?= $usuarios['id_personal'] ?>"
                                    method="post">
                                    <div class="form-group">
                                        <label>Nombres:</label>
                                        <input type="text" name="nombres" class="form-control"
                                            value="<?php echo $nombres; ?>">
                                    </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Apellidos:</label>
                                    <input type="text" name="apellidos" class="form-control"
                                        value="<?php echo $apellidos; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tipo de Documento:</label>
                                    <input type="text" name="tipo_documento" class="form-control"
                                        value="<?php echo $tipoDocumento; ?>">
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>No. de Documento:</label>
                                    <input type="number" name="noDocumento" class="form-control"
                                        value="<?php echo $numDocumento; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fecha de Nacimiento:</label>
                                    <input type="date" name="fechaNacimiento" class="form-control"
                                        value="<?php echo $fechaNacimiento; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Correo:</label>
                                    <input type="email" name="correo" class="form-control"
                                        value="<?php echo $correo; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Telefono:</label>
                                    <input type="number" name="telefono" class="form-control"
                                        value="<?php echo $telefono; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Ciudad:</label>
                                    <input type="text" name="ciudad" class="form-control"
                                        value="<?php echo $ciudad; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Cargo:</label>
                                    <input type="text" name="cargo" class="form-control" value="<?php echo $cargo; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Telefono Corporativo:</label>
                                    <input type="number" name="telCorporativo" class="form-control"
                                        value="<?php echo $telefonoCor; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Piso:</label>
                                    <input type="text" name="piso" class="form-control" value="<?php echo $piso; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Area:</label>
                                    <input type="text" name="area" class="form-control" value="<?php echo $area; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Oficina:</label>
                                    <input type="text" name="oficina" class="form-control"
                                        value="<?php echo $oficina; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Estado:</label>
                                    <select class="form-control" name="estado" id="">
                                        <option value="0">Desactivo</option>
                                        <option value="1">Activado</option>
                                        <option value="2">Inactivo</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Usuario:</label>
                                    <input type="text" name="name_user" class="form-control"
                                        value="<?php echo $name_user; ?>">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Contraseña:</label>
                                    <input type="password" name="contrasena" class="form-control"
                                        value="<?php echo ""; ?>">
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <button type="submit" name="submit" class="btn btn-success" value="Confirmar">Actualizar
                                    usuario</button>
                            </div>
                            <div class="col-md-4"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }  ?>
    <!------------------------------------ Fin Modal Actualizar ---------------------------------->

</div> <?php require('../../FOOTER/index.php'); ?> </div>

<!-- ./wrapper -->



<!-- REQUIRED SCRIPTS -->



<!--  -->




<script src="../../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- AdminLTE -->

<script src="../../../DESIGN/JS/principal_js/adminlte.js"></script>




<script src="../../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>

<script src="../../../DESIGN/JS/principal_js/demo.js"></script>

<script src="../../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>

<script src="../../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="../../../DESIGN/JS/principal_js/adminlte.js"></script>
<!-- OPTIONAL SCRIPTS -->
<script src="../../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>
<script src="../../../DESIGN/JS/principal_js/demo.js"></script>
<script src="../../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>
</body>



</html>

<?php

} else {
    echo 'No tiene permisos para ingresar a la informaci&oacute;n';
}

?>