<?php
header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');
if ($user_name != '' && $id_user != '') {
    $consul_user = mysqli_query($conex, 'SELECT * FROM `userlogin` AS A LEFT JOIN user AS B ON A.id_user = B.id_user  WHERE A.`id_user` = ' . $id_user . '');
    while ($consul = (mysqli_fetch_array($consul_user))) {
        $nombre = $consul['names'];
        $apellido = $consul['surnames'];
    }
    include('../DROPDOWN/menu_received_internal.php');
?>
    <style>
        :not(.layout-fixed) .main-sidebar {
            height: inherit;
            min-height: 110%;
            position: absolute;
            top: 0;
        }

        body {
            margin: 0;
            font-family: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color:  !important;
            text-align: left;
            background-color: none !important;
        }
    </style>
    <script type="text/javascript">
        function ajax_formulario_solicitud() { // ajax_formulario_solicitud 
            var codigo_manual = $('#codigo_manual').val();
            var tipo_gest = $('#tipo_gest').val();
            $.ajax({
                url: '../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_received_consultar.php',
                data: {
                    codigo_manual: codigo_manual,
                    tipo_gest: tipo_gest
                },
                type: 'post',
                beforesend: function() {

                },
                success: function(data) {
                    $('#retur_ajax_solicitud').html(data);
                }

            });

        }

        $(document).ready(function() {
            $('#btn_consultar').click(function() {
                ajax_formulario_solicitud();
            });

            $('#ingreso_qr').change(function() {
                var ingreso_qr = $('#ingreso_qr').val();
                if (ingreso_qr == "Manualmente") {
                    $("#tipo_gestion").css('display', 'block');

                } else if (ingreso_qr == "") {
                    $("#tipo_gestion").css('display', 'none');
                    $("#gestion").css('display', 'none');
                    $("#mostrar_resibir").css('display', 'none');
                    $("#mostrar_entregar").css('display', 'none');
                }
            });
            $('#tipo_gest').change(function() {
                var tipo_gest = $('#tipo_gest').val();
                if (tipo_gest == "Resibir") {
                    $("#gestion").css('display', 'block');
                    $("#mostrar_resibir").css('display', 'block');
                    $("#mostrar_entregar").css('display', 'none');
                } else if (tipo_gest == "Entregar") {
                    $("#gestion").css('display', 'block');
                    $("#mostrar_resibir").css('display', 'none');
                    $("#mostrar_entregar").css('display', 'block');
                }
            });
        });
    </script>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" id="cambiar">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"> Resivir / Entregar</h1>
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Resivir /</a></li>
                            <li class="breadcrumb-item active">Entregar</li>
                        </ol>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <br>
                        <select name="ingreso_qr" id="ingreso_qr" class="form-control">
                            <option value="">Selecionar...</option>
                            <option value="Manualmente">Manualmente</option>
                            <option value="" disabled>Scanner-QR</option>
                        </select>
                    </div>

                    <div class="col-md-4">
                    </div>
                </div>
                <div class="tipo_gestion" id="tipo_gestion" style="display:none;">
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <br>
                            <select name="tipo_gest" id="tipo_gest" class="form-control">
                                <option value="">Selecionar...</option>
                                <option value="Resibir">Resibir</option>
                                <option value="Entregar">Entregar</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                </div>
                <div id="gestion" style="display: none;">
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <div id="mostrar_resibir" style="display: none;">
                                <br>
                                <center>
                                    <h5>Resibir</h5>
                                </center>
                            </div>
                            <div id="mostrar_entregar" style="display: none;">
                                <br>
                                <center>
                                    <h5>Entregar</h5>
                                </center>
                            </div>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">
                            <label for="codigo_manual">Escribe El Codigo </label>
                            <input type="text" name="codigo_manual" id="codigo_manual" placeholder="Codigo" class="form-control">
                            <br><br>
                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                        </div>
                        <div class="col-md-4">
                            <button type="button" id="btn_consultar" class="btn btn-info btn-block">Consultar</button>
                            <br><br>
                        </div>
                        <div class="col-md-4">
                        </div>
                    </div>
                </div>
            </div>
            <div id="retur_ajax_solicitud"></div><!--  retorna formulario de la  solicitud-->
        </div>
    </div>
    <!-- /.content-wrapper -->
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    <!-- Main Footer -->
    <?php require('../FOOTER/index.php'); ?>
    </div>
    <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <!-- Bootstrap -->
    <script src="../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE -->
    <script src="../../DESIGN/JS/principal_js/adminlte.js"></script>
    <!-- OPTIONAL SCRIPTS -->
    <script src="../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>
    <script src="../../DESIGN/JS/principal_js/demo.js"></script>
    <script src="../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>

    </body>

    </html>
<?php
} else {
    echo 'No tiene permisos para ingresar a la informaci&oacute;n';
}
