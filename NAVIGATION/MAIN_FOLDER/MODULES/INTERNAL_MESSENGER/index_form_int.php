<?php
header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');
if ($user_name != '' && $id_user != '') {
    $consul_user = mysqli_query($conex, 'SELECT * FROM `userlogin` AS A LEFT JOIN user AS B ON A.id_user = B.id_user  WHERE A.`id_user` = ' . base64_decode($id_user) . '');
    while ($consul = (mysqli_fetch_array($consul_user))) {
        $nombre = $consul['names'];
        $apellido = $consul['surnames'];
        $id_userlog = $consul['id_loginrol'];
   }
    if ($id_userlog == base64_decode($id_loginrol)) {
        //echo 'Bueno';
    }
    $boton = 1;
    include('../../DROPDOWN/menu_internal_messenger.php');
    ?>
    <style>
       .form-controlb {
           display: block;
            width: 100%;
            /* height: calc(2.25rem + 2px); */
            /* padding: .375rem .75rem; */
            /* font-size: 1rem; */
            /* font-weight: 400; */
            /* line-height: 1.5; */
            color: #261072;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            box-shadow: inset 0 0 0 transparent;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }
        .table td,
        .table th {
            vertical-align: top;
            padding: 0.40rem;
            border-top: 1px solid rgb(222, 226, 230);
        }
    </style>
    <script>
        $(document).ready(function() {
            function bandeja_corrFu() {
                var estado = "1"; //solo usa para activar el ajax
                $.ajax(
                    {
                        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_asignacion_men_int.php',
                        data:
                        {
                           estado: estado
                        },
                        type: 'post',
                        beforesend: function() {
                        },
                        success: function(data)
                        {
                            $('#variable').html(data);
                        }
                    });
            }
            function Pool_corrFu() {
                var estado = "2"; //solo usa para activar el ajax
                $.ajax(
                    {
                        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_asignacion_men_int.php',
                        data:
                        {
                            estado: estado
                        },
                        type: 'post',
                        beforesend: function() {
                        },
                        success: function(data)
                        {
                            $('#variable').html(data);
                        }
                    });
            }
            $("#bandeja_corr").click(function() {
                bandeja_corrFu();
            });
            $("#Pool_corr").click(function() {
                Pool_corrFu();
            });
        });
    </script>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
       <!-- Content Header (Page header) -->
        <div class="content-header">
           <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Lista de Asignaciones</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <!--<li class="breadcrumb-item"><a href="#">Inicio</a></li>-->
                            <li class="breadcrumb-item active">Inicio</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <section class="content">
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <a class="btn btn-app" <?php if (isset($_GET['click']) == '1') {
                                                       echo 'style="background-color: #d81b60; color:#FFFFFF"';
                                                    } else {
                                                        echo 'style="color:#d81b60"';
                                                   } ?> id="" href="index_form_int.php?click=1">
                                <span class="badge bg-teal"><span id="bandeja_not_aj">11</span></span>
                                <i class="fas fa-inbox"></i> Bandeja
                            </a>

                            <a class="btn btn-app" <?php if (isset($_GET['click2']) == '2') {
                                                        echo 'style="background-color: #3d9970; color:#FFFFFF"';
                                                    } else {
                                                        echo 'style="color:#3d9970;"';
                                                    } ?> id="" href="index_form_int.php?click2=2">
                                <span class="badge bg-teal"><span id="pool_not_aj">1</span></span>
                                <i class="fas fa-boxes"></i> Pool
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <span id='variable'> </span>
            <?php if (isset($_GET['click'])  == '1') {  ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header" style="background-color:#b9104e; color:#FFFFFF">
                                <h3 class="card-title">Asignaciones Bandeja de Correspondencia</h3>
                            </div>
                            <div class="card-body">
                                <form method="post" id="form" action="../../../FUNCTIONS/CRUD/ajax_bandeja_mensj_inter.php">
                                    <table id="myTable" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Codigo<?= base64_decode($id_user); ?></th>
                                                <th>Producto</th>
                                                <th>Recibir</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                            $select_asig = mysqli_query($conex, "SELECT * FROM `point_courier` AS A LEFT JOIN shipping AS B ON A.`id_ship` = B.id_shipping LEFT JOIN qr_generated AS C ON B.id_generate = C.id_qr WHERE A.id_mensajero = '". base64_decode($id_user)."' AND A.estado = '1'");

                                            while ($dato_asig = mysqli_fetch_array($select_asig)) {   ?>
                                                <tr>
                                                    <td><?php echo $dato_asig['id_qr_generado']; ?></td>
                                                    <td><?php echo $dato_asig['tipo_solicitud']; ?> <input name="IdUser" id="IdUser" type="hidden" value="<?php echo $id_user; ?>" /></td>
                                                    <td>
                                                        <center>
                                                           <div class="icheck-success d-inline">
                                                                <input type="checkbox" name="IdShipping[]" id="<?php echo $dato_asig['id_shipping']; ?>" value="<?php echo $dato_asig['id_shipping']; ?>">
                                                                <label for="<?php echo $dato_asig['id_shipping']; ?>">
                                                                </label>
                                                            </div>
                                                        </center>
                                                    </td>
                                               </tr>
                                           <?php } ?>
                                        </tbody>
                                    </table>
                                    <br>
                                    <button type="submit" id="RecibirB" name="RecibirB" class="btn btn-block btn-success">Recibir</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php  } elseif (isset($_GET['click2'])  == '2') {  ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header" style="background-color:#3d9970; color:#FFFFFF">
                                <h3 class="card-title">Asignaciones Pool Correspondencia</h3>
                            </div>
                            <div class="card-body">
                                <form method="post" id="" name="" action="">
                                    <table id="myTable" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Codigo</th>
                                                <th>Producto</th>
                                                <th>Recibir</th>
                                           </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $select_asig = mysqli_query($conex, "SELECT * FROM `office_courier` AS A LEFT JOIN shipping AS B ON A.`id_ship` = B.id_shipping LEFT JOIN qr_generated AS C ON B.id_generate = C.id_qr WHERE A.id_mensajero = '" . base64_decode($id_user) . "' AND A.estado = '1'");
                                            while ($dato_asig = mysqli_fetch_array($select_asig)) {  ?>
                                                <tr>
                                                    <td><?php echo $dato_asig['id_qr_generado']; ?></td>
                                                    <td><?php echo $dato_asig['tipo_solicitud']; ?></td>
                                                    <td>
                                                        <center>
                                                            <div class="icheck-success d-inline">
                                                                <input type="checkbox" name="activacion[]" id="<?php echo $dato_asig['id_shipping']; ?>" value="<?php echo $dato_asig['id_shipping']; ?>">
                                                                <label for="<?php echo $dato_asig['id_shipping']; ?>">
                                                                </label>
                                                            </div>
                                                        </center>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <br>
                                    <button type="submit" id="AsignarB" name="AsignarB" class="btn btn-block btn-success">Recibir</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }   ?>
        </section>
    </div>
    <!-- /.content-wrapper -->
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    <!-- Main Footer -->
    <?php require('../../FOOTER/index.php'); ?>
    </div>
   <!-- ./wrapper -->
    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <!-- Bootstrap -->
    <script src="../../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE -->
    <script src="../../../DESIGN/JS/principal_js/adminlte.js"></script>
    <script>
        $(function() {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
            });
        });
    </script>

   <!-- OPTIONAL SCRIPTS -->
    <script src="../../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>
    <script src="../../../DESIGN/JS/principal_js/demo.js"></script>
    <script src="../../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>
    </body>
    </html>
<?php } else {  echo 'No tiene permisos para ingresar a la informaci&oacute;n'; } 