<?php

header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');


if ($user_name != '' && $id_user != '') {



$boton = 4;



$id_users = base64_decode($id_user);



include ('../../DROPDOWN/menu_request.php');



// consultas 

$select_informacion_general=mysqli_query($conex,"SELECT A.`names` AS names, A.`surnames` AS surnames, A.`fecha_nacimiento` AS fecha_nacimiento, A.`correo` AS correo, A.`telefono` AS telefono, A.`ciudad` AS ciudad, A.`foto` AS foto_profile FROM personal AS A WHERE id_personal = '".$id_users."';");



while ($info_general =(mysqli_fetch_array($select_informacion_general))){

$nombres = $info_general['names'];

$apellidos = $info_general['surnames'];

$fecha_nacimiento = $info_general['fecha_nacimiento'];

$correo = $info_general['correo'];

$telefono = $info_general['telefono'];

$ciudad = $info_general['ciudad'];

$foto_profile = $info_general['foto_profile'];

}        



$select_informacion_oficina=mysqli_query($conex,"SELECT A.`cargo` AS cargo, A.`telefono_cor` AS telefono_cor, A.`piso` AS piso, A.`area` AS `area`, A.`oficina` AS oficina FROM personal AS A WHERE id_personal='".$id_users."';");



while ($info_oficina =(mysqli_fetch_array($select_informacion_oficina))) {



$cargo = $info_oficina['cargo'];

$telefono_cor = $info_oficina['telefono_cor'];

$piso = $info_oficina['piso'];

$area = $info_oficina['area'];

$oficina = $info_oficina['oficina'];



}   



$select_shipping=mysqli_query($conex,"SELECT * FROM shipping WHERE id_personal = '".$id_users."';");

$count_shipping = mysqli_num_rows($select_shipping);



$select_delivery=mysqli_query($conex,"SELECT * FROM delivery WHERE id_user = '".$id_users."';");

$count_delivery = mysqli_num_rows($select_delivery);

?>



<div class="content-wrapper">

    <!-- Content Header (Page header) -->

    <section class="content-header">

        <div class="container-fluid">

            <div class="row mb-2">

                <div class="col-sm-6">

                    <h1>Profile</h1>

                </div>

                <div class="col-sm-6">

                    <ol class="breadcrumb float-sm-right">

                        <li class="breadcrumb-item"><a href="#">Home</a></li>

                        <li class="breadcrumb-item active">User Profile</li>

                    </ol>

                </div>

            </div>

        </div><!-- /.container-fluid -->

    </section>



    <!-- Main content -->

    <section class="content">

        <div class="container-fluid">

            <div class="row">

                <div class="col-md-3">



                    <!-- Profile Image -->

                    <?php

                    $imagen = "../../FILES/PHOTOS_PROFILE/$foto_profile";

                    ?>

                    <div class="card card-info card-outline">

                        <div class="card-body box-profile">

                            <div class="text-center">

                                <img class="profile-user-img img-fluid img-circle"

                                     src="<?php echo $imagen; ?>"

                                     alt="User profile picture"

                                     width="128" height="128">

                            </div>



                            <h3 class="profile-username text-center"><?php echo $nombres." ".$apellidos ?></h3>



                            <p class="text-muted text-center"><?php echo $cargo ?></p>



                            <ul class="list-group list-group-unbordered mb-3">

                                <li class="list-group-item">

                                    <b>Solicitudes</b> <a class="float-right"><?php echo $count_shipping; ?></a>

                                </li>

                                <li class="list-group-item">

                                    <b>Entregas</b> <a class="float-right"><?php echo $count_delivery ?></a>

                                </li> 

                            </ul>



                        </div>

                        <!-- /.card-body -->

                    </div>

                    <!-- /.card -->

                </div>

                <!-- /.col -->

                <div class="col-md-9">

                    <div class="card">

                        <div class="card-header p-2">

                            <ul class="nav nav-pills">

                                <li class="nav-item"><a class="nav-link active" id="infoGeneral" href="#update_general" data-toggle="tab">Informacion General</a></li>

                                <li class="nav-item"><a class="nav-link" id="actualizarInfoGeneral" href="#" data-toggle="tab"><i class="fas fa-pencil-alt"></i></a></li>

                                <li class="nav-item" id="div_boton"></li>    

                                <li class="nav-item"><a class="nav-link" id="infoLaboral" href="#update_oficina" data-toggle="tab">Informacion Laboral</a></li>

                                <li class="nav-item"><a class="nav-link" id="actualizarInfoLaboral" href="#" data-toggle="tab"><i class="fas fa-pencil-alt"></i></a></li>

                                <li class="nav-item" id="div_boton"></li> 

                                <li class="nav-item"><a class="nav-link" href="#photo" data-toggle="tab"><i class="fas fa-camera-retro"></i></a></li>

                                <li class="nav-item" id="div_boton"></li> 

                                <li class="nav-item"><a class="nav-link" href="#" data-toggle="tab" id="recargar" onclick="location.reload()"><i class="fas fa-spinner"></i></a></li>

                            </ul>

                        </div><!-- /.card-header -->

                        <div class="card-body">

                            <div class="tab-content">

                                <div class="active tab-pane" id="update_general">

                                    <form class="form-horizontal">

                                        <div class="form-group row">

                                            <div class="col-sm-6">

                                                <label for="txtNombres">Nombres <span class="asterisco_obligatorio">*</span></label>

                                                <input type="text" class="form-control" id="txtNombres" value="<?php echo $nombres; ?>" placeholder="Nombres" disabled>

                                            </div>

                                            <div class="col-sm-6">

                                                <label for="txtApellidos">Apellidos <span class="asterisco_obligatorio">*</span></label>

                                                <input type="text" class="form-control" id="txtApellidos" value="<?php echo $apellidos; ?>" placeholder="Apellidos" disabled>

                                            </div>

                                        </div>

                                        <div class="form-group row"> 

                                            <div class="col-sm-6">

                                                <label for="txtEmail">Email <span class="asterisco_obligatorio">*</span></label>

                                                <input type="email" class="form-control" id="txtEmail" value="<?php echo $correo; ?>" placeholder="Email" disabled>

                                            </div>

                                            <div class="col-sm-6">

                                                <label for="txtTelefono">Telefono <span class="asterisco_obligatorio">*</span></label>

                                                <input type="text" class="form-control" id="txtTelefono" value="<?php echo $telefono; ?>" placeholder="Telefono" disabled>

                                            </div>

                                        </div>

                                        <div class="form-group row">

                                            <div class="col-sm-6">

                                                <label for="txtCiudad">Ciudad <span class="asterisco_obligatorio">*</span></label>

                                                <input type="text" class="form-control" id="txtCiudad" value="<?php echo $ciudad; ?>" placeholder="Ciudad" disabled>

                                            </div>

                                            <div class="col-sm-6">

                                                <label for="txtFechaNac">Fecha Nacimiento <span class="asterisco_obligatorio">*</span></label>

                                                <input type="date" class="form-control" id="txtFechaNac" value="<?php echo $fecha_nacimiento; ?>" placeholder="Fecha Nacimiento" disabled>

                                            </div>

                                        </div>

                                        <div class="form-group row" id="dataPerson" style="display:none;"> 

                                            <div class="col-sm-6">

                                                <label for="txtTipoDoc">Tipo Documento <span class="asterisco_obligatorio">*</span></label>

                                                <select class="form-control" name="txtTipoDoc" id="txtTipoDoc" required="required" disabled>   

                                                    <option value="">SELECCIONAR...</option>

                                                    <option value="CC">C&eacute;dula de Ciudadan&iacute;a</option>

                                                    <option value="TI">Tarjeta de Identidad</option>

                                                    <option value="CE">C&eacute;dula de Extranjer&iacute;a</option>

                                                    <option value="PS">Pasaporte</option>

                                                </select> 

                                            </div>

                                            <div class="col-sm-6">

                                                <label for="numeroDocumento">Documento <span class="asterisco_obligatorio">*</span></label>

                                                <input type="text" class="form-control" id="numeroDocumento" placeholder="Documento" disabled>

                                            </div>

                                        </div>

                                        <br>

                                            <div class="form-group row">

                                                <div class="col-sm-6">

                                                    <button type="button" id="ActualizarInformacion" class="btn btn-outline-info btn-block" disabled>Actualizar</button>

                                                </div>

                                            </div>

                                            <div class="form-group row">

                                                <div id="returnFuctionUpdateGeneral"></div>

                                            </div>



                                    </form>

                                </div>

                                <!-- /.tab-pane -->

                                <div class="tab-pane" id="update_oficina">

                                    <form class="form-horizontal">

                                        <div class="form-group row">

                                            <div class="col-sm-6">

                                                <label>Cargo <span class="asterisco_obligatorio">*</span></label>

                                                <input type="text" class="form-control" id="txtCargo" value="<?php echo $cargo; ?>" placeholder="Cargo" disabled>

                                            </div>

                                            <div class="col-sm-6">

                                                <label for="txtTelefonoCor">Telefono <span class="asterisco_obligatorio">*</span></label>

                                                <input type="text" class="form-control" id="txtTelefonoCor" value="<?php echo $telefono_cor; ?>" placeholder="Telefono" disabled>

                                            </div>

                                        </div>

                                        <div class="form-group row">

                                            <div class="col-sm-4">

                                                <label for="txtPiso">Piso <span class="asterisco_obligatorio">*</span></label>

                                                <input type="text" class="form-control" id="txtPiso" value="<?php echo $piso; ?>" placeholder="Piso" disabled>

                                            </div>

                                            <div class="col-sm-4">

                                                <label for="txtArea">Area <span class="asterisco_obligatorio">*</span></label>

                                                <input type="text" class="form-control" id="txtArea" value="<?php echo $area; ?>" placeholder="Area" disabled>

                                            </div>

                                            <div class="col-sm-4">

                                                <label for="txtOficina">Oficina <span class="asterisco_obligatorio">*</span></label>

                                                <input type="text" class="form-control" id="txtOficina" value="<?php echo $oficina; ?>" placeholder="Oficina" disabled>

                                            </div>

                                        </div>

                                        <br>

                                            <div class="form-group row"> 

                                                <div class="col-sm-6">

                                                    <button type="button" id="ActualizarOficina" class="btn btn-outline-info btn-block" disabled>Actualizar</button>

                                                </div>

                                            </div>

                                            <div class="form-group row">

                                                <div id="retunFunctionUpdateLaboral"></div>

                                            </div>

                                    </form>

                                </div>

                                <!-- /.tab-pane -->

                                <!-- .tab-pane -->

                                <div class="tab-pane" id="photo">

                                    <form class="form-horizontal" method="post" name="form" enctype="multipart/form-data">

                                        <div class="form-group row">

                                          <div class="col-sm-4">

                                            <div class="text-center">

                                                <div class="">

                                                    <center>

                                                    <span for="imgSalida">

                                                        <img class="profile-user-img img-fluid img-circle"

                                                             id="imgSalida"

                                                             src="<?php echo $imagen; ?>"

                                                             alt="User profile picture"

                                                             width="128" height="128">

                                                    </span>

                                                    </center>

                                                </div> 

                                            </div> 

                                            </div>

                                              <div class="col-sm-6">

                                                <div class="custom-file">

                                                  <input name="ZmMyZTQ5NWMyN2Q2ODE1NDRhNmIxYjc5NWI5ZWM4MzQ=" id="customFile" type="file" size="15" class="custom-file-input">

                                                  <label class="custom-file-label" for="customFile">Adjuntar Imagen</label>

                                                </div>

                                              </div>

                                              <div class="col-sm-2">

                                                <input value="Guardar" type="submit" id="insertar" name="insertar" class="btn btn-info">

                                              </div>

                                            

                                        </div>

                                    </form>



                                    <?php 



                                    if(isset($_POST['insertar'])) {

                                    if ($_POST['insertar']) {

                                    // obtenemos los datos del archivo

                                    $tamano = $_FILES["ZmMyZTQ5NWMyN2Q2ODE1NDRhNmIxYjc5NWI5ZWM4MzQ="]['size'];

                                    $tipo = $_FILES["ZmMyZTQ5NWMyN2Q2ODE1NDRhNmIxYjc5NWI5ZWM4MzQ="]['type'];

                                    $archivo1 = $_FILES["ZmMyZTQ5NWMyN2Q2ODE1NDRhNmIxYjc5NWI5ZWM4MzQ="]['name'];

                                    $archivo1 = str_replace(" ","_",$archivo1);

                                    $prefijo1 = "../../FILES/PHOTOS_PROFILE/";

                                    $basicEncrip = substr(md5(uniqid(rand())),0 ,3);

                                    $prefijo2 = $prefijo1.$basicEncrip;

                                    $nombreDefault = $basicEncrip."_".$archivo1;

                                    if ($archivo1 != "") {

                                    // guardamos el archivo a la carpeta files

                                    $destino =  "".$prefijo2."_".$archivo1;



                                    if (copy($_FILES['ZmMyZTQ5NWMyN2Q2ODE1NDRhNmIxYjc5NWI5ZWM4MzQ=']['tmp_name'],$destino)) {

                                    $update_photo=mysqli_query($conex,"UPDATE personal SET foto = '$nombreDefault' WHERE id_personal = '$id_users' ;");

                                    } else {

                                    $status = "Error al subir el archivo";

                                    }

                                    } else {

                                    $status = "Error al subir archivo";

                                    }



                                    if ($archivo1 != "") {



                                    // echo " El Archivo N&deg; 1 con nombre: ".$archivo1." Fue subido con exito<br>";  



                                    }else {

                                    echo "";

                                    }



                                    }



                                    echo $status; ?>



                                    <?php 

                                    if ($archivo1 != ""){



                                    $enlace1 = $archivo1."";

                                    $nombre1 = "".$archivo1."";



                                    }else {

                                    $enlace1 = "";

                                    $nombre1 = "";



                                    }

                                    echo $enlace1;



                                    }



                                    ?>

                                </div>

                                <!-- /.tab-pane -->

                            </div><!-- /.card-body -->

                        </div>

                        <!-- /.nav-tabs-custom -->

                    </div>

                    <!-- /.col -->

                </div>

                <!-- /.row -->

            </div><!-- /.container-fluid -->

    </section>

    <!-- /.content -->

</div>

<aside class="control-sidebar control-sidebar-dark">

    <!-- Control sidebar content goes here -->

</aside>

<!-- /.control-sidebar -->



<!-- Main Footer -->

<?php require('../../FOOTER/index.php'); ?>

</div>

<!-- ./wrapper -->



<!-- REQUIRED SCRIPTS -->



<!-- jQuery -->



<!-- Bootstrap -->

<script src="../../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- AdminLTE -->

<script src="../../../DESIGN/JS/principal_js/adminlte.js"></script>



<!-- OPTIONAL SCRIPTS -->

<script src="../../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>

<script src="../../../DESIGN/JS/principal_js/demo.js"></script>

<script src="../../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>

</body>

<!-- Ajax -->

<script type="text/javascript">

    function actualizar()

    {

        location.reload(true);

    }

    function actualizarAlerta()

    {

        Swal.fire({

            position: 'top-end',

            icon: 'success',

            title: 'Actualizado',

            showConfirmButton: false,

            timer: 1500

        })

        setInterval("actualizar()", 2500);

    }

    function alert2normal(titulo, contenido, icon)

    {

        Swal.fire(

                titulo,

                contenido,

                icon

                )

    }

    function fuctionUpdateGeneral()

    {

        var tipo = "1";

        var nombres = $('#txtNombres').val();

        var apellidos = $('#txtApellidos').val();

        var email = $('#txtEmail').val();

        var telefono = $('#txtTelefono').val();

        var ciudad = $('#txtCiudad').val();

        var fechaNac = $('#txtFechaNac').val();

        var tipoDoc = $('#txtTipoDoc').val();

        var documento1 = $('#numeroDocumento').val();



        $.ajax(

                {



                    url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_crud_profile.php',



                    data:

                            {

                                tipo: tipo,

                                nombres: nombres,

                                apellidos: apellidos,

                                email: email,

                                telefono: telefono,

                                ciudad: ciudad,

                                fechaNac: fechaNac,

                                tipoDoc: tipoDoc,

                                documento1: documento1

                            },



                    type: 'post',



                    beforesend: function ()

                    {



                    },



                    success: function (data)

                    {

                        $('#returnFuctionUpdateGeneral').html(data);



                        actualizarAlerta();

                    }

                });

    }



    function functionUpdateLaboral()

    {

        var tipo = "2";

        var cargo = $('#txtCargo').val();

        var telefonoCor = $('#txtTelefonoCor').val();

        var piso = $('#txtPiso').val();

        var area = $('#txtArea').val();

        var oficina = $('#txtOficina').val();



        $.ajax(

                {



                    url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_crud_profile.php',



                    data:

                            {

                                tipo: tipo,

                                cargo: cargo,

                                telefonoCor: telefonoCor,

                                piso: piso,

                                area: area,

                                oficina: oficina

                            },



                    type: 'post',



                    beforesend: function ()

                    {



                    },



                    success: function (data)

                    {

                        $('#retunFunctionUpdateLaboral').html(data);



                        actualizarAlerta();



                    }

                });

    }



    function validacionInformacionGeneral()

    {

        var nombres = $('#txtNombres').val();

        var apellidos = $('#txtApellidos').val();

        var email = $('#txtEmail').val();

        var telefono = $('#txtTelefono').val();

        var ciudad = $('#txtCiudad').val();

        var fechaNac = $('#txtFechaNac').val();

        var tipoDoc = $('#txtTipoDoc').val();

        var documento1 = $('#numeroDocumento').val();



        if (nombres === "") {



            var titulo = "Error";

            var contenido = "El campo nombres no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        } else if (apellidos === "") {



            var titulo = "Error";

            var contenido = "El campo apellidos no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        } else if (email === "") {

            var titulo = "Error";

            var contenido = "El campo email no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        } else if (telefono === "") {

            var titulo = "Error";

            var contenido = "El campo telefono no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        } else if (ciudad === "") {

            var titulo = "Error";

            var contenido = "El campo ciudad no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        } else if (fechaNac === "") {

            var titulo = "Error";

            var contenido = "El campo fecha de nacimiento no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        } else if (tipoDoc === "") {

            var titulo = "Error";

            var contenido = "El campo tipo documento no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        } else if (documento1 === "") {

            var titulo = "Error";

            var contenido = "El campo documento no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        }

        return true;

    }



    function validacionInformacionLaboral()

    {

        var cargo = $('#txtCargo').val();

        var telefonoCor = $('#txtTelefonoCor').val();

        var piso = $('#txtPiso').val();

        var area = $('#txtArea').val();

        var oficina = $('#txtOficina').val();



        if (cargo === "") {



            var titulo = "Error";

            var contenido = "El campo cargo no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        } else if (telefonoCor === "") {



            var titulo = "Error";

            var contenido = "El campo telefono no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        } else if (piso === "") {



            var titulo = "Error";

            var contenido = "El campo piso no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        } else if (area === "") {



            var titulo = "Error";

            var contenido = "El campo area no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);

            return false;



        } else if (oficina === "") {



            var titulo = "Error";

            var contenido = "El campo oficina no puede estar vacio";

            var icon = "error";



            alert2normal(titulo, contenido, icon);



            return false;

        }

        return true;

    }



    $(document).ready(function ()

    {



        $('#ActualizarInformacion').click(function ()

        {

            var validacion = validacionInformacionGeneral();

            if (validacion) {

                fuctionUpdateGeneral();

            }

        });



        $('#insertar').click(function ()

        {

          alert("Actualiza la pagina por favor para ver tu foto de perfil");

        });



        $('#ActualizarOficina').click(function ()

        {

            var validacion = validacionInformacionLaboral();

            if (validacion) {

                functionUpdateLaboral();

            }

        });



        $('#infoGeneral').click(function ()

        {

            $("#txtNombres").attr('disabled', true);

            $("#txtApellidos").attr('disabled', true);

            $("#txtEmail").attr('disabled', true);

            $("#txtTelefono").attr('disabled', true);

            $("#txtCiudad").attr('disabled', true);

            $("#txtFechaNac").attr('disabled', true);

            $("#dataPerson").css('display', 'none');

            $("#txtTipoDoc").attr('disabled', true);

            $("#numeroDocumento").attr('disabled', true);

            $("#ActualizarInformacion").attr('disabled', true);

            

        });



        $('#actualizarInfoGeneral').click(function ()

        {

            alert('Esta función esta desabilitada contactese con el Administrador');

          /*  $("#txtNombres").attr('disabled', false);

            $("#txtApellidos").attr('disabled', false);

            $("#txtEmail").attr('disabled', false);

            $("#txtTelefono").attr('disabled', false);

            $("#txtCiudad").attr('disabled', false);

            $("#txtFechaNac").attr('disabled', false);

            $("#dataPerson").css('display', 'block');

            $("#txtTipoDoc").attr('disabled', false);

            $("#numeroDocumento").attr('disabled', false);

            $("#ActualizarInformacion").attr('disabled', false);*/

        });



        $('#infoLaboral').click(function ()

        {

            $("#txtCargo").attr('disabled', true);

            $("#txtTelefonoCor").attr('disabled', true);

            $("#txtPiso").attr('disabled', true);

            $("#txtArea").attr('disabled', true);

            $("#txtOficina").attr('disabled', true);

            $("#ActualizarOficina").attr('disabled', true);

        });

        

        $('#actualizarInfoLaboral').click(function ()

        {
            alert('Esta función esta desabilitada contactese con el Administrador');
/*
            $("#txtCargo").attr('disabled', false);

            $("#txtTelefonoCor").attr('disabled', false);

            $("#txtPiso").attr('disabled', false);

            $("#txtArea").attr('disabled', false);

            $("#txtOficina").attr('disabled', false);

            $("#ActualizarOficina").attr('disabled', false);*/

        });



    });

</script>

<script type="text/javascript" id="rendered-js">



    $(window).load(function () {



        $(function () {

            $('#customFile').change(function (e) {

                addImage(e);

            });



            function addImage(e) {

                var file = e.target.files[0],

                        imageType = /image.*/;



                if (!file.type.match(imageType))

                    return;



                var reader = new FileReader();

                reader.onload = fileOnload;

                reader.readAsDataURL(file);

            }



            function fileOnload(e) {

                var result = e.target.result;

                $('#imgSalida').attr("src", result);

                $('#imgSalida').attr("width", "150px");

                $('#imgSalida').attr("height", "150px");

            }



        });

    });

</script>

<script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/sweetalert2/dist/sweetalert2.min.js"></script>

<link rel="stylesheet" href="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/sweetalert2/dist/sweetalert2.min.css">



    <?php

    }else{ echo 'No tiene permisos para ingresar a la warningrmaci&oacute;n';

    }

    ?>