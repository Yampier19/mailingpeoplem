<?php

header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');


if ($user_name != '' && $id_user != '') {
  $boton = 3;
  $id_users = base64_decode($id_user);
  include('../../DROPDOWN/menu_request.php');

?>

  <style type="text/css">
    .alert-info {
      color: #0c5460;
      background-color: #d1ecf1;
      border-color: #bee5eb;
    }

    .btn {
      color: black;
    }

    .card-header {
      background-color: #ffc107;
    }

    .btn-light:hover {
      color: black;
      background-color: #bee5eb;
      border-color: #bee5eb;
    }

    #agrupacion_n1 {
      border: 1px solid #ccc;
      margin-top: 1%;
      margin-bottom: 1%;
      padding: 1%;
    }

    #agrupacion_n2 {
      border: 1px solid #ccc;
      margin-top: 1%;
      margin-bottom: 1%;
      padding: 1%;
    }

    #subir_inventario {
      margin-top: 0.4%;
    }

    #nombre_archivo {
      padding: 7.5px;
      width: 100%;
    }

    #nombre_archivo:hover {

      background-color: #bee5eb;

    }
  </style>

  <!--alertas -->

  <script src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/sweetalert2/dist/sweetalert2.min.js"></script>

  <link rel="stylesheet" href="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/sweetalert2/dist/sweetalert2.min.css">

  <script type="text/javascript">
    function subir_base_ajax() {
      var variable_recorer = "1"; //solo usa para activar el ajax
      $.ajax({
        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/leer_excel_request.php',
        data: {
          variable_recorer: variable_recorer
        },
        type: 'post',
        beforesend: function() {},
        success: function(data) {
          var num_rows = $('#num_rows').val();
          $('#return_data_subir_datos').html(data);
        }
      });
    }

    function calcular_registros_ajax() {
      var calculo = "1"; //solo usa para activar el ajax y devolver el valor
      $.ajax({
        url: '../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/calcular_registros_request.php',
        data: {
          calculo: calculo
        },
        type: 'post',
        beforesend: function() {


        },
        success: function(data) {
          $('#retrurn_registros').html(data);
        }
      });
    }
    $(document).ready(function() {
          $('#cargar_base').click(function() {
            var rows_numero1 = $('#rows_numero').val();
            var rows_numero = rows_numero1 - 1;
            var tiempo_r = rows_numero * 60;
            var minutos = tiempo_r / 60000;
            activar_alerta(rows_numero, tiempo_r, minutos);
            subir_base_ajax();
            $("#descargar_plan").attr('disabled', true);
            $("#subir_inventario").attr('disabled', true);
            $("#calcular_registros").attr('disabled', true);
            $("#cargar_base").attr('disabled', true);
          });
          $('#calcular_registros').click(function() {
            calcular_registros_ajax();
            $("#cargar_base").attr('disabled', false);
          });
        });
  </script>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Solicitudes - Masivas</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Solicitudes Masivas</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <div class="posicion" align="center">
      <div class="card border-dark mb-3" style="max-width: 97%;">

        <div class="card-header">
          <h4>Solicitudes Masivas</h4>
        </div>

        <div class="card-body text-dark">

          <h5 class="card-title">Descargar plantilla de inventario / Adjuntar Inventario</h5>

          <div class="alert alert-info" role="alert">

            <p class="card-text"><strong><span class="fa fa-warning-circle"></span></strong> Instrucciones: Por favor descargar la plantilla de solicitudes_masivas.xml .

              Llenar la warningrmaci&oacute;n de inventarios <b>sin cambiar la plantilla ni el encabezado</b>, luego adjuntar el archivo excel <b>sin cambiar el nombre ni la extensi&oacute;n del archivo,</b></p>

          </div>

          <div class="form-group row">

            <div class="col-md-6" id="agrupacion_n1" name="agrupacion_n1">

              <label id="nombre_archivo">solicitudes_masivas.xml</label>

              <form method="post">

                <button class="btn btn-outline-success btn-lg btn-block" formaction="../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/exportable_solcitudes_m.php" name="descargar_plan" id="descargar_plan" type="submit"><span class="fa fa-cloud-download"></span>&nbsp;Descargar Plantilla</button>

              </form>

            </div>

            <div class="col-md-6" id="agrupacion_n2" name="agrupacion_n2">

              <form id="FormData" name="FormData" content="text/html;" enctype="multipart/form-data" method="post">

                <input type="file" class="btn btn-light" name="id_excel" id="id_excel"><br>

                <button class="btn btn-outline-warning btn-lg btn-block" name="subir_inventario" id="subir_inventario" type="submit"><span id="#"><i class="fa fa-paperclip" aria-hidden="true"></i></span>&nbsp;Guardar Archivo</button>

              </form>

            </div>

          </div>

          <div class="form-group row">

            <div class="col-md-12">

              <?php
              $tamano = $_FILES["id_excel"]['size'];
              $tipo = $_FILES["id_excel"]['type'];
              $archivo1 = $_FILES["id_excel"]['name'];
              $error = $_FILES['id_excel']['error'];
              $archivo1 = str_replace(" ", "_", $archivo1);
              $prefijo1 = $archivo1;
              if ($archivo1 != "") {
                // guardamos el archivo a la carpeta files
                $destino =  "../../FILES/" . $archivo1;
                if (copy($_FILES['id_excel']['tmp_name'], $destino)) {
                  $status = "Archivo excel: <b>" . $archivo1 . "</b> se ha guardado.<br> Ahora calcular el numero de registros que tiene el archivo excel";
                  echo "<div class='alert alert-info' role='alert'>
                                <p class='card-text'>";
                  echo "<strong><span class='fa fa-info-circle'></span></strong>" . $status;
                  echo "</p>
                                </div>";
                  echo " <div class='form-group row'>
                                <div class='col-md-4'>
                                <button class='btn btn-outline-warning btn-lg btn-block' name='calcular_registros' id='calcular_registros' type='submit'><i class='fa fa-calculator' aria-hidden='true'></i>&nbsp;Calcular</button>
                                </div>
                                <div class='col-md-8'>
                                <button class='btn btn-outline-warning btn-lg btn-block' name='cargar_base' id='cargar_base' type='submit' disabled='true'><span class='fa fa-cloud-upload'></span>&nbsp;Subir inventarios</button>
                                </div>
                                </div>";
                  echo "<html>
                                <script></script> </html>";
                } else {
                  echo "<div class='alert alert-info' role='alert'>
                                <p class='card-text'>";
                  $status = "Error al subir archivo";
                  echo $status;
                  echo "</p>
                                </div>";
                }
              }
              ?>
              <div id="retrurn_registros"></div>
              <div id="return_data_subir_datos"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    function activar_alerta(rows_numero, tiempo_r, minutos) {
      let timerInterval
      Swal.fire({
        title: ' Subiendo Base de datos masiva!',
        html: 'Subiedo ' + rows_numero + ' registros en ' + minutos + ' minutos y  <b></b> milliseconds.',
        timer: tiempo_r,
        timerProgressBar: true,
        onBeforeOpen: () => {
          Swal.showLoading()
          timerInterval = setInterval(() => {
            const content = Swal.getContent()
            if (content) {
              const b = content.querySelector('b')
              if (b) {
                b.textContent = Swal.getTimerLeft()
              }
            }
          }, 100)
        },
        onClose: () => {
          clearInterval(timerInterval)
        }
      }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
          console.log('I was closed by the timer')
        }
      })
    }
    /*$(document).ready(function () {
    		$('#cargar_base').click(function ()
    		{
    				activar_alerta();
    		});
        $('#cantidad_r').click(function ()
        {
            if (catidad_regis=='') {
              Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong!',
      footer: '<a href>Why do I have this issue?</a>'
    })
            }else if (catidad_regis!='') {
                $("#cargar_base").attr('disabled', false);
            } 
        });*/
    /*function activar_alerta(tiempo_r){
  let timerInterval
  Swal.fire({
    title: ' Subiendo Base de datos masiva!',
    html: ' Subiedo registros <b></b> milliseconds.',
    timer: 5000,
    timerProgressBar: true,
    onBeforeOpen: () => {
      Swal.showLoading()
      timerInterval = setInterval(() => {
        const content = Swal.getContent()
        if (content) {
          const b = content.querySelector('b')
          if (b) {
            b.textContent = Swal.getTimerLeft()
          }
        }
      }, 100)
    },
    onClose: () => {
      clearInterval(timerInterval)
    }
  }).then((result) => { */
    /* Read more about handling dismissals below */
    /* if (result.dismiss === Swal.DismissReason.timer) {
      console.log('I was closed by the timer')
      alert(' Se va recargar la pagina por seguridad '+imageUrl+'si');

      

    }

  })



  //location.reload();



}

});*/
  </script>




  <script src="../../../DESIGN/JS/principal_bootstrap/js/bootstrap.bundle.min.js"></script>

   <script src="../../../DESIGN/JS/principal_js/adminlte.js"></script>



  <!-- OPTIONAL SCRIPTS -->

  <script src="../../../DESIGN/JS/principal_chart.js/Chart.min.js"></script>

  <script src="../../../DESIGN/JS/principal_js/demo.js"></script>

  <script src="../../../DESIGN/JS/principal_js/pages/dashboard3.js"></script>

  </body>



  </html>

<?php

} else {
  echo 'No tiene permisos para ingresar a la informaci&oacute;n';
}

?>