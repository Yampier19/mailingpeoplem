<!DOCTYPE html>

<html lang="es">

<head>

	<title>MailingPeopleM</title>

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1">

<!--===============================================================================================-->	

	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/login_bootstrap/css/bootstrap.min.css">

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="../../DESIGN/FONTS/font-awesome-4.7.0/css/font-awesome.min.css">

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/login_animate/animate.css">

<!--===============================================================================================-->	

	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/login_css-hamburgers/hamburgers.min.css">

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="../../DESIGN/JS/login_select2/select2.min.css">

<!--===============================================================================================-->

	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/login_css/util.css">

	<link rel="stylesheet" type="text/css" href="../../DESIGN/CSS/login_css/main.css">

<!--===============================================================================================-->

</head>

<body>

	

	<div class="limiter">

		<div class="container-login100">

			<div class="wrap-login100">

				<div class="login100-pic js-tilt" data-tilt>

					<img src="../../DESIGN/IMG/PEOPLE 316X289.png" alt="IMG">

				</div>



				<form method="post" action="./../../FUNCTIONS/LOGIN/login.php" class="login100-form validate-form">

					<span class="login100-form-title">

						<span style="color:#248eae !important;" >Mailing</span><span style="color:#d30304;">PeopleM</span>

					</span>



					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">

						<input class="input100" type="text" name="dXNlcmxvZw==" id="dXNlcmxvZw==" placeholder="Email">

						<span class="focus-input100"></span>

						<span class="symbol-input100">

							<i class="fa fa-envelope" aria-hidden="true"></i>

						</span>

					</div>



					<div class="wrap-input100 validate-input" data-validate = "Password is required">

						<input class="input100" type="password" name="cGFzc3dvcmRsb2c=" id="cGFzc3dvcmRsb2c=" placeholder="Password">

						<span class="focus-input100"></span>

						<span class="symbol-input100">

							<i class="fa fa-lock" aria-hidden="true"></i>

						</span>

					</div>

					

					<div class="container-login100-form-btn">

						<button class="login100-form-btn">

							Iniciar sesi&oacute;n

						</button>

					</div>



					<!--<div class="text-center p-t-12">

						<span class="txt1">

							Forgot

						</span>

						<a class="txt2" href="#recupt">

							Username / Password?

						</a>

					</div>-->



					<div class="text-center p-t-136">

						

					</div>

				</form>

			</div>

			<!--
			<div class="row my-3 bg-dark p-3" style="border-radius: 20px;">
  <div class="col mx-auto">
    <form action="" method="POST" >
            <label for="" class="mx-auto text-center lead text-white">Registrar usuario</label>
            <input type="text" name="name_user" placeholder="Nombre de usuario" class="form-control mb-2">
            <input type="text" name="password" placeholder="Contraseña" class="form-control mb-2">
            <input type="text" name="id_user" value="1" disabled class="form-control mb-2">
            <input type="text" name="id_loginrol" value="1" disabled class="form-control mb-2">
            <input type="text" name="activo" value="1" disabled class="form-control mb-2">

            <button type="submit" name="registrar" class="btn btn-success btn-block mx-auto my-3">Registrar</button>
    </form>
  </div>
</div>

<?php

/*
include('./../../CONNECTION/SECURITY/conex.php');


  if(isset($_POST['registrar'])){

    $name_user = $_POST['name_user'];
    $password = $_POST['password'];
    $encrypt_pass = base64_encode(MD5($password));
    $id_user = 1;
    $id_loginrol = 1;
    $activo = 1;

    $sql = "INSERT INTO `userlogin`(`name_user`, `password`, `id_user`, `id_loginrol`, `activo`) VALUES ('$name_user','$encrypt_pass','$id_user','$id_loginrol','$activo')";
    
    if (mysqli_query($conex, $sql)) {
      echo "<script>alert('Usuario registrado')</script>";
    } else {
      echo "<script>alert('Error: ' . $sql . '<br>' . mysqli_error($conex) . ')</script>";
    }
  }
*/
?>

		</div>

	</div>





	

	



	

<!--===============================================================================================-->	

	<script src="../../DESIGN/JS/login_jquery/jquery-3.2.1.min.js"></script>

<!--===============================================================================================-->

	<script src="../../DESIGN/CSS/login_bootstrap/js/popper.js"></script>

	<script src="../../DESIGN/CSS/login_bootstrap/js/bootstrap.min.js"></script>

<!--===============================================================================================-->

	<script src="../../DESIGN/JS/login_select2/select2.min.css"></script>

<!--===============================================================================================-->

	<script src="../../DESIGN/JS/login_tilt/tilt.jquery.min.js"></script>

	<script >

		$('.js-tilt').tilt({

			scale: 1.1

		})

	</script>

<!--===============================================================================================-->

	<script src="../../DESIGN/JS/login_js/main.js"></script>



</body>

</html>