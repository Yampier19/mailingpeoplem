<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Mailing people | Messenger</title>
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="../../../DESIGN/CSS/principal_fontawesome-free/css/all.min.css">
    <!-- IonIcons -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../../DESIGN/CSS/principal_css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="../../../DESIGN/CSS/jquery.dataTables.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script type="text/javascript" src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/consulta_envios.js"> </script>
    <script type="text/javascript" src="../../../DESIGN/JS/jquery.dataTables.js"></script>
    <link rel="stylesheet" href="../../../DESIGN/CSS/icheck-bootstrap/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="../../../DESIGN/CSS/select2/css/select2.min.css">
    <link rel="stylesheet" href="../../../DESIGN/CSS/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
    </script>

</head>
<!--
BODY TAG OPTIONS:
================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition sidebar-mini" style="background-color:#FFFFFF; color:#000000;">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-cyan navbar-dark">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
            <!-- SEARCH FORM -->
            <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Messages Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-clock"></i>
                        <!----------------- Notificaciones Fuera de tiempo --------------------------->
                        <span class="badge badge-danger navbar-badge">
                            <span id="noti_fuera_tiempo">0</span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">Fuera de Tiempo</span>
                        <div class="dropdown-divider"></div>
                        <span id="consulta_fuera_tiempo"> </span>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-sync-alt"></i>
                        <!----------------- Notificaciones Devueltos --------------------------->
                        <span class="badge badge navbar-badge" style="background-color:#ff851b; color:#000000;">
                            <span id="noti_devuelto">0</span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">Devueltos</span>
                        <div class="dropdown-divider"></div>
                        <span id="consulta_devuelto"> </span>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-vote-yea"></i>
                        <!----------------- Notificaciones Entregados --------------------------->
                        <span class="badge badge-success navbar-badge">
                            <span id="noti_entrga">0</span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">Entregados</span>
                        <div class="dropdown-divider"></div>
                        <span id="consulta_entrega"> </span>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-shipping-fast"></i>
                        <!----------------- Notificaciones asignadas al mensajero Externo--------------------------->
                        <span class="badge badge-info navbar-badge">
                            <span id="noti_mensj_ext">0</span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">Mensajero Externo</span>
                        <div class="dropdown-divider"></div>
                        <span id="consulta_mensajero_ext"> </span>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-boxes"></i>
                        <!----------------- Recibidos Despacho--------------------------->
                        <span class="badge badge navbar-badge" style="background-color:#3d9970; color:#ffffff;">
                            <span id="noti_despacho">0</span>
                        </span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">Pool Correspondencia</span>
                        <div class="dropdown-divider"></div>
                        <span id="consulta_despacho"> </span>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-dolly"></i>
                        <!----------------- Notificaciones asignadas al mensajero Interno--------------------------->
                        <span class="badge badge navbar-badge" style="background-color:#39cccc; color:#FFFFFF;">
                            <span id="noti_mensj_int">0</span>
                        </span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">Mensajero Int.</span>
                        <div class="dropdown-divider"></div>
                        <span id="consulta_mensajero_int"></span>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-inbox"></i>
                        <!------------------Asignados al punto------------------>
                        <span class="badge badge navbar-badge" style="background-color:#d81b60; color:#FFFFFF;">
                            <span id="noti_punto">0</span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">Bandeja Correspondencia</span>
                        <div class="dropdown-divider"></div>
                        <span id="consulta_punto"></span>
                    </div>
                </li>

                <!-- Notifications Dropdown Menu -->

                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-mail-bulk"></i>
                        <!----------------- Notificaciones Solicitudes--------------------------->
                        <span class="badge badge-warning navbar-badge">
                            <span id="noti_soli">0</span>
                        </span>
                    </a>

                    <!--<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

					<span class="dropdown-item dropdown-header">Solicitudes</span>

                        <div class="dropdown-divider"></div>

					<span id="consulta_so"> </span>

					</div>-->
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-cyan elevation-4">
            <!-- Brand Logo -->
            <a href="formulario.html" class="brand-link">
                <img src="../../../DESIGN/IMG/favicon.ico" alt="MailingPeople Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <strong> <span class="brand-text font-weight-light" style="color:#248eae;">Mailing</span><span class="brand-text font-weight-light" style="color:#d30304;">PeopleM</span></strong>
            </a>
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <div class="nav-item has-treeview">
                            <a href="#" class="d-block"> <i class="fas fa-user-alt"></i>
                                <?php echo $nombre . ' ' . $apellido; ?></a>
                        </div>
                    </div>
                </div>

                <!-- Sidebar Menu -->

                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                        <!---------------------////////////////////////////////////////////////////////////////////////------------------------->
                        <li class="nav-item has-treeview">
                            <a id="boton" href="../../MODULES/ADMIN/form_administrador.php" class="nav-link <?php if (basename($_SERVER['PHP_SELF']) == "form_administrador.php" || basename($_SERVER['PHP_SELF']) == "form_administrador.php") {
                                                                                                                echo "active";
                                                                                                            } else {
                                                                                                                echo "";
                                                                                                            } ?> ">
                                <i class="fas fa-mail-bulk"></i>
                                <p>
                                    Correspondencia
                                </p>
                            </a>
                        </li>
                        <!---------------------////////////////////////////////////////////////////////////////////////------------------------->
                        <li class="nav-item has-treeview">
                            <a id="boton2" href="../../MODULES/ADMIN/form_administrador_solicitud.php" class="nav-link <?php if (basename($_SERVER['PHP_SELF']) == "form_administrador_solicitud.php" || basename($_SERVER['PHP_SELF']) == "form_administrador_solicitud.php") {
                                                                                                                            echo "active";
                                                                                                                        } else {
                                                                                                                            echo "";
                                                                                                                        } ?> ">
                                <i class="fas fa-dolly-flatbed"></i>
                                <p>
                                    Realizar Solicitud
                                </p>
                            </a>
                        </li>
                        <!---------------------////////////////////////////////////////////////////////////////////////------------------------->

                        <li class="nav-item has-treeview">
                            <a id="boton3" href="../../MODULES/ADMIN/form_admin_massive.php" class="nav-link <?php if (basename($_SERVER['PHP_SELF']) == "form_admin_massive.php" || basename($_SERVER['PHP_SELF']) == "form_admin_massive.php") {
                                                                                                                    echo "active";
                                                                                                                } else {
                                                                                                                    echo "";
                                                                                                                } ?> ">
                                <i class="fas fa-dolly-flatbed"></i>
                                <p>
                                    Solicitudes Masivas
                                </p>
                            </a>
                        </li>
                        <!---------------------////////////////////////////////////////////////////////////////////////------------------------->
                        <li class="nav-item has-treeview">
                            <a id="boton4" href="../../MODULES/ADMIN/form_pdf.php" class="nav-link <?php if (basename($_SERVER['PHP_SELF']) == "form_pdf.php" || basename($_SERVER['PHP_SELF']) == "form_pdf.php") {
                                                                                                        echo "active";
                                                                                                    } else {
                                                                                                        echo "";
                                                                                                    } ?> ">
                                <i class="fas fa-qrcode"></i>
                                <p>
                                    Imprimir QR
                                </p>
                            </a>
                        </li>
                        <!---------------------////////////////////////////////////////////////////////////////////////------------------------->
                        <li class="nav-item has-treeview">
                            <a id="boton5" href="../../MODULES/ADMIN/form_admin_traking.php" class="nav-link <?php if (basename($_SERVER['PHP_SELF']) == "form_admin_traking.php" || basename($_SERVER['PHP_SELF']) == "form_admin_traking.php") {
                                                                                                                    echo "active";
                                                                                                                } else {
                                                                                                                    echo "";
                                                                                                                } ?> ">
                                <i class="fas fa-tasks"></i>
                                <p>
                                    Consulta Envio
                                </p>
                            </a>
                        </li>
                        <!---------------------////////////////////////////////////////////////////////////////////////------------------------->
                        <li class="nav-item has-treeview">
                            <a id="boton6" href="../../MODULES/ADMIN/graficas.php" class="nav-link  <?php if (basename($_SERVER['PHP_SELF']) == "graficas.php" || basename($_SERVER['PHP_SELF']) == "graficas.php") {
                                                                                                        echo "active";
                                                                                                    } else {
                                                                                                        echo "";
                                                                                                    } ?> ">
                                <i class="fas fa-chart-line"></i>
                                <p>
                                    Reportes
                                </p>
                            </a>
                        </li>

                        <!---------------------////////////////////////////////////////////////////////////////////////------------------------->

                        <li class="nav-item has-treeview">
                            <a id="boton7" href="../../MODULES/ADMIN/reportes.php" class="nav-link  <?php if (basename($_SERVER['PHP_SELF']) == "reportes.php" || basename($_SERVER['PHP_SELF']) == "reportes.php") {
                                                                                                        echo "active";
                                                                                                    } else {
                                                                                                        echo "";
                                                                                                    } ?> ">
                                <i class="fas fa-file-excel"></i>
                                <p>
                                    Exportables
                                </p>
                            </a>
                        </li>


                        <!-----------------------------///////////////////////////////////////////////////////////----------------------------------------------------->

                        <li class="nav-item">
                            <a id="boton8" href="#" class="nav-link <?php if (basename($_SERVER['PHP_SELF']) == "#" || basename($_SERVER['PHP_SELF']) == "#") {
                                                                        echo "active";
                                                                    } else {
                                                                        echo "";
                                                                    } ?> ">
                                <i class="nav-icon fas fa-copy"></i>
                                <p>
                                    Solicitudes Externas
                                    <span class="badge badge-info right">6</span>
                                </p>
                            </a>
                        </li>
                        <!---------------------////////////////////////////////////////////////////////////////////////------------------------->

                        <li class="nav-item has-treeview">
                            <a id="boton9" href="#" class="nav-link  <?php if (basename($_SERVER['PHP_SELF']) == "user.php" || basename($_SERVER['PHP_SELF']) == "admin_user.php") {
                                                                            echo "active";
                                                                        } else {
                                                                            echo "";
                                                                        } ?> ">
                                <i class="fas fa-users-cog"></i>
                                <p>
                                    Admin Usuarios
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a id="boton20" href="../../MODULES/ADMIN/user.php" class="nav-link <?php if (basename($_SERVER['PHP_SELF']) == "user.php" || basename($_SERVER['PHP_SELF']) == "user.php") {
                                                                                                            echo "active";
                                                                                                        } else {
                                                                                                            echo "";
                                                                                                        } ?> ">
                                        <i class="fas fa-users"></i>
                                        <p>User Interno</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link class=" nav-link <?php if (basename($_SERVER['PHP_SELF']) == "#" || basename($_SERVER['PHP_SELF']) == "#") {
                                                                                        echo "active";
                                                                                    } else {
                                                                                        echo "";
                                                                                    } ?> ">
                                        <i class=" fas fa-users"></i>
                                        <p>User Externo</p>
                                    </a>
                                </li>


                                <li class="nav-item">
                                    <a href="../../MODULES/ADMIN/admin_user.php" class="nav-link class=" nav-link <?php if (basename($_SERVER['PHP_SELF']) == "admin_user.php" || basename($_SERVER['PHP_SELF']) == "admin_user.php") {
                                                                                                                        echo "active";
                                                                                                                    } else {
                                                                                                                        echo "";
                                                                                                                    } ?> ">
                                        <i class=" fas fa-user-friends"></i>
                                        <p>Admin User</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!---------------------////////////////////////////////////////////////////////////////////////------------------------->

                        <li class="nav-item">
                            <a id="boton10" href="" class="nav-link class=" nav-link <?php if (basename($_SERVER['PHP_SELF']) == "#" || basename($_SERVER['PHP_SELF']) == "#") {
                                                                                            echo "active";
                                                                                        } else {
                                                                                            echo "";
                                                                                        } ?> ">
                                <i class=" fas fa-user-cog"></i>
                                <p>
                                    Admin Personal
                                </p>
                            </a>
                        </li>
                        <!---------------------////////////////////////////////////////////////////////////////////////------------------------->



                                                <li class="nav-item has-treeview">
                            <a id="boton" href="./../../../FUNCTIONS/LOGIN/logout.php" class="nav-link ">
                                <i class="fas fa-mail-bulk"></i>
                                <p>
                                    Cerrar Sesión
                                </p>
                            </a>
                        </li>


                        <!---------------------////////////////////////////////////////////////////////////////////////------------------------->
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>





        <script>
            setInterval(NotificacionSolicitud, 2000);
            setInterval(NotificacionPunto, 2000);
            //setInterval(NotificacionesMensajeroPunto, 1000); 
            NotificacionesMensajeroPunto();
            NotificacionSolicitud();
            NotificacionPunto();
        </script>
        <script>
            setInterval(NotificacionesDespacho, 2000);
            //setInterval(NotificacionesMensajeroExterno, 1000);
            setInterval(NotificacionesEntregado, 2000);
            NotificacionesMensajeroExterno();
            NotificacionesDespacho();
            NotificacionesEntregado();
        </script>
        <script>
            setInterval(NotificacionesDevuelto, 2000);
            //setInterval(NotificacionesFueraTiempo, 100000);
            //setInterval(InforNotPuntoCorres, 1000);
            NotificacionesDevuelto();
            InforNotPuntoCorres();
            NotificacionesFueraTiempo();
        </script>
        <script>
            //setInterval(InforNotDespacho, 1000);
            // setInterval(InforNotDevuelto, 1000);
            //setInterval(InfoNotFueraTiempo, 1000);
        </script>