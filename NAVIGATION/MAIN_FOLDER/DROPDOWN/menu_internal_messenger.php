<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Mailing people | Messenger</title>
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="../../../DESIGN/CSS/principal_fontawesome-free/css/all.min.css">
    <!-- IonIcons -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../../DESIGN/CSS/principal_css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	<link rel="stylesheet" href="../../../DESIGN/CSS/jquery.dataTables.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	 <link rel="stylesheet" href="../../../DESIGN/CSS/icheck-bootstrap/icheck-bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	 <script type="text/javascript" src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/consulta_envios.js" > </script>
	    <script type="text/javascript" src="../../../DESIGN/JS/jquery.dataTables.js"></script>

   

        <script>
		$(document).ready( function () {
    $('#myTable').DataTable();
          });
		</script>

<?php if($boton == '1'){ ?>
<script type="text/javascript">
$(document).ready(function(){
function boton(){
  $('#boton').addClass("nav-link active");
};
boton();
});
</script>
<?php } if($boton == '2'){ ?>
<script type="text/javascript">
$(document).ready(function(){
function boton2(){
  $('#boton2').addClass("nav-link active");
};
boton2();
});
</script>
<?php } if($boton == '3'){?>
<script type="text/javascript">
$(document).ready(function(){
function boton3(){
  $('#boton3').addClass("nav-link active");
};
boton3();
});
</script>
<?php } if($boton == '4'){?>
<script type="text/javascript">
$(document).ready(function(){
function boton4(){
  $('#boton4').addClass("nav-link active");
};
boton4();
});
</script>
<?php } if($boton == '5'){?>
<script type="text/javascript">
$(document).ready(function(){
function boton5(){
  $('#boton5').addClass("nav-link active");
};
boton5();
});
</script>
<?php } if($boton == '6'){?>
<script type="text/javascript">
$(document).ready(function(){
function boton6(){
  $('#boton6').addClass("nav-link active");
};
boton6();
});
</script>
<?php } if($boton == '7'){?>
<script type="text/javascript">
$(document).ready(function(){
function boton7(){
  $('#boton7').addClass("nav-link active");
};
boton7();
});
</script>
<?php } if($boton == '8'){?>
<script type="text/javascript">
$(document).ready(function(){
function boton8(){
  $('#boton8').addClass("nav-link active");
};
boton8();
});
</script>
<?php } if($boton == '9'){?>
<script type="text/javascript">
$(document).ready(function(){
function boton9(){
  $('#boton9').addClass("nav-link active");
};
boton9();
});
</script>
<?php } if($boton == '10'){?>
<script type="text/javascript">
$(document).ready(function(){
function boton10(){
  $('#boton10').addClass("nav-link active");
};
boton10();
});
</script>
<?php } ?>
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" style="background-color:#FFFFFF; color:#000000;">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-cyan navbar-dark">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
            </ul>
            <!-- SEARCH FORM -->
            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Messages Dropdown Menu -->
				<li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-clock"></i><!----------------- Notificaciones Fuera de tiempo --------------------------->
                        <span class="badge badge-danger navbar-badge">
						<span id="noti_fuera_tiempo">0</span>
						</span>
                    </a>
					<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
					<span class="dropdown-item dropdown-header">Fuera de Tiempo</span>
                        <div class="dropdown-divider"></div>
					<span id="consulta_fuera_tiempo"> </span>
					</div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-cyan elevation-4">
            <!-- Brand Logo -->
            <a href="formulario.html" class="brand-link">
                <img src="../../../DESIGN/IMG/favicon.ico" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
               <strong> <span class="brand-text font-weight-light" style="color:#248eae;">Mailing</span><span class="brand-text font-weight-light"  style="color:#d30304;">PeopleM</span></strong>
            </a>
 <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
<div class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <div class="nav-item has-treeview">
                        <a href="#" class="d-block"> <i class="fas fa-user-alt"></i> <?php echo $nombre.' '.$apellido; ?></a>
                    </div>
					</div>
                </div>
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
			   		<!---------------------////////////////////////////////////////////////////////////////////////------------------------->	
                         <li class="nav-item has-treeview">
                            <a id="boton" href="../../MODULES/INTERNAL_MESSENGER/index_form_int.php" class="nav-link">
                              <i class="fas fa-qrcode"></i>
                                <p>Lista de Asignaciones</p>
                            </a>
                </li>

		  <!---------------------////////////////////////////////////////////////////////////////////////------------------------->

                        <li class="nav-item">
                            <a id="boton10" href="../../MODULES/INTERNAL_MESSENGER/entrega_form_int.php" class="nav-link" > <i class="fas fa-people-carry"></i>
							<p>Entrega Correspondencia </p>
                            </a>
                        </li>
						<li class="nav-item has-treeview"> <!-- menu Mis Envios-->
                            <a id="boton4" href="profile_request.php" class="nav-link">
                            <i class="fas fa-user-cog"></i>
                                <p>
                                Admin Personal
                                </p>
                            </a>
                        </li>
						<!---------------------////////////////////////////////////////////////////////////////////////------------------------->		
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
