<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Mailing people | Messenger</title>
    <link rel="stylesheet" href="../../DESIGN/CSS/principal_fontawesome-free/css/all.min.css">
    <!-- open-icons -->
    <link href="../../DESIGN/CSS/open-iconic-master/font/css/open-iconic-bootstrap.min.css" rel="stylesheet">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../DESIGN/CSS/principal_css/adminlte.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="../../DESIGN/CSS/jquery.dataTables.css" >
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="../../DESIGN/JS/jquery.dataTables.js"></script>
    <!-- Notificaciones-->
    <script type="text/javascript" src="../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/notification_received_internal.js" ></script>
    <script>
        $(document).ready( function () 
        {
            $('#myTable').DataTable();
        });
    </script>
</head>
<style>
body {
    margin: 0;
    font-family: "Source Sans Pro", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #343a40 !important; 
    text-align: left;
    background-color: #ffffff !important; 
}
</style>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->


        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-cyan navbar-dark">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Lector QR</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Resibir</a>
                </li>
            </ul>

            <!-- SEARCH FORM -->
            <form class="form-inline ml-3"> 
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                        </button> 
                    </div>
                </div>
            </form>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Messages Dropdown Menu -->
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                    <span class="oi oi-bell"></span><!-----------------Asignacion--------------------------->
                        <span class="badge badge-warning navbar-badge">
                        <span id="return_notifi_received_int"></span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">Asignacion</span>
                        <div class="dropdown-divider"></div>
                    <span id="retun_info_received_int"> </span>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-cyan elevation-4">
            <!-- Brand Logo -->
            <a href="formulario.html" class="brand-link">
                <img src="../../DESIGN/IMG/favicon.ico" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">MailingPeopleM</span>
            </a>
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="info">
                        <a href="#" class="d-block"><?php echo $nombre.' '.$apellido; ?></a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                         <li class="nav-item has-treeview"> <!-- menu Resibido-->
                            <a href="form_received_internal.php" class="nav-link active">
                               <i class="fas fa-newspaper"></i>
                                <p>
                                    Resibido
                                    
                                </p>
                            </a>
                            
                        </li><!-- /menu Resivido-->
                        <li class="nav-item has-treeview"> <!-- menu Mis Envios-->
                            <a href="form_received_traking_internal.php" class="nav-link">
                                <i class="nav-icon fas fa-chart-pie"></i>
                                <p>
                                    Mis Envios
                    
                                </p>
                            </a>
                        </li><!-- /menu Mis Envios-->
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
<script>

    // function consulta notificaciones received internal
    setInterval(notifi_received_int, 1000);
    // function consulta informacion received internal
    setInterval(info_received_int, 1000);

</script>   