<!DOCTYPE html>

<html lang="es">

<?php 


require ('../../../CONNECTION/SECURITY/conex.php');

?>

<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Mailing people | Messenger</title>

    <!-- open-icons -->

    <link rel="stylesheet" href="../../../DESIGN/CSS/principal_fontawesome-free/css/all.min.css">

    <link href="../../../DESIGN/CSS/open-iconic-master/font/css/open-iconic-bootstrap.min.css" rel="stylesheet">

    <!-- Theme style -->

    <link rel="stylesheet" href="../../../DESIGN/CSS/principal_css/adminlte.min.css">

    <!-- Google Font: Source Sans Pro -->

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="../../../DESIGN/CSS/jquery.dataTables.css" >

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>



    <!-- Notificaciones-->

    <script type="text/javascript" src="../../../FUNCTIONS/INTERACTIVE/GLOBAL_JS/notification_request.js" > </script>

    <script type="text/javascript" src="../../../DESIGN/JS/jquery.dataTables.js"></script>

   

    <!--SELECT2 -->

    <link rel="stylesheet" href="../../../DESIGN/CSS/select2/css/select2.min.css">

    <link rel="stylesheet" href="../../../DESIGN/CSS/select2-bootstrap4-theme/select2-bootstrap4.min.css">



    <style>

    #btn_enviar

    {

        min-width:200px;

        padding:5px;

        box-shadow: 0px 5px 4px #CCD1D1;

        color:white;

        background-color:#3d9970;

        border-color:#3d9970;

    }

    .asterisco_obligatorio

    {

        color: red;

    }

    #div_boton

    {

        border-right:2px solid #dfdfdf;

        margin-left:4px;

        margin-right:4px;

    }

    .nav-pills .nav-link.active, .nav-pills .show>.nav-link

    {

        color:#fff;

        background-color:#17a2b8;

    }

    .nav-pills .nav-link:not(.active):hover 

    {

        color: #17a2b8;

    }

    .division_j

    {

        border-bottom:1px solid #17a2b8;

    }

    .custom-file-label::after {

    position: absolute;

    top: 0;

    right: 0;

    bottom: 0;

    z-index: 3;

    display: block;

    height: 2.25rem;

    padding: .375rem .75rem;

    line-height: 1.5;

    color: #495057;

    content: "Adjuntar";

    background-color: #e9ecef;

    border-left: inherit;

    border-radius: 0 .25rem .25rem 0;

    }

    </style>



    <script>

        $(document).ready( function () {

            $('#myTable').DataTable();

        });

    </script>

<?php if($boton == '1'){ ?>

<script type="text/javascript">



$(document).ready(function(){



function boton(){



  $('#boton1').addClass("nav-link active");

    

};

boton();

});

</script>



<?php } if($boton == '2'){ ?>

<script type="text/javascript">



$(document).ready(function(){

function boton2(){



  $('#boton2').addClass("nav-link active");

   

  

};

boton2();



});

</script>

<?php } if($boton == '3'){?>

<script type="text/javascript">



$(document).ready(function(){

function boton3(){

  $('#boton3').addClass("nav-link active");

 

};

boton3();



});

</script>

<?php } if($boton == '4'){?>

<script type="text/javascript">



$(document).ready(function(){

function boton4(){



  $('#boton4').addClass("nav-link active");

  

};

boton4();



});

</script>

<?php } ?>



</head>



<!--

BODY TAG OPTIONS:

=================

Apply one or more of the following classes to to the body tag

to get the desired effect

|---------------------------------------------------------|

|LAYOUT OPTIONS | sidebar-collapse                        |

|               | sidebar-mini                            |

|---------------------------------------------------------|

-->

<?php 

$select_photo = mysqli_query($conex,"SELECT names AS nombreUsuario, surnames AS apellidosUsuario, foto AS foto_profile FROM personal WHERE id_personal = '".$id_users."';");



while($photo = (mysqli_fetch_array($select_photo))) {   



$nombreUsuario = $photo['nombreUsuario'];

$apellidosUsuario = $photo['apellidosUsuario'];

$foto_profile = $photo['foto_profile'];

$imagen = "../../FILES/PHOTOS_PROFILE/$foto_profile";

}

?>
<body class="hold-transition sidebar-mini" style="background-color:#FFFFFF; color:#000000;">

    <div class="wrapper">

        <!-- Navbar -->

        <nav class="main-header navbar navbar-expand navbar-cyan navbar-dark">

            <!-- Left navbar links -->

            <ul class="navbar-nav">

                <li class="nav-item">

                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>

                </li>

            </ul>



            <!-- SEARCH FORM -->

            <form class="form-inline ml-3"> 

                <div class="input-group input-group-sm">

                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">

                    <div class="input-group-append">

                        <button class="btn btn-navbar" type="submit">

                        <i class="fas fa-search"></i>

                        </button>

                    </div>

                </div>

            </form>



            <!-- Right navbar links -->

            <ul class="navbar-nav ml-auto">

                <!-- Messages Dropdown Menu -->

				<li class="nav-item dropdown">

                    <a class="nav-link" data-toggle="dropdown" href="#">

                    <i class="fas fa-clock"></i><!----------------- Fuera De Tiempo  --------------------------->

                        <span class="badge badge-danger navbar-badge">

						<span id="noti_devuelto"></span>

						</span>

                    </a>

					<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

					<span class="dropdown-item dropdown-header">Fuera de tiempo</span>

                        <div class="dropdown-divider"></div>

					<span id="consulta_devuelto"> </span>

					</div>

                </li>

				

				<li class="nav-item dropdown">

                    <a class="nav-link" data-toggle="dropdown" href="#">

                    <i class="fas fa-shipping-fast"></i><!----------------- Resividos --------------------------->

                        <span class="badge badge-primary navbar-badge">

						<span id="retun_notification_received"></span>

						</span>

                    </a>

					<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

					<span class="dropdown-item dropdown-header">Recibidos</span>

                        <div class="dropdown-divider"></div>

					<span id="return_info_received"></span>

					</div>

                </li>



                <li class="nav-item dropdown">

                    <a class="nav-link" data-toggle="dropdown" href="#">

                    <i class="fas fa-sync-alt"></i><!------------------Devuluciones------------------>

                        <span class="badge badge-warning navbar-badge" style="background-color:#ff851b; color:#000000;">

						<span id="return_notification_returns"></span>

						</span>

                    </a>

                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

					<span class="dropdown-item dropdown-header">Devoluciones</span>

                        <div class="dropdown-divider"></div>

					<span id="return_info_returned"> </span>

					</div>

                </li>

				

                <!-- Notifications Dropdown Menu -->

                <li class="nav-item dropdown">

                    <a class="nav-link" data-toggle="dropdown" href="#">

                    <i class="fas fa-vote-yea"></i><!-----------------Entregado--------------------------->

                        <span class="badge badge-success navbar-badge">

						<span id="return_notification_delivery"></span>

						</span>

                    </a>

					<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

					<span class="dropdown-item dropdown-header">Entregado</span>

                        <div class="dropdown-divider"></div>

					<span id="return_info_delivery"> </span>

					</div>

                </li>

            </ul>

        </nav>

        <!-- /.navbar -->

        

        <!-- Main Sidebar Container -->

        <aside class="main-sidebar sidebar-light-cyan elevation-4">

            <!-- Brand Logo -->

            <a href="#" class="brand-link">

            

                <img src="../../../DESIGN/IMG/favicon.ico" class="brand-image img-circle elevation-3" style="opacity: .8">

                <strong><span class="brand-text font-weight-light" style="color:#248eae;">Mailing</span><span class="brand-text font-weight-light"  style="color:#d30304;">PeopleM</span></strong>

            </a>

            <!-- Sidebar -->

            <div class="sidebar">

                <!-- Sidebar user panel (optional) -->

                <div class="user-panel mt-3 pb-3 mb-3 d-flex">

                    <div class="image">

                    <img src="<?php echo $imagen ?>" class="img-circle elevation-2" alt="User Image">

                    </div>

                    <div class="info">

                    <a href="#" class="d-block"><?php echo $nombreUsuario; ?></a>

                    </div>

                </div>



                <!-- Sidebar Menu -->

                <nav class="mt-2">

                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                        <!-- Add icons to the links using the .nav-icon class

                        with font-awesome or any other icon font library -->

                        <li class="nav-item has-treeview"> <!-- menu Resivido-->

                            <a id="boton1" href="form_request.php" class="nav-link">

                            <i class="fas fa-dolly-flatbed"></i>

                                <p>

                                    Generar Solicitud

                                </p>

                            </a>

                        </li><!-- /menu Resivido-->

                        <li class="nav-item has-treeview"> <!-- menu Mis Envios-->

                            <a id="boton2" href="form_request_traking.php" class="nav-link">

                            <i class="fas fa-mail-bulk"></i>

                                <p>

                                    Mis Solicitudes

                                </p>

                            </a>

                        </li><!-- /menu Mis Envios-->

                        <li class="nav-item has-treeview"> <!-- menu Mis Envios-->

                            <a id="boton3" href="form_request_massive.php" class="nav-link">

                            <i class="fab fa-buffer"></i>

                                <p>

                                    Solicitudes Masivas

                                </p>

                            </a>

                        </li><!-- /menu SOLICITUDES MASIVAS-->

                        <li class="nav-item has-treeview"> <!-- menu Mis Envios-->

                            <a id="boton4" href="profile_request.php" class="nav-link">

                            <i class="fas fa-user-cog"></i>

                                <p>

                                Admin Personal

                                </p>

                            </a>

                        </li><!-- /menu SOLICITUDES MASIVAS-->

                    </ul>

                </nav>

                <!-- /.sidebar-menu -->

            </div>

            <!-- /.sidebar -->

        </aside>

    <script>

        // Numero de notificaciones functions 

        setInterval(notification_received, 1000);

        setInterval(notification_returns, 1000);

        setInterval(notification_delivery, 1000);

        

        // informacion de la notificaion functions

        setInterval(info_received, 1000);

        setInterval(info_returned, 1000);  

        setInterval(info_delivery, 1000);



    </script>   

    

