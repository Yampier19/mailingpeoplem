<?php
header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');
$estado = $_POST['estado'];
if ($estado == '1') {
?>
    <style>
        .form-controlb {
            display: block;
            width: 100%;
            /* height: calc(2.25rem + 2px); */
            /* padding: .375rem .75rem; */
            /* font-size: 1rem; */
            /* font-weight: 400; */
            /* line-height: 1.5; */
            color: #261072;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            box-shadow: inset 0 0 0 transparent;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .table td,
        .table th {
            vertical-align: top;
            padding: 0.40rem;
            border-top: 1px solid rgb(222, 226, 230);
        }
    </style>
    <script>
        $(document).ready(function() {
            $('#myTable').DataTable();
        });
    </script>
    <table id="myTable" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>Usuario</th>
                <th>Producto</th>
                <th style="background-color:#ffc107">Solicitud</th>
                <th>Entrega</th>
                <th>Fecha Max</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $tabla_solicitudes = mysqli_query($conex, "SELECT B.names, A.tipo_solicitud, A.fecha_registro AS fecha_registro, A.fecha_inicio, A.fecha_fin FROM `shipping` AS A INNER JOIN personal AS B ON A.`id_personal` = B.id_personal WHERE A.`estado` = 1");
            while ($dato = mysqli_fetch_array($tabla_solicitudes)) {
            ?>
                <tr>
                    <td><?php echo $dato['names']; ?></td>
                    <td><?php echo $dato['tipo_solicitud']; ?></td>
                    <td><?php echo $dato['fecha_registro'];  ?></td>
                    <td><?php echo $dato['fecha_inicio']; ?></td>
                    <td><?php echo $dato['fecha_fin']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
<?php
}
//////////////////////
else if ($estado == '2') {
?> <style>
        .form-controlb {
            display: block;
            width: 100%;
            /* height: calc(2.25rem + 2px); */
            /* padding: .375rem .75rem; */
            /* font-size: 1rem; */
            /* font-weight: 400; */
            /* line-height: 1.5; */
            color: #261072;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            box-shadow: inset 0 0 0 transparent;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .table td,
        .table th {
            vertical-align: top;
            padding: 0.40rem;
            border-top: 1px solid rgb(222, 226, 230);
        }
    </style>
    <script>
        $(document).ready(function() {
            $('#example1').DataTable();
        });
    </script>

    <form action="../../../FUNCTIONS/CRUD/asig_bandejaCo.php" method="post" name="formBanCo" id="formBanCo">
        <table id="example1" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Producto</th>
                    <th>Fch Solicitud</th>
                    <th>Tray Pack</th>
                    <th>Fch Entrega</th>
                    <th>Fch Max</th>
                    <th>Asignaci&oacute;n</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $tabla_ban_corresponde = mysqli_query($conex, "SELECT B.id_shipping, B.tipo_solicitud, B.fecha_inicio, B.fecha_fin, B.fecha_registro,A.fecha_registro AS fecha_regispoint FROM `point` as A INNER JOIN shipping AS B ON A.id_ship = B.id_shipping WHERE A.estado = '1'");
                while ($dato = mysqli_fetch_array($tabla_ban_corresponde)) { ?>
                    <tr>
                        <td><?php echo $dato['tipo_solicitud']; ?></td>
                        <td><?php echo $dato['fecha_registro']; ?></td>
                        <td><?php echo $dato['fecha_regispoint']; ?></td>
                        <td><?php echo $dato['fecha_inicio']; ?><input id="IdUser" name="IdUser" value="<?php echo $id_user; ?>" type="hidden" /> </td>
                        <td><?php echo $dato['fecha_fin']; ?><input id="IdShipBan" name="IdShipBan[]" value="<?php echo $dato['id_shipping']; ?>" type="hidden"></td>
                        <td><select id="IdMenInteB" name="IdMenInteB[]" class="form-controlb">
                                <option value="">Seleccione...</option><?php $select_internal = mysqli_query($conex, "SELECT * FROM `userlogin` as A INNER JOIN user AS B ON A.`id_user` = B.id_user WHERE `id_loginrol` = 5 "); //OR `id_loginrol` = 3
                                                                        while ($datosel = mysqli_fetch_array($select_internal)) {  ?>
                                    <option value="<?php echo $datosel['id_log']; ?>"><?php echo $datosel['names']; ?></option>
                                <?php } ?>
                            </select></td>
                    </tr>
                <?php
                } ?>
            </tbody>
        </table> <button type="submit" id="AsignarB" name="AsignarB" class="btn btn-block btn-danger">Asignar</button>
    </form>

<?php

}

//////////////////////

else if ($estado == '3') {
?>
    <style>
        .form-controlb {
            display: block;
            width: 100%;
            /* height: calc(2.25rem + 2px); */
            /* padding: .375rem .75rem; */
            /* font-size: 1rem; */
            /* font-weight: 400; */
            /* line-height: 1.5; */
            color: #261072;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            box-shadow: inset 0 0 0 transparent;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .table td,
        .table th {
            vertical-align: top;
            padding: 0.40rem;
            border-top: 1px solid rgb(222, 226, 230);
        }
    </style>

    <script>
        $(document).ready(function() {
            $('#myTable2').DataTable();
        });
    </script>

    <table id="myTable2" class="table table-bordered table-hover">
        <thead>
            <tr>
                <th>Mensajero</th>
                <th>Producto</th>
                <th>Fch Asignaci&oacute;n</th>
                <th>Entrega</th>
                <th>Fecha Max</th>
            </tr>
        </thead>
        <tbody>

            <?php

            $tabla_mensajero_interno = mysqli_query($conex, "SELECT A.`id_rec`, A.`id_ship`, A.`id_user`, A.`id_mensajero`, A.`fecha_registro` AS fecha_registro, A.`novedad`, A.`estado`,B.`id_shipping`, B.`id_generate`, B.`id_personal`, B.`id_detail`, B.`nombres`, B.`apellidos`, B.`tipo_documento`, B.`documento`, B.`telefono`, B.`email`, B.`direccion`, B.`prioridad`, B.`ciudad_origen`, B.`ciudad_destino`, B.`barrio_destino`, B.`valor_declarado`, B.`centro_costos`, B.`tipo_solicitud`, B.`ubicacion`, B.`fecha_inicio`, B.`fecha_fin`, B.`fecha_registro`AS fecha_registro_ini, B.`estado_proceso`, B.`estado`, C.`id_user`, C.`names`, C.`surnames`, C.`tipo_documento`, C.`documento`, C.`correo`, C.`estado` FROM `point_courier` AS A INNER JOIN shipping AS B ON A.`id_ship` = B.id_shipping INNER JOIN user AS C ON A.id_mensajero = C.id_user WHERE A.`estado` = '1'");

            while ($dato = (mysqli_fetch_array($tabla_mensajero_interno))) {
            ?>
                <tr>
                    <td><?php echo $dato['names']; ?></td>
                    <td><?php echo $dato['tipo_solicitud']; ?></td>
                    <td><?php echo $dato['fecha_registro']; ?></td>
                    <td><?php echo $dato['fecha_inicio']; ?></td>
                    <td><?php echo $dato['fecha_fin']; ?></td>
                </tr>
            <?php
            }
            ?></tbody>
    </table>
<?php
} else if ($estado == '4') {
    $TipoMensajero = $_POST['TipoMensajero'];
?>
    <div class="form-group">
        <label>Lista de Mensajeros</label>
        <select class="form-control" id="ListaMensajerosI" name="ListaMensajerosI">
            <option value="" selected="">Seleccione...</option>
            <?php if ($TipoMensajero == 'interno') {
                $select_mensajero = mysqli_query($conex, "SELECT * FROM `userlogin` AS A LEFT JOIN user AS B ON A.id_user = B.id_user WHERE `id_loginrol` = '5' AND B.estado = '1'");
            } else if ($TipoMensajero == 'externo') {
                $select_mensajero = mysqli_query($conex, "SELECT * FROM `userlogin` AS A LEFT JOIN user AS B ON A.id_user = B.id_user WHERE `id_loginrol` = '4' AND B.estado = '1'");
            }
            while ($dato = mysqli_fetch_array($select_mensajero)) {
                echo "<option value='" . $dato['id_user'] . "'>" . $dato['names'] . "</option>";
            }
            ?>
        </select>
    </div>
    <?php
} else if ($estado == '4.1') {
    $ListaMensajerosI = $_POST['ListaMensajerosI'];
    $TipoMensajero = $_POST['TipoMensajero'];
    $QueDeseaHacer = $_POST['QueDeseaHacer'];

    if ($TipoMensajero == 'interno' && $QueDeseaHacer == 'recibir') {
    ?>
        <script>
            $(document).ready(function() {
                $('#myTable3').DataTable();

            });
        </script>
        <div class="row">
            <div class="col-md-12">

                <div class="card-header" style="background-color:#b9104e; color:#FFFFFF">
                    <h3 class="card-title">Recibir</h3>
                </div>
                <div class="card-body">
                    <form method="post" id="form" action="../../../FUNCTIONS/CRUD/ajaxAdminRecibirMensajeroInterno.php">
                        <table id="myTable3" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Producto</th>
                                    <th>Recibir</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php

                                $select_asig = mysqli_query($conex, "SELECT * FROM `received_punto` AS A LEFT JOIN shipping AS B ON A.`id_ship` = B.id_shipping LEFT JOIN qr_generated AS C ON B.id_generate = C.id_qr WHERE A.id_mensajero = '" . $ListaMensajerosI . "' AND A.estado = '1'");

                                while ($dato_asig = mysqli_fetch_array($select_asig)) {
                                ?>
                                    <tr>
                                        <td><?php echo $dato_asig['id_qr_generado']; ?><input type="hidden" id="IdMensajero" name="IdMensajero" value="<?php echo $ListaMensajerosI ?>" /></td>
                                        <td><?php echo $dato_asig['tipo_solicitud']; ?> <input name="IdUser" id="IdUser" type="hidden" value="<?php echo $id_user; ?>" /></td>
                                        <td>
                                            <center>
                                                <div class="icheck-success d-inline">
                                                    <input type="checkbox" name="IdShipping[]" id="<?php echo $dato_asig['id_shipping']; ?>" value="<?php echo $dato_asig['id_shipping']; ?>">
                                                    <label for="<?php echo $dato_asig['id_shipping']; ?>">
                                                    </label>
                                                </div>
                                            </center>
                                        </td>
                                    </tr>

                                <?php } ?>
                            </tbody>
                        </table>
                        <br>
                        <button type="submit" id="RecibirB" name="RecibirB" class="btn btn-block btn-success">Recibir</button>
                    </form>
                </div>
            </div>
        </div>
    <?php

    } elseif ($TipoMensajero == 'externo' && $QueDeseaHacer == 'asignar') {

        if ($ListaMensajerosI != '') {

            $datoMen = "AND A.id_user = '" . $ListaMensajerosI . "'";
        } else {
            $datoMen = '';
        }

    ?>

        <style>
            .form-controlb {
                display: block;
                width: 100%;
                /* height: calc(2.25rem + 2px); */
                /* padding: .375rem .75rem; */
                /* font-size: 1rem; */
                /* font-weight: 400; */
                /* line-height: 1.5; */
                color: #261072;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                box-shadow: inset 0 0 0 transparent;
                transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
            }

            .table td,
            .table th {
                vertical-align: top;
                padding: 0.40rem;
                border-top: 1px solid rgb(222, 226, 230);
            }
        </style>

        <script>
            $(document).ready(function() {
                $('#example4').DataTable();
            });
        </script>

        <form action="../../../FUNCTIONS/CRUD/asigMensaExterno.php" method="post" name="formBanCo" id="formBanCo">
            <table id="example4" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Codigo</th>
                        <th>Producto</th>
                        <th>Direcci&oacute;n</th>
                        <th>Zona</th>
                        <th>Asignaci&oacute;n</th>

                    </tr>
                </thead>
                <tbody>

                    <?php

                    $tabla_ban_corresponde = mysqli_query($conex, "SELECT C.id_qr_generado, B.id_shipping, B.direccion, B.tipo_solicitud, B.fecha_inicio, B.fecha_fin, B.fecha_registro,A.fecha_registro AS fecha_regispoint FROM `office_received` as A LEFT JOIN shipping AS B ON A.id_ship = B.id_shipping LEFT JOIN qr_generated as C  ON B.id_generate = C.id_qr WHERE A.estado = '1'");

                    while ($dato = mysqli_fetch_array($tabla_ban_corresponde)) {
                    ?>
                        <tr>
                            <td><?php echo $dato['id_qr_generado']; ?></td>
                            <td><?php echo $dato['tipo_solicitud']; ?></td>
                            <td><?php echo $dato['direccion']; ?><input id="IdUser" name="IdUser" value="<?php echo $id_user; ?>" type="hidden" /> </td>
                            <td><select id="zona" name="zona" class="form-controlb">
                                    <option value="">Seleccione...</option>
                                    <option>NORTE ARRIBA</option>
                                    <option>NORTE ABAJO</option>
                                    <option>ALREDEDORES 1</option>
                                    <option>ALREDEDORES 2</option>
                                    <option>SUR ARRIBA</option>
                                    <option>SUR ABAJO</option>
                                    <option>CHICO</option>
                                    <option>CENTRO</option>
                                    <option>SUBA</option>
                                </select><input id="IdShipBan" name="IdShipBan[]" value="<?php echo $dato['id_shipping']; ?>" type="hidden"></td>
                            <td><select id="IdMenInteB" name="IdMenInteB[]" class="form-controlb">
                                    <option value="">Seleccione...</option><?php $select_internal = mysqli_query($conex, "SELECT * FROM `userlogin` as A INNER JOIN user AS B ON A.`id_user` = B.id_user WHERE `id_loginrol` = '4'" . $datoMen . ""); //OR `id_loginrol` = 3
                                                                            while ($datosel = mysqli_fetch_array($select_internal)) {
                                                                            ?>
                                        <option value="<?php echo $datosel['id_log']; ?>"><?php echo $datosel['names']; ?></option>
                                    <?php } ?>
                                </select></td>
                        </tr>
                    <?php
                    }
                    ?></tbody>
            </table> <button type="submit" id="AsignarB" name="AsignarB" class="btn btn-block btn-danger">Asignar</button>
        </form>
    <?php
    } else if ($QueDeseaHacer == 'recibir') {
    ?>
        <script>
            $(document).ready(function() {
                $('#myTable3').DataTable();
            });
        </script>
        <div class="row">
            <div class="col-md-12">
                <div class="card-header" style="background-color:#b9104e; color:#FFFFFF">
                    <h3 class="card-title">Recibir</h3>
                </div>
                <div class="card-body">
                    <form method="post" id="form" action="../../../FUNCTIONS/CRUD/ajaxAdminRecibirenPool.php">
                        <table id="myTable3" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Producto</th>
                                    <th>Recibir</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $select_asig = mysqli_query($conex, "SELECT *, A.id_user as id_useri FROM `office` AS A LEFT JOIN shipping AS B ON A.`id_ship` = B.id_shipping LEFT JOIN qr_generated AS C ON B.id_generate = C.id_qr WHERE A.`estado` = '1'");
                                while ($dato_asig = mysqli_fetch_array($select_asig)) {
                                ?>
                                    <tr>
                                        <td><?php echo $dato_asig['id_qr_generado']; ?><input type="hidden" id="IdMensajero" name="IdMensajero" value="<?php echo $dato_asig['id_useri']; ?>" /></td>
                                        <td><?php echo $dato_asig['tipo_solicitud']; ?> <input name="IdUser" id="IdUser" type="hidden" value="<?php echo $id_user; ?>" /></td>
                                        <td>
                                            <center>
                                                <div class="icheck-success d-inline">
                                                    <input type="checkbox" name="IdShipping[]" id="<?php echo $dato_asig['id_shipping']; ?>" value="<?php echo $dato_asig['id_shipping']; ?>">
                                                    <label for="<?php echo $dato_asig['id_shipping']; ?>">
                                                    </label>
                                                </div>
                                            </center>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <br>
                        <button type="submit" id="RecibirB" name="RecibirB" class="btn btn-block btn-success">Recibir</button>
                    </form>
                </div>
            </div>
        </div>
    <?php
    }
} else if ($estado == '5') {
    ?><style>
        .form-controlb {
            display: block;
            width: 100%;
            /* height: calc(2.25rem + 2px); */
            /* padding: .375rem .75rem; */
            /* font-size: 1rem; */
            /* font-weight: 400; */
            /* line-height: 1.5; */
            color: #261072;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            box-shadow: inset 0 0 0 transparent;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .table td,
        .table th {
            vertical-align: top;
            padding: 0.40rem;
            border-top: 1px solid rgb(222, 226, 230);

        }
    </style>

    <script>
        $(document).ready(function() {

            $('#example2').DataTable();

        });
    </script>
    <table id='example2' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th>Mensajero</th>
                <th>Producto</th>
                <th>Fch Asignaci&oacute;n</th>
                <th>Entrega</th>
                <th>Fecha Max</th>
            </tr>
        </thead>
        <tbody>
            <?php

            $tabla_mensajero_externo = mysqli_query($conex, "SELECT A.`id_rec`, A.`id_ship`, A.`id_user`, A.`id_mensajero`, A.`fecha_registro` AS fecha_registro, A.`novedad`, A.`estado`,B.`id_shipping`, B.`id_generate`, B.`id_personal`, B.`id_detail`, B.`nombres`, B.`apellidos`, B.`tipo_documento`, B.`documento`, B.`telefono`, B.`email`, B.`direccion`, B.`prioridad`, B.`ciudad_origen`, B.`ciudad_destino`, B.`barrio_destino`, B.`valor_declarado`, B.`centro_costos`, B.`tipo_solicitud`, B.`ubicacion`, B.`fecha_inicio`, B.`fecha_fin`, B.`fecha_registro`AS fecha_registro_ini, B.`estado_proceso`, B.`estado`, C.`id_user`, C.`names`, C.`surnames`, C.`tipo_documento`, C.`documento`, C.`correo`, C.`estado`  FROM `external_courier` AS A INNER JOIN shipping AS B ON A.`id_ship` = B.id_shipping INNER JOIN user AS C ON A.id_mensajero = C.id_user WHERE A.`estado` = '1'");
            while ($dato = (mysqli_fetch_array($tabla_mensajero_externo))) {
            ?>
                <tr>
                    <td><?php echo $dato['names']; ?></td>
                    <td><?php echo $dato['tipo_solicitud']; ?></td>
                    <td><?php echo $dato['fecha_registro']; ?></td>
                    <td><?php echo $dato['fecha_inicio']; ?></td>
                    <td><?php echo $dato['fecha_fin']; ?></td>
                </tr>
            <?php
            } ?></tbody>
    </table>
<?php
} else if ($estado == '6') {
    
?><style>
        .form-controlb {
            display: block;
            width: 100%;
            /* height: calc(2.25rem + 2px); */
            /* padding: .375rem .75rem; */
            /* font-size: 1rem; */
            /* font-weight: 400; */
            /* line-height: 1.5; */
            color: #261072;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            box-shadow: inset 0 0 0 transparent;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .table td,
        .table th {
            vertical-align: top;
            padding: 0.40rem;
            border-top: 1px solid rgb(222, 226, 230);

        }
    </style>

    <script>
        $(document).ready(function() {

            $('#MyTable4').DataTable();

        });
    </script>
    <table id='MyTable4' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th>Mensajero</th>
                <th>Producto</th>
                <th>Fch Asignaci&oacute;n</th>
                <th>Entrega</th>
                <th>Fecha Max</th>
            </tr>
        </thead>
        <tbody>
            <?php

            $tabla_entregado = mysqli_query($conex, "SELECT A.`id_ship`, A.`id_user`, A.`id_mensajero`, A.`fecha_registro` AS fecha_registro, A.`novedad`, A.`estado`,B.`id_shipping`, B.`id_generate`, B.`id_personal`, B.`id_detail`, B.`nombres`, B.`apellidos`, B.`tipo_documento`, B.`documento`, B.`telefono`, B.`email`, B.`direccion`, B.`prioridad`, B.`ciudad_origen`, B.`ciudad_destino`, B.`barrio_destino`, B.`valor_declarado`, B.`centro_costos`, B.`tipo_solicitud`, B.`ubicacion`, B.`fecha_inicio`, B.`fecha_fin`, B.`fecha_registro`AS fecha_registro_ini, B.`estado_proceso`, B.`estado`, C.`id_user`, C.`names`, C.`surnames`, C.`tipo_documento`, C.`documento`, C.`correo`, C.`estado`  FROM `delivery` AS A INNER JOIN shipping AS B ON A.`id_ship` = B.id_shipping INNER JOIN user AS C ON A.id_mensajero = C.id_user WHERE A.`estado` = '1'");
            while ($dato = (mysqli_fetch_array($tabla_entregado))) {  ?>
                <tr>
                    <td><?php echo $dato['names']; ?></td>
                    <td><?php echo $dato['tipo_solicitud']; ?></td>
                    <td><?php echo $dato['fecha_registro']; ?></td>
                    <td><?php echo $dato['fecha_inicio']; ?></td>
                    <td><?php echo $dato['fecha_fin']; ?></td>
                </tr>
            <?php } ?>
			</tbody>
    </table>
<?php
	
}else if ($estado == '7') {

?><style>
        .form-controlb {
            display: block;
            width: 100%;
            /* height: calc(2.25rem + 2px); */
            /* padding: .375rem .75rem; */
            /* font-size: 1rem; */
            /* font-weight: 400; */
            /* line-height: 1.5; */
            color: #261072;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            box-shadow: inset 0 0 0 transparent;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .table td,
        .table th {
            vertical-align: top;
            padding: 0.40rem;
            border-top: 1px solid rgb(222, 226, 230);

        }
    </style>

    <script>
        $(document).ready(function() {

            $('#MyTable5').DataTable();

        });
    </script>
    <table id='MyTable5' class='table table-bordered table-hover'>
        <thead>
            <tr>
                <th>Mensajero</th>
                <th>Producto</th>
                <th>Fch Asignaci&oacute;n</th>
                <th>Entrega</th>
                <th>Fecha Max</th>
            </tr>
        </thead>
        <tbody>
            <?php

            $tabla_Devuelto = mysqli_query($conex, "SELECT A.`id_ship`, A.`id_user`, A.`id_mensajero`, A.`fecha_registro` AS fecha_registro, A.`novedad`, A.`estado`,B.`id_shipping`, B.`id_generate`, B.`id_personal`, B.`id_detail`, B.`nombres`, B.`apellidos`, B.`tipo_documento`, B.`documento`, B.`telefono`, B.`email`, B.`direccion`, B.`prioridad`, B.`ciudad_origen`, B.`ciudad_destino`, B.`barrio_destino`, B.`valor_declarado`, B.`centro_costos`, B.`tipo_solicitud`, B.`ubicacion`, B.`fecha_inicio`, B.`fecha_fin`, B.`fecha_registro`AS fecha_registro_ini, B.`estado_proceso`, B.`estado`, C.`id_user`, C.`names`, C.`surnames`, C.`tipo_documento`, C.`documento`, C.`correo`, C.`estado`  FROM `returned` AS A INNER JOIN shipping AS B ON A.`id_ship` = B.id_shipping INNER JOIN user AS C ON A.id_mensajero = C.id_user WHERE A.`estado` = '1' ");
            while ($dato = mysqli_fetch_array($tabla_Devuelto)) {  ?>
                <tr>
                    <td><?php echo $dato['names']; ?></td>
                    <td><?php echo $dato['tipo_solicitud']; ?></td>
                    <td><?php echo $dato['fecha_registro']; ?></td>
                    <td><?php echo $dato['fecha_inicio']; ?></td>
                    <td><?php echo $dato['fecha_fin']; ?></td>
                </tr>
            <?php } ?>
			</tbody>
    </table>
<?php
 

}else if ($estado == '8') {

  ?><style>
        .form-controlb {
            display: block;
            width: 100%;
            /* height: calc(2.25rem + 2px); */
            /* padding: .375rem .75rem; */
            /* font-size: 1rem; */
            /* font-weight: 400; */
            /* line-height: 1.5; */
            color: #261072;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            box-shadow: inset 0 0 0 transparent;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .table td,
        .table th {
            vertical-align: top;
            padding: 0.40rem;
            border-top: 1px solid rgb(222, 226, 230);

        }
    </style>

    <script>
        $(document).ready(function() {

            $('#MyTable6').DataTable();

        });
    </script>
    <table id='MyTable6' class='table table-bordered table-hover'>
        <thead>
            <tr>
            
                <th>Producto</th>
                <th>Fch Asignaci&oacute;n</th>
                <th>Entrega</th>
                <th>Fecha Max</th>
            </tr>
        </thead>
        <tbody>
            <?php

            $tabla_FueraTiempo = mysqli_query($conex, "SELECT * FROM office AS A LEFT JOIN `shipping` AS B ON A.id_ship = B.id_shipping where A.`estado` = '1' AND (CURDATE() > B.fecha_fin)");
            while ($dato = (mysqli_fetch_array($tabla_FueraTiempo))) {  ?>
                <tr>
                    <td><?php echo $dato['tipo_solicitud']; ?></td>
                    <td><?php echo $dato['fecha_registro']; ?></td>
                    <td><?php echo $dato['fecha_inicio']; ?></td>
                    <td><?php echo $dato['fecha_fin']; ?></td>
                </tr>
            <?php } ?>
			</tbody>
    </table>
<?php

}

?>