<?php

	require 'LIBRERIA_PHP_EXCEL/Classes/PHPExcel.php';
	require 'LIBRERIA_PHP_EXCEL/Classes/PHPExcel/IOFactory.php';

	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getProperties()
	->setCreator('People Marketing')
	->setTitle('Excel en PHP')
	->setDescription('Documento')
	->setKeywords('excel phpexcel php')
	->setCategory('excel');

	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('solicitudes');

	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'NOMBRES');
	$objPHPExcel->getActiveSheet()->setCellValue('B1', 'APELLIDOS');
	$objPHPExcel->getActiveSheet()->setCellValue('C1', 'TELEFONO');
	$objPHPExcel->getActiveSheet()->setCellValue('D1', 'EMAIL');
	$objPHPExcel->getActiveSheet()->setCellValue('E1', 'DIRECCION');
	$objPHPExcel->getActiveSheet()->setCellValue('F1', 'PRIORIDAD');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'FECHA INICIAL');
	$objPHPExcel->getActiveSheet()->setCellValue('H1', 'FECHA FINAL');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'CIUDAD DESTINO');
    $objPHPExcel->getActiveSheet()->setCellValue('J1', 'BARRIO DESTINO');
	$objPHPExcel->getActiveSheet()->setCellValue('K1', 'CENTRO COSTOS');
	$objPHPExcel->getActiveSheet()->setCellValue('L1', 'PRODUCTO');
    $objPHPExcel->getActiveSheet()->setCellValue('M1', 'UBICACION CORRESPONDENCIA');
	$objPHPExcel->getActiveSheet()->setCellValue('N1', 'OBSERVACION');

	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Disposition: attachment;filename="solicitudes_masivas.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');

?>
