<?php
require('rotation.php');
 require('conex.php');


 class PDF extends FPDF_Rotate
 {
 function RotatedText($x,$y,$txt,$angle)
 {
     //Text rotated around its origin
     $this->Rotate($angle,$x,$y);
     $this->Text($x,$y,$txt);
     $this->Rotate(0);
 }
 
 function RotatedImage($file,$x,$y,$w,$h,$angle)
 {
     //Image rotated around its upper-left corner
     $this->Rotate($angle,$x,$y);
     $this->Image($file,$x,$y,$w,$h);
     $this->Rotate(0);
 }
 }

$pdf = new PDF('P','mm','letter');
$pdf ->SetMargins(0, 0 ,0 );
$pdf->SetAutoPageBreak(false);
$pdf ->AddPage('');
//tipo de letra//
$pdf ->SetFont('Arial', '',8);


$select_qr_generado=mysqli_query($conex,"SELECT A.id_qr AS id_qr, A.id_qr_generado AS id_qr_generado ,B.celda_1_x AS celda_1_x, B.celda_1_y AS celda_1_y, B.celda_2_x AS celda_2_x, B.celda_2_y AS celda_2_y, B.celda_3_x AS celda_3_x, B.celda_3_y AS celda_3_y, B.celda_4_x AS celda_4_x, B.celda_4_y AS celda_4_y, B.Set_X AS Set_X, B.Set_Y AS Set_Y, B.Set2_X AS Set2_X, B.Set2_Y AS Set2_Y, B.Set3_X AS Set3_X, B.Set3_Y AS Set3_Y, B.Set4_X AS Set4_X, B.Set4_Y AS Set4_Y, B.Set5_X AS Set5_X, B.Set5_Y AS Set5_Y, B.Set6_X AS Set6_X, B.Set6_Y AS Set6_Y, B.Set7_X AS Set7_X, B.Set7_Y AS Set7_Y,B.Set8_X AS Set8_X, B.Set8_Y AS Set8_Y, B.Set9_X AS Set9_X, B.Set9_Y AS Set9_Y,B.Set10_X AS Set10_X, B.Set10_Y AS Set10_Y, B.Set11_X AS Set11_X, B.Set11_Y AS Set11_Y,B.Set12_X AS Set12_X, B.Set12_Y AS Set12_Y,B.Set13_X AS Set13_X, B.Set13_Y AS Set13_Y, B.Set14_X AS Set14_X, B.Set14_Y AS Set14_Y, B.Set15_X AS Set15_X, B.Set15_Y AS Set15_Y, B.Set16_X AS Set16_X, B.Set16_Y AS Set16_Y, B.Set17_X AS Set17_X, B.Set17_Y AS Set17_Y, B.Set18_X AS Set18_X, B.Set18_Y AS Set18_Y,  B.imagen_1_x AS imagen_1_x, B.imagen_1_y AS imagen_1_y, B.imagen_2_x AS imagen_2_x ,B.imagen_2_y AS imagen_2_y FROM qr_generated AS A
LEFT JOIN coordenadas_x_y AS B ON A.posicion = B.id_coordenadas; ");

while ($qr =(mysqli_fetch_array($select_qr_generado))){


//verde -> celda 3

$pdf->Rect($qr['celda_1_x'], $qr['celda_1_y'], 40, 45, false );

$pdf->SetXY($qr['Set_X'], $qr['Set_Y'] );

$pdf->Cell(6, 6,'Remitente:');

$pdf->SetXY($qr['Set2_X'], $qr['Set2_Y']);

$pdf->Cell(6, 6,$qr['id_qr_generado']);

$pdf->SetXY($qr['Set3_X'], $qr['Set3_Y']);

$pdf->Cell(6, 6,$qr['id_qr_generado']);

$pdf->SetXY($qr['Set16_X'], $qr['Set16_Y']);

$pdf->Cell(6, 6,'_____________________________');

$pdf->SetXY($qr['Set17_X'], $qr['Set17_Y']);

$pdf->Cell(6, 6,'C.C');



//azul

$pdf->Rect($qr['celda_2_x'], $qr['celda_2_y'], 25, 70, false );
$pdf->RotatedText(100,60,'ALV',120);


//cafe

$pdf->Rect($qr['celda_3_x'], $qr['celda_3_y'] , 85, 25, false );

$pdf->SetXY($qr['Set4_X'], $qr['Set4_Y']);

$pdf->Cell(6, 6,'FECHA:');

$pdf->SetXY($qr['Set5_X'], $qr['Set5_Y']);

$pdf->Cell(6, 6,'REMITENTE');

$pdf->SetXY($qr['Set6_X'], $qr['Set6_Y']);

$pdf->Cell(6, 6, $qr['id_qr_generado']);

$pdf->SetXY($qr['Set7_X'], $qr['Set7_Y']);

$pdf->Cell(6, 6,'Producto:');

$pdf->SetXY($qr['Set8_X'], $qr['Set8_Y']);

$pdf->Cell(6, 6,$qr['id_qr_generado']);

$pdf->SetXY($qr['Set9_X'], $qr['Set9_Y']);

$pdf->Cell(6, 6,'Edicion');

$pdf->SetXY($qr['Set10_X'], $qr['Set10_Y']);

$pdf->Cell(6, 6,$qr['id_qr_generado']);

$pdf->SetXY($qr['Set11_X'], $qr['Set11_Y']);
$pdf->Cell(6, 6,'Nombre:');

$pdf->SetXY($qr['Set12_X'], $qr['Set12_Y']);

$pdf->Cell(6, 6,$qr['id_qr_generado']);

$pdf->SetXY($qr['Set13_X'], $qr['Set13_Y']);

$pdf->Cell(6, 6,'direccion');

$pdf->SetXY($qr['Set14_X'], $qr['Set14_Y']);

$pdf->Cell(6, 6,$qr['id_qr_generado']);

 

//azul otro//

$pdf->Rect($qr['celda_4_x'], $qr['celda_4_y'], 45, 45, false );

$pdf->Cell(15, 6, $pdf->Image('test.png',$qr['imagen_2_x'],$qr['imagen_2_y'],30),false);

$pdf->SetXY($qr['Set15_X'], $qr['Set15_Y']);

$pdf->Cell(6, 6,$qr['id_qr_generado']);


if ($qr['id_qr']%2==0){
   $pdf ->Ln();
}else{

}
if ($qr['id_qr']%8==0){
        $pdf->AddPage('PORTRAIT','LETTER');
}

        }

        

$pdf->OutPut();

 ?>