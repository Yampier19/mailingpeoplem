<?php
require('fpdf.php');
class PDF extends FPDF
{
var $widths;
var $aligns;
function SetWidths($w)
{
    $this->widths=$w;
}
function SetAligns($a)
{
    $this->aligns=$a;
}
function Row($data)
{
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=8*$nb;
    $this->CheckPageBreak($h);
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        $x=$this->GetX();
        $y=$this->GetY();
        $this->Rect($x,$y,$w,$h);
        $this->MultiCell($w,8,$data[$i],0,$a);
        $this->SetXY($x+$w,$y);
    }
    //Por defecto, el valor de Ln() es igual a la altura de la última celda impresa.
    $this->Ln();
}
function CheckPageBreak($h)
{
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}
function NbLines($w,$txt)
{
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
}
//atributos del archivo//
$fpdf = new FPDF('P','mm','letter');
$fpdf ->SetMargins(0, 0 ,0 );
$fpdf->SetAutoPageBreak(false);
//inicio de la primera pagina//
$fpdf ->AddPage('');
//tipo de letra//
$fpdf ->SetFont('Arial', '',8);
//creacion de celdas//
///////////////////////
//verde
$fpdf->Rect(25, 25, 40, 45, false );
$fpdf->Cell(15, 6, $fpdf->Image('logo.png',1,1,15),false);
//azul
$fpdf->Rect(0, 0, 25, 70, false );
$fpdf->Cell(15, 6);
//cafe
$fpdf->Rect(25, 0, 85, 25, false );
$fpdf->Cell(0.1, 10,'Destinatario:'); 
$fpdf->Cell(0.1, 18,'Nombre:'); 
$fpdf->Cell(0.1, 28,'Direccion:');
$fpdf->Cell(0.1, 60,'Remitente:');
$fpdf->Cell(0.1, 80,'Pedro Alfarez');
$fpdf->Cell(0.1, 90,'3125449875');
$fpdf->Cell(18, 38,'Ciudad:'); 
$fpdf->Cell(0.1, 18,'Felipe Andres Gomez Garcia');
$fpdf->Cell(0.1, 28,'Calle 97a #56 - 13'); 
$fpdf->Cell(29, 38,'Bogota'); 
$fpdf->Cell(0.1, 125,'5789511');
$fpdf->Cell(12, 38,'Telefono:'); 
$fpdf->Cell(0.1, 38,'3114416893'); 


//azul otro//
$fpdf->Rect(65, 25, 45, 45, false );
$fpdf->Cell(15, 6, $fpdf->Image('test.png',70,30,30),false);
//////////////////////////////
$fpdf->Rect(135, 25, 40, 45, false );
$fpdf->Cell(15, 6, $fpdf->Image('tiempo.png',30,48,30),false);
//azul
$fpdf->Rect(110, 0, 25, 70, false );
$fpdf->Cell(60, 6, $fpdf->Image('logo.png',112,1,15),false);
//cafe
$fpdf->Rect(135, 0, 85, 25, false );
$fpdf->Cell(01, 10,'Destinatario:'); 
$fpdf->Cell(0.1, 18,'Nombre:'); 
$fpdf->Cell(0.1, 28,'Direccion:');
$fpdf->Cell(0.1, 60,'Remitente:');
$fpdf->Cell(0.1, 80,'Pedro Alfarez');
$fpdf->Cell(0.1, 90,'3125449875');
$fpdf->Cell(18, 38,'Ciudad:'); 
$fpdf->Cell(0.1, 18,'Felipe Andres Gomez Garcia');
$fpdf->Cell(0.1, 28,'Calle 97a #56 - 13'); 
$fpdf->Cell(29, 38,'Bogota'); 
$fpdf->Cell(0.1, 125,'5789511');
$fpdf->Cell(12, 38,'Telefono:'); 
$fpdf->Cell(0.1, 38,'3114416893'); 
//azul otro//
$fpdf->Rect(175, 25, 45, 45, false );
$fpdf->Cell(15, 6 , $fpdf->Image('test.png',180,30,30),false);
$fpdf ->Ln();///////////////////////
$fpdf->Rect(25, 95, 40, 45, false );
$fpdf->Cell(15, 6, $fpdf->Image('logo.png',1,75,15),false);
//azul
$fpdf->Rect(0, 70, 25, 70, false );
$fpdf->Cell(15, 6);
//cafe
$fpdf->Rect(25, 70, 85, 25, false );
$fpdf->Cell(0.1, 150,'hola que hace el exito'); 
$fpdf->Cell(50, 170,'hola que hace el exito'); 
//azul otro//
$fpdf->Rect(65, 95, 45, 45, false );
$fpdf->Cell(15, 6, $fpdf->Image('test.png',70,100,30),false);
//////////////////////////////
$fpdf->Rect(135, 95, 40, 45, false );
$fpdf->Cell(15, 6  );
//azul
$fpdf->Rect(110, 70, 25, 70, false );
$fpdf->Cell(15, 6, $fpdf->Image('logo.png',112,75,15),false);
//cafe
$fpdf->Rect(135, 70, 85, 25, false );
$fpdf->Cell(15, 6 );
$fpdf->Cell(0.1, 150,'hola que hace el exito'); 
$fpdf->Cell(60, 170,'hola que hace el exito'); 
//azul otro//
$fpdf->Rect(175, 95, 45, 45, false );
$fpdf->Cell(15, 6 , $fpdf->Image('test.png',180,170,30),false);
////////////
$fpdf->Rect(25, 165, 40, 45, false );
$fpdf->Cell(15, 6,  $fpdf->Image('logo.png',1,145,15),false);
//azul
$fpdf->Rect(0, 140, 25, 70, false );
$fpdf->Cell(15, 6);
//cafe
$fpdf->Rect(25, 140, 85, 25, false );
$fpdf->Cell(15, 6  );
//azul otro//
$fpdf->Rect(65, 165, 45, 45, false );
$fpdf->Cell(15, 6, $fpdf->Image('test.png',70,170,30),false);
//////////////////////////////
$fpdf->Rect(135, 165, 40, 45, false );
$fpdf->Cell(15, 6  );
//azul
$fpdf->Rect(110, 140, 25, 70 , false);
$fpdf->Cell(15, 6, $fpdf->Image('logo.png',112,145,15),false);
//cafe
$fpdf->Rect(135, 140, 85, 25, false );
$fpdf->Cell(15, 6 );
//azul otro//
$fpdf->Rect(175, 165, 45, 45, false );
$fpdf->Cell(15, 6 , $fpdf->Image('test.png',180,100,30),false);
/////////////////////////////
$fpdf->Rect(25, 235, 40, 45, false );
$fpdf->Cell(15, 6, '10, 235', $fpdf->Image('logo.png',1,215,15),false);
//azul
$fpdf->Rect(0, 210, 25, 70, false );
$fpdf->Cell(15, 6);
//cafe
$fpdf->Rect(25, 210, 85, 25, false );
$fpdf->Cell(15, 6  );
//azul otro//
$fpdf->Rect(65, 235, 45, 45 );
$fpdf->Cell(15, 6, $fpdf->Image('test.png',70,240,30),false);
//////////////////////////////
$fpdf->Rect(135, 235, 40, 45 , false);
$fpdf->Cell(15, 6  );
//azul
$fpdf->Rect(110, 210, 25, 70 );
$fpdf->Cell(15, 6, '110, 235', $fpdf->Image('logo.png',112,215,15),false);
//cafe
$fpdf->Rect(135, 210, 85, 25, false );
$fpdf->Cell(15, 6 );
//azul otro//
$fpdf->Rect(175, 235, 45, 45, false );
$fpdf->Cell(15, 6 , $fpdf->Image('test.png',180,240,30),false);
$fpdf ->Output();
?><?php
require('fpdf.php');
class PDF extends FPDF
{
var $widths;
var $aligns;
function SetWidths($w)
{
    $this->widths=$w;
}
function SetAligns($a)
{
    $this->aligns=$a;
}
function Row($data)
{
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=8*$nb;
    $this->CheckPageBreak($h);
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        $x=$this->GetX();
        $y=$this->GetY();
        $this->Rect($x,$y,$w,$h);
        $this->MultiCell($w,8,$data[$i],0,$a);
        $this->SetXY($x+$w,$y);
    }
    //Por defecto, el valor de Ln() es igual a la altura de la última celda impresa.
    $this->Ln();
}
function CheckPageBreak($h)
{
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}
function NbLines($w,$txt)
{
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}
}
