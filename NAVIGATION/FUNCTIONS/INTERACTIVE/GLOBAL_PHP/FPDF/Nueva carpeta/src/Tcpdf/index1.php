<?php
require('fpdf.php');

//atributos del archivo//
$fpdf = new FPDF('P','mm','letter');
$fpdf ->SetMargins(0, 0 ,0 );
$fpdf->SetAutoPageBreak(false);
//inicio de la primera pagina//
$fpdf ->AddPage('');
//tipo de letra//
$fpdf ->SetFont('Arial', '',8);
//creacion de celdas//
///////////////////////
//verde
$fpdf->Rect(25, 25, 40, 45, false );
$fpdf->SetXY(30,30 );
$fpdf->Cell(6, 6,'Remitente:');
$fpdf->SetXY(30,35 );
$fpdf->Cell(6, 6,'Pedro Alfarez');
$fpdf->SetXY(30,40 );
$fpdf->Cell(6, 6,'3125449875');
$fpdf->Cell(6, 6, $fpdf->Image('tiempo.png',30,45,30),false);
//azul
$fpdf->Rect(0, 0, 25, 70, false );
$fpdf->Cell(15, 6, $fpdf->Image('logo.png',1,1,15),false);
//cafe
$fpdf->Rect(25, 0, 85, 25, false );
$fpdf->SetXY(28, 2);
$fpdf->Cell(6, 6,'Destinatario:');
$fpdf->SetXY(28, 6);
$fpdf->Cell(6, 6,'Nombre:                           Felipe Andres Gomez Garcia');
$fpdf->SetXY(28, 10);
$fpdf->Cell(6, 6,'Direccion:                         Calle 97 a #56 - 13');
$fpdf->SetXY(28, 14);
$fpdf->Cell(6, 6,'Ciudad:                             Bogota');
$fpdf->SetXY(28, 18);
$fpdf->Cell(6, 6,'Telefono:                          3227586815');

//azul otro//
$fpdf->Rect(65, 25, 45, 45, false );
$fpdf->Cell(15, 6, $fpdf->Image('test1.png',70,30,30),false);
$fpdf->SetXY(78, 60);
$fpdf->Cell(6, 6,'QR:16a100');
//////////////////////////////

$fpdf->Rect(135, 25, 40, 45, false );
$fpdf->SetXY(140,30 );
$fpdf->Cell(6, 6,'Remitente:');
$fpdf->SetXY(140,35 );
$fpdf->Cell(6, 6,'Pedro Alfarez');
$fpdf->SetXY(140,40 );
$fpdf->Cell(6, 6,'3125449875');
$fpdf->Cell(6, 6, $fpdf->Image('tiempo.png',140,45,30),false);
//azul
$fpdf->Rect(110, 0, 25, 70, false );
$fpdf->Cell(60, 6, $fpdf->Image('logo.png',112,1,15),false);
//cafe
$fpdf->Rect(135, 0, 85, 25, false );
$fpdf->SetXY(140, 2);
$fpdf->Cell(6, 6,'Destinatario:');
$fpdf->SetXY(140, 6);
$fpdf->Cell(6, 6,'Nombre:                            Felipe Andres Gomez Garcia');
$fpdf->SetXY(140, 10);
$fpdf->Cell(6, 6,'Direccion:                          Calle 97 a #56 - 13');
$fpdf->SetXY(140, 14);
$fpdf->Cell(6, 6,'Ciudad:                              Bogota');
$fpdf->SetXY(140, 18);
$fpdf->Cell(6, 6,'Telefono:                           3227586815');

//azul otro//
$fpdf->Rect(175, 25, 45, 45, false );
$fpdf->Cell(15, 6 , $fpdf->Image('test1.png',180,30,30),false);
$fpdf->SetXY(188, 60);
$fpdf->Cell(6, 6,'QR:16a100');
$fpdf ->Ln();

///////////////////////
$fpdf->Rect(25, 95, 40, 45, false );
$fpdf->SetXY(30,100 );
$fpdf->Cell(6, 6,'Remitente:');
$fpdf->SetXY(30,105 );
$fpdf->Cell(6, 6,'Pedro Alfarez');
$fpdf->SetXY(30,110 );
$fpdf->Cell(6, 6,'3125449875');
$fpdf->Cell(6, 6, $fpdf->Image('tiempo.png',30,115,30),false);
//azul
$fpdf->Rect(0, 70, 25, 70, false );
$fpdf->Cell(15, 6, $fpdf->Image('logo.png',1,75,15),false);
//cafe
$fpdf->Rect(25, 70, 85, 25, false );
$fpdf->SetXY(28, 72);
$fpdf->Cell(6, 6,'Destinatario:');
$fpdf->SetXY(28, 76);
$fpdf->Cell(6, 6,'Nombre:                           Felipe Andres Gomez Garcia');
$fpdf->SetXY(28, 80);
$fpdf->Cell(6, 6,'Direccion:                         Calle 97 a #56 - 13');
$fpdf->SetXY(28, 84);
$fpdf->Cell(6, 6,'Ciudad:                             Bogota');
$fpdf->SetXY(28, 88);
$fpdf->Cell(6, 6,'Telefono:                          3227586815');
//azul otro//
$fpdf->Rect(65, 95, 45, 45, false );
$fpdf->Cell(15, 6, $fpdf->Image('test1.png',70,100,30),false);
$fpdf->SetXY(78,130);
$fpdf->Cell(6, 6,'QR:16a100');
//////////////////////////////
$fpdf->Rect(25, 95, 40, 45, false );
$fpdf->SetXY(140,100 );
$fpdf->Cell(6, 6,'Remitente:');
$fpdf->SetXY(140,105 );
$fpdf->Cell(6, 6,'Pedro Alfarez');
$fpdf->SetXY(140,110 );
$fpdf->Cell(6, 6,'3125449875');
$fpdf->Cell(6, 146, $fpdf->Image('tiempo.png',140,115,30),false);

//azul
$fpdf->Rect(110, 70, 25, 70, false );
$fpdf->Cell(15, 6, $fpdf->Image('logo.png',112,75,15),false);
//cafe
$fpdf->Rect(135, 70, 85, 25, false );
$fpdf->SetXY(140, 72);
$fpdf->Cell(6, 6,'Destinatario:');
$fpdf->SetXY(140, 76);
$fpdf->Cell(6, 6,'Nombre:                           Felipe Andres Gomez Garcia');
$fpdf->SetXY(140, 80);
$fpdf->Cell(6, 6,'Direccion:                         Calle 97 a #56 - 13');
$fpdf->SetXY(140, 84);
$fpdf->Cell(6, 6,'Ciudad:                             Bogota');
$fpdf->SetXY(140, 88);
$fpdf->Cell(6, 6,'Telefono:                          3227586815');
//azul otro//
$fpdf->Rect(175, 95, 45, 45, false );
$fpdf->Cell(15, 6 , $fpdf->Image('test1.png',180,170,30),false);
$fpdf->SetXY(188,130);
$fpdf->Cell(6, 6,'QR:16a100');

////////////
$fpdf->Rect(25, 95, 40, 45, false );
$fpdf->SetXY(30,170 );
$fpdf->Cell(6, 6,'Remitente:');
$fpdf->SetXY(30,175 );
$fpdf->Cell(6, 6,'Pedro Alfarez');
$fpdf->SetXY(30,180 );
$fpdf->Cell(6, 6,'3125449875');
$fpdf->Cell(6, 6, $fpdf->Image('tiempo.png',30,185,30),false);
//azul
$fpdf->Rect(25, 165, 40, 45, false );
$fpdf->Cell(15, 6,  $fpdf->Image('logo.png',1,145,15),false);
//cafe
$fpdf->Rect(25, 140, 85, 25, false );
$fpdf->SetXY(28, 142);
$fpdf->Cell(6, 6,'Destinatario:');
$fpdf->SetXY(28, 146);
$fpdf->Cell(6, 6,'Nombre:                           Felipe Andres Gomez Garcia');
$fpdf->SetXY(28, 150);
$fpdf->Cell(6, 6,'Direccion:                         Calle 97 a #56 - 13');
$fpdf->SetXY(28, 154);
$fpdf->Cell(6, 6,'Ciudad:                             Bogota');
$fpdf->SetXY(28, 158);
$fpdf->Cell(6, 6,'Telefono:                          3227586815');
//azul otro//
$fpdf->Rect(65, 165, 45, 45, false );
$fpdf->Cell(15, 6, $fpdf->Image('test1.png',70,170,30),false);
$fpdf->SetXY(78,200);
$fpdf->Cell(6, 6,'QR:16a100');
//////////////////////////////
$fpdf->Rect(135, 165, 40, 45, false );
$fpdf->SetXY(140,170 );
$fpdf->Cell(6, 6,'Remitente:');
$fpdf->SetXY(140,175 );
$fpdf->Cell(6, 6,'Pedro Alfarez');
$fpdf->SetXY(140,180 );
$fpdf->Cell(6, 6,'3125449875');
$fpdf->Cell(6, 6, $fpdf->Image('tiempo.png',140,185,30),false);
//azul
$fpdf->Rect(110, 140, 25, 70 , false);
$fpdf->Cell(15, 6, $fpdf->Image('logo.png',112,145,15),false);
//cafe
$fpdf->Rect(135, 140, 85, 25, false );
$fpdf->SetXY(140, 142);
$fpdf->Cell(6, 6,'Destinatario:');
$fpdf->SetXY(140, 146);
$fpdf->Cell(6, 6,'Nombre:                           Felipe Andres Gomez Garcia');
$fpdf->SetXY(140, 150);
$fpdf->Cell(6, 6,'Direccion:                         Calle 97 a #56 - 13');
$fpdf->SetXY(140, 154);
$fpdf->Cell(6, 6,'Ciudad:                             Bogota');
$fpdf->SetXY(140, 158);
$fpdf->Cell(6, 6,'Telefono:                          3227586815');
//azul otro//
$fpdf->Rect(175, 165, 45, 45, false );
$fpdf->Cell(15, 6 , $fpdf->Image('test1.png',180,100,30),false);
$fpdf->SetXY(188,200);
$fpdf->Cell(6, 6,'QR:16a100');

/////////////////////////////
$fpdf->Rect(25, 235, 40, 45, false );
$fpdf->SetXY(30,240 );
$fpdf->Cell(6, 6,'Remitente:');
$fpdf->SetXY(30,245 );
$fpdf->Cell(6, 6,'Pedro Alfarez');
$fpdf->SetXY(30,250 );
$fpdf->Cell(6, 6,'3125449875');
$fpdf->Cell(6, 6, $fpdf->Image('tiempo.png',30,255,30),false);
//azul
$fpdf->Rect(0, 210, 25, 70, false );
$fpdf->Cell(15, 6, $fpdf->Image('logo.png',1,215,15),false);
//cafe
$fpdf->Rect(25, 210, 85, 25, false );
$fpdf->SetXY(28, 212);
$fpdf->Cell(6, 6,'Destinatario:');
$fpdf->SetXY(28, 216);
$fpdf->Cell(6, 6,'Nombre:                           Felipe Andres Gomez Garcia');
$fpdf->SetXY(28, 220);
$fpdf->Cell(6, 6,'Direccion:                         Calle 97 a #56 - 13');
$fpdf->SetXY(28, 224);
$fpdf->Cell(6, 6,'Ciudad:                             Bogota');
$fpdf->SetXY(28, 228);
$fpdf->Cell(6, 6,'Telefono:                          3227586815');
//azul otro//
$fpdf->Rect(65, 235, 45, 45 );
$fpdf->Cell(15, 6, $fpdf->Image('test1.png',70,240,30),false);
$fpdf->SetXY(78,270);
$fpdf->Cell(6, 6,'QR:16a100');
//////////////////////////////
$fpdf->Rect(135, 235, 40, 45 , false);
$fpdf->SetXY(140,240 );
$fpdf->Cell(6, 6,'Remitente:');
$fpdf->SetXY(140,245 );
$fpdf->Cell(6, 6,'Pedro Alfarez');
$fpdf->SetXY(140,250 );
$fpdf->Cell(6, 6,'3125449875');
$fpdf->Cell(6, 6, $fpdf->Image('tiempo.png',140,255,30),false);
//azul
$fpdf->Rect(110, 210, 25, 70 );
$fpdf->Cell(15, 6,  $fpdf->Image('logo.png',112,215,15),false);
//cafe
$fpdf->Rect(135, 210, 85, 25, false );
$fpdf->Rect(25, 210, 85, 25, false );
$fpdf->SetXY(140, 212);
$fpdf->Cell(6, 6,'Destinatario:');
$fpdf->SetXY(140, 216);
$fpdf->Cell(6, 6,'Nombre:                           Felipe Andres Gomez Garcia');
$fpdf->SetXY(140, 220);
$fpdf->Cell(6, 6,'Direccion:                         Calle 97 a #56 - 13');
$fpdf->SetXY(140, 224);
$fpdf->Cell(6, 6,'Ciudad:                             Bogota');
$fpdf->SetXY(140, 228);
$fpdf->Cell(6, 6,'Telefono:                          3227586815');
//azul otro//
$fpdf->Rect(175, 235, 45, 45, false );
$fpdf->Cell(15, 6 , $fpdf->Image('test1.png',180,240,30),false);
$fpdf->SetXY(188,270);
$fpdf->Cell(6, 6,'QR:16a100');
$fpdf ->Output();
?>