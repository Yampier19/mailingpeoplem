<style>

.alert-success {

    color: #155724;

    background-color: #d4edda;

    border-color: #c3e6cb;

}



</style>

<?php

	require '../GLOBAL_PHP/LIBRERIA_PHP_EXCEL/Classes/PHPExcel/IOFactory.php';

	header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');
	include "GENERAR_QR/qrlib.php";
	$id_users = base64_decode($id_user);
	$nombreArchivo = '../../../MAIN_FOLDER/FILES/solicitudes_masivas.xlsx';
	$objPHPExcel = PHPEXCEL_IOFactory::load($nombreArchivo);
	$objPHPExcel->setActiveSheetIndex(0);
	$numRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
	$numRows1 = $numRows - 1;
	?>
<div>
    <?php
	echo '
	<table border=1 style="display:none;">
	<tr>
	<td>NOMBRES</td>
	<td>APELLIDOS</td>
	<td>TELEFONO</td>
    <td>EMAIL</td>
    <td>DIRECCION</td>
    <td>PRIORIDAD</td>
    <td>FECHA INICIAL</td>
    <td>FECHA FINAL</td>
    <td>CIUDAD DESTINO</td>
    <td>BARRIO DESTINO</td>
    <td>CENTROS COSTOS</td>
    <td>PRODUCTO</td>
    <td>UBICACION CORRESPONDENCIA</td>
    <td>OBSERVACION</td>
	</tr>';
	for($i = 2; $i <= $numRows; $i++){
                            /*
                            * PHP QR Code encoder
                            *
                            * Exemplatory usage
                            *
                            * PHP QR Code is distributed under LGPL 3
                            * Copyright (C) 2010 Dominik Dzienia <deltalab at poczta dot fm>
                            *
                            * This library is free software; you can redistribute it and/or
                            * modify it under the terms of the GNU Lesser General Public
                            * License as published by the Free Software Foundation; either
                            * version 3 of the License, or any later version.
                            *
                            * This library is distributed in the hope that it will be useful,
                            * but WITHOUT ANY WARRANTY; without even the implied warranty of
                            * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
                            * Lesser General Public License for more details.
                            *
                            * You should have received a copy of the GNU Lesser General Public
                            * License along with this library; if not, write to the Free Software
                            * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
                            */
							$select_proxima_gestion=mysqli_query($conex,"SELECT id_shipping FROM shipping ORDER BY id_shipping DESC LIMIT 1; ");
	while ($gestion =(mysqli_fetch_array($select_proxima_gestion))){
	$id_shipping = $gestion['id_shipping'];
	}
   	$id_proxima_shipping = $id_shipping + 1;
    	$select_proxima_qr=mysqli_query($conex,"SELECT id_qr FROM qr_generated ORDER BY id_qr DESC LIMIT 1; ");
    	while ($qr_nuevo =(mysqli_fetch_array($select_proxima_qr))){
    	$id_qr = $qr_nuevo['id_qr'];
    	}
    	$id_proximo_generated = $id_qr + 1;
    	$id_users = base64_decode($id_user);
    	$usuario = $id_users;
    	$DesdeLetra = "a";
    	$HastaLetra = "z";
    	$letraAleatoria = chr(rand(ord($DesdeLetra), ord($HastaLetra)));
    	$id_correponde = $id_proxima_shipping.$letraAleatoria.$usuario."0";
		$token = "0";
							// declara la variable
						$level = "L";
							$size = "7";
							$qr = $id_correponde;
                            //set it to writable location, a place for temp generated PNG files
                            $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'../../../MAIN_FOLDER/FILES/QR_MASSIVE_TEMP/'.DIRECTORY_SEPARATOR;
                            //html PNG location prefix
                            $PNG_WEB_DIR = '../../../MAIN_FOLDER/FILES/QR_MASSIVE_TEMP/';
                           //ofcourse we need rights to create temp dir
                            if (!file_exists($PNG_TEMP_DIR))
                            mkdir($PNG_TEMP_DIR);
							$filename = $PNG_TEMP_DIR.'test.png';
							// echo "entra 1";
							// echo $filename;
                            //processing form input
                            //remember to sanitize user input in real-life solution !!!
                            $errorCorrectionLevel = 'L';
                            if (isset($level) && in_array($level, array('L','M','Q','H')))
                            $errorCorrectionLevel = $level;    
						// echo "entra 2";
                            $matrixPointSize = 7;
                            if (isset($size))
                            $matrixPointSize = min(max((int)$size, 1), 10);
						// echo "entra 2";
                            if (isset($qr)) { 
                            //it's very important!
                            if (trim($qr) == '')
                            die('data cannot be empty! <a href="?">back</a>');
							// echo "entra 3";
                           // user data
                            // url server
                            //$PNG_TEMP_DIR2 = "https://app-peoplemarketing.com/MailingPeopleM/NAVIGATION/MAIN_FOLDER/FILES/QR_MASSIVE_TEMP/";
                            // url local 
                            $PNG_TEMP_DIR2 = "https://app-peoplemarketing.com/MailingPeopleM/NAVIGATION/MAIN_FOLDER/FILES/QR_MASSIVE_TEMP/";
                            $filename2 = $PNG_TEMP_DIR2.'qr'.md5($qr.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
                            $filename = $PNG_TEMP_DIR.'qr'.md5($qr.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
                            QRcode::png($qr, $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
							// echo $filename;
                            } else {    
                            //default data
                            QRcode::png('PHP QR Code :)', $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
							// echo $filename;
							// echo "4";
                            }    
                            //display generated file

		$NOMBRES = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
		$APELLIDOS = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();
		$TELEFONO = $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue();
		$EMAIL = $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getCalculatedValue();
		$DIRECCION = $objPHPExcel->getActiveSheet()->getCell('E'.$i)->getCalculatedValue();
        $PRIORIDAD = $objPHPExcel->getActiveSheet()->getCell('F'.$i)->getCalculatedValue();
        $FECHA_INICIAL = date('Y/m/d', PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCell('G'.$i)->getCalculatedValue()));
		$FECHA_FINAL = date('Y/m/d', PHPExcel_Shared_Date::ExcelToPHP($objPHPExcel->getActiveSheet()->getCell('H'.$i)->getCalculatedValue()));
		$CIUDAD_DESTINO = $objPHPExcel->getActiveSheet()->getCell('I'.$i)->getCalculatedValue();
		$BARRIO_DESTINO = $objPHPExcel->getActiveSheet()->getCell('J'.$i)->getCalculatedValue();
        $CENTRO_COSTOS = $objPHPExcel->getActiveSheet()->getCell('K'.$i)->getCalculatedValue();
        $PRODUCTO = $objPHPExcel->getActiveSheet()->getCell('L'.$i)->getCalculatedValue();
		$UBICACION_CORRESPONDENCIA = $objPHPExcel->getActiveSheet()->getCell('M'.$i)->getCalculatedValue();
        $OBSERVACION = $objPHPExcel->getActiveSheet()->getCell('N'.$i)->getCalculatedValue();
		echo '<tr>';
		echo '<td>'.$NOMBRES.'</td>';
		echo '<td>'.$APELLIDOS.'</td>';
		echo '<td>'.$TELEFONO.'</td>';
		echo '<td>'.$EMAIL.'</td>'; 
		echo '<td>'.$DIRECCION.'</td>';
        echo '<td>'.$PRIORIDAD.'</td>';
        echo '<td>'.$FECHA_INICIAL.'</td>';
        echo '<td>'.$FECHA_FINAL.'</td>';
		echo '<td>'.$CIUDAD_DESTINO.'</td>';
		echo '<td>'.$BARRIO_DESTINO.'</td>';
        echo '<td>'.$CENTRO_COSTOS.'</td>';
        echo '<td>'.$PRODUCTO.'</td>';
		echo '<td>'.$UBICACION_CORRESPONDENCIA.'</td>';
        echo '<td>'.$OBSERVACION.'</td>';
		echo '</tr>';
		$select_position=mysqli_query($conex,"SELECT id_qr,posicion FROM qr_generated ORDER BY id_qr DESC LIMIT 1; ");
                            while ($data_posicion =(mysqli_fetch_array($select_position))){
                            $id_qr = $data_posicion['id_qr'];
                            $posicion = $data_posicion['posicion'];
                            }
                            if ($id_qr%8==0) {
                            $asignacion_posicion = '1';
                            }else {
                            $asignacion_posicion = $posicion + 1;
                            }
                            $insert_solicitud=mysqli_query($conex,"INSERT INTO `qr_generated` (`id_qr_generado`, `id_unico`, `link_qr` , `posicion`)VALUES('".$qr."', '".$token."', '".$filename2."' , '".$asignacion_posicion."');");
                            $select_id_qr=mysqli_query($conex,"SELECT id_qr FROM qr_generated ORDER BY id_qr DESC LIMIT 1; ");
                            while ($id_qr =(mysqli_fetch_array($select_id_qr))){
                            $id_qr1 = $id_qr['id_qr'];
                            $id_qr_general = $id_qr1;
                            }

                            if ($UBICACION_CORRESPONDENCIA == "Bandeja") {
  
                                $insert_point=mysqli_query($conex,"INSERT INTO `point` (id_ship, id_user, fecha_registro, novedad, estado)VALUES('".$id_proxima_shipping."','".$id_users."',NOW(),'N/A','1');");

                                $insert_shipping=mysqli_query($conex,"INSERT INTO `shipping` (id_generate, id_personal, id_detail, nombres, apellidos, tipo_documento, documento, telefono, email, direccion, prioridad, ciudad_origen, ciudad_destino, barrio_destino, valor_declarado, centro_costos, tipo_solicitud, ubicacion, fecha_inicio, fecha_fin, fecha_registro, estado_proceso, observacion, estado)VALUES('".$id_qr_general."','".$id_users."','0','".$NOMBRES."','".$APELLIDOS."','','','".$TELEFONO."','".$EMAIL."','".$DIRECCION."','".$PRIORIDAD."','','".$$CIUDAD_DESTINO."','".$BARRIO_DESTINO."','','".$CENTRO_COSTOS."','".$PRODUCTO."','".$UBICACION_CORRESPONDENCIA."','".$FECHA_INICIAL."','".$FECHA_FINAL."',NOW(),'SOLICITUD MASIVA', '".$OBSERVACION."', '0');");
							// echo "ubicacion      puntos correspondencia";
                            }else if ($UBICACION_CORRESPONDENCIA == "Pool") {
                                $insert_office=mysqli_query($conex,"INSERT INTO `office` (id_ship, id_user, fecha_registro, novedad, estado)VALUES('".$id_proxima_shipping."','".$id_users."',NOW(),'N/A','1');");

                                $insert_shipping=mysqli_query($conex,"INSERT INTO `shipping` (id_generate, id_personal, id_detail, nombres, apellidos, tipo_documento, documento, telefono, email, direccion, prioridad, ciudad_origen, ciudad_destino, barrio_destino, valor_declarado, centro_costos, tipo_solicitud, ubicacion, fecha_inicio, fecha_fin, fecha_registro, estado_proceso, observacion, estado)VALUES('".$id_qr_general."','".$id_users."','0','".$NOMBRES."','".$APELLIDOS."','','','".$TELEFONO."','".$EMAIL."','".$DIRECCION."','".$PRIORIDAD."','','".$$CIUDAD_DESTINO."','".$BARRIO_DESTINO."','','".$CENTRO_COSTOS."','".$PRODUCTO."','".$UBICACION_CORRESPONDENCIA."','".$FECHA_INICIAL."','".$FECHA_FINAL."',NOW(),'SOLICITUD MASIVA', '".$OBSERVACION."', '0');");
								// echo "ubicacion       office";
                            }else if ($UBICACION_CORRESPONDENCIA == "Pendiente") {


                                $insert_shipping=mysqli_query($conex,"INSERT INTO `shipping` (id_generate, id_personal, id_detail, nombres, apellidos, tipo_documento, documento, telefono, email, direccion, prioridad, ciudad_origen, ciudad_destino, barrio_destino, valor_declarado, centro_costos, tipo_solicitud, ubicacion, fecha_inicio, fecha_fin, fecha_registro, estado_proceso, observacion, estado)VALUES('".$id_qr_general."','".$id_users."','0','".$NOMBRES."','".$APELLIDOS."','','','".$TELEFONO."','".$EMAIL."','".$DIRECCION."','".$PRIORIDAD."','','".$$CIUDAD_DESTINO."','".$BARRIO_DESTINO."','','".$CENTRO_COSTOS."','".$PRODUCTO."','".$UBICACION_CORRESPONDENCIA."','".$FECHA_INICIAL."','".$FECHA_FINAL."',NOW(),'SOLICITUD MASIVA', '".$OBSERVACION."', '1');");
                        }
	}
	echo '</table>';

?>
<div class="alert alert-success" role="alert">
    <strong><span class="fa fa-info-circle"></span>
    </strong> Se han registrado correctamente la informaci&oacute;n <b><?php echo $numRows1; ?> </b>registros, Gracias por su atencion.
    <br> <a style="color:black;" href="form_request_massive.php">Volver a subir otra base masiva </a> 
</div>
</div>

