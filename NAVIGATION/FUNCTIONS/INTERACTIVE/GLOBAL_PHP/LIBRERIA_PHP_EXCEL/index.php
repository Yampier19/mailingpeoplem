<?php

	require 'Classes/PHPExcel.php';
	require 'Classes/PHPExcel/IOFactory.php';

	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getProperties()
	->setCreator('Codigos de Programacion')
	->setTitle('Excel en PHP')
	->setDescription('Documento de prueba')
	->setKeywords('excel phpexcel php')
	->setCategory('Ejemplos');

	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setTitle('Hoja1');

	$objPHPExcel->getActiveSheet()->setCellValue('A1', 'ICCID');
	$objPHPExcel->getActiveSheet()->setCellValue('B1', 'PORTABILIDAD');
	$objPHPExcel->getActiveSheet()->setCellValue('C1', 'ID ASESOR');
	$objPHPExcel->getActiveSheet()->setCellValue('D1', 'NOMBRES ASESOR');
	$objPHPExcel->getActiveSheet()->setCellValue('E1', 'ID TROPA');
	$objPHPExcel->getActiveSheet()->setCellValue('F1', 'NOMBRE TROPA');
	$objPHPExcel->getActiveSheet()->setCellValue('G1', 'ID SUPERVISOR');
	$objPHPExcel->getActiveSheet()->setCellValue('H1', 'NOMBRE SUPERVISOR');
	$objPHPExcel->getActiveSheet()->setCellValue('I1', 'ESTADO');
	$objPHPExcel->getActiveSheet()->setCellValue('J1', 'NUMERO BLOQUE');

	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Disposition: attachment;filename="Excel.xlsx"');
	header('Cache-Control: max-age=0');

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');


?>
