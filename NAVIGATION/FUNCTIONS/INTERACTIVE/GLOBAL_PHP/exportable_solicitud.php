<?php  

header("Content-Type: text/html;charset=utf-8");
require('../../../CONNECTION/SECURITY/conex.php');
require('../../../CONNECTION/SECURITY/session_cookie.php');
//Exportar datos de php a Excel

header("Content-Type: application/vnd.ms-excel");

header("Expires: 0");

header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

header("content-disposition: attachment;filename=Solicitudes.xls");

?>

 <table class="table table-striped" id="myTable">

        <thead class="thead-dark">

          <tr>

            <th scope="col">QR</th>

            <th scope="col">Destinatario</th>

            <th scope="col">Ciudad_Destino</th>

            <th scope="col">Barrio</th>

            <th scope="col">Direccion</th>

            <th scope="col">Telefono_Destinatario</th>

            <th scope="col">Tipo</th>

            <th scope="col">Remitente</th> 

            <th scope="col">Estado</th>

            <th scope="col">Gestion</th>

          </tr>

        </thead>



        <tbody>

          <?php  



            $resultado = mysqli_query($conex,"SELECT C.`id_qr_generado` AS id_qr_generado, A.`nombres` AS nombres, A.`apellidos` AS apellidos, A.`ciudad_destino` AS ciudad_destino, A.`barrio_destino` AS barrio_destino, A.`direccion` AS direccion, A.`telefono` AS telefono_destinatario, A.`tipo_solicitud` AS tipo_solicitud, B.`names` AS nombre_remitente, B.`surnames` AS apellido_remitente, A.`estado_proceso` AS estado_proceso FROM `shipping` AS A LEFT JOIN personal AS B ON A.id_personal = B.id_personal LEFT JOIN qr_generated AS C ON A.id_generate = C.id_qr" );



            while($resul = mysqli_fetch_array($resultado))

            {

              $id_qr_generado = $resul['id_qr_generado'];

              $nombre_destinatario = $resul['nombres'];

              $apellido_destinatarios = $resul['apellidos'];

              $ciudad_destino = $resul['ciudad_destino'];

              $barrio_destino = $resul['barrio_destino'];

              $direccion_des = $resul['direccion'];

              $telefono_destinatario = $resul['telefono_destinatario'];

              $tipo_solicitud = $resul['tipo_solicitud'];

              $nombre_remitente = $resul['nombre_remitente'];

              $apellido_remitente = $resul['apellido_remitente'];

              $estado = $resul['estado_proceso'];

          ?>

          

          <tr>

            <td height="5px" width="5px"><?php echo $id_qr_generado ?></td>

            <td><?php echo $nombre_destinatario." ".$apellido_destinatarios; ?></td>

            <td><?php echo $ciudad_destino; ?></td>

            <td><?php echo $barrio_destino; ?></td>

            <td><?php echo $direccion_des; ?></td>

            <td><?php echo $telefono_destinatario; ?></td>

            <td><?php echo $tipo_solicitud; ?></td>

            <td><?php echo $nombre_remitente."".$apellido_remitente; ?></td>

            <td><?php echo $estado; ?></td> 

            <td><?php if ($tipo_solicitud == "Documento") { ?>

              <span class="oi oi-document"></span>

              <?php }else if ($tipo_solicitud == "Paquete") {  ?>

              <span class="oi oi-box"></span>

              <?php }?></td>

             

          </tr>

          <?php }  ?>

        </tbody>

      </table>

 