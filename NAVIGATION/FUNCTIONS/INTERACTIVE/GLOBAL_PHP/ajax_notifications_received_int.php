<?php

header("Content-Type: text/html;charset=utf-8");
include('../../../CONNECTION/SECURITY/conex.php');

$notification = $_POST['notification'];

if($notification == 1){

    $select_external_courier_n =mysqli_query($conex,"SELECT * FROM mailingpeoplem.external_courier AS A LEFT JOIN mailingpeoplem.shipping AS B ON A.id_ship = B.id_shipping LEFT JOIN mailingpeoplem.USER AS C ON C.id_user = A.id_user WHERE A.estado='0' AND A.id_user = '1';");

    $count_external_courier = mysqli_num_rows($select_external_courier_n);

    $select_point_courier_n =mysqli_query($conex,"SELECT * FROM mailingpeoplem.point_courier AS A LEFT JOIN mailingpeoplem.shipping AS B ON A.id_ship = B.id_shipping LEFT JOIN mailingpeoplem.USER AS C ON C.id_user = A.id_user WHERE A.estado='0' AND A.id_user = '1';");

    $count_point_courier = mysqli_num_rows($select_point_courier_n);

    $select_office_courier_n =mysqli_query($conex,"SELECT * FROM mailingpeoplem.office_courier AS A LEFT JOIN mailingpeoplem.shipping AS B ON A.id_ship = B.id_shipping LEFT JOIN mailingpeoplem.USER AS C ON C.id_user = A.id_user WHERE A.estado='0' AND A.id_user = '1';");

    $count_office_courier = mysqli_num_rows($select_office_courier_n);

    $number_notification = $count_external_courier + $count_point_courier + $count_office_courier;

    echo $number_notification;
    
}

if($notification == 2){

    $select_external_courier =  mysqli_query($conex,"SELECT C.NAMES AS nombres, C.surnames AS apellidos, A.fecha_registro FROM mailingpeoplem.external_courier AS A LEFT JOIN mailingpeoplem.shipping AS B ON A.id_ship = B.id_shipping LEFT JOIN mailingpeoplem.USER AS C ON C.id_user = A.id_user WHERE A.estado='0' AND A.id_user = '1';");
    
    while($info = (mysqli_fetch_array($select_external_courier)))
    {
        
        $nombre_completo = $info['nombres'].' '.$info['apellidos'];
        $fecha_registro = $info['fecha_registro'];
                  
        echo '<a href="#" class="dropdown-item">
                <i class="fas fa-envelope mr-2"></i> '.$nombre_completo.' Externo
                <span class="float-right text-muted text-sm">'.$fecha_registro.'</span>
            </a>
                            
        ';	
    }

    $select_point_courier =  mysqli_query($conex,"SELECT C.NAMES AS nombres, C.surnames AS apellidos, A.fecha_registro FROM mailingpeoplem.point_courier AS A LEFT JOIN mailingpeoplem.shipping AS B ON A.id_ship = B.id_shipping LEFT JOIN mailingpeoplem.USER AS C ON C.id_user = A.id_user WHERE A.estado='0' AND A.id_user = '1';");
    
    while($info2 = (mysqli_fetch_array($select_point_courier)))
    {
        
        $nombre_completo2 = $info2['nombres'].' '.$info2['apellidos'];
        $fecha_registro2= $info2['fecha_registro'];
                  
        echo '<a href="#" class="dropdown-item">
                <i class="fas fa-envelope mr-2"></i> '.$nombre_completo2.' Punto
                <span class="float-right text-muted text-sm">'.$fecha_registro2.'</span>
            </a>
                            
        ';	
    }

    $select_office_courier =  mysqli_query($conex,"SELECT C.NAMES AS nombres, C.surnames AS apellidos, A.fecha_registro FROM mailingpeoplem.office_courier AS A LEFT JOIN mailingpeoplem.shipping AS B ON A.id_ship = B.id_shipping LEFT JOIN mailingpeoplem.USER AS C ON C.id_user = A.id_user WHERE A.estado='0' AND A.id_user = '1';");
    
    while($info3 = (mysqli_fetch_array($select_office_courier)))
    {
        
        $nombre_completo3 = $info3['nombres'].' '.$info3['apellidos'];
        $fecha_registro3 = $info3['fecha_registro'];
                  
        echo '<a href="#" class="dropdown-item">
                <i class="fas fa-envelope mr-2"></i> '.$nombre_completo3.' Despacho
                <span class="float-right text-muted text-sm">'.$fecha_registro3.'</span>
            </a>
                            
        ';	
    }

}

?>