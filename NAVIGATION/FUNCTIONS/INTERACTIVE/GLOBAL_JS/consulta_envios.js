// JavaScript Document

///////Notificacion solicitud//////////////////////////////////////

function NotificacionSolicitud(){
	valor=1;
	$.ajax({
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
		valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data)
		{
			$('#noti_soli').html(data);
			$('#consul_form_solicitud').html(data);
		}
	});
}
/////////////////////////Notificacion que se encuentra en el punto//////////////////////////
function NotificacionPunto()
{
	valor=2;
	$.ajax({
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
		valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data)
		{
			$('#noti_punto').html(data);
			$('#consul_form_pun_corres').html(data);
		}
	});
}
/////////////////////// Notificaciones de que se le asigno al mensajero que hay correspondencia en el punto //////////////////////////

function NotificacionesMensajeroPunto(){
	valor=3;
	$.ajax({
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
		valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data)	{
			$('#noti_mensj_int').html(data);
			$('#consul_form_mens_int').html(data);
	}
	});
}
////////////////////// Notificaciones recibido en despacho //////////////

function NotificacionesDespacho(){
	valor=4;
	$.ajax({
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
			valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data)
		{
			$('#noti_despacho').html(data);
			$('#consul_form_despacho').html(data);
		}
	});
}

////////////////////////// Notificaciones asignado a mensajero externo ///////////////////

function NotificacionesMensajeroExterno(){
	valor=5;
	$.ajax(
	{
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
			valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data)
		{
			$('#noti_mensj_ext').html(data);
			$('#consul_form_mens_exter').html(data);
		}
	});
}
//////////////////////////Notificaciones cuando es entregado //////////////////////

function NotificacionesEntregado(){
	valor=6;
	$.ajax({
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
			valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data){
			$('#noti_entrga').html(data);
			$('#consul_form_entregado').html(data);
		}
	});
}
////////////////////////////Notificaciones cuando esta devuelto ///////////////////////

function NotificacionesDevuelto(){
	valor=7;
	$.ajax({
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
			valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data)
		{
			$('#noti_devuelto').html(data);
			$('#consult_form_devuelto').html(data);
		}
	});
}
/////////////////////////Notificaciones cuando esta fuera de tiempo /////////////////////////////

function NotificacionesFueraTiempo()
{
	valor=8;
	$.ajax({
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
			valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data)
		{
			$('#noti_fuera_tiempo').html(data);
			$('#consult_form_fuera_tiempo').html(data);
		}
	});
}
/////////////// Informacion Notificacion punto correspondencia /////////////////////////

function InforNotPuntoCorres()
{
	valor=9;
	$.ajax({
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
			valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data)
		{
			$('#consulta_punto').html(data);
		}
	});
}
///////////////////////Informacion Notificacion Despacho ////////////////////////

function InforNotDespacho(){
	valor=10;
	$.ajax({
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
		valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data)	{
			$('#noti_punto12').html(data);
		}
	});
}

//////////////////////////Informacion Notificacion Devuelto////////////////////////

function InforNotDevuelto(){
	valor=11;
	$.ajax({
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
			valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data)
		{
			$('#noti_punto12').html(data);
		}
	});
}

//////////////////////////////////// Informacion Notificacion Fuera de Tiempo/////////////////////
function InfoNotFueraTiempo(){
	valor=12;
	$.ajax({
		url:'../../../FUNCTIONS/INTERACTIVE/GLOBAL_PHP/ajax_solicitud.php',
		data:
		{
			valor: valor,
		},
		type: 'post',
		beforeSend: function () 
		{},
		success: function(data)
		{
			$('#noti_punto21').html(data);
		}
	});
}
